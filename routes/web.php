<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/gioi-thieu', [App\Http\Controllers\HomeController::class, 'gioithieu'])->name('gioithieu');
Route::get('/lien-he', [App\Http\Controllers\HomeController::class, 'lienhe'])->name('lienhe'); 
Route::get('/chinh-sach-bao-mat', [App\Http\Controllers\HomeController::class, 'chinhsachbaomat'])->name('chinhsachbaomat');
Route::get('/chinh-sach-bao-hanh', [App\Http\Controllers\HomeController::class, 'chinhsachbaohanh'])->name('chinhsachbaohanh');
Route::get('/phuong-thuc-thanh-toan', [App\Http\Controllers\HomeController::class, 'phuongthucthanhtoan'])->name('phuongthucthanhtoan');
Route::get('/phuong-thuc-van-chuyen', [App\Http\Controllers\HomeController::class, 'phuongthucvanchuyen'])->name('phuongthucvanchuyen');
Route::get('/huong-dan', [App\Http\Controllers\HomeController::class, 'huongdan'])->name('huongdan');


Route::get('/cay-trong', [App\Http\Controllers\ProductController::class, 'indexplant'])->name('caytrong');
Route::get('/chau-cay', [App\Http\Controllers\ProductController::class, 'indexpots'])->name('chaucay');
Route::get('/phu-kien', [App\Http\Controllers\ProductController::class, 'indexaccessory'])->name('phukien');


Route::get('/gio-hang', [App\Http\Controllers\CartController::class, 'cart'])->name('giohang');
