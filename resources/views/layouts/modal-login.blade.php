<div id="login-form-popup" class="lightbox-content mfp-hide">
        <div class="woocommerce">
            <div class="woocommerce-notices-wrapper"></div>
            <div class="account-container">
                <div class="col2-set row row-collapse row-large" id="customer_login">
                    <div class="col-1 large-6 col flex-row">
                        <div class="account-register-bg fill bg-fill"
                            style="background-image:url(https://mowgarden.com/wp-content/uploads/2021/04/chandra-oh-u9MM5mcML2Q-unsplash.jpg);background-color:#446084;">
                            <div class="account-register-bg-overlay fill"
                                style="background-color:rgba(214,214,214,0.37);"></div>
                        </div>
                        <div class="account-register-inner relative flex-col flex-grow dark text-center">
                            <h3 class="uppercase">Đăng ký</h3>
                            <p>Don&#039;t have an account? Register one!</p> <a
                                href="https://mowgarden.com/my-account/" class="button white is-outline">Register an
                                Account</a>
                        </div>
                    </div>
                    <div class="col-2 large-6 col">
                        <div class="account-login-inner inner-padding">
                            <h3 class="uppercase">Đăng nhập</h3>
                            <form class="woocommerce-form woocommerce-form-login login" action="{{ route('login') }}" method="post">
                                @csrf
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="username">Tên tài khoản hoặc địa chỉ email&nbsp;<span
                                            class="required">*</span></label> <input type="text"
                                        class="woocommerce-Input woocommerce-Input--text input-text" name="username"
                                        id="username" autocomplete="username" value="" /> </p>
                                <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                                    <label for="password">Mật khẩu&nbsp;<span class="required">*</span></label>
                                    <input class="woocommerce-Input woocommerce-Input--text input-text"
                                        type="password" name="password" id="password"
                                        autocomplete="current-password" /> </p>
                                <p class="form-row"> <label
                                        class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                                        <input class="woocommerce-form__input woocommerce-form__input-checkbox"
                                            name="rememberme" type="checkbox" id="rememberme" value="forever" />
                                        <span>Ghi nhớ mật khẩu</span> </label> <input type="hidden"
                                        id="woocommerce-login-nonce" name="woocommerce-login-nonce"
                                        value="b148dc2380" /><input type="hidden" name="_wp_http_referer"
                                        value="/" /> <button type="submit"
                                        class="woocommerce-button button woocommerce-form-login__submit"
                                        name="login" value="Đăng nhập">Đăng nhập</button> </p>
                                <p class="woocommerce-LostPassword lost_password"> <a
                                        href="https://mowgarden.com/my-account/lost-password/">Quên mật khẩu?</a> </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>