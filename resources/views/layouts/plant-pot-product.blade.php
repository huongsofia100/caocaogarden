<div class="row row-full-width align-middle align-center" id="row-899049350">
    <div id="col-1650545583" class="col small-12 large-12">
        <div class="col-inner text-center">
            <div id="text-3703443466" class="text">
                <h3><span style="font-size: 150%;">CÂY CẢNH ĐỂ BÀN</span></h3>
                <style>
                    #text-3703443466 {
                        text-align: center;
                    }
                </style>
            </div>
            <div id="gap-1367964024" class="gap-element clearfix"
                style="display:block; height:auto;">
                <style>
                    #gap-1367964024 {
                        padding-top: 15px;
                    }
                </style>
            </div>
            <div
                class="row large-columns-5 medium-columns-3 small-columns-2 row-small row-full-width has-shadow row-box-shadow-2-hover">
                <div
                    class="product-small col has-hover product type-product post-14421 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                        aria-label="Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan 3 than mowgarden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-chau-trang-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay kim ngan 3 than chau trang mowgarden"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14421 wishlist-fragment on-first-load"
                                                data-fragment-ref="14421"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14421,&quot;parent_product_id&quot;:14421,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14421&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14421"
                                                        data-product-type="simple"
                                                        data-original-product-id="14421"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14421" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14421" data-product_sku="LONI040"
                                        aria-label="Thêm &ldquo;Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14421"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040</a>
                                    </p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>280.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14419 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                        aria-label="Cây trầu bà đế vương xanh chậu mặt cool &#039;Imperial Green&#039; chậu sứ PHIG006">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-trau-ba-de-vuong-xanh-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay trau ba de vuong xanh mowgarden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="12 cay trau ba de vuong xanh de ban chau su trang 2">
                                    </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14419 wishlist-fragment on-first-load"
                                                data-fragment-ref="14419"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14419,&quot;parent_product_id&quot;:14419,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14419&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14419"
                                                        data-product-type="simple"
                                                        data-original-product-id="14419"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14419" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14419" data-product_sku="PHIG006"
                                        aria-label="Thêm &ldquo;Cây trầu bà đế vương xanh chậu mặt cool &#039;Imperial Green&#039; chậu sứ PHIG006&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14419"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà đế vương xanh chậu mặt cool &#8216;Imperial
                                            Green&#8217; chậu sứ PHIG006</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14417 status-publish last instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-mini product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                        aria-label="Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="luoi ho nga voi mowgarden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="luoi ho nga voi mowgarden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14417 wishlist-fragment on-first-load"
                                                data-fragment-ref="14417"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14417,&quot;parent_product_id&quot;:14417,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14417&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14417"
                                                        data-product-type="simple"
                                                        data-original-product-id="14417"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14417" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14417" data-product_sku="sans001-1"
                                        aria-label="Thêm &ldquo;Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14417"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica
                                            SANS002</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14415 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                        aria-label="Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="co lan chi de ban mowgaden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="co lan chi de ban mowgaden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14415 wishlist-fragment on-first-load"
                                                data-fragment-ref="14415"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14415,&quot;parent_product_id&quot;:14415,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14415&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14415"
                                                        data-product-type="simple"
                                                        data-original-product-id="14415"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14415" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14415" data-product_sku=""
                                        aria-label="Thêm &ldquo;Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14415"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            cỏ lan chi để bàn chậu sứ mặt cười SPI004</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14413 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-9%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                        aria-label="Cây trầu bà đế vương đỏ để bàn &#039;Red Rojo&#039; chậu sứ PHIR008">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="trau ba de vuong do de ban mowgarden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="trau ba de vuong do de ban mowgarden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14413 wishlist-fragment on-first-load"
                                                data-fragment-ref="14413"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14413,&quot;parent_product_id&quot;:14413,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14413&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14413"
                                                        data-product-type="simple"
                                                        data-original-product-id="14413"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14413" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14413" data-product_sku="PHIR008"
                                        aria-label="Thêm &ldquo;Cây trầu bà đế vương đỏ để bàn &#039;Red Rojo&#039; chậu sứ PHIR008&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14413"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà đế vương đỏ để bàn &#8216;Red Rojo&#8217; chậu sứ
                                            PHIR008</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14341 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                        aria-label="Cây lưỡi hổ Bantel Sensation chậu ươm STBS001">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay luoi ho bengar 1"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay luoi ho bengar 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14341 wishlist-fragment on-first-load"
                                                data-fragment-ref="14341"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14341,&quot;parent_product_id&quot;:14341,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14341&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14341"
                                                        data-product-type="simple"
                                                        data-original-product-id="14341"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14341" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14341" data-product_sku="STBS001"
                                        aria-label="Thêm &ldquo;Cây lưỡi hổ Bantel Sensation chậu ươm STBS001&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14341"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            lưỡi hổ Bantel Sensation chậu ươm STBS001</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13989 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                        aria-label="Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hong mon de ban chau su 1"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hong mon de ban chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13989 wishlist-fragment on-first-load"
                                                data-fragment-ref="13989"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13989,&quot;parent_product_id&quot;:13989,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13989&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13989"
                                                        data-product-type="simple"
                                                        data-original-product-id="13989"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13989" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13989" data-product_sku="ANTH010"
                                        aria-label="Thêm &ldquo;Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13989"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hồng môn cỡ nhỏ chậu sứ trắng ANTH010</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13986 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                        aria-label="Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay ngu gia bi cam thach"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay ngu gia bi cam thach 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13986 wishlist-fragment on-first-load"
                                                data-fragment-ref="13986"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13986,&quot;parent_product_id&quot;:13986,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13986&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13986"
                                                        data-product-type="simple"
                                                        data-original-product-id="13986"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13986" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13986" data-product_sku="SCHE020"
                                        aria-label="Thêm &ldquo;Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13986"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13905 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-6%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                        aria-label="Cây kim ngân một thân để bàn chậu sứ LONI039">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan mot than chau su 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay kim ngan mot than chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13905 wishlist-fragment on-first-load"
                                                data-fragment-ref="13905"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13905,&quot;parent_product_id&quot;:13905,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13905&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13905"
                                                        data-product-type="simple"
                                                        data-original-product-id="13905"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13905" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13905" data-product_sku="LONI039"
                                        aria-label="Thêm &ldquo;Cây kim ngân một thân để bàn chậu sứ LONI039&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13905"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân một thân để bàn chậu sứ LONI039</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>480.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>450.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13901 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                        aria-label="Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1a-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay co la xe de ban chau su trang 1a"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay co la xe de ban chau su trang 1"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13901 wishlist-fragment on-first-load"
                                                data-fragment-ref="13901"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13901,&quot;parent_product_id&quot;:13901,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13901&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13901"
                                                        data-product-type="simple"
                                                        data-original-product-id="13901"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13901" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13901" data-product_sku="LIVI005"
                                        aria-label="Thêm &ldquo;Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13901"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <a href="https://mowgarden.com/cay-canh-de-ban/" target="_self"
                class="button primary is-link is-large lowercase"> <span>Xem thêm</span> </a>
        </div>
    </div>
</div>