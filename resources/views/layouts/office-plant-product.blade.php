<div class="row row-full-width align-middle align-center" id="row-2110297687">
    <div id="col-1776265356" class="col small-12 large-12">
        <div class="col-inner text-center">
            <div id="text-3661167742" class="text">
                <h3><span style="font-size: 150%;">CÂY CẢNH VĂN PHÒNG</span></h3>
                <style>
                    #text-3661167742 {
                        text-align: center;
                    }
                </style>
            </div>
            <div id="gap-214357834" class="gap-element clearfix"
                style="display:block; height:auto;">
                <style>
                    #gap-214357834 {
                        padding-top: 15px;
                    }
                </style>
            </div>
            <div
                class="row large-columns-5 medium-columns-3 small-columns-2 row-small row-full-width has-shadow row-box-shadow-2-hover">
                <div
                    class="product-small col has-hover product type-product post-13835 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-8%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                        aria-label="Cây hạnh phúc để sàn chậu sứ RADE031"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-trang-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hanh phuc de san chau su trang"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hanh phuc de san chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13835 wishlist-fragment on-first-load"
                                                data-fragment-ref="13835"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13835,&quot;parent_product_id&quot;:13835,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13835&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13835"
                                                        data-product-type="simple"
                                                        data-original-product-id="13835"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13835" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13835" data-product_sku="RADE031"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc để sàn chậu sứ RADE031&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13835"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc để sàn chậu sứ RADE031</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>600.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>550.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13777 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-ban-cong product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                        aria-label="Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="11 cay bang dai loan cam thach chau su trang 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-3-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="11 cay bang dai loan cam thach chau su trang 3"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13777 wishlist-fragment on-first-load"
                                                data-fragment-ref="13777"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13777,&quot;parent_product_id&quot;:13777,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13777&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13777"
                                                        data-product-type="simple"
                                                        data-original-product-id="13777"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13777" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13777" data-product_sku="BUBU007"
                                        aria-label="Thêm &ldquo;Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13777"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            bàng Đài Loan cẩm thạch chậu sứ BUBU007</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13080 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-8%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-phat-tai-nui-2-tang-chau-da-mai-cptn013/"
                                        aria-label="Cây phát tài núi 2 tầng chậu đá mài CPTN013"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/02/cay-phat-tai-nui-chau-da-mai-den-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay phat tai nui chau da mai den"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13080 wishlist-fragment on-first-load"
                                                data-fragment-ref="13080"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13080,&quot;parent_product_id&quot;:13080,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13080&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13080"
                                                        data-product-type="simple"
                                                        data-original-product-id="13080"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13080" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13080" data-product_sku="CPTN013"
                                        aria-label="Thêm &ldquo;Cây phát tài núi 2 tầng chậu đá mài CPTN013&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13080"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-phat-tai-nui-2-tang-chau-da-mai-cptn013/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            phát tài núi 2 tầng chậu đá mài CPTN013</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.900.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>1.750.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12838 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-mot-than-cao-1m6-chau-dat-nung-rade024/"
                                        aria-label="Cây hạnh phúc một thân cao 1m6 chậu đất nung RADE024">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/01/cay-hanh-phuc-mot-than-chau-dat-nung-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hanh phuc mot than chau dat nung"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12838 wishlist-fragment on-first-load"
                                                data-fragment-ref="12838"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12838,&quot;parent_product_id&quot;:12838,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12838&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12838"
                                                        data-product-type="simple"
                                                        data-original-product-id="12838"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12838" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12838" data-product_sku="RADE024"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc một thân cao 1m6 chậu đất nung RADE024&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12838"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-mot-than-cao-1m6-chau-dat-nung-rade024/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc một thân cao 1m6 chậu đất nung RADE024</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>900.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12825 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-ngu-gia-bi-de-san-chau-tru-tron-da-mai-sche017/"
                                        aria-label="Cây ngũ gia bì để sàn chậu trụ tròn đá mài SCHE017">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/01/cay-ngu-gia-bi-chau-da-mai-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay ngu gia bi chau da mai 1"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/01/cay-ngu-gia-bi-chau-da-mai-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay ngu gia bi chau da mai 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12825 wishlist-fragment on-first-load"
                                                data-fragment-ref="12825"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12825,&quot;parent_product_id&quot;:12825,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12825&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12825"
                                                        data-product-type="simple"
                                                        data-original-product-id="12825"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12825" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12825" data-product_sku="SCHE017"
                                        aria-label="Thêm &ldquo;Cây ngũ gia bì để sàn chậu trụ tròn đá mài SCHE017&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12825"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-de-san-chau-tru-tron-da-mai-sche017/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            ngũ gia bì để sàn chậu trụ tròn đá mài SCHE017</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>750.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12800 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-cay-noi-that-mix-dai-phu-gia-trau-ba-ntmx021/"
                                        aria-label="Chậu cây nội thất mix Đại phú gia, Trầu bà Neon NTMX021">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/01/cay-dai-phu-gia-mix-trau-ba-neon-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay dai phu gia mix trau ba neon"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12800 wishlist-fragment on-first-load"
                                                data-fragment-ref="12800"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12800,&quot;parent_product_id&quot;:12800,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12800&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12800"
                                                        data-product-type="simple"
                                                        data-original-product-id="12800"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12800" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12800" data-product_sku="NTMX021"
                                        aria-label="Thêm &ldquo;Chậu cây nội thất mix Đại phú gia, Trầu bà Neon NTMX021&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12800"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-cay-noi-that-mix-dai-phu-gia-trau-ba-ntmx021/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            cây nội thất mix Đại phú gia, Trầu bà Neon NTMX021</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>820.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12639 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-truc-bach-hop-mot-than-chau-xi-mang-ctbh008/"
                                        aria-label="Cây trúc bách hợp một thân chậu xi măng CTBH008">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/cay-truc-bach-hop-chau-da-mai-giot-nuoc-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay truc bach hop chau da mai giot nuoc"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12639 wishlist-fragment on-first-load"
                                                data-fragment-ref="12639"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12639,&quot;parent_product_id&quot;:12639,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12639&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12639"
                                                        data-product-type="simple"
                                                        data-original-product-id="12639"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12639" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12639" data-product_sku="CTBH008"
                                        aria-label="Thêm &ldquo;Cây trúc bách hợp một thân chậu xi măng CTBH008&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12639"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-truc-bach-hop-mot-than-chau-xi-mang-ctbh008/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trúc bách hợp một thân chậu xi măng CTBH008</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>950.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12570 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-thanh-xuan-co-lon-chau-da-mai-tbtx008/"
                                        aria-label="Cây trầu bà thanh xuân cỡ lớn chậu đá mài TBTX008">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/cay-trau-ba-thanh-xuan-chau-da-mai-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay trau ba thanh xuan chau da mai"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/08/trau-ba-thanh-xuan-chau-su-trang-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="trau ba thanh xuan chau su trang 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12570 wishlist-fragment on-first-load"
                                                data-fragment-ref="12570"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12570,&quot;parent_product_id&quot;:12570,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12570&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12570"
                                                        data-product-type="simple"
                                                        data-original-product-id="12570"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12570" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12570" data-product_sku="TBTX008"
                                        aria-label="Thêm &ldquo;Cây trầu bà thanh xuân cỡ lớn chậu đá mài TBTX008&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12570"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-thanh-xuan-co-lon-chau-da-mai-tbtx008/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà thanh xuân cỡ lớn chậu đá mài TBTX008</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12535 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-nam-my-monstera-borsigiana-chau-su-mons106/"
                                        aria-label="Cây trầu bà Nam Mỹ Monstera &#039;Borsigiana&#039; chậu sứ MONS106">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/cay-trau-ba-nam-mymonstera-borsigiana-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay trau ba nam mymonstera borsigiana"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12535 wishlist-fragment on-first-load"
                                                data-fragment-ref="12535"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12535,&quot;parent_product_id&quot;:12535,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12535&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12535"
                                                        data-product-type="simple"
                                                        data-original-product-id="12535"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12535" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12535" data-product_sku="MONS106"
                                        aria-label="Thêm &ldquo;Cây trầu bà Nam Mỹ Monstera &#039;Borsigiana&#039; chậu sứ MONS106&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12535"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-nam-my-monstera-borsigiana-chau-su-mons106/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà Nam Mỹ Monstera &#8216;Borsigiana&#8217; chậu sứ
                                            MONS106</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>750.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12449 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-cay-noi-that-mix-dai-phu-gia-do-la-may-man-ntmx011/"
                                        aria-label="Chậu cây nội thất mix đại phú gia, đô la, may mắn NTMX011">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/cay-canh-van-phong-mix-dai-phu-gia-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay canh van phong mix dai phu gia"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12449 wishlist-fragment on-first-load"
                                                data-fragment-ref="12449"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12449,&quot;parent_product_id&quot;:12449,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12449&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12449"
                                                        data-product-type="simple"
                                                        data-original-product-id="12449"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12449" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12449" data-product_sku="NTMX011"
                                        aria-label="Thêm &ldquo;Chậu cây nội thất mix đại phú gia, đô la, may mắn NTMX011&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12449"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-cay-noi-that-mix-dai-phu-gia-do-la-may-man-ntmx011/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            cây nội thất mix đại phú gia, đô la, may mắn NTMX011</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.400.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12373 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-cau-vang-nhat-ban-co-trung-chau-da-mai-chry009/"
                                        aria-label="Cây cau vàng Nhật Bản cỡ trung chậu đá mài CHRY009">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/11/cay-cau-vang-nhat-ban-chau-da-mai-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay cau vang nhat ban chau da mai"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12373 wishlist-fragment on-first-load"
                                                data-fragment-ref="12373"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12373,&quot;parent_product_id&quot;:12373,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12373&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12373"
                                                        data-product-type="simple"
                                                        data-original-product-id="12373"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12373" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12373" data-product_sku="CHRY009"
                                        aria-label="Thêm &ldquo;Cây cau vàng Nhật Bản cỡ trung chậu đá mài CHRY009&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12373"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-cau-vang-nhat-ban-co-trung-chau-da-mai-chry009/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            cau vàng Nhật Bản cỡ trung chậu đá mài CHRY009</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>740.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-10267 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-2-tang-chau-su-trang-rade009/"
                                        aria-label="Cây hạnh phúc 2 tầng chậu sứ trắng RADE009"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/10/cay-hanh-phuc-2-tang-chau-su-trang-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cây hạnh phúc 2 tầng" /> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-10267 wishlist-fragment on-first-load"
                                                data-fragment-ref="10267"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:10267,&quot;parent_product_id&quot;:10267,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=10267&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="10267"
                                                        data-product-type="simple"
                                                        data-original-product-id="10267"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=10267" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="10267" data-product_sku="RADE009"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc 2 tầng chậu sứ trắng RADE009&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="10267"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-2-tang-chau-su-trang-rade009/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc 2 tầng chậu sứ trắng RADE009</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>990.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-8840 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-ban-cong product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-bang-singapore-cao-2m-dang-tree-chau-su-lyra023/"
                                        aria-label="Cây bàng Singapore cao 2m dáng tree chậu sứ LYRA023">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/08/cay-bang-singapore-co-lon-dang-tree-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay bang singapore co lon dang tree chau su 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/08/cay-bang-singapore-co-lon-dang-tree-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay bang singapore co lon dang tree chau su 2"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-8840 wishlist-fragment on-first-load"
                                                data-fragment-ref="8840"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:8840,&quot;parent_product_id&quot;:8840,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=8840&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="8840"
                                                        data-product-type="simple"
                                                        data-original-product-id="8840"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=8840" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="8840" data-product_sku="LYRA023"
                                        aria-label="Thêm &ldquo;Cây bàng Singapore cao 2m dáng tree chậu sứ LYRA023&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="8840"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-bang-singapore-cao-2m-dang-tree-chau-su-lyra023/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            bàng Singapore cao 2m dáng tree chậu sứ LYRA023</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>2.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-9915 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-that-binh-kich-thuoc-lon-loni017/"
                                        aria-label="Cây kim ngân thắt bính kích thước lớn LONI017">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/09/cay-kim-ngan-binh-kich-thuoc-lon-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan binh kich thuoc lon"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-9915 wishlist-fragment on-first-load"
                                                data-fragment-ref="9915"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:9915,&quot;parent_product_id&quot;:9915,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=9915&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="9915"
                                                        data-product-type="simple"
                                                        data-original-product-id="9915"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=9915" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="9915" data-product_sku="LONI017"
                                        aria-label="Thêm &ldquo;Cây kim ngân thắt bính kích thước lớn LONI017&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="9915"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-that-binh-kich-thuoc-lon-loni017/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân thắt bính kích thước lớn LONI017</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12941 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-8%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-phat-tai-nui-2-tang-chau-su-cptn012/"
                                        aria-label="Cây phát tài núi 2 tầng chậu sứ CPTN012"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/01/phattaituimowgarden3-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="phattaituimowgarden3"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12941 wishlist-fragment on-first-load"
                                                data-fragment-ref="12941"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12941,&quot;parent_product_id&quot;:12941,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12941&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12941"
                                                        data-product-type="simple"
                                                        data-original-product-id="12941"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12941" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12941" data-product_sku=""
                                        aria-label="Thêm &ldquo;Cây phát tài núi 2 tầng chậu sứ CPTN012&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12941"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-phat-tai-nui-2-tang-chau-su-cptn012/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            phát tài núi 2 tầng chậu sứ CPTN012</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.900.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>1.750.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <a href="https://mowgarden.com/cay-canh-van-phong/" target="_self"
                class="button primary is-link is-large lowercase"> <span>Xem thêm</span> </a>
        </div>
    </div>
</div>