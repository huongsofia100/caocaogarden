<div class="row row-full-width align-middle" id="row-639036596">
    <div id="col-1521738329" class="col small-12 large-12">
        <div class="col-inner text-left">
            <div id="text-2217857191" class="text">
                <h3><span style="font-size: 150%;">SẢN PHẨM MỚI</span></h3>
                <style>
                    #text-2217857191 {
                        text-align: center;
                    }
                </style>
            </div>
            <div id="gap-2141550327" class="gap-element clearfix"
                style="display:block; height:auto;">
                <style>
                    #gap-2141550327 {
                        padding-top: 15px;
                    }
                </style>
            </div>
            <div
                class="row equalize-box large-columns-5 medium-columns-3 small-columns-2 row-small row-full-width has-shadow row-box-shadow-2-hover">
                <div
                    class="product-small col has-hover product type-product post-14478 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-phat-tai-bo-5-cay-thiet-moc-lan-cptk001/"
                                        aria-label="Cây phát tài bộ 5 - Cây thiết mộc lan CPTK001">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-phat-tai-bo-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay phat tai bo mowgarden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-phat-tai-bo-mowgarden-02-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay phat tai bo mowgarden 02"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14478 wishlist-fragment on-first-load"
                                                data-fragment-ref="14478"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14478,&quot;parent_product_id&quot;:14478,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14478&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14478"
                                                        data-product-type="simple"
                                                        data-original-product-id="14478"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14478" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14478" data-product_sku="CPTK001"
                                        aria-label="Thêm &ldquo;Cây phát tài bộ 5 - Cây thiết mộc lan CPTK001&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14478"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-phat-tai-bo-5-cay-thiet-moc-lan-cptk001/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            phát tài bộ 5 &#8211; Cây thiết mộc lan CPTK001</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>750.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14421 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                        aria-label="Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan 3 than mowgarden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-chau-trang-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay kim ngan 3 than chau trang mowgarden"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14421 wishlist-fragment on-first-load"
                                                data-fragment-ref="14421"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14421,&quot;parent_product_id&quot;:14421,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14421&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14421"
                                                        data-product-type="simple"
                                                        data-original-product-id="14421"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14421" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14421" data-product_sku="LONI040"
                                        aria-label="Thêm &ldquo;Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14421"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040</a>
                                    </p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>280.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14419 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                        aria-label="Cây trầu bà đế vương xanh chậu mặt cool &#039;Imperial Green&#039; chậu sứ PHIG006">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/cay-trau-ba-de-vuong-xanh-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay trau ba de vuong xanh mowgarden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="12 cay trau ba de vuong xanh de ban chau su trang 2">
                                    </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14419 wishlist-fragment on-first-load"
                                                data-fragment-ref="14419"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14419,&quot;parent_product_id&quot;:14419,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14419&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14419"
                                                        data-product-type="simple"
                                                        data-original-product-id="14419"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14419" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14419" data-product_sku="PHIG006"
                                        aria-label="Thêm &ldquo;Cây trầu bà đế vương xanh chậu mặt cool &#039;Imperial Green&#039; chậu sứ PHIG006&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14419"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà đế vương xanh chậu mặt cool &#8216;Imperial
                                            Green&#8217; chậu sứ PHIG006</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14417 status-publish last instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-mini product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                        aria-label="Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="luoi ho nga voi mowgarden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="luoi ho nga voi mowgarden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14417 wishlist-fragment on-first-load"
                                                data-fragment-ref="14417"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14417,&quot;parent_product_id&quot;:14417,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14417&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14417"
                                                        data-product-type="simple"
                                                        data-original-product-id="14417"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14417" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14417" data-product_sku="sans001-1"
                                        aria-label="Thêm &ldquo;Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14417"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica
                                            SANS002</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14415 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                        aria-label="Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="co lan chi de ban mowgaden"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="co lan chi de ban mowgaden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14415 wishlist-fragment on-first-load"
                                                data-fragment-ref="14415"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14415,&quot;parent_product_id&quot;:14415,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14415&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14415"
                                                        data-product-type="simple"
                                                        data-original-product-id="14415"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14415" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14415" data-product_sku=""
                                        aria-label="Thêm &ldquo;Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14415"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            cỏ lan chi để bàn chậu sứ mặt cười SPI004</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14413 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-9%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                        aria-label="Cây trầu bà đế vương đỏ để bàn &#039;Red Rojo&#039; chậu sứ PHIR008">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="trau ba de vuong do de ban mowgarden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="trau ba de vuong do de ban mowgarden"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14413 wishlist-fragment on-first-load"
                                                data-fragment-ref="14413"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14413,&quot;parent_product_id&quot;:14413,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14413&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14413"
                                                        data-product-type="simple"
                                                        data-original-product-id="14413"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14413" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14413" data-product_sku="PHIR008"
                                        aria-label="Thêm &ldquo;Cây trầu bà đế vương đỏ để bàn &#039;Red Rojo&#039; chậu sứ PHIR008&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14413"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà đế vương đỏ để bàn &#8216;Red Rojo&#8217; chậu sứ
                                            PHIR008</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14341 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                        aria-label="Cây lưỡi hổ Bantel Sensation chậu ươm STBS001">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay luoi ho bengar 1"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay luoi ho bengar 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14341 wishlist-fragment on-first-load"
                                                data-fragment-ref="14341"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14341,&quot;parent_product_id&quot;:14341,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14341&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14341"
                                                        data-product-type="simple"
                                                        data-original-product-id="14341"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14341" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14341" data-product_sku="STBS001"
                                        aria-label="Thêm &ldquo;Cây lưỡi hổ Bantel Sensation chậu ươm STBS001&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14341"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            lưỡi hổ Bantel Sensation chậu ươm STBS001</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14334 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-lon-chau-da-mai-rade033/"
                                        aria-label="Cây hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hanh-phuc-2-tang-chau-da-mai-tron-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hanh phuc 2 tang chau da mai tron 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hanh-phuc-2-tang-chau-da-mai-tron-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hanh phuc 2 tang chau da mai tron 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14334 wishlist-fragment on-first-load"
                                                data-fragment-ref="14334"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14334,&quot;parent_product_id&quot;:14334,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14334&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14334"
                                                        data-product-type="simple"
                                                        data-original-product-id="14334"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14334" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14334" data-product_sku="RADE033"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14334"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-lon-chau-da-mai-rade033/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13997 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-cot-chau-xi-mang-tru-vuong-ctbc007/"
                                        aria-label="Cây trầu bà cột chậu xi măng trụ vuông CTBC007">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-trau-ba-cot-chau-xi-mang-van-soc-ngang-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay trau ba cot chau xi mang van soc ngang 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-trau-ba-cot-chau-xi-mang-van-soc-ngang-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay trau ba cot chau xi mang van soc ngang 2"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13997 wishlist-fragment on-first-load"
                                                data-fragment-ref="13997"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13997,&quot;parent_product_id&quot;:13997,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13997&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13997"
                                                        data-product-type="simple"
                                                        data-original-product-id="13997"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13997" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13997" data-product_sku="CTBC007"
                                        aria-label="Thêm &ldquo;Cây trầu bà cột chậu xi măng trụ vuông CTBC007&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13997"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-cot-chau-xi-mang-tru-vuong-ctbc007/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà cột chậu xi măng trụ vuông CTBC007</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.100.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13989 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                        aria-label="Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hong mon de ban chau su 1"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hong mon de ban chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13989 wishlist-fragment on-first-load"
                                                data-fragment-ref="13989"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13989,&quot;parent_product_id&quot;:13989,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13989&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13989"
                                                        data-product-type="simple"
                                                        data-original-product-id="13989"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13989" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13989" data-product_sku="ANTH010"
                                        aria-label="Thêm &ldquo;Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13989"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hồng môn cỡ nhỏ chậu sứ trắng ANTH010</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13986 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                        aria-label="Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay ngu gia bi cam thach"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay ngu gia bi cam thach 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13986 wishlist-fragment on-first-load"
                                                data-fragment-ref="13986"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13986,&quot;parent_product_id&quot;:13986,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13986&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13986"
                                                        data-product-type="simple"
                                                        data-original-product-id="13986"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13986" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13986" data-product_sku="SCHE020"
                                        aria-label="Thêm &ldquo;Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13986"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13905 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-6%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                        aria-label="Cây kim ngân một thân để bàn chậu sứ LONI039">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan mot than chau su 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay kim ngan mot than chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13905 wishlist-fragment on-first-load"
                                                data-fragment-ref="13905"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13905,&quot;parent_product_id&quot;:13905,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13905&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13905"
                                                        data-product-type="simple"
                                                        data-original-product-id="13905"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13905" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13905" data-product_sku="LONI039"
                                        aria-label="Thêm &ldquo;Cây kim ngân một thân để bàn chậu sứ LONI039&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13905"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân một thân để bàn chậu sứ LONI039</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>480.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>450.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13901 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                        aria-label="Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1a-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay co la xe de ban chau su trang 1a"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay co la xe de ban chau su trang 1"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13901 wishlist-fragment on-first-load"
                                                data-fragment-ref="13901"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13901,&quot;parent_product_id&quot;:13901,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13901&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13901"
                                                        data-product-type="simple"
                                                        data-original-product-id="13901"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13901" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13901" data-product_sku="LIVI005"
                                        aria-label="Thêm &ldquo;Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13901"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13860 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-chau-su-hoa-van-rade032/"
                                        aria-label="Cây hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-van-phong-2-tang-chau-su-hoa-tiet-mowgarden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hanh phuc van phong 2 tang chau su hoa tiet mowgarden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-van-phong-2-tang-chau-su-hoa-tiet-mowgarden-01-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hanh phuc van phong 2 tang chau su hoa tiet mowgarden 01">
                                    </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13860 wishlist-fragment on-first-load"
                                                data-fragment-ref="13860"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13860,&quot;parent_product_id&quot;:13860,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13860&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13860"
                                                        data-product-type="simple"
                                                        data-original-product-id="13860"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13860" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13860" data-product_sku="RADE032"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13860"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-chau-su-hoa-van-rade032/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>900.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13835 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1">
                            <div class="callout badge badge-square">
                                <div class="badge-inner secondary on-sale"><span
                                        class="onsale">-8%</span></div>
                            </div>
                        </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                        aria-label="Cây hạnh phúc để sàn chậu sứ RADE031"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-trang-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay hanh phuc de san chau su trang"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay hanh phuc de san chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13835 wishlist-fragment on-first-load"
                                                data-fragment-ref="13835"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13835,&quot;parent_product_id&quot;:13835,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13835&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13835"
                                                        data-product-type="simple"
                                                        data-original-product-id="13835"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13835" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13835" data-product_sku="RADE031"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc để sàn chậu sứ RADE031&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13835"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc để sàn chậu sứ RADE031</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><del
                                            aria-hidden="true"><span
                                                class="woocommerce-Price-amount amount"><bdi>600.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></del>
                                        <ins><span class="woocommerce-Price-amount amount"><bdi>550.000<span
                                                        class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></ins></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13812 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-de-ban-that-binh-tieu-canh-chau-su-loni038/"
                                        aria-label="Cây kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-that-binh-lon-de-san-chau-su-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay kim ngan that binh lon de san chau su"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-de-ban-that-binh-chau-su-mowgarden-02-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay kim ngan de ban that binh chau su mowgarden 02">
                                    </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13812 wishlist-fragment on-first-load"
                                                data-fragment-ref="13812"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13812,&quot;parent_product_id&quot;:13812,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13812&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13812"
                                                        data-product-type="simple"
                                                        data-original-product-id="13812"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13812" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13812" data-product_sku="LONI038"
                                        aria-label="Thêm &ldquo;Cây kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13812"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-de-ban-that-binh-tieu-canh-chau-su-loni038/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>380.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13807 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-luoi-ho-xanh-mini-black-gold-chau-su-shbg004-sao-chep/"
                                        aria-label="Cây lưỡi hổ xanh để bàn mini &#039;Black Gold&#039; chậu sứ SHBG005">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/luoi-ho-xanh-mini-mowgarden-01-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="luoi ho xanh mini mowgarden 01"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/luoi-ho-xanh-mini-mowgarden-02-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="luoi ho xanh mini mowgarden 02"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13807 wishlist-fragment on-first-load"
                                                data-fragment-ref="13807"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13807,&quot;parent_product_id&quot;:13807,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13807&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13807"
                                                        data-product-type="simple"
                                                        data-original-product-id="13807"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13807" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13807" data-product_sku="SHBG005"
                                        aria-label="Thêm &ldquo;Cây lưỡi hổ xanh để bàn mini &#039;Black Gold&#039; chậu sứ SHBG005&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13807"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-luoi-ho-xanh-mini-black-gold-chau-su-shbg004-sao-chep/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            lưỡi hổ xanh để bàn mini &#8216;Black Gold&#8217; chậu sứ
                                            SHBG005</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13802 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-lan-y-chau-co-lon-de-ban-chau-su-trang-peac005/"
                                        aria-label="Cây Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-lan-y-de-ban-chau-su-mowgarden-01-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay lan y de ban chau su mowgarden 01"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-lan-y-de-ban-chau-su-mowgarden-02-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay lan y de ban chau su mowgarden 02"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13802 wishlist-fragment on-first-load"
                                                data-fragment-ref="13802"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13802,&quot;parent_product_id&quot;:13802,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13802&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13802"
                                                        data-product-type="simple"
                                                        data-original-product-id="13802"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13802" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13802" data-product_sku="PEAC005"
                                        aria-label="Thêm &ldquo;Cây Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13802"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-lan-y-chau-co-lon-de-ban-chau-su-trang-peac005/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13797 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-canh-nhiet-doi product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-phu-quy-chau-su-tho-cam-de-ban-agla104/"
                                        aria-label="Cây phú quý chậu sứ thổ cẩm để bàn AGLA104"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phu-quy-chau-su-tho-cam-mow-garden-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="cay phu quy chau su tho cam mow garden"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phu-quy-chau-su-tho-cam-mow-garden-02-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="cay phu quy chau su tho cam mow garden 02"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13797 wishlist-fragment on-first-load"
                                                data-fragment-ref="13797"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13797,&quot;parent_product_id&quot;:13797,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13797&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13797"
                                                        data-product-type="simple"
                                                        data-original-product-id="13797"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13797" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13797" data-product_sku="AGLA104"
                                        aria-label="Thêm &ldquo;Cây phú quý chậu sứ thổ cẩm để bàn AGLA104&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13797"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-phu-quy-chau-su-tho-cam-de-ban-agla104/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            phú quý chậu sứ thổ cẩm để bàn AGLA104</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13789 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-imperial-green-chau-su-phig005/"
                                        aria-label="Cây trầu bà đế vương xanh &#039;Imperial Green&#039; chậu sứ PHIG005">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="12 cay trau ba de vuong xanh de ban chau su trang 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="12 cay trau ba de vuong xanh de ban chau su trang 2">
                                    </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13789 wishlist-fragment on-first-load"
                                                data-fragment-ref="13789"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13789,&quot;parent_product_id&quot;:13789,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13789&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13789"
                                                        data-product-type="simple"
                                                        data-original-product-id="13789"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13789" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13789" data-product_sku="PHIG005"
                                        aria-label="Thêm &ldquo;Cây trầu bà đế vương xanh &#039;Imperial Green&#039; chậu sứ PHIG005&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13789"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-imperial-green-chau-su-phig005/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            trầu bà đế vương xanh &#8216;Imperial Green&#8217; chậu sứ
                                            PHIG005</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13784 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-ngoc-ngan-de-ban-chau-su-agsn010/"
                                        aria-label="Cây ngọc ngân để bàn chậu sứ AGSN010"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/10-cay-ngoc-ngan-nho-de-ban-chau-su--800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="10 cay ngoc ngan nho de ban chau su"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/10-cay-ngoc-ngan-nho-de-ban-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="10 cay ngoc ngan nho de ban chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13784 wishlist-fragment on-first-load"
                                                data-fragment-ref="13784"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13784,&quot;parent_product_id&quot;:13784,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13784&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13784"
                                                        data-product-type="simple"
                                                        data-original-product-id="13784"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13784" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13784" data-product_sku="AGSN010"
                                        aria-label="Thêm &ldquo;Cây ngọc ngân để bàn chậu sứ AGSN010&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13784"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-ngoc-ngan-de-ban-chau-su-agsn010/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            ngọc ngân để bàn chậu sứ AGSN010</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13781 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-kim-ngan-3-than-de-ban-chau-marle-loni037/"
                                        aria-label="Cây kim ngân 3 thân để bàn chậu marle LONI037">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/9-cay-kim-ngan-3-than-nho-de-ban-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="9 cay kim ngan 3 than nho de ban 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/9-cay-kim-ngan-3-than-nho-de-ban-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="9 cay kim ngan 3 than nho de ban 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13781 wishlist-fragment on-first-load"
                                                data-fragment-ref="13781"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13781,&quot;parent_product_id&quot;:13781,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13781&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13781"
                                                        data-product-type="simple"
                                                        data-original-product-id="13781"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13781" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13781" data-product_sku="LONI037"
                                        aria-label="Thêm &ldquo;Cây kim ngân 3 thân để bàn chậu marle LONI037&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13781"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-kim-ngan-3-than-de-ban-chau-marle-loni037/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            kim ngân 3 thân để bàn chậu marle LONI037</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>140.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13777 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-ban-cong product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                        aria-label="Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="11 cay bang dai loan cam thach chau su trang 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-3-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="11 cay bang dai loan cam thach chau su trang 3"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13777 wishlist-fragment on-first-load"
                                                data-fragment-ref="13777"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13777,&quot;parent_product_id&quot;:13777,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13777&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13777"
                                                        data-product-type="simple"
                                                        data-original-product-id="13777"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13777" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13777" data-product_sku="BUBU007"
                                        aria-label="Thêm &ldquo;Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13777"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            bàng Đài Loan cẩm thạch chậu sứ BUBU007</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13773 status-publish last instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-hanh-phuc-mot-than-co-thu-de-ban-chau-su-rade030/"
                                        aria-label="Cây hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/8-cay-hanh-phuc-de-ban-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="8 cay hanh phuc de ban chau su 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/8-cay-hanh-phuc-de-ban-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="8 cay hanh phuc de ban chau su 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13773 wishlist-fragment on-first-load"
                                                data-fragment-ref="13773"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13773,&quot;parent_product_id&quot;:13773,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13773&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13773"
                                                        data-product-type="simple"
                                                        data-original-product-id="13773"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13773" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13773" data-product_sku="RADE030"
                                        aria-label="Thêm &ldquo;Cây hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13773"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-hanh-phuc-mot-than-co-thu-de-ban-chau-su-rade030/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13768 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/cay-bang-singapore-mini-de-ban-chau-su-lyra048/"
                                        aria-label="Cây bàng Singapore mini để bàn chậu sứ LYRA048">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/7-cay-bang-singapore-nho-de-ban-chau-su-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="7 cay bang singapore nho de ban chau su 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/7-cay-bang-singapore-nho-de-ban-chau-su-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="7 cay bang singapore nho de ban chau su 2"> </a>
                                </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13768 wishlist-fragment on-first-load"
                                                data-fragment-ref="13768"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13768,&quot;parent_product_id&quot;:13768,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13768&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13768"
                                                        data-product-type="simple"
                                                        data-original-product-id="13768"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=13768" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="13768" data-product_sku="LYRA048"
                                        aria-label="Thêm &ldquo;Cây bàng Singapore mini để bàn chậu sứ LYRA048&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13768"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/cay-bang-singapore-mini-de-ban-chau-su-lyra048/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                            bàng Singapore mini để bàn chậu sứ LYRA048</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>