<section class="section" id="section_895630406">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
    <div class="section-content relative">
        <div id="text-3143143753" class="text">
            <h3><span style="font-family: 'book antiqua', palatino, serif; font-size: 100%;">Thông tin
                    &amp; Blog</span></h3>
            <style>
                #text-3143143753 {
                    font-size: 2rem;
                    text-align: center;
                }
            </style>
        </div>
        <div class="row row-small" style="max-width:1300px" id="row-906026445">
            <div id="col-1027271686" class="col small-12 large-12">
                <div class="col-inner">
                    <div class="row large-columns-3 medium-columns-3 small-columns-1 row-small row-full-width slider row-slider slider-nav-reveal"
                        data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/top-10-cay-phong-thuy-hop-menh-kim/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="800" height="600"
                                                    src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-800x600.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="cay phong thuy hop menh kim"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim.jpg 1200w"
                                                    sizes="(max-width: 800px) 100vw, 800px"> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Phong Thủy </p>
                                                <h5 class="post-title is-large">Top 10 cây phong thủy
                                                    hợp mệnh Kim giúp kích hoạt vượng khí và mang lại
                                                    may mắn.</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Trong phong thủy, mỗi
                                                    người đều có một mệnh theo ngày sinh và mỗi mệnh...
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/trong-cay-phong-thuy-trong-nha/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="800" height="599"
                                                    src="https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-800x599.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="huong dan trong cay phong thuy trong nha"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-800x599.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-1068x800.jpg 1068w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-768x575.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha.jpg 1200w"
                                                    sizes="(max-width: 800px) 100vw, 800px"> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Phong Thủy </p>
                                                <h5 class="post-title is-large">Hướng dẫn lựa chọn cây
                                                    phong thủy trong nhà để tăng vượng khí</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Theo quan niệm phong
                                                    thủy, vượng khí là yếu tố quan trọng giúp cho
                                                    ngôi... </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/cay-hanh-phuc-cach-cham-soc-cay-hanh-phuc/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="800" height="534"
                                                    src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-800x534.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="cay hanh phuc la cay gi"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-800x534.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi.jpg 1000w"
                                                    sizes="(max-width: 800px) 100vw, 800px"> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Kiến thức và cách chăm sóc Thông tin về cây </p>
                                                <h5 class="post-title is-large">Cây hạnh phúc là cây
                                                    gì? Hướng dẫn cách chăm sóc cây hạnh phúc trong nhà
                                                </h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Có một loại cây cảnh
                                                    trong nhà mà tên gọi của nó chính là điều... </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/cung-cap-cay-xanh-van-phong-cho-cong-ty-thiet-ke-cua-nhat/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="800" height="600"
                                                    src="https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-800x600.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="cay canh van phong 7"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-1536x1152.jpg 1536w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-2048x1536.jpg 2048w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-20x15.jpg 20w"
                                                    sizes="(max-width: 800px) 100vw, 800px"> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    MOW Garden 360° </p>
                                                <h5 class="post-title is-large">Cung cấp cây xanh văn
                                                    phòng cho công ty thiết kế Gifu của Nhật</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Tại chốn công sở,
                                                    việc phải tiếp xúc với các thiết bị điện tử hằng...
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/bang-gia-ban-cay-monstera/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="600" height="400"
                                                    src="https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-600x400.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="bảng giá bán cây monstera"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-510x340.jpg 510w"
                                                    sizes="(max-width: 600px) 100vw, 600px" /> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    MOW Garden 360° </p>
                                                <h5 class="post-title is-large">Bảng giá bán cây
                                                    Monstera mới nhất hôm nay 2021</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Cây trầu bà Monstera
                                                    (hay Trầu Bà Lá Xẻ/Trầu Bà Nam Mỹ) đang là loại...
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/dat-trong-sen-da/" class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="600" height="400"
                                                    src="https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-600x400.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="đất trồng sen đá"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-510x340.jpg 510w"
                                                    sizes="(max-width: 600px) 100vw, 600px" /> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Kiến thức và cách chăm sóc </p>
                                                <h5 class="post-title is-large">Hướng dẫn cách trộn
                                                    đất trồng sen đá đơn giản</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Không giống như các
                                                    loại cây khác, đất trồng dành cho sen đá có những...
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a
                                    href="https://mowgarden.com/sen-da-kim-cuong/" class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="600" height="400"
                                                    src="https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-600x400.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="các loại sen đá kim cương"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-510x340.jpg 510w"
                                                    sizes="(max-width: 600px) 100vw, 600px" /> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Thông tin về cây </p>
                                                <h5 class="post-title is-large">Sự thật thú vị về cây
                                                    Sen Đá Kim Cương</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Nhắc tới hai từ
                                                    &#8220;kim cương&#8221; thì hẳn là bạn sẽ liên tưởng
                                                    ngay tới... </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                        <div class="col post-item">
                            <div class="col-inner"> <a href="https://mowgarden.com/cac-loai-sen-da/"
                                    class="plain">
                                    <div class="box box-push box-text-bottom box-blog-post has-hover">
                                        <div class="box-image">
                                            <div class="image-glow image-cover"
                                                style="padding-top:75%;"> <img loading="lazy"
                                                    decoding="async" width="600" height="400"
                                                    src="https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-600x400.jpg"
                                                    class="attachment-medium size-medium wp-post-image"
                                                    alt="tên các loại sen đá"
                                                    srcset="https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-510x340.jpg 510w"
                                                    sizes="(max-width: 600px) 100vw, 600px" /> </div>
                                        </div>
                                        <div class="box-text text-left">
                                            <div class="box-text-inner blog-post-inner">
                                                <p
                                                    class="cat-label tag-label is-xxsmall op-7 uppercase">
                                                    Thông tin về cây </p>
                                                <h5 class="post-title is-large">Thư viện Các Loại Sen
                                                    Đá phổ thông và quý hiếm</h5>
                                                <div class="is-divider"></div>
                                                <p class="from_the_blog_excerpt">Sen đá là một cái tên
                                                    đã trở nên quá quen thuộc với nhiều người,... </p>
                                            </div>
                                        </div>
                                    </div>
                                </a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #section_895630406 {
            padding-top: 30px;
            padding-bottom: 30px;
        }

        #section_895630406 .ux-shape-divider--top svg {
            height: 150px;
            --divider-top-width: 100%;
        }

        #section_895630406 .ux-shape-divider--bottom svg {
            height: 150px;
            --divider-width: 100%;
        }
    </style>
</section>