<header id="header" class="header has-sticky sticky-jump sticky-hide-on-scroll">
    <div class="header-wrapper">
        <div id="top-bar" class="header-top hide-for-sticky nav-dark">
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <ul class="nav nav-left medium-nav-center nav-small nav-divided">
                        <li class="header-contact-wrapper">
                            <ul id="header-contact" class="nav nav-divided nav-uppercase header-contact">
                                <li class=""> <a class="tooltip" title="08:00 - 19:30 "> <i class="fa-regular fa-clock" style="font-size: 12px"></i> <span>08:30 -
                                            19:00</span> </a> </li>
                                <li class=""> <a href="tel:0783 655 566" class="tooltip"
                                        title="0783 655 566"> <i class="fa-solid fa-phone" style="font-size: 12px"></i> <span>0783 655 566</span> </a> </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="flex-col hide-for-medium flex-center">
                    <ul class="nav nav-center nav-small nav-divided"> </ul>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="nav top-bar-nav nav-right nav-small nav-divided">
                        <li class="header-wishlist-icon">
                            <div class="header-button"> <a href="https://mowgarden.com/wishlist/"
                                    class="wishlist-link icon primary button circle is-small"> <i class="fa-solid fa-heart"></i> </a> </div>
                        </li>
                        @if (Auth::guard('web')->check())
                              <div class="dropdown">
                                <button type="button" class="dropdown-toggle" data-bs-toggle="dropdown" style="color: white">
                                    {{Auth::user()->name}}
                                </button>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="#">Trang Cá Nhân</a></li>
                                  <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Đăng Xuất</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                                </ul>
                              </div>
                        @else
                        {{-- <li class="account-item has-icon"> <a href="{{route('login')}}"
                            class="nav-top-link nav-top-not-logged-in is-small"
                            data-open="#login-form-popup"> <i class="fa-solid fa-user"></i><span> Đăng nhập </span> </a> </li> --}}
                            <li class="account-item has-icon"> <a href="{{route('login')}}"> <i class="fa-solid fa-user"></i><span> Đăng nhập </span> </a> </li>
                        @endif
                    </ul>
                </div>
                <div class="flex-col show-for-medium flex-grow">
                    <ul class="nav nav-center nav-small mobile-nav nav-divided">
                        <li class="html custom html_topbar_left"><strong style="font-size: 1.2em;">HOTLINE: 0783 655 566 (8h00 - 19h30)</strong></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="masthead" class="header-main">
            <div class="header-inner flex-row container logo-left medium-logo-center" role="navigation">
                <!-- Logo -->
                <div id="logo" class="flex-col logo"> <!-- Header logo --> <a
                        href="{{route('home')}}" title="Cào Cào Garden - Planting with love" rel="home">
                        <img width="200" height="103"
                            src="{{asset('img/logocaocao.png')}}"
                            class="header_logo header-logo" alt="𝓒𝓪̀𝓸 𝓒𝓪̀𝓸 𝓖𝓪𝓻𝓭𝓮𝓷" /><img width="200"
                            height="103"
                            rc="{{asset('img/logocaocao.png')}}"
                            class="header-logo-dark" alt="𝓒𝓪̀𝓸 𝓒𝓪̀𝓸 𝓖𝓪𝓻𝓭𝓮𝓷" /></a> </div>
                <!-- Mobile Left Elements -->
                <div class="flex-col show-for-medium flex-left">
                    <ul class="mobile-nav nav nav-left">
                        <li class="nav-icon has-icon">
                            <div class="header-button"> <a href="#" data-open="#main-menu"
                                    data-pos="center" data-bg="main-menu-overlay" data-color="dark"
                                    class="icon button circle is-outline is-small" aria-label="Menu"
                                    aria-controls="main-menu" aria-expanded="false"> <i class="fa-solid fa-bars"></i> </a> </div>
                        </li>
                    </ul>
                </div> <!-- Left Elements -->
                <div class="flex-col hide-for-medium flex-left flex-grow">
                    <ul
                        class="header-nav header-nav-main nav nav-left nav-size-xlarge nav-spacing-xlarge nav-prompts-overlay">
                        <li id="menu-item-1009"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1009 menu-item-design-full-width menu-item-has-block has-dropdown">
                            <a href="{{route('gioithieu')}}" class="nav-top-link"
                                aria-expanded="false" aria-haspopup="menu">Giới thiệu<i
                                    class="icon-angle-down"></i></a>
                        </li>
                        <li id="menu-item-631"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-631 menu-item-design-full-width menu-item-has-block has-dropdown">
                            <a href="{{route('caytrong')}}" class="nav-top-link"
                                aria-expanded="false" aria-haspopup="menu">Cây ở vườn<i
                                    class="icon-angle-down"></i></a>
                            <div class="sub-menu nav-dropdown">
                                <div class="row row-collapse row-full-width align-equal" id="row-1439174260">
                                    <div id="col-16767454" class="col medium-12 small-12 large-3">
                                        <div class="col-inner text-left dark"
                                            style="background-color:rgb(252, 249, 243);"
                                            data-parallax-fade="true" data-parallax="-2">
                                            <div class="row row-collapse align-equal" id="row-1607017636">
                                                <div id="col-1619675352" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-755157724" class="text">
                                                            <p><span
                                                                    style="color: #214738; font-size: 110%; font-family: 'book antiqua', palatino, serif;"><strong>Theo
                                                                        kiểu dáng</strong></span></p>
                                                            <style>
                                                                #text-755157724 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-649361608" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-649361608 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-tam-trung/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Tầm
                                                                        Trung </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-tam-thap/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Tầm
                                                                        Thấp </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-xanh-tan-hinh-tron/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Tán Hình
                                                                        Tròn </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-xanh-tan-hinh-thap/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Tán Hình
                                                                        Tháp </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-xanh-tan-phan-tang/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Tán Phân
                                                                        Tầng </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-1607017636>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-1017201941" class="col medium-12 small-12 large-3">
                                        <div class="col-inner" style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-1472560245">
                                                <div id="col-1054144804" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-1989232513" class="text">
                                                            <p><span
                                                                    style="color: #214738; font-family: 'book antiqua', palatino, serif;"><strong><span
                                                                            style="font-size: 110%;">Theo đặc
                                                                            điểm</span></strong></span></p>
                                                            <style>
                                                                #text-1989232513 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-2098849096" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-2098849096 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-thuong-xanh/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây
                                                                        Thường Xanh </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-la-mau/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Lá
                                                                        Màu </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-dang-bui/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Dạng
                                                                        Bụi </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-leo-dan/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Leo
                                                                        Dàn </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-than-dot/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Thân
                                                                        Đốt </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-1472560245>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-1128435827" class="col medium-12 small-12 large-3">
                                        <div class="col-inner" style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-1488612758">
                                                <div id="col-1623123785" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-4089756176" class="text">
                                                            <p><span
                                                                    style="font-family: 'book antiqua', palatino, serif;"><strong><span
                                                                            style="color: #214738; font-size: 110%;">Theo
                                                                            chức năng</span></strong></span></p>
                                                            <style>
                                                                #text-4089756176 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-1909155776" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-1909155776 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-bong-mat/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Bóng
                                                                        Mát </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-cong-trinh-co-hoa/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Có
                                                                        Hoa </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-an-qua/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Ăn
                                                                        Quả </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-hang-rao/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Hàng
                                                                        Rào </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/cay-che-phu-nen/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cây Phủ
                                                                        Nền </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item colormenu">
                                                                <a class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/co-canh-quan/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Cỏ Nền
                                                                    </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-1488612758>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-583607222" class="col medium-12 small-12 large-3">
                                        <div class="col-inner text-shadow-1 dark"
                                            style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-856494151">
                                                <div id="col-657781796" class="col small-12 large-12">
                                                    <div class="col-inner">
                                                        <div class="banner has-hover" id="banner-1666746740">
                                                            <div class="banner-inner fill">
                                                                <div class="banner-bg fill">
                                                                    <div class="bg fill bg-fill bg-loaded">
                                                                    </div>
                                                                </div>
                                                                <div class="banner-layers container">
                                                                    <div class="fill banner-link"></div>
                                                                </div>
                                                            </div>
                                                            <style>
                                                                #banner-1666746740 {
                                                                    padding-top: 300px;
                                                                    background-color: rgb(171, 161, 156);
                                                                }

                                                                #banner-1666746740 .bg.bg-loaded {
                                                                    background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/menu-cay-ngoai-troi-533x800.jpg);
                                                                }

                                                                #banner-1666746740 .bg {
                                                                    background-position: 24% 69%;
                                                                }

                                                                #banner-1666746740 .ux-shape-divider--top svg {
                                                                    height: 150px;
                                                                    --divider-top-width: 100%;
                                                                }

                                                                #banner-1666746740 .ux-shape-divider--bottom svg {
                                                                    height: 150px;
                                                                    --divider-width: 100%;
                                                                }
                                                            </style>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-856494151>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        #row-1439174260>.col>.col-inner {
                                            padding: 20px 60px 19px 60px;
                                        }
                                    </style>
                                </div>
                            </div>
                        </li>
                        <li id="menu-item-623"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-623 menu-item-design-full-width menu-item-has-block has-dropdown">
                            <a href="{{route('chaucay')}}" class="nav-top-link"
                                aria-expanded="false" aria-haspopup="menu">Chậu cây<i
                                    class="icon-angle-down"></i></a>
                            <div class="sub-menu nav-dropdown">
                                <div class="row row-collapse row-full-width align-equal" id="row-87118992">
                                    <div id="col-433924531" class="col medium-12 small-12 large-3">
                                        <div class="col-inner" style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-689989836">
                                                <div id="col-1933726385" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-2275968201" class="text">
                                                            <p><span
                                                                    style="font-size: 110%; color: #214738; font-family: 'book antiqua', palatino, serif;"><strong>Theo
                                                                        kiểu dáng</strong></span></p>
                                                            <style>
                                                                #text-2275968201 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-1935507728" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-1935507728 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start colormenu ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/kieu-chau-vuong/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Hình
                                                                        Vuông </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-cay-hinh-chu-nhat/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Chữ
                                                                        Nhật </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/kieu-hinh-bau-tron/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Hình Tròn
                                                                        Bầu </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-cay-kieu-tru-dung/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Trụ Tròn
                                                                        Đứng </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/kieu-chau-treo/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Treo
                                                                    </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-689989836>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-1410864167" class="col medium-12 small-12 large-3">
                                        <div class="col-inner" style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-971201055">
                                                <div id="col-792597698" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-288334508" class="text">
                                                            <p><span
                                                                    style="color: #214738; font-size: 110%; font-family: 'book antiqua', palatino, serif;"><strong>Theo
                                                                        vật liệu</strong></span></p>
                                                            <style>
                                                                #text-288334508 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-2058878514" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-2058878514 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start colormenu ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-nhua/"> <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Nhựa
                                                                    </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-xi-mang/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Xi
                                                                        Măng </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-dat-nung/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Đất
                                                                        Nung </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-gom-su/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Gốm
                                                                        Sứ </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-go/"> <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Gỗ
                                                                    </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-971201055>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-476935362" class="col medium-12 small-12 large-3">
                                        <div class="col-inner" style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse align-equal" id="row-578360660">
                                                <div id="col-1160341505" class="col small-12 large-12">
                                                    <div class="col-inner dark">
                                                        <div id="text-2637807258" class="text">
                                                            <p><span
                                                                    style="color: #214738; font-size: 110%; font-family: 'book antiqua', palatino, serif;"><strong>Theo
                                                                        kích thước</strong></span></p>
                                                            <style>
                                                                #text-2637807258 {
                                                                    font-size: 1.25rem;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div id="gap-2059385188" class="gap-element clearfix"
                                                            style="display:block; height:auto;">
                                                            <style>
                                                                #gap-2059385188 {
                                                                    padding-top: 15px;
                                                                }
                                                            </style>
                                                        </div>
                                                        <div
                                                            class="ux-menu stack stack-col justify-start colormenu ux-menu--divider-solid">
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-mini-de-ban/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Nhỏ
                                                                        Để Bàn </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-co-trung/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Cỡ
                                                                        Trung </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-co-lon/">
                                                                    <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Cỡ
                                                                        Lớn </span> </a> </div>
                                                            <div class="ux-menu-link flex menu-item"> <a
                                                                    class="ux-menu-link__link flex"
                                                                    href="https://mowgarden.com/chau-cao/"> <i
                                                                        class="ux-menu-link__icon text-center icon-angle-right"></i>
                                                                    <span class="ux-menu-link__text"> Chậu Dáng
                                                                        Cao </span> </a> </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-578360660>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="col-76276895" class="col medium-12 small-12 large-3">
                                        <div class="col-inner dark"
                                            style="background-color:rgb(252, 249, 243);">
                                            <div class="row row-collapse row-full-width align-equal"
                                                id="row-983359508">
                                                <div id="col-2097798451" class="col small-12 large-12">
                                                    <div class="col-inner">
                                                        <div class="banner has-hover" id="banner-799782971">
                                                            <div class="banner-inner fill">
                                                                <div class="banner-bg fill">
                                                                    <div class="bg fill bg-fill bg-loaded">
                                                                    </div>
                                                                </div>
                                                                <div class="banner-layers container">
                                                                    <div class="fill banner-link"></div>
                                                                </div>
                                                            </div>
                                                            <style>
                                                                #banner-799782971 {
                                                                    padding-top: 300px;
                                                                }

                                                                #banner-799782971 .bg.bg-loaded {
                                                                    background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/menu-chau-cay-kieng.jpg);
                                                                }

                                                                #banner-799782971 .ux-shape-divider--top svg {
                                                                    height: 150px;
                                                                    --divider-top-width: 100%;
                                                                }

                                                                #banner-799782971 .ux-shape-divider--bottom svg {
                                                                    height: 150px;
                                                                    --divider-width: 100%;
                                                                }
                                                            </style>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #row-983359508>.col>.col-inner {
                                                        padding: 10px 10px 10px 10px;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        #row-87118992>.col>.col-inner {
                                            padding: 20px 60px 20px 80px;
                                        }
                                    </style>
                                </div>
                            </div>
                        </li>
                        <li id="menu-item-628"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-628 menu-item-design-full-width menu-item-has-block has-dropdown">
                            <a href="{{route('phukien')}}" class="nav-top-link" aria-expanded="false"
                                aria-haspopup="menu">Phụ kiện<i class="icon-angle-down"></i></a>
                            <div class="sub-menu nav-dropdown">
                                <section class="section" id="section_1642374280">
                                    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
                                    <div class="section-content relative">
                                        <div class="row row-full-width align-middle align-center"
                                            id="row-561741415">
                                            <div id="col-2021182913" class="col medium-9 small-12 large-9">
                                                <div class="col-inner">
                                                    <div class="row large-columns-5 medium-columns-3 small-columns-2 row-xsmall slider row-slider slider-nav-circle slider-nav-push"
                                                        data-flickity-options='{"imagesLoaded": true, "groupCells": "100%", "dragThreshold" : 5, "cellAlign": "left","wrapAround": true,"prevNextButtons": true,"percentPosition": true,"pageDots": false, "rightToLeft": false, "autoPlay" : false}'>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-chau-au-pknc035/"
                                                                                aria-label="Phụ kiện trang trí biệt thự Châu Âu PKNC035">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    fetchpriority="high"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon"> <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11733 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11733"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11733,&quot;parent_product_id&quot;:11733,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11733&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11733"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11733"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-chau-au-pknc035/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí biệt thự Châu
                                                                                    Âu PKNC035</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-vuon-cay-pknc034/"
                                                                                aria-label="Phụ kiện trang trí biệt thự vườn cây PKNC034">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon"> <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11730 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11730"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11730,&quot;parent_product_id&quot;:11730,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11730&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11730"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11730"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-vuon-cay-pknc034/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí biệt thự vườn
                                                                                    cây PKNC034</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-pknc033/"
                                                                                aria-label="Phụ kiện trang trí nhà mái rơm PKNC033">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon"> <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11726 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11726"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11726,&quot;parent_product_id&quot;:11726,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11726&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11726"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11726"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-pknc033/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí nhà mái rơm
                                                                                    PKNC033</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-lau-dai-da-pknc032/"
                                                                                aria-label="Phụ kiện trang trí lâu đài đá PKNC032">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon"> <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11722 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11722"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11722,&quot;parent_product_id&quot;:11722,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11722&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11722"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11722"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-lau-dai-da-pknc032/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí lâu đài đá
                                                                                    PKNC032</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-toa-thap-nhieu-tang-pknc031/"
                                                                                aria-label="Phụ kiện trang trí tòa tháp nhiều tầng PKNC031">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon">
                                                                                <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11719 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11719"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11719,&quot;parent_product_id&quot;:11719,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11719&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11719"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11719"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-toa-thap-nhieu-tang-pknc031/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí tòa tháp
                                                                                    nhiều tầng PKNC031</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-ven-bien-pknc030/"
                                                                                aria-label="Phụ kiện trang trí biệt thự ven biển PKNC030">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon">
                                                                                <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11715 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11715"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11715,&quot;parent_product_id&quot;:11715,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11715&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11715"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11715"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-ven-bien-pknc030/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí biệt thự ven
                                                                                    biển PKNC030</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-van-ly-truong-thanh-pknc029/"
                                                                                aria-label="Phụ kiện trang trí vạn lý trường thành PKNC029">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon">
                                                                                <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11712 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11712"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11712,&quot;parent_product_id&quot;:11712,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11712&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11712"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11712"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-van-ly-truong-thanh-pknc029/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí vạn lý trường
                                                                                    thành PKNC029</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="col-inner">
                                                                <div
                                                                    class="badge-container absolute left top z-1">
                                                                </div>
                                                                <div
                                                                    class="product-small box has-hover box-normal box-text-bottom">
                                                                    <div class="box-image">
                                                                        <div class=""> <a
                                                                                href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-nhieu-mau-pknc028/"
                                                                                aria-label="Phụ kiện trang trí hàng rào gỗ nhiều màu PKNC028">
                                                                                <img width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2.jpg"
                                                                                    class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" /><img
                                                                                    width="800"
                                                                                    height="800"
                                                                                    src="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1.jpg"
                                                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                    alt=""
                                                                                    decoding="async"
                                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-20x20.jpg 20w"
                                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                                            </a> </div>
                                                                        <div
                                                                            class="image-tools top right show-on-hover">
                                                                            <div class="wishlist-icon">
                                                                                <button
                                                                                    class="wishlist-button button is-outline circle icon"
                                                                                    aria-label="Wishlist"> <i
                                                                                        class="icon-heart"></i>
                                                                                </button>
                                                                                <div
                                                                                    class="wishlist-popup dark">
                                                                                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11709 wishlist-fragment on-first-load"
                                                                                        data-fragment-ref="11709"
                                                                                        data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11709,&quot;parent_product_id&quot;:11709,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                                                        <!-- ADD TO WISHLIST -->
                                                                                        <div
                                                                                            class="yith-wcwl-add-button">
                                                                                            <a href="?add_to_wishlist=11709&#038;_wpnonce=523ba72503"
                                                                                                class="add_to_wishlist single_add_to_wishlist"
                                                                                                data-product-id="11709"
                                                                                                data-product-type="simple"
                                                                                                data-original-product-id="11709"
                                                                                                data-title="Add to wishlist"
                                                                                                rel="nofollow">
                                                                                                <i
                                                                                                    class="yith-wcwl-icon fa fa-heart-o"></i>
                                                                                                <span>Add to
                                                                                                    wishlist</span>
                                                                                            </a> </div>
                                                                                        <!-- COUNT TEXT -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div
                                                                            class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                                                        </div>
                                                                    </div>
                                                                    <div class="box-text text-center">
                                                                        <div class="title-wrapper">
                                                                            <p
                                                                                class="name product-title woocommerce-loop-product__title">
                                                                                <a href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-nhieu-mau-pknc028/"
                                                                                    class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                                                    kiện trang trí hàng rào gỗ
                                                                                    nhiều màu PKNC028</a></p>
                                                                        </div>
                                                                        <div class="price-wrapper"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        #section_1642374280 {
                                            padding-top: 30px;
                                            padding-bottom: 30px;
                                        }

                                        #section_1642374280 .ux-shape-divider--top svg {
                                            height: 150px;
                                            --divider-top-width: 100%;
                                        }

                                        #section_1642374280 .ux-shape-divider--bottom svg {
                                            height: 150px;
                                            --divider-width: 100%;
                                        }
                                    </style>
                                </section>
                            </div>
                        </li>
                        <li id="menu-item-629"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-629 menu-item-design-full-width menu-item-has-block has-dropdown">
                            <a href="{{route('huongdan')}}" class="nav-top-link"
                                aria-expanded="false" aria-haspopup="menu">Hướng dẫn<i
                                    class="icon-angle-down"></i></a>
                            <div class="sub-menu nav-dropdown">
                                <section class="section" id="section_1222010035">
                                    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
                                    <div class="section-content relative">
                                        <div class="row row-small row-full-width align-center"
                                            id="row-1983951634">
                                            <div id="col-485413115" class="col medium-3 small-6 large-3">
                                                <div class="col-inner text-left">
                                                    <div class="img has-hover x md-x lg-x y md-y lg-y"
                                                        id="image_634970320"> <a class=""
                                                            href="https://mowgarden.com/category/thong-tin-cay-canh/">
                                                            <div class="img-inner dark"> <img width="800"
                                                                    height="800"
                                                                    src="https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1.jpg"
                                                                    class="attachment-large size-large"
                                                                    alt="" decoding="async"
                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-1-20x20.jpg 20w"
                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                            </div>
                                                        </a>
                                                        <style>
                                                            #image_634970320 {
                                                                width: 100%;
                                                            }
                                                        </style>
                                                    </div>
                                                    <h4>THÔNG TIN VỀ CÂY</h4>
                                                    <p>Toàn bộ hồ sơ thông tin về các loại cây cảnh gồm có hình
                                                        ảnh, đặc điểm, tên khoa học... </p>
                                                </div>
                                            </div>
                                            <div id="col-2007511876" class="col medium-3 small-6 large-3">
                                                <div class="col-inner">
                                                    <div class="img has-hover x md-x lg-x y md-y lg-y"
                                                        id="image_676290710"> <a class=""
                                                            href="https://mowgarden.com/category/kien-thuc-va-cach-cham-soc/">
                                                            <div class="img-inner dark"> <img width="800"
                                                                    height="800"
                                                                    src="https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-800x800.jpg"
                                                                    class="attachment-large size-large"
                                                                    alt="" decoding="async"
                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-800x800.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/02/hinh-menu-huong-dan-2.jpg 1000w"
                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                            </div>
                                                        </a>
                                                        <style>
                                                            #image_676290710 {
                                                                width: 100%;
                                                            }
                                                        </style>
                                                    </div>
                                                    <h4>KIẾN THỨC &amp; CÁCH CHĂM SÓC</h4>
                                                    <p>Các bài viết hướng dẫn chăm sóc &amp; những thông tin hữu
                                                        ích về cây cảnh.</p>
                                                </div>
                                            </div>
                                            <div id="col-670447978" class="col medium-3 small-6 large-3">
                                                <div class="col-inner">
                                                    <div class="img has-hover x md-x lg-x y md-y lg-y"
                                                        id="image_1764731090"> <a class=""
                                                            href="https://mowgarden.com/category/cam-hung-va-y-tuong/">
                                                            <div class="img-inner dark"> <img width="800"
                                                                    height="800"
                                                                    src="https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-800x800.jpg"
                                                                    class="attachment-large size-large"
                                                                    alt="" decoding="async"
                                                                    srcset="https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-800x800.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/hinh-menu-huong-dan-3.jpg 1000w"
                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                            </div>
                                                        </a>
                                                        <style>
                                                            #image_1764731090 {
                                                                width: 100%;
                                                            }
                                                        </style>
                                                    </div>
                                                    <h4>CẢM HỨNG &amp; Ý TƯỞNG</h4>
                                                    <p>Tổng hợp những mẹo và ý tưởng về cây giúp bạn có không
                                                        gian sống lý tưởng.</p>
                                                </div>
                                            </div>
                                            <div id="col-14215576" class="col medium-3 small-6 large-3">
                                                <div class="col-inner">
                                                    <div class="img has-hover x md-x lg-x y md-y lg-y"
                                                        id="image_202915100"> <a class=""
                                                            href="https://mowgarden.com/category/mow-garden-360/">
                                                            <div class="img-inner dark"> <img width="800"
                                                                    height="800"
                                                                    src="https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-800x800.jpg"
                                                                    class="attachment-large size-large"
                                                                    alt="" decoding="async"
                                                                    srcset="https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-800x800.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/01/hinh-menu-huong-dan-4.jpg 1000w"
                                                                    sizes="(max-width: 800px) 100vw, 800px" />
                                                            </div>
                                                        </a>
                                                        <style>
                                                            #image_202915100 {
                                                                width: 100%;
                                                            }
                                                        </style>
                                                    </div>
                                                    <h4>MOW GARDEN 360°</h4>
                                                    <p>Hãy cùng lắng nghe những câu chuyện thú vị về cây xanh từ
                                                        MOW nhé.</p>
                                                </div>
                                            </div>
                                            <style>
                                                #row-1983951634>.col>.col-inner {
                                                    padding: 0px px 0px 0px;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                    <style>
                                        #section_1222010035 {
                                            padding-top: 30px;
                                            padding-bottom: 30px;
                                        }

                                        #section_1222010035 .ux-shape-divider--top svg {
                                            height: 150px;
                                            --divider-top-width: 100%;
                                        }

                                        #section_1222010035 .ux-shape-divider--bottom svg {
                                            height: 150px;
                                            --divider-width: 100%;
                                        }
                                    </style>
                                </section>
                            </div>
                        </li>
                    </ul>
                </div> <!-- Right Elements -->
                <div class="flex-col hide-for-medium flex-right">
                    <ul
                        class="header-nav header-nav-main nav nav-right nav-size-xlarge nav-spacing-xlarge nav-prompts-overlay">
                        <li class="cart-item has-icon has-dropdown"> <a href="{{route('giohang')}}"
                                title="Giỏ hàng" class="header-cart-link is-small"> <span
                                    class="header-cart-title"> <span class="cart-price"><span
                                            class="woocommerce-Price-amount amount"><bdi>0<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </span> <i class="fa-solid fa-cart-shopping" data-icon-label="0"></i> </a>
                            <ul class="nav-dropdown nav-dropdown-simple">
                                <li class="html widget_shopping_cart">
                                    <div class="widget_shopping_cart_content">
                                        <p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong
                                            giỏ hàng.</p>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="header-search header-search-lightbox has-icon"> <a
                                href="#search-lightbox" aria-label="Tìm kiếm" data-open="#search-lightbox"
                                data-focus="input.search-field" class="is-small"> <i class="fa-solid fa-magnifying-glass" style="font-size: 18px"></i></a>
                            <div id="search-lightbox" class="mfp-hide dark text-center">
                                <div class="searchform-wrapper ux-search-box relative form-flat is-large">
                                    <form role="search" method="get" class="searchform"
                                        action="https://mowgarden.com/">
                                        <div class="flex-row relative">
                                            <div class="flex-col flex-grow"> <label
                                                    class="screen-reader-text"
                                                    for="woocommerce-product-search-field-0">Tìm kiếm:</label>
                                                <input type="search"
                                                    id="woocommerce-product-search-field-0"
                                                    class="search-field mb-0"
                                                    placeholder="Tìm kiếm sản phẩm" value=""
                                                    name="s" /> <input type="hidden"
                                                    name="post_type" value="product" /> </div>
                                            <div class="flex-col"> <button type="submit" value="Tìm kiếm"
                                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                                    aria-label="Submit"> <i class="icon-search"></i>
                                                </button> </div>
                                        </div>
                                        <div class="live-search-results text-left z-top"></div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> <!-- Mobile Right Elements -->
                <div class="flex-col show-for-medium flex-right">
                    <ul class="mobile-nav nav nav-right">
                        <li class="cart-item has-icon"> <a href="{{route('giohang')}}"
                                class="header-cart-link off-canvas-toggle nav-top-link is-small"
                                data-open="#cart-popup" data-class="off-canvas-cart" title="Giỏ hàng"
                                data-pos="right"> <i class="icon-shopping-cart" data-icon-label="0"> </i>
                            </a> <!-- Cart Sidebar Popup -->
                            <div id="cart-popup" class="mfp-hide widget_shopping_cart">
                                <div class="cart-popup-inner inner-padding">
                                    <div class="cart-popup-title text-center">
                                        <h4 class="uppercase">Giỏ hàng</h4>
                                        <div class="is-divider"></div>
                                    </div>
                                    <div class="widget_shopping_cart_content">
                                        <p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong
                                            giỏ hàng.</p>
                                    </div>
                                    <div class="cart-sidebar-content relative"></div>
                                    <div class="payment-icons inline-block">
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewbox="0 0 64 32">
                                                <path
                                                    d="M10.781 7.688c-0.251-1.283-1.219-1.688-2.344-1.688h-8.376l-0.061 0.405c5.749 1.469 10.469 4.595 12.595 10.501l-1.813-9.219zM13.125 19.688l-0.531-2.781c-1.096-2.907-3.752-5.594-6.752-6.813l4.219 15.939h5.469l8.157-20.032h-5.501l-5.062 13.688zM27.72 26.061l3.248-20.061h-5.187l-3.251 20.061h5.189zM41.875 5.656c-5.125 0-8.717 2.72-8.749 6.624-0.032 2.877 2.563 4.469 4.531 5.439 2.032 0.968 2.688 1.624 2.688 2.499 0 1.344-1.624 1.939-3.093 1.939-2.093 0-3.219-0.251-4.875-1.032l-0.688-0.344-0.719 4.499c1.219 0.563 3.437 1.064 5.781 1.064 5.437 0.032 8.97-2.688 9.032-6.843 0-2.282-1.405-4-4.376-5.439-1.811-0.904-2.904-1.563-2.904-2.499 0-0.843 0.936-1.72 2.968-1.72 1.688-0.029 2.936 0.314 3.875 0.752l0.469 0.248 0.717-4.344c-1.032-0.406-2.656-0.844-4.656-0.844zM55.813 6c-1.251 0-2.189 0.376-2.72 1.688l-7.688 18.374h5.437c0.877-2.467 1.096-3 1.096-3 0.592 0 5.875 0 6.624 0 0 0 0.157 0.688 0.624 3h4.813l-4.187-20.061h-4zM53.405 18.938c0 0 0.437-1.157 2.064-5.594-0.032 0.032 0.437-1.157 0.688-1.907l0.374 1.72c0.968 4.781 1.189 5.781 1.189 5.781-0.813 0-3.283 0-4.315 0z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewbox="0 0 64 32">
                                                <path
                                                    d="M35.255 12.078h-2.396c-0.229 0-0.444 0.114-0.572 0.303l-3.306 4.868-1.4-4.678c-0.088-0.292-0.358-0.493-0.663-0.493h-2.355c-0.284 0-0.485 0.28-0.393 0.548l2.638 7.745-2.481 3.501c-0.195 0.275 0.002 0.655 0.339 0.655h2.394c0.227 0 0.439-0.111 0.569-0.297l7.968-11.501c0.191-0.275-0.006-0.652-0.341-0.652zM19.237 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM22.559 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.166-0.241c-0.517-0.749-1.667-1-2.817-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.251-0.155-0.479-0.41-0.479zM8.254 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.262 0.307 0.341 0.761 0.242 1.388zM7.68 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.155 0.479 0.41 0.479h2.378c0.34 0 0.63-0.248 0.683-0.584l0.543-3.444c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.628-1.272zM60.876 7.823l-2.043 12.998c-0.040 0.252 0.155 0.479 0.41 0.479h2.055c0.34 0 0.63-0.248 0.683-0.584l2.015-12.765c0.040-0.252-0.155-0.479-0.41-0.479h-2.299c-0.205 0.001-0.379 0.148-0.41 0.351zM54.744 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM58.066 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.167-0.241c-0.516-0.749-1.667-1-2.816-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.252-0.156-0.479-0.41-0.479zM43.761 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.261 0.307 0.34 0.761 0.241 1.388zM43.187 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.156 0.479 0.41 0.479h2.554c0.238 0 0.441-0.173 0.478-0.408l0.572-3.619c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.627-1.272z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewbox="0 0 64 32">
                                                <path
                                                    d="M13.043 8.356c-0.46 0-0.873 0.138-1.24 0.413s-0.662 0.681-0.885 1.217c-0.223 0.536-0.334 1.112-0.334 1.727 0 0.568 0.119 0.99 0.358 1.265s0.619 0.413 1.141 0.413c0.508 0 1.096-0.131 1.765-0.393v1.327c-0.693 0.262-1.389 0.393-2.089 0.393-0.884 0-1.572-0.254-2.063-0.763s-0.736-1.229-0.736-2.161c0-0.892 0.181-1.712 0.543-2.462s0.846-1.32 1.452-1.709 1.302-0.584 2.089-0.584c0.435 0 0.822 0.038 1.159 0.115s0.7 0.217 1.086 0.421l-0.616 1.276c-0.369-0.201-0.673-0.333-0.914-0.398s-0.478-0.097-0.715-0.097zM19.524 12.842h-2.47l-0.898 1.776h-1.671l3.999-7.491h1.948l0.767 7.491h-1.551l-0.125-1.776zM19.446 11.515l-0.136-1.786c-0.035-0.445-0.052-0.876-0.052-1.291v-0.184c-0.153 0.408-0.343 0.84-0.569 1.296l-0.982 1.965h1.739zM27.049 12.413c0 0.711-0.257 1.273-0.773 1.686s-1.213 0.62-2.094 0.62c-0.769 0-1.389-0.153-1.859-0.46v-1.398c0.672 0.367 1.295 0.551 1.869 0.551 0.39 0 0.694-0.072 0.914-0.217s0.329-0.343 0.329-0.595c0-0.147-0.024-0.275-0.070-0.385s-0.114-0.214-0.201-0.309c-0.087-0.095-0.303-0.269-0.648-0.52-0.481-0.337-0.818-0.67-1.013-1s-0.293-0.685-0.293-1.066c0-0.439 0.108-0.831 0.324-1.176s0.523-0.614 0.922-0.806 0.857-0.288 1.376-0.288c0.755 0 1.446 0.168 2.073 0.505l-0.569 1.189c-0.543-0.252-1.044-0.378-1.504-0.378-0.289 0-0.525 0.077-0.71 0.23s-0.276 0.355-0.276 0.607c0 0.207 0.058 0.389 0.172 0.543s0.372 0.36 0.773 0.615c0.421 0.272 0.736 0.572 0.945 0.9s0.313 0.712 0.313 1.151zM33.969 14.618h-1.597l0.7-3.22h-2.46l-0.7 3.22h-1.592l1.613-7.46h1.597l-0.632 2.924h2.459l0.632-2.924h1.592l-1.613 7.46zM46.319 9.831c0 0.963-0.172 1.824-0.517 2.585s-0.816 1.334-1.415 1.722c-0.598 0.388-1.288 0.582-2.067 0.582-0.891 0-1.587-0.251-2.086-0.753s-0.749-1.198-0.749-2.090c0-0.902 0.172-1.731 0.517-2.488s0.82-1.338 1.425-1.743c0.605-0.405 1.306-0.607 2.099-0.607 0.888 0 1.575 0.245 2.063 0.735s0.73 1.176 0.73 2.056zM43.395 8.356c-0.421 0-0.808 0.155-1.159 0.467s-0.627 0.739-0.828 1.283-0.3 1.135-0.3 1.771c0 0.5 0.116 0.877 0.348 1.133s0.558 0.383 0.979 0.383 0.805-0.148 1.151-0.444c0.346-0.296 0.617-0.714 0.812-1.255s0.292-1.148 0.292-1.822c0-0.483-0.113-0.856-0.339-1.12-0.227-0.264-0.546-0.396-0.957-0.396zM53.427 14.618h-1.786l-1.859-5.644h-0.031l-0.021 0.163c-0.111 0.735-0.227 1.391-0.344 1.97l-0.757 3.511h-1.436l1.613-7.46h1.864l1.775 5.496h0.021c0.042-0.259 0.109-0.628 0.203-1.107s0.407-1.942 0.94-4.388h1.43l-1.613 7.461zM13.296 20.185c0 0.98-0.177 1.832-0.532 2.556s-0.868 1.274-1.539 1.652c-0.672 0.379-1.464 0.568-2.376 0.568h-2.449l1.678-7.68h2.15c0.977 0 1.733 0.25 2.267 0.751s0.801 1.219 0.801 2.154zM8.925 23.615c0.536 0 1.003-0.133 1.401-0.399s0.71-0.657 0.934-1.174c0.225-0.517 0.337-1.108 0.337-1.773 0-0.54-0.131-0.95-0.394-1.232s-0.64-0.423-1.132-0.423h-0.624l-1.097 5.001h0.575zM18.64 24.96h-4.436l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM20.509 24.96l1.678-7.68h1.661l-1.39 6.335h2.78l-0.294 1.345h-4.436zM26.547 24.96l1.694-7.68h1.656l-1.694 7.68h-1.656zM33.021 23.389c0.282-0.774 0.481-1.27 0.597-1.487l2.346-4.623h1.716l-4.061 7.68h-1.814l-0.689-7.68h1.602l0.277 4.623c0.015 0.157 0.022 0.39 0.022 0.699-0.007 0.361-0.018 0.623-0.033 0.788h0.038zM41.678 24.96h-4.437l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM45.849 22.013l-0.646 2.947h-1.656l1.678-7.68h1.949c0.858 0 1.502 0.179 1.933 0.536s0.646 0.881 0.646 1.571c0 0.554-0.15 1.029-0.451 1.426s-0.733 0.692-1.298 0.885l1.417 3.263h-1.803l-1.124-2.947h-0.646zM46.137 20.689h0.424c0.474 0 0.843-0.1 1.108-0.3s0.396-0.504 0.396-0.914c0-0.287-0.086-0.502-0.258-0.646s-0.442-0.216-0.812-0.216h-0.402l-0.456 2.076zM53.712 20.39l2.031-3.11h1.857l-3.355 4.744-0.646 2.936h-1.645l0.646-2.936-1.281-4.744h1.694l0.7 3.11z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                                viewbox="0 0 64 32">
                                                <path
                                                    d="M8.498 23.915h-1.588l1.322-5.127h-1.832l0.286-1.099h5.259l-0.287 1.099h-1.837l-1.323 5.127zM13.935 21.526l-0.62 2.389h-1.588l1.608-6.226h1.869c0.822 0 1.44 0.145 1.853 0.435 0.412 0.289 0.62 0.714 0.62 1.273 0 0.449-0.145 0.834-0.432 1.156-0.289 0.322-0.703 0.561-1.245 0.717l1.359 2.645h-1.729l-1.077-2.389h-0.619zM14.21 20.452h0.406c0.454 0 0.809-0.081 1.062-0.243s0.38-0.409 0.38-0.741c0-0.233-0.083-0.407-0.248-0.523s-0.424-0.175-0.778-0.175h-0.385l-0.438 1.682zM22.593 22.433h-2.462l-0.895 1.482h-1.666l3.987-6.252h1.942l0.765 6.252h-1.546l-0.125-1.482zM22.515 21.326l-0.134-1.491c-0.035-0.372-0.052-0.731-0.052-1.077v-0.154c-0.153 0.34-0.342 0.701-0.567 1.081l-0.979 1.64h1.732zM31.663 23.915h-1.78l-1.853-4.71h-0.032l-0.021 0.136c-0.111 0.613-0.226 1.161-0.343 1.643l-0.755 2.93h-1.432l1.608-6.226h1.859l1.77 4.586h0.021c0.042-0.215 0.109-0.524 0.204-0.924s0.406-1.621 0.937-3.662h1.427l-1.609 6.225zM38.412 22.075c0 0.593-0.257 1.062-0.771 1.407s-1.21 0.517-2.088 0.517c-0.768 0-1.386-0.128-1.853-0.383v-1.167c0.669 0.307 1.291 0.46 1.863 0.46 0.389 0 0.693-0.060 0.911-0.181s0.328-0.285 0.328-0.495c0-0.122-0.024-0.229-0.071-0.322s-0.114-0.178-0.2-0.257c-0.088-0.079-0.303-0.224-0.646-0.435-0.479-0.28-0.817-0.559-1.011-0.835-0.195-0.275-0.292-0.572-0.292-0.89 0-0.366 0.108-0.693 0.323-0.982 0.214-0.288 0.522-0.512 0.918-0.673 0.398-0.16 0.854-0.24 1.372-0.24 0.753 0 1.442 0.14 2.067 0.421l-0.567 0.993c-0.541-0.21-1.041-0.316-1.499-0.316-0.289 0-0.525 0.064-0.708 0.192-0.185 0.128-0.276 0.297-0.276 0.506 0 0.173 0.057 0.325 0.172 0.454 0.114 0.129 0.371 0.3 0.771 0.513 0.419 0.227 0.733 0.477 0.942 0.752 0.21 0.273 0.314 0.593 0.314 0.959zM41.266 23.915h-1.588l1.608-6.226h4.238l-0.281 1.082h-2.645l-0.412 1.606h2.463l-0.292 1.077h-2.463l-0.63 2.461zM49.857 23.915h-4.253l1.608-6.226h4.259l-0.281 1.082h-2.666l-0.349 1.367h2.484l-0.286 1.081h-2.484l-0.417 1.606h2.666l-0.28 1.091zM53.857 21.526l-0.62 2.389h-1.588l1.608-6.226h1.869c0.822 0 1.44 0.145 1.853 0.435s0.62 0.714 0.62 1.273c0 0.449-0.145 0.834-0.432 1.156-0.289 0.322-0.703 0.561-1.245 0.717l1.359 2.645h-1.729l-1.077-2.389h-0.619zM54.133 20.452h0.406c0.454 0 0.809-0.081 1.062-0.243s0.38-0.409 0.38-0.741c0-0.233-0.083-0.407-0.248-0.523s-0.424-0.175-0.778-0.175h-0.385l-0.438 1.682zM30.072 8.026c0.796 0 1.397 0.118 1.804 0.355s0.61 0.591 0.61 1.061c0 0.436-0.144 0.796-0.433 1.080-0.289 0.283-0.699 0.472-1.231 0.564v0.026c0.348 0.076 0.625 0.216 0.831 0.421 0.207 0.205 0.31 0.467 0.31 0.787 0 0.666-0.266 1.179-0.797 1.539s-1.267 0.541-2.206 0.541h-2.72l1.611-6.374h2.221zM28.111 13.284h0.938c0.406 0 0.726-0.084 0.957-0.253s0.347-0.403 0.347-0.701c0-0.471-0.317-0.707-0.954-0.707h-0.86l-0.428 1.661zM28.805 10.55h0.776c0.421 0 0.736-0.071 0.946-0.212s0.316-0.344 0.316-0.608c0-0.398-0.296-0.598-0.886-0.598h-0.792l-0.36 1.418zM37.242 12.883h-2.466l-0.897 1.517h-1.669l3.993-6.4h1.945l0.766 6.4h-1.548l-0.125-1.517zM37.163 11.749l-0.135-1.526c-0.035-0.381-0.053-0.748-0.053-1.103v-0.157c-0.153 0.349-0.342 0.718-0.568 1.107l-0.98 1.679h1.736zM46.325 14.4h-1.782l-1.856-4.822h-0.032l-0.021 0.14c-0.111 0.628-0.226 1.188-0.344 1.683l-0.756 3h-1.434l1.611-6.374h1.861l1.773 4.695h0.021c0.042-0.22 0.11-0.536 0.203-0.946s0.406-1.66 0.938-3.749h1.428l-1.611 6.374zM54.1 14.4h-1.763l-1.099-2.581-0.652 0.305-0.568 2.276h-1.59l1.611-6.374h1.596l-0.792 3.061 0.824-0.894 2.132-2.166h1.882l-3.097 3.052 1.517 3.322zM23.040 8.64c0-0.353-0.287-0.64-0.64-0.64h-14.080c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h14.080c0.353 0 0.64-0.287 0.64-0.64v0zM19.2 11.2c0-0.353-0.287-0.64-0.64-0.64h-10.24c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h10.24c0.353 0 0.64-0.287 0.64-0.64v0zM15.36 13.76c0-0.353-0.287-0.64-0.64-0.64h-6.4c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h6.4c0.353 0 0.64-0.287 0.64-0.64v0z">
                                                </path>
                                            </svg> </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <div class="top-divider full-width"></div>
            </div>
        </div>
        <div class="header-bg-container fill">
            <div class="header-bg-image fill"></div>
            <div class="header-bg-color fill"></div>
        </div>
    </div>
</header>