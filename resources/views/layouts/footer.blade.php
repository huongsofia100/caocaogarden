<footer id="footer" class="footer-wrapper">
    <section class="section" id="section_118690245">
        <div class="bg section-bg fill bg-fill bg-loaded">
            <div class="section-bg-overlay absolute fill"></div>
        </div>
        <div class="section-content relative">
            <div class="row row-full-width align-center" id="row-966390914">
                <div id="col-1155123197" class="col medium-12 small-12 large-3">
                    <div class="col-inner">
                        <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_545611111"> <a class=""
                                href="{{ route('login') }}">
                                <div class="img-inner dark"> <img width="1020" height="319"
                                        src="{{ asset('img/logocaocao.png') }}" class="attachment-large size-large"
                                        alt="" decoding="async" loading="lazy"
                                        srcset="{{ asset('img/logonormbg.png') }}" style="border-radius: 15%"
                                        sizes="(max-width: 1020px) 100vw, 1020px" /> </div>
                            </a>
                            <style>
                                #image_545611111 {
                                    width: 80%;
                                }
                            </style>
                        </div>
                        <div class="is-divider divider clearfix"
                            style="margin-top:1em;margin-bottom:1em;max-width:60px;height:2px;background-color:rgb(212, 217, 210);">
                        </div>
                        <div id="text-3039648251" class="text">
                            <p style="text-align: justify;"><strong>Cào Cào Garden</strong> mong mang đến không
                                gian sống xanh, như là một cách để khơi nguồn cảm hứng, cải thiện chất lượng
                                tinh thần tươi và đồng thời còn mang lại tính thẩm mỹ cho không gian nội thất.
                            </p>
                            <style>
                                #text-3039648251 {
                                    font-size: 1.1rem;
                                    color: rgb(255, 255, 255);
                                }

                                #text-3039648251>* {
                                    color: rgb(255, 255, 255);
                                }
                            </style>
                        </div>
                    </div>
                </div>
                <div id="col-1819345048" class="col medium-12 small-12 large-3">
                    <div class="col-inner">
                        <div id="text-2691616324" class="text">
                            <p class="uppercase"><span style="font-family: helvetica, arial, sans-serif;"><strong><span
                                            style="color: #ffffff;">Về chúng tôi</span></strong></span></p>
                            <style>
                                #text-2691616324 {
                                    font-size: 1.25rem;
                                    color: #edba45;
                                }

                                #text-2691616324>* {
                                    color: #edba45;
                                }
                            </style>
                        </div>
                        <div class="is-divider divider clearfix"
                            style="max-width:60px;height:2px;background-color:rgb(212, 217, 210);"></div>
                        <div class="row row-collapse" id="row-1607795935">
                            <div id="col-120561016" class="col small-12 large-12">
                                <div class="col-inner">
                                    <div id="text-1330704499" class="text">
                                        <ul>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('gioithieu')}}"><span
                                                            style="color: #ffffff;">Giới
                                                            thiệu</span></a></span></li>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('lienhe')}}"><span
                                                            style="color: #ffffff;">Liên hệ</span></a></span>
                                            </li>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('chinhsachbaomat')}}"><span
                                                            style="color: #ffffff;">Chính sách bảo
                                                            mật</span></a></span></li>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('chinhsachbaohanh')}}"><span
                                                            style="color: #ffffff;">Chính sách bảo
                                                            hành</span></a></span></li>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('phuongthucthanhtoan')}}"><span
                                                            style="color: #ffffff;">Phương thức thanh
                                                            toán</span></a></span></li>
                                            <li class="bullet-arrow"><span style="font-size: 100%;"><a
                                                        href="{{route('phuongthucvanchuyen')}}"><span
                                                            style="color: #ffffff;">Phương thức vận
                                                            chuyển</span></a></span></li>
                                        </ul>
                                        <style>
                                            #text-1330704499 {
                                                font-size: 1.1rem;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="col-1841003070" class="col medium-12 small-12 large-3">
                    <div class="col-inner">
                        <div id="text-411066414" class="text">
                            <p class="uppercase"><strong><span
                                        style="font-family: helvetica, arial, sans-serif; color: #ffffff;">liên
                                        hệ</span></strong></p>
                            <style>
                                #text-411066414 {
                                    font-size: 1.25rem;
                                }
                            </style>
                        </div>
                        <div class="is-divider divider clearfix"
                            style="max-width:60px;height:2px;background-color:rgb(212, 217, 210);"></div>
                        <div id="text-2375373173" class="text">
                            <ul style="list-style-type: disc;">
                                <li class="bullet-arrow"><span style="font-size: 100%;">Hotline: 0783 655 566</span>
                                </li>
                                <li class="bullet-arrow">Email: caocaogarden@gmail.com</li>
                                <li class="bullet-arrow"><span style="font-size: 100%;">Địa chỉ: 18 Kiệt 116 Nguyễn Lộ
                                        Trạch, tổ 14, Thành phố Huế, Thừa Thiên Huế</span></li>
                            </ul>
                            <style>
                                #text-2375373173 {
                                    font-size: 1.1rem;
                                    color: rgb(255, 255, 255);
                                }

                                #text-2375373173>* {
                                    color: rgb(255, 255, 255);
                                }
                            </style>
                        </div>
                        <div class="social-icons follow-icons full-width text-center"><a
                                href="https://www.facebook.com/caocaogarden" target="_blank" data-label="Facebook"
                                rel="noopener noreferrer nofollow"
                                class="icon button circle is-outline facebook tooltip" title="Follow on Facebook"
                                aria-label="Follow on Facebook"><i class="fa-brands fa-facebook"></i></a><a
                                href="https://twitter.com/mow_garden" target="_blank" data-label="Instagram"
                                rel="noopener noreferrer nofollow"
                                class="icon button circle is-outline twitter tooltip" title="Follow on Instagram"
                                aria-label="Follow on Twitter"><i class="fa-brands fa-square-instagram"></i></a><a
                                href="https://www.linkedin.com/company/mowgarden" target="_blank"
                                rel="noopener noreferrer nofollow" data-label="LinkedIn"
                                class="icon button circle is-outline linkedin tooltip" title="Follow on LinkedIn"
                                aria-label="Follow on LinkedIn"><i class="icon-linkedin"></i></a><a
                                href="https://www.youtube.com/channel/UChZpVwEYiAl49NKRo2FdwhQ" target="_blank"
                                rel="noopener noreferrer nofollow" data-label="YouTube"
                                class="icon button circle is-outline youtube tooltip" title="Follow on YouTube"
                                aria-label="Follow on YouTube"><i class="icon-youtube"></i></a></div>
                    </div>
                </div>
                <div id="col-647170397" class="col medium-12 small-12 large-3">
                    <div class="col-inner">
                        <div id="text-3344183574" class="text">
                            <p class="uppercase"><strong><span
                                        style="font-family: helvetica, arial, sans-serif; color: #ffffff;">mạng
                                        xã hội</span></strong></p>
                            <style>
                                #text-3344183574 {
                                    font-size: 1.25rem;
                                    color: #edba45;
                                }

                                #text-3344183574>* {
                                    color: #edba45;
                                }
                            </style>
                        </div>
                        <div class="is-divider divider clearfix"
                            style="max-width:60px;height:2px;background-color:rgb(212, 217, 210);"></div>
                        <p>
                        <div class="fb-page" data-href="https://www.facebook.com/caocaogarden" data-tabs="timeline"
                            data-width="" data-height="320" data-small-header="false"
                            data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="https://www.facebook.com/caocaogarden" class="fb-xfbml-parse-ignore"><a
                                    href="https://www.facebook.com/caocaogarden">Cào Cào Garden</a></blockquote>
                        </div>
                        </p>
                    </div>
                </div>
                <style>
                    #row-966390914>.col>.col-inner {
                        padding: 15px 20px 0px 20px;
                    }
                </style>
            </div>
            <div class="row row-collapse hide-for-medium" style="max-width:" id="row-986296642">
                <div id="col-1604230778" class="col small-12 large-12">
                    {{-- <div class="col-inner">
                        <p style="text-align: center;"><strong><span
                                    style="font-size: 140%; color: #ffffff; font-family: helvetica, arial, sans-serif;">Hướng
                                    dẫn tới vườn Cào Cào Garden</span></strong></p>
                        <p>
                        <div class="mapouter">
                            <div class="gmap_canvas"><iframe
                                    src="https://maps.google.com/maps?q=c%C3%A0o%20c%C3%A0o&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                                    frameborder="0" scrolling="no" style="width: 1080px; height: 360px;"></iframe>
                                <style>
                                    .mapouter {
                                        position: relative;
                                        height: 360px;
                                        width: 1080px;
                                        background: #fff;
                                    }

                                    .maprouter a {
                                        color: #fff !important;
                                        position: absolute !important;
                                        top: 0 !important;
                                        z-index: 0 !important;
                                    }
                                </style><a href="https://blooketjoin.org/">blooket</a>
                                <style>
                                    .gmap_canvas {
                                        overflow: hidden;
                                        height: 360px;
                                        width: 1080px
                                    }

                                    .gmap_canvas iframe {
                                        position: relative;
                                        z-index: 2
                                    }
                                </style>
                            </div>
                        </div>
                        </p>
                    </div> --}}
                </div>
            </div>
        </div>
        <style>
            #section_118690245 {
                padding-top: 30px;
                padding-bottom: 30px;
            }

            #section_118690245 .section-bg-overlay {
                background-color: rgba(34, 66, 41, 0.401);
            }

            #section_118690245 .section-bg.bg-loaded {
                background-image: url(https://mowgarden.com/wp-content/uploads/2022/11/vuon-cay-canh-noi-that.jpg);
            }

            #section_118690245 .section-bg {
                background-position: 50% 50%;
            }

            #section_118690245 .ux-shape-divider--top svg {
                height: 150px;
                --divider-top-width: 100%;
            }

            #section_118690245 .ux-shape-divider--bottom svg {
                height: 150px;
                --divider-width: 100%;
            }
        </style>
    </section>
    <div class="absolute-footer dark medium-text-center text-center">
        <div class="container clearfix">
            <div class="footer-primary pull-left">
                <div class="copyright-footer"> Copyright 2023 © <strong>CAOCAOGARDEN</strong> </div>
            </div>
        </div>
    </div> <a href="#top"
        class="back-to-top button icon invert plain fixed bottom z-1 is-outline left hide-for-medium circle"
        id="top-link"><i class="icon-angle-up"></i></a>
</footer>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v18.0"
    nonce="40ZuQODF"></script>
