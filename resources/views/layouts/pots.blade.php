<div class="row row-full-width align-middle align-center" id="row-167517490">
    <div id="col-1543724223" class="col small-12 large-12">
        <div class="col-inner text-center">
            <div id="text-2575823144" class="text">
                <h3><span style="font-size: 150%;">CHẬU XI MĂNG ĐÁ MÀI</span></h3>
                <style>
                    #text-2575823144 {
                        text-align: center;
                    }
                </style>
            </div>
            <div id="gap-1499799174" class="gap-element clearfix"
                style="display:block; height:auto;">
                <style>
                    #gap-1499799174 {
                        padding-top: 15px;
                    }
                </style>
            </div>
            <div
                class="row large-columns-5 medium-columns-3 small-columns-2 row-small row-full-width has-shadow row-box-shadow-2-hover">
                <div
                    class="product-small col has-hover product type-product post-14289 status-publish instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-variable">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                        aria-label="Chậu đá mài Granito dáng trụ vót màu trắng XMDM018">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/chau-da-mai-vot-day-3-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau da mai vot day 3"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-remy-5-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang da mai remy 5"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14289 wishlist-fragment on-first-load"
                                                data-fragment-ref="14289"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14289,&quot;parent_product_id&quot;:14289,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14289&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14289"
                                                        data-product-type="variable"
                                                        data-original-product-id="14289"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                        data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                        data-product_id="14289" data-product_sku="XMDM018"
                                        aria-label="Lựa chọn cho &ldquo;Chậu đá mài Granito dáng trụ vót màu trắng XMDM018&rdquo;"
                                        aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                        rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Lựa chọn các tùy chọn"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14289"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            đá mài Granito dáng trụ vót màu trắng XMDM018</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span>
                                        &ndash; <span
                                            class="woocommerce-Price-amount amount"><bdi>420.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-14000 status-publish last instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-hinh-tru-vuong-van-soc-ngang-mau-den-xmdm017/"
                                        aria-label="Chậu xi măng hình trụ vuông vân sọc ngang màu đen XMDM017">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/chau-xi-mang-hinh-tru-son-den-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang hinh tru son den 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/04/chau-xi-mang-hinh-tru-son-den-2-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang hinh tru son den 2"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14000 wishlist-fragment on-first-load"
                                                data-fragment-ref="14000"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14000,&quot;parent_product_id&quot;:14000,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=14000&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="14000"
                                                        data-product-type="simple"
                                                        data-original-product-id="14000"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=14000" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="14000" data-product_sku="XMDM017"
                                        aria-label="Thêm &ldquo;Chậu xi măng hình trụ vuông vân sọc ngang màu đen XMDM017&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="14000"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-tru-vuong-van-soc-ngang-mau-den-xmdm017/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng hình trụ vuông vân sọc ngang màu đen XMDM017</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>500.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13886 status-publish first instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-variable">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                        aria-label="Chậu đá mài Granito cao cấp dáng Remy màu trắng XMDM015">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-granito-dang-remy-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai granito dang remy"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-remy-5-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang da mai remy 5"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13886 wishlist-fragment on-first-load"
                                                data-fragment-ref="13886"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13886,&quot;parent_product_id&quot;:13886,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13886&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13886"
                                                        data-product-type="variable"
                                                        data-original-product-id="13886"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                        data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                        data-product_id="13886" data-product_sku="XMDM015"
                                        aria-label="Lựa chọn cho &ldquo;Chậu đá mài Granito cao cấp dáng Remy màu trắng XMDM015&rdquo;"
                                        aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                        rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Lựa chọn các tùy chọn"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13886"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            đá mài Granito cao cấp dáng Remy màu trắng XMDM015</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>360.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span>
                                        &ndash; <span
                                            class="woocommerce-Price-amount amount"><bdi>560.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-13361 status-publish instock product_cat-chau-xi-mang has-post-thumbnail shipping-taxable purchasable product-type-variable">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                        aria-label="Chậu xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-van-quan-roi-1-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai van quan roi 1"><img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-van-quan-roi-4-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang da mai van quan roi 4"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13361 wishlist-fragment on-first-load"
                                                data-fragment-ref="13361"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13361,&quot;parent_product_id&quot;:13361,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=13361&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="13361"
                                                        data-product-type="variable"
                                                        data-original-product-id="13361"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                        data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                        data-product_id="13361" data-product_sku="XMDM014"
                                        aria-label="Lựa chọn cho &ldquo;Chậu xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014&rdquo;"
                                        aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                        rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Lựa chọn các tùy chọn"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="13361"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>220.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span>
                                        &ndash; <span
                                            class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12489 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay-kieu-tru-dung has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-ve-zigzac-xmdm013/"
                                        aria-label="Chậu xi măng đá mài trụ tròn vẽ zigzac XMDM013">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/chau-xi-mang-da-mai-ve-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai ve"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12489 wishlist-fragment on-first-load"
                                                data-fragment-ref="12489"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12489,&quot;parent_product_id&quot;:12489,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12489&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12489"
                                                        data-product-type="simple"
                                                        data-original-product-id="12489"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12489" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12489" data-product_sku="XMDM013"
                                        aria-label="Thêm &ldquo;Chậu xi măng đá mài trụ tròn vẽ zigzac XMDM013&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12489"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-ve-zigzac-xmdm013/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng đá mài trụ tròn vẽ zigzac XMDM013</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-1608 status-publish last instock product_cat-chau-xi-mang product_cat-chau-cay-kieu-tru-dung has-post-thumbnail shipping-taxable purchasable product-type-variable">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                        aria-label="Chậu xi măng đá mài trụ tròn dáng thấp XMDM012">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2021/04/chau-xi-mang-da-mai-tru-tron-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai tru tron"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2021/04/chau-xi-mang-da-mai-tron-ong-thap-5-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang da mai tron ong thap 5"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-1608 wishlist-fragment on-first-load"
                                                data-fragment-ref="1608"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:1608,&quot;parent_product_id&quot;:1608,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=1608&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="1608"
                                                        data-product-type="variable"
                                                        data-original-product-id="1608"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                        data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                        data-product_id="1608" data-product_sku="XMDM012"
                                        aria-label="Lựa chọn cho &ldquo;Chậu xi măng đá mài trụ tròn dáng thấp XMDM012&rdquo;"
                                        aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                        rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Lựa chọn các tùy chọn"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="1608"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng đá mài trụ tròn dáng thấp XMDM012</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span>
                                        &ndash; <span
                                            class="woocommerce-Price-amount amount"><bdi>150.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12453 status-publish first instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-decor-32x52cm-xmdm011/"
                                        aria-label="Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM011">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/12/chau-xi-mang-dai-mai-son-hoa-tiet-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang dai mai son hoa tiet"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12453 wishlist-fragment on-first-load"
                                                data-fragment-ref="12453"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12453,&quot;parent_product_id&quot;:12453,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12453&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12453"
                                                        data-product-type="simple"
                                                        data-original-product-id="12453"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12453" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12453" data-product_sku="XMDM011"
                                        aria-label="Thêm &ldquo;Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM011&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12453"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-decor-32x52cm-xmdm011/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng hình giọt nước sơn decor 32x52cm XMDM011</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>440.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12438 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-trang-tri-32x52cm-xmdm010/"
                                        aria-label="Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM010">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-son-mau-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang son mau"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12438 wishlist-fragment on-first-load"
                                                data-fragment-ref="12438"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12438,&quot;parent_product_id&quot;:12438,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12438&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12438"
                                                        data-product-type="simple"
                                                        data-original-product-id="12438"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12438" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12438" data-product_sku="XMDM010"
                                        aria-label="Thêm &ldquo;Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM010&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12438"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-trang-tri-32x52cm-xmdm010/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng hình giọt nước sơn decor 32x52cm XMDM010</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>480.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12434 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-32x52cm-xmdm009/"
                                        aria-label="Chậu xi măng hình giọt nước 32x52cm XMDM009"> <img
                                            loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai giot nuoc"><img loading="lazy"
                                            decoding="async" width="800" height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-mau-den-800x960.jpg"
                                            class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                            alt="chau xi mang da mai giot nuoc mau den"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12434 wishlist-fragment on-first-load"
                                                data-fragment-ref="12434"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12434,&quot;parent_product_id&quot;:12434,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12434&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12434"
                                                        data-product-type="simple"
                                                        data-original-product-id="12434"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12434" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12434" data-product_sku="XMDM009"
                                        aria-label="Thêm &ldquo;Chậu xi măng hình giọt nước 32x52cm XMDM009&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12434"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-32x52cm-xmdm009/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng hình giọt nước 32x52cm XMDM009</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="product-small col has-hover product type-product post-12431 status-publish last instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                    <div class="col-inner">
                        <div class="badge-container absolute left top z-1"> </div>
                        <div class="product-small box">
                            <div class="box-image">
                                <div class="image-zoom_in"> <a
                                        href="https://mowgarden.com/chau-xi-mang-hinh-tru-son-hoa-tiet-40x40cm-xmdm008/"
                                        aria-label="Chậu xi măng hình trụ sơn họa tiết 40x40cm XMDM008">
                                        <img loading="lazy" decoding="async" width="800"
                                            height="960"
                                            src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-son-mau-800x960.jpg"
                                            class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                            alt="chau xi mang da mai son mau"> </a> </div>
                                <div class="image-tools is-small top right show-on-hover">
                                    <div class="wishlist-icon"> <button
                                            class="wishlist-button button is-outline circle icon"
                                            aria-label="Wishlist"> <i class="icon-heart"></i>
                                        </button>
                                        <div class="wishlist-popup dark">
                                            <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12431 wishlist-fragment on-first-load"
                                                data-fragment-ref="12431"
                                                data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12431,&quot;parent_product_id&quot;:12431,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                <!-- ADD TO WISHLIST -->
                                                <div class="yith-wcwl-add-button"> <a
                                                        href="?add_to_wishlist=12431&#038;_wpnonce=523ba72503"
                                                        class="add_to_wishlist single_add_to_wishlist"
                                                        data-product-id="12431"
                                                        data-product-type="simple"
                                                        data-original-product-id="12431"
                                                        data-title="Add to wishlist" rel="nofollow">
                                                        <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                        <span>Add to wishlist</span> </a> </div>
                                                <!-- COUNT TEXT -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="image-tools is-small hide-for-small bottom left show-on-hover">
                                </div>
                                <div
                                    class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                    <a href="?add-to-cart=12431" data-quantity="1"
                                        class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                        data-product_id="12431" data-product_sku="40x40cm"
                                        aria-label="Thêm &ldquo;Chậu xi măng hình trụ sơn họa tiết 40x40cm XMDM008&rdquo; vào giỏ hàng"
                                        aria-describedby="" rel="nofollow">
                                        <div class="cart-icon tooltip is-small"
                                            title="Thêm vào giỏ hàng"><strong>+</strong></div>
                                    </a> <a class="quick-view" data-prod="12431"
                                        href="#quick-view">Xem nhanh</a> </div>
                            </div>
                            <div class="box-text box-text-products">
                                <div class="title-wrapper">
                                    <p class="name product-title woocommerce-loop-product__title"><a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-tru-son-hoa-tiet-40x40cm-xmdm008/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                            xi măng hình trụ sơn họa tiết 40x40cm XMDM008</a></p>
                                </div>
                                <div class="price-wrapper"> <span class="price"><span
                                            class="woocommerce-Price-amount amount"><bdi>520.000<span
                                                    class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <a href="https://mowgarden.com/chau-xi-mang/" target="_self"
                class="button primary is-link is-large lowercase"> <span>Xem thêm</span> </a>
        </div>
    </div>
</div>