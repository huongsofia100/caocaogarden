<!DOCTYPE html>
<html lang="vi" prefix="og: https://ogp.me/ns#" class="loading-site no-js">

<head>
    <meta charset="UTF-8">
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="https://mowgarden.com/xmlrpc.php" />
    <script>
        document.documentElement.className = document.documentElement.className + ' yes-js js_active js'
    </script>
    <script>
        (function(html) {
            html.className = html.className.replace(/\bno-js\b/, 'js')
        })(document.documentElement);
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Search Engine Optimization by Rank Math PRO - https://rankmath.com/ -->
    <title>Cào Cào Garden</title>
    <meta name="description"
        content="Shop cây cảnh MOW Garden là địa chỉ bán cây cảnh, đất trồng cây, chậu cây cảnh, phân bón và phụ kiện vật tư nông nghiệp tại HCM" />
    <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large" />
    <link rel="canonical" href="https://mowgarden.com/" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="MOW Garden • Cửa hàng Bán Cây Cảnh Online Giá Rẻ tại TpHCM" />
    <meta property="og:description"
        content="Shop cây cảnh MOW Garden là địa chỉ bán cây cảnh, đất trồng cây, chậu cây cảnh, phân bón và phụ kiện vật tư nông nghiệp tại HCM" />
    <meta property="og:url" content="https://mowgarden.com/" />
    <meta property="og:site_name" content="MOW Garden" />
    <meta property="og:updated_time" content="2023-04-03T16:48:56+07:00" />
    <meta property="og:image"
        content="https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg" />
    <meta property="og:image:secure_url"
        content="https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="900" />
    <meta property="og:image:alt" content="bán cây cảnh" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="article:published_time" content="2021-07-05T23:30:33+07:00" />
    <meta property="article:modified_time" content="2023-04-03T16:48:56+07:00" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="MOW Garden • Cửa hàng Bán Cây Cảnh Online Giá Rẻ tại TpHCM" />
    <meta name="twitter:description"
        content="Shop cây cảnh MOW Garden là địa chỉ bán cây cảnh, đất trồng cây, chậu cây cảnh, phân bón và phụ kiện vật tư nông nghiệp tại HCM" />
    <meta name="twitter:site" content="@mow_garden" />
    <meta name="twitter:creator" content="@mow_garden" />
    <meta name="twitter:image"
        content="https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="phphuoc93" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="8 minutes" />
    <script type="application/ld+json" class="rank-math-schema-pro">{"@context":"https://schema.org","@graph":[{"@type":"Place","@id":"https://mowgarden.com/#place","geo":{"@type":"GeoCoordinates","latitude":"10.809595","longitude":"106.5869351"},"hasMap":"https://www.google.com/maps/search/?api=1&amp;query=10.809595,106.5869351","address":{"@type":"PostalAddress","streetAddress":"256 B\u00ecnh Th\u00e0nh","addressLocality":"B\u00ecnh H\u01b0ng H\u00f2a B, B\u00ecnh T\u00e2n","addressRegion":"Tp H\u1ed3 Ch\u00ed Minh","postalCode":"71914","addressCountry":"Vi\u1ec7t Nam"}},{"@type":["GardenStore","Organization"],"@id":"https://mowgarden.com/#organization","name":"MOW Garden","url":"https://mowgarden.com","sameAs":["http://facebook.com/mowgarden","https://twitter.com/mow_garden"],"email":"mowgardensg@gmail.com","address":{"@type":"PostalAddress","streetAddress":"256 B\u00ecnh Th\u00e0nh","addressLocality":"B\u00ecnh H\u01b0ng H\u00f2a B, B\u00ecnh T\u00e2n","addressRegion":"Tp H\u1ed3 Ch\u00ed Minh","postalCode":"71914","addressCountry":"Vi\u1ec7t Nam"},"logo":{"@type":"ImageObject","@id":"https://mowgarden.com/#logo","url":"https://mowgarden.com/wp-content/uploads/2021/03/logo-MOW-v2.png","contentUrl":"https://mowgarden.com/wp-content/uploads/2021/03/logo-MOW-v2.png","caption":"MOW Garden","inLanguage":"vi","width":"1200","height":"375"},"openingHours":["Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday 08:30-19:00"],"location":{"@id":"https://mowgarden.com/#place"},"image":{"@id":"https://mowgarden.com/#logo"},"telephone":"0899799968"},{"@type":"WebSite","@id":"https://mowgarden.com/#website","url":"https://mowgarden.com","name":"MOW Garden","publisher":{"@id":"https://mowgarden.com/#organization"},"inLanguage":"vi","potentialAction":{"@type":"SearchAction","target":"https://mowgarden.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg","url":"https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg","width":"1200","height":"900","inLanguage":"vi"},{"@type":"WebPage","@id":"https://mowgarden.com/#webpage","url":"https://mowgarden.com/","name":"MOW Garden \u2022 C\u1eeda h\u00e0ng B\u00e1n C\u00e2y C\u1ea3nh Online Gi\u00e1 R\u1ebb t\u1ea1i TpHCM","datePublished":"2021-07-05T23:30:33+07:00","dateModified":"2023-04-03T16:48:56+07:00","about":{"@id":"https://mowgarden.com/#organization"},"isPartOf":{"@id":"https://mowgarden.com/#website"},"primaryImageOfPage":{"@id":"https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg"},"inLanguage":"vi"},{"@type":"Person","@id":"https://mowgarden.com/tac-gia/phphuoc93/","name":"phphuoc93","url":"https://mowgarden.com/tac-gia/phphuoc93/","image":{"@type":"ImageObject","@id":"https://secure.gravatar.com/avatar/ca83afe8df34e155d2dd617307c0d220?s=96&amp;d=mm&amp;r=g","url":"https://secure.gravatar.com/avatar/ca83afe8df34e155d2dd617307c0d220?s=96&amp;d=mm&amp;r=g","caption":"phphuoc93","inLanguage":"vi"},"worksFor":{"@id":"https://mowgarden.com/#organization"}},{"@type":"Article","headline":"MOW Garden \u2022 C\u1eeda h\u00e0ng B\u00e1n C\u00e2y C\u1ea3nh Online Gi\u00e1 R\u1ebb t\u1ea1i TpHCM","keywords":"b\u00e1n c\u00e2y c\u1ea3nh","datePublished":"2021-07-05T23:30:33+07:00","dateModified":"2023-04-03T16:48:56+07:00","author":{"@id":"https://mowgarden.com/tac-gia/phphuoc93/","name":"phphuoc93"},"publisher":{"@id":"https://mowgarden.com/#organization"},"description":"Shop c\u00e2y c\u1ea3nh MOW Garden l\u00e0 \u0111\u1ecba ch\u1ec9 b\u00e1n c\u00e2y c\u1ea3nh, \u0111\u1ea5t tr\u1ed3ng c\u00e2y, ch\u1eadu c\u00e2y c\u1ea3nh, ph\u00e2n b\u00f3n v\u00e0 ph\u1ee5 ki\u1ec7n v\u1eadt t\u01b0 n\u00f4ng nghi\u1ec7p t\u1ea1i HCM","name":"MOW Garden \u2022 C\u1eeda h\u00e0ng B\u00e1n C\u00e2y C\u1ea3nh Online Gi\u00e1 R\u1ebb t\u1ea1i TpHCM","@id":"https://mowgarden.com/#richSnippet","isPartOf":{"@id":"https://mowgarden.com/#webpage"},"image":{"@id":"https://mowgarden.com/wp-content/uploads/2023/02/cua-hang-ban-cay-canh-MOW-Garden.jpg"},"inLanguage":"vi","mainEntityOfPage":{"@id":"https://mowgarden.com/#webpage"}}]}</script>
    <meta name="google-site-verification" content="qTBDkUELkdmZuyq9GR0bAzS6nQFAdvob7pE-DeL_PUg" />
    <meta name="p:domain_verify" content="3e9c2f09f187bdb6391c009254302461" />
    <meta name="norton-safeweb-site-verification"
        content="kzkshokl7d142qbrjypyx9z2wvv7u24s5dc6c4pywmsow82pxa3cknveqx09ijluyxwuuqm7a87yam5u4sx2ry7yis30nxsfumepuiispjvagi0rjkoqyrxk6831ixi" />
    <!-- /Rank Math WordPress SEO plugin -->
    <link rel='prefetch'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/js/chunk.countup.js?ver=3.16.8' />
    <link rel='prefetch'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/js/chunk.sticky-sidebar.js?ver=3.16.8' />
    <link rel='prefetch'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/js/chunk.tooltips.js?ver=3.16.8' />
    <link rel='prefetch'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/js/chunk.vendors-popups.js?ver=3.16.8' />
    <link rel='prefetch'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/js/chunk.vendors-slider.js?ver=3.16.8' />
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin MOW Garden &raquo;"
        href="https://mowgarden.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi MOW Garden &raquo;"
        href="https://mowgarden.com/comments/feed/" />
    <link rel='dns-prefetch' href='//use.fontawesome.com'>
    <link rel='dns-prefetch' href='//www.googletagmanager.com'>
    <link rel='dns-prefetch' href='//images.dmca.com'>
    <link rel='dns-prefetch' href='//www.facebook.com'>
    <link rel='dns-prefetch' href='//www.google.com'>
    <link rel='dns-prefetch' href='//cdn.jsdelivr.net'>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://kit.fontawesome.com/0006c1b545.js" crossorigin="anonymous"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
            "svgExt": ".svg",
            "source": {
                "concatemoji": "https:\/\/mowgarden.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.4.1"
            }
        };
        /*! This file is auto-generated */
        ! function(i, n) {
            var o, s, e;

            function c(e) {
                try {
                    var t = {
                        supportTests: e,
                        timestamp: (new Date).valueOf()
                    };
                    sessionStorage.setItem(o, JSON.stringify(t))
                } catch (e) {}
            }

            function p(e, t, n) {
                e.clearRect(0, 0, e.canvas.width, e.canvas.height), e.fillText(t, 0, 0);
                var t = new Uint32Array(e.getImageData(0, 0, e.canvas.width, e.canvas.height).data),
                    r = (e.clearRect(0, 0, e.canvas.width, e.canvas.height), e.fillText(n, 0, 0), new Uint32Array(e
                        .getImageData(0, 0, e.canvas.width, e.canvas.height).data));
                return t.every(function(e, t) {
                    return e === r[t]
                })
            }

            function u(e, t, n) {
                switch (t) {
                    case "flag":
                        return n(e, "\ud83c\udff3\ufe0f\u200d\u26a7\ufe0f", "\ud83c\udff3\ufe0f\u200b\u26a7\ufe0f") ? !1 : !
                            n(e, "\ud83c\uddfa\ud83c\uddf3", "\ud83c\uddfa\u200b\ud83c\uddf3") && !n(e,
                                "\ud83c\udff4\udb40\udc67\udb40\udc62\udb40\udc65\udb40\udc6e\udb40\udc67\udb40\udc7f",
                                "\ud83c\udff4\u200b\udb40\udc67\u200b\udb40\udc62\u200b\udb40\udc65\u200b\udb40\udc6e\u200b\udb40\udc67\u200b\udb40\udc7f"
                                );
                    case "emoji":
                        return !n(e, "\ud83e\udef1\ud83c\udffb\u200d\ud83e\udef2\ud83c\udfff",
                            "\ud83e\udef1\ud83c\udffb\u200b\ud83e\udef2\ud83c\udfff")
                }
                return !1
            }

            function f(e, t, n) {
                var r = "undefined" != typeof WorkerGlobalScope && self instanceof WorkerGlobalScope ? new OffscreenCanvas(
                        300, 150) : i.createElement("canvas"),
                    a = r.getContext("2d", {
                        willReadFrequently: !0
                    }),
                    o = (a.textBaseline = "top", a.font = "600 32px Arial", {});
                return e.forEach(function(e) {
                    o[e] = t(a, e, n)
                }), o
            }

            function t(e) {
                var t = i.createElement("script");
                t.src = e, t.defer = !0, i.head.appendChild(t)
            }
            "undefined" != typeof Promise && (o = "wpEmojiSettingsSupports", s = ["flag", "emoji"], n.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, e = new Promise(function(e) {
                i.addEventListener("DOMContentLoaded", e, {
                    once: !0
                })
            }), new Promise(function(t) {
                var n = function() {
                    try {
                        var e = JSON.parse(sessionStorage.getItem(o));
                        if ("object" == typeof e && "number" == typeof e.timestamp && (new Date).valueOf() <
                            e.timestamp + 604800 && "object" == typeof e.supportTests) return e.supportTests
                    } catch (e) {}
                    return null
                }();
                if (!n) {
                    if ("undefined" != typeof Worker && "undefined" != typeof OffscreenCanvas && "undefined" !=
                        typeof URL && URL.createObjectURL && "undefined" != typeof Blob) try {
                        var e = "postMessage(" + f.toString() + "(" + [JSON.stringify(s), u.toString(), p
                                .toString()
                            ].join(",") + "));",
                            r = new Blob([e], {
                                type: "text/javascript"
                            }),
                            a = new Worker(URL.createObjectURL(r), {
                                name: "wpTestEmojiSupports"
                            });
                        return void(a.onmessage = function(e) {
                            c(n = e.data), a.terminate(), t(n)
                        })
                    } catch (e) {}
                    c(n = f(s, u, p))
                }
                t(n)
            }).then(function(e) {
                for (var t in e) n.supports[t] = e[t], n.supports.everything = n.supports.everything && n
                    .supports[t], "flag" !== t && (n.supports.everythingExceptFlag = n.supports
                        .everythingExceptFlag && n.supports[t]);
                n.supports.everythingExceptFlag = n.supports.everythingExceptFlag && !n.supports.flag, n
                    .DOMReady = !1, n.readyCallback = function() {
                        n.DOMReady = !0
                    }
            }).then(function() {
                return e
            }).then(function() {
                var e;
                n.supports.everything || (n.readyCallback(), (e = n.source || {}).concatemoji ? t(e
                    .concatemoji) : e.wpemoji && e.twemoji && (t(e.twemoji), t(e.wpemoji)))
            }))
        }((window, document), window._wpemojiSettings);
        /* ]]> */
    </script>
    <style id='wp-emoji-styles-inline-css' type='text/css'>
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <style id='wp-block-library-inline-css' type='text/css'>
        :root {
            --wp-admin-theme-color: #007cba;
            --wp-admin-theme-color--rgb: 0, 124, 186;
            --wp-admin-theme-color-darker-10: #006ba1;
            --wp-admin-theme-color-darker-10--rgb: 0, 107, 161;
            --wp-admin-theme-color-darker-20: #005a87;
            --wp-admin-theme-color-darker-20--rgb: 0, 90, 135;
            --wp-admin-border-width-focus: 2px;
            --wp-block-synced-color: #7a00df;
            --wp-block-synced-color--rgb: 122, 0, 223
        }

        @media (min-resolution:192dpi) {
            :root {
                --wp-admin-border-width-focus: 1.5px
            }
        }

        .wp-element-button {
            cursor: pointer
        }

        :root {
            --wp--preset--font-size--normal: 16px;
            --wp--preset--font-size--huge: 42px
        }

        :root .has-very-light-gray-background-color {
            background-color: #eee
        }

        :root .has-very-dark-gray-background-color {
            background-color: #313131
        }

        :root .has-very-light-gray-color {
            color: #eee
        }

        :root .has-very-dark-gray-color {
            color: #313131
        }

        :root .has-vivid-green-cyan-to-vivid-cyan-blue-gradient-background {
            background: linear-gradient(135deg, #00d084, #0693e3)
        }

        :root .has-purple-crush-gradient-background {
            background: linear-gradient(135deg, #34e2e4, #4721fb 50%, #ab1dfe)
        }

        :root .has-hazy-dawn-gradient-background {
            background: linear-gradient(135deg, #faaca8, #dad0ec)
        }

        :root .has-subdued-olive-gradient-background {
            background: linear-gradient(135deg, #fafae1, #67a671)
        }

        :root .has-atomic-cream-gradient-background {
            background: linear-gradient(135deg, #fdd79a, #004a59)
        }

        :root .has-nightshade-gradient-background {
            background: linear-gradient(135deg, #330968, #31cdcf)
        }

        :root .has-midnight-gradient-background {
            background: linear-gradient(135deg, #020381, #2874fc)
        }

        .has-regular-font-size {
            font-size: 1em
        }

        .has-larger-font-size {
            font-size: 2.625em
        }

        .has-normal-font-size {
            font-size: var(--wp--preset--font-size--normal)
        }

        .has-huge-font-size {
            font-size: var(--wp--preset--font-size--huge)
        }

        .has-text-align-center {
            text-align: center
        }

        .has-text-align-left {
            text-align: left
        }

        .has-text-align-right {
            text-align: right
        }

        #end-resizable-editor-section {
            display: none
        }

        .aligncenter {
            clear: both
        }

        .items-justified-left {
            justify-content: flex-start
        }

        .items-justified-center {
            justify-content: center
        }

        .items-justified-right {
            justify-content: flex-end
        }

        .items-justified-space-between {
            justify-content: space-between
        }

        .screen-reader-text {
            clip: rect(1px, 1px, 1px, 1px);
            word-wrap: normal !important;
            border: 0;
            -webkit-clip-path: inset(50%);
            clip-path: inset(50%);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px
        }

        .screen-reader-text:focus {
            clip: auto !important;
            background-color: #ddd;
            -webkit-clip-path: none;
            clip-path: none;
            color: #444;
            display: block;
            font-size: 1em;
            height: auto;
            left: 5px;
            line-height: normal;
            padding: 15px 23px 14px;
            text-decoration: none;
            top: 5px;
            width: auto;
            z-index: 100000
        }

        html :where(.has-border-color) {
            border-style: solid
        }

        html :where([style*=border-top-color]) {
            border-top-style: solid
        }

        html :where([style*=border-right-color]) {
            border-right-style: solid
        }

        html :where([style*=border-bottom-color]) {
            border-bottom-style: solid
        }

        html :where([style*=border-left-color]) {
            border-left-style: solid
        }

        html :where([style*=border-width]) {
            border-style: solid
        }

        html :where([style*=border-top-width]) {
            border-top-style: solid
        }

        html :where([style*=border-right-width]) {
            border-right-style: solid
        }

        html :where([style*=border-bottom-width]) {
            border-bottom-style: solid
        }

        html :where([style*=border-left-width]) {
            border-left-style: solid
        }

        html :where(img[class*=wp-image-]) {
            height: auto;
            max-width: 100%
        }

        :where(figure) {
            margin: 0 0 1em
        }

        html :where(.is-position-sticky) {
            --wp-admin--admin-bar--position-offset: var(--wp-admin--admin-bar--height, 0px)
        }

        @media screen and (max-width:600px) {
            html :where(.is-position-sticky) {
                --wp-admin--admin-bar--position-offset: 0px
            }
        }
    </style>
    <style id='classic-theme-styles-inline-css' type='text/css'>
        /*! This file is auto-generated */
        .wp-block-button__link {
            color: #fff;
            background-color: #32373c;
            border-radius: 9999px;
            box-shadow: none;
            text-decoration: none;
            padding: calc(.667em + 2px) calc(1.333em + 2px);
            font-size: 1.125em
        }

        .wp-block-file__button {
            background: #32373c;
            color: #fff;
            text-decoration: none
        }
    </style>
    <link rel='stylesheet' id='contact-form-7-css'
        href='https://mowgarden.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.7.7'
        type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <link rel='stylesheet' id='contactus.css-css'
        href='https://mowgarden.com/wp-content/plugins/ar-contactus/res/css/contactus.min.css?ver=2.2.6'
        type='text/css' media='all' />
    <link rel='stylesheet' id='contactus.generated.desktop.css-css'
        href='https://mowgarden.com/wp-content/plugins/ar-contactus/res/css/generated-desktop.css?ver=1700104756'
        type='text/css' media='all' />
    <link rel='stylesheet' id='contactus.fa.css-css'
        href='https://use.fontawesome.com/releases/v5.8.1/css/all.css?ver=2.2.6' type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-woocommerce-wishlist-css'
        href='https://mowgarden.com/wp-content/themes/flatsome/inc/integrations/wc-yith-wishlist/wishlist.css?ver=3.10.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-swatches-frontend-css'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/css/extensions/flatsome-swatches-frontend.css?ver=3.16.8'
        type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-main-css'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/css/flatsome.css?ver=3.16.8' type='text/css'
        media='all' />
    <style id='flatsome-main-inline-css' type='text/css'>
        @font-face {
            font-family: "fl-icons";
            font-display: block;
            src: url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.eot?v=3.16.8);
            src: url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.eot#iefix?v=3.16.8) format("embedded-opentype"), url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.woff2?v=3.16.8) format("woff2"), url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.ttf?v=3.16.8) format("truetype"), url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.woff?v=3.16.8) format("woff"), url(https://mowgarden.com/wp-content/themes/flatsome/assets/css/icons/fl-icons.svg?v=3.16.8#fl-icons) format("svg");
        }
    </style>
    <link rel='stylesheet' id='flatsome-shop-css'
        href='https://mowgarden.com/wp-content/themes/flatsome/assets/css/flatsome-shop.css?ver=3.16.8'
        type='text/css' media='all' />
    <link rel='stylesheet' id='flatsome-style-css'
        href='https://mowgarden.com/wp-content/themes/flatsome-child/style.css?ver=3.0' type='text/css'
        media='all' />
    <script type="text/javascript">
        window._nslDOMReady = function(callback) {
            if (document.readyState === "complete" || document.readyState === "interactive") {
                callback();
            } else {
                document.addEventListener("DOMContentLoaded", callback);
            }
        };
    </script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/jquery/jquery.min.js?ver=3.7.1"
        id="jquery-core-js"></script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.4.1"
        id="jquery-migrate-js"></script>
    <script type="text/javascript" id="contactus-js-extra">
        /* <![CDATA[ */
        /*swift-is-localization*/
        var arCUVars = {
            "url": "https:\/\/mowgarden.com\/wp-admin\/admin-ajax.php",
            "version": "2.2.6",
            "_wpnonce": "<input type=\"hidden\" id=\"_wpnonce\" name=\"_wpnonce\" value=\"17c1f16d86\" \/><input type=\"hidden\" name=\"_wp_http_referer\" value=\"\/\" \/>"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/ar-contactus/res/js/contactus.min.js?ver=2.2.6" id="contactus-js">
    </script>
    <script type="text/javascript" src="https://mowgarden.com/wp-content/plugins/ar-contactus/res/js/scripts.js?ver=2.2.6"
        id="contactus.scripts-js"></script>
    <link rel="https://api.w.org/" href="https://mowgarden.com/wp-json/" />
    <link rel="alternate" type="application/json" href="https://mowgarden.com/wp-json/wp/v2/pages/3762" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://mowgarden.com/xmlrpc.php?rsd" />
    <meta name="generator" content="WordPress 6.4.1" />
    <link rel='shortlink' href='https://mowgarden.com/' />
    <link rel="alternate" type="application/json+oembed"
        href="https://mowgarden.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmowgarden.com%2F" />
    <link rel="alternate" type="text/xml+oembed"
        href="https://mowgarden.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmowgarden.com%2F&#038;format=xml" />
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MRDFS37C');
    </script> <!-- End Google Tag Manager -->
    <link rel="shortcut icon" type="image/png"
        href="https://mowgarden.com/wp-content/themes/flatsome/assets/img/favicon-ico.ico" />
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-594S45J');
    </script> <!-- End Google Tag Manager -->
    <meta name="facebook-domain-verification" content="u8xtoxd80ro70xs0s06in0lw5e0ghb" /> <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <link rel="icon" href="https://mowgarden.com/wp-content/uploads/2022/08/cropped-logo-favicon-mow-32x32.png"
        sizes="32x32" />
    <link rel="icon" href="https://mowgarden.com/wp-content/uploads/2022/08/cropped-logo-favicon-mow-192x192.png"
        sizes="192x192" />
    <link rel="apple-touch-icon"
        href="https://mowgarden.com/wp-content/uploads/2022/08/cropped-logo-favicon-mow-180x180.png" />
    <meta name="msapplication-TileImage"
        content="https://mowgarden.com/wp-content/uploads/2022/08/cropped-logo-favicon-mow-270x270.png" />
    <style id="custom-css" type="text/css">
        :root {
            --primary-color: #214738;
        }

        .sticky-add-to-cart--active,
        #wrapper,
        #main,
        #main.dark {
            background-color: #fcf9f3
        }

        .header-main {
            height: 103px
        }

        #logo img {
            max-height: 103px
        }

        #logo {
            width: 200px;
        }

        .stuck #logo img {
            padding: 8px 0;
        }

        .header-bottom {
            min-height: 10px
        }

        .header-top {
            min-height: 40px
        }

        .transparent .header-main {
            height: 90px
        }

        .transparent #logo img {
            max-height: 90px
        }

        .has-transparent+.page-title:first-of-type,
        .has-transparent+#main>.page-title,
        .has-transparent+#main>div>.page-title,
        .has-transparent+#main .page-header-wrapper:first-of-type .page-title {
            padding-top: 120px;
        }

        .header.show-on-scroll,
        .stuck .header-main {
            height: 92px !important
        }

        .stuck #logo img {
            max-height: 92px !important
        }

        .search-form {
            width: 100%;
        }

        .header-bg-color {
            background-color: #fcf9f3
        }

        .header-bottom {
            background-color: #f1f1f1
        }

        .top-bar-nav>li>a {
            line-height: 16px
        }

        .header-main .nav>li>a {
            line-height: 16px
        }

        .stuck .header-main .nav>li>a {
            line-height: 50px
        }

        .header-bottom-nav>li>a {
            line-height: 16px
        }

        @media (max-width: 549px) {
            .header-main {
                height: 70px
            }

            #logo img {
                max-height: 70px
            }
        }

        .nav-dropdown {
            font-size: 100%
        }

        /* Color */
        .accordion-title.active,
        .has-icon-bg .icon .icon-inner,
        .logo a,
        .primary.is-underline,
        .primary.is-link,
        .badge-outline .badge-inner,
        .nav-outline>li.active>a,
        .nav-outline>li.active>a,
        .cart-icon strong,[data-color='primary'],
        .is-outline.primary {
            color: #214738;
        }

        /* Color !important */[data-text-color="primary"]{color: #214738!important;}/* Background Color */[data-text-bg="primary"]{background-color: #214738;}/* Background */
        .scroll-to-bullets a,
        .featured-title,
        .label-new.menu-item>a:after,
        .nav-pagination>li>.current,
        .nav-pagination>li>span:hover,
        .nav-pagination>li>a:hover,
        .has-hover:hover .badge-outline .badge-inner,button[type="submit"],
        .button.wc-forward:not(.checkout):not(.checkout-button),
        .button.submit-button,
        .button.primary:not(.is-outline),
        .featured-table .title,
        .is-outline:hover,
        .has-icon:hover .icon-label,
        .nav-dropdown-bold .nav-column li>a:hover,
        .nav-dropdown.nav-dropdown-bold>li>a:hover,
        .nav-dropdown-bold.dark .nav-column li>a:hover,
        .nav-dropdown.nav-dropdown-bold.dark>li>a:hover,
        .header-vertical-menu__opener,
        .is-outline:hover,
        .tagcloud a:hover,
        .grid-tools a,
        input[type='submit']:not(.is-form),
        .box-badge:hover .box-text,
        input.button.alt,
        .nav-box>li>a:hover,
        .nav-box>li.active>a,
        .nav-pills>li.active>a,
        .current-dropdown .cart-icon strong,
        .cart-icon:hover strong,
        .nav-line-bottom>li>a:before,
        .nav-line-grow>li>a:before,
        .nav-line>li>a:before,
        .banner,
        .header-top,
        .slider-nav-circle .flickity-prev-next-button:hover svg,
        .slider-nav-circle .flickity-prev-next-button:hover .arrow,
        .primary.is-outline:hover,
        .button.primary:not(.is-outline),
        input[type='submit'].primary,
        input[type='submit'].primary,
        input[type='reset'].button,
        input[type='button'].primary,
        .badge-inner {
            background-color: #214738;
        }

        /* Border */
        .nav-vertical.nav-tabs>li.active>a,
        .scroll-to-bullets a.active,
        .nav-pagination>li>.current,
        .nav-pagination>li>span:hover,
        .nav-pagination>li>a:hover,
        .has-hover:hover .badge-outline .badge-inner,
        .accordion-title.active,
        .featured-table,
        .is-outline:hover,
        .tagcloud a:hover,
        blockquote,
        .has-border,
        .cart-icon strong:after,
        .cart-icon strong,
        .blockUI:before,
        .processing:before,
        .loading-spin,
        .slider-nav-circle .flickity-prev-next-button:hover svg,
        .slider-nav-circle .flickity-prev-next-button:hover .arrow,
        .primary.is-outline:hover {
            border-color: #214738
        }

        .nav-tabs>li.active>a {
            border-top-color: #214738
        }

        .widget_shopping_cart_content .blockUI.blockOverlay:before {
            border-left-color: #214738
        }

        .woocommerce-checkout-review-order .blockUI.blockOverlay:before {
            border-left-color: #214738
        }

        /* Fill */
        .slider .flickity-prev-next-button:hover svg,
        .slider .flickity-prev-next-button:hover .arrow {
            fill: #214738;
        }

        /* Focus */
        .primary:focus-visible,
        .submit-button:focus-visible,
        button[type="submit"]:focus-visible {
            outline-color: #214738 !important;
        }

        /* Background Color */
        [data-icon-label]:after,
        .secondary.is-underline:hover,
        .secondary.is-outline:hover,
        .icon-label,
        .button.secondary:not(.is-outline),
        .button.alt:not(.is-outline),
        .badge-inner.on-sale,
        .button.checkout,
        .single_add_to_cart_button,
        .current .breadcrumb-step {
            background-color: #edba45;

            }[data-text-bg="secondary"] {
                background-color: #edba45;
            }

            /* Color */
            .secondary.is-underline,
            .secondary.is-link,
            .secondary.is-outline,
            .stars a.active,
            .star-rating:before,
            .woocommerce-page .star-rating:before,
            .star-rating span:before,
            .color-secondary {
                color: #edba45
            }

            /* Color !important */[data-text-color="secondary"]{color: #edba45!important;}/* Border */
            .secondary.is-outline:hover {
                border-color: #edba45
            }

            /* Focus */
            .secondary:focus-visible,
            .alt:focus-visible {
                outline-color: #edba45 !important;
            }

            .success.is-underline:hover,
            .success.is-outline:hover,
            .success {
                background-color: #f6cfb2
            }

            .success-color,
            .success.is-link,
            .success.is-outline {
                color: #f6cfb2;
            }

            .success-border {
                border-color: #f6cfb2 !important;
            }

            /* Color !important */[data-text-color="success"]{color: #f6cfb2!important;}/* Background Color */[data-text-bg="success"]{background-color: #f6cfb2;}body{color: #224229}h1,h2,h3,h4,h5,h6,.heading-font{color: #224229;}body{font-size: 100%;}@media screen and (max-width: 549px){body{font-size: 100%;}}body{font-family: Quicksand, sans-serif;}body {font-weight: 500;font-style: normal;}.nav > li > a {font-family: Arsenal, sans-serif;}.mobile-sidebar-levels-2 .nav > li > ul > li > a {font-family: Arsenal, sans-serif;}.nav > li > a,.mobile-sidebar-levels-2 .nav > li > ul > li > a {font-weight: 400;font-style: normal;}h1,h2,h3,h4,h5,h6,.heading-font, .off-canvas-center .nav-sidebar.nav-vertical > li > a{font-family: Lora, sans-serif;}h1,h2,h3,h4,h5,h6,.heading-font,.banner h1,.banner h2 {font-weight: 500;font-style: normal;}.alt-font{font-family: "Dancing Script", sans-serif;}.alt-font {font-weight: 400!important;font-style: normal!important;}.header:not(.transparent) .header-nav-main.nav > li > a {color: #224229;}.header:not(.transparent) .header-nav-main.nav > li > a:hover,.header:not(.transparent) .header-nav-main.nav > li.active > a,.header:not(.transparent) .header-nav-main.nav > li.current > a,.header:not(.transparent) .header-nav-main.nav > li > a.active,.header:not(.transparent) .header-nav-main.nav > li > a.current{color: #018342;}.header-nav-main.nav-line-bottom > li > a:before,.header-nav-main.nav-line-grow > li > a:before,.header-nav-main.nav-line > li > a:before,.header-nav-main.nav-box > li > a:hover,.header-nav-main.nav-box > li.active > a,.header-nav-main.nav-pills > li > a:hover,.header-nav-main.nav-pills > li.active > a{color:#FFF!important;background-color: #018342;}a{color: #224229;}a:hover{color: #224229;}.tagcloud a:hover{border-color: #224229;background-color: #224229;}.widget a{color: #224229;}.widget a:hover{color: #018342;}.widget .tagcloud a:hover{border-color: #018342; background-color: #018342;}.is-divider{background-color: #e3b845;}.current .breadcrumb-step, [data-icon-label]:after, .button#place_order,.button.checkout,.checkout-button,.single_add_to_cart_button.button{background-color: #018342!important }.has-equal-box-heights .box-image {padding-top: 125%;}.badge-inner.on-sale{background-color: #309b47}.star-rating span:before,.star-rating:before, .woocommerce-page .star-rating:before, .stars a:hover:after, .stars a.active:after{color: #0d3707}.price del, .product_list_widget del, del .woocommerce-Price-amount { color: #0d3707; }ins .woocommerce-Price-amount { color: #00ab84; }@media screen and (min-width: 550px){.products .box-vertical .box-image{min-width: 800px!important;width: 800px!important;}}.header-main .social-icons,.header-main .cart-icon strong,.header-main .menu-title,.header-main .header-button > .button.is-outline,.header-main .nav > li > a > i:not(.icon-angle-down){color: #224229!important;}.header-main .header-button > .button.is-outline,.header-main .cart-icon strong:after,.header-main .cart-icon strong{border-color: #224229!important;}.header-main .header-button > .button:not(.is-outline){background-color: #224229!important;}.header-main .current-dropdown .cart-icon strong,.header-main .header-button > .button:hover,.header-main .header-button > .button:hover i,.header-main .header-button > .button:hover span{color:#FFF!important;}.header-main .menu-title:hover,.header-main .social-icons a:hover,.header-main .header-button > .button.is-outline:hover,.header-main .nav > li > a:hover > i:not(.icon-angle-down){color: #018342!important;}.header-main .current-dropdown .cart-icon strong,.header-main .header-button > .button:hover{background-color: #018342!important;}.header-main .current-dropdown .cart-icon strong:after,.header-main .current-dropdown .cart-icon strong,.header-main .header-button > .button:hover{border-color: #018342!important;}.absolute-footer, html{background-color: rgba(34,66,41,0.92)}button[name='update_cart'] { display: none; }.nav-vertical-fly-out > li + li {border-top-width: 1px; border-top-style: solid;}/* Custom CSS */
            .colormenu {
                color: #224229;
            }

            .woocommerce-Price-amount.amount {
                color: #008749;
            }

            /*This CSS Code will help you to make your checkout as one page but don't hesitate to follow our tutorials on flatzone.cc.*/
            .checkout_coupon {
                display: block !important;
            }

            .woocommerce-info {
                display: none;
            }

            /** END - Open automatically Checkout Coupon **/
            /** START - Cart on Checkout **/
            .woocommerce .cart-auto-refresh {
                max-width: 100%;
                flex-basis: 100%;
            }

            .woocommerce .cart-collaterals {
                display: none;
            }

            .continue-shopping .button-continue-shopping {
                display: none;
            }

            .woocommerce-checkout-review-order-table .cart_item {
                display: none;
            }

            .woocommerce-checkout-review-order-table thead {
                display: none;
            }

            #order_review_heading {
                display: none;
            }

            /** END - Cart on Checkout **/
            /** Customize Fields of Chechout **/
            .fl-labels .form-row input:not([type="checkbox"]),
            .fl-labels .form-row textarea,
            .fl-labels .form-row select {
                border-radius: 5px;
                box-shadow: none;
                border-color: #dcdcdc;
            }

            /** Cart Customize **/
            .is-form,
            button.is-form,
            input[type='submit'].is-form,
            input[type='reset'].is-form,
            input[type='button'].is-form {
                border-radius: 10px;
            }

            /** END - Cart Customize **/
            /*STICKY PRODUCT OPTIONS NAVBAR*/
            .sticky-down {
                position: -webkit-sticky;
                position: sticky;
                top: 0;
                overflow: hidden;
                z-index: 999;
            }

            /** Rounded Thumbnails on the Product Page **/
            .product-thumbnails a:hover,
            .product-thumbnails .is-nav-selected a {
                border-color: #c0c0c0;
                border-radius: 15%;
                /** Change percentage **/
            }

            .product-thumbnails img {
                border-radius: 15%;
                /** Change percentage **/
            }

            Copied to the clipboard !

            /*START - DESIGN ONLY FOR THE PRODUCT PAGE: FLAT FIVE*/
            .quantity {
                border: 1px solid black;
            }

            /*CUSTOMIZATION OF TABS*/
            .tabs-wallet .nav-tabs>li.active>a {
                border: 1px black solid;
                background-color: #f9f9f9;
                border-bottom-color: #f9f9f9;
            }

            .tabs-wallet .nav-tabs>li>a {
                border: none;
                background: transparent;
            }

            .tabs-wallet .nav-tabs+.tab-panels {
                border: 1px solid black;
                background-color: #f9f9f9;
                padding: 30px;
            }

            /* WooCommerce Price */
            .woocommerce-variation-price {
                font-size: 1.5em !important;
            }

            .woocommerce-Price-amount.amount {
                font-size: 1.5em !important;
            }

            /* xóa đoạn chữ dưới blog */
            .single footer.entry-meta {
                display: none;
            }

            @charset "utf-8";

            /*Comment style*/
            span.title_comment {
                font-size: 20px;
                color: #606664;
                line-height: 1.3em;
                font-weight: 700;
                margin: 0 0 15px;
                display: inline-block;
            }

            .comment-author.vcard {
                width: 25px;
            }

            #formcmmaxweb {
                overflow: hidden;
                position: relative;
                margin-bottom: 20px;
            }

            #formcmmaxweb .nameuser {
                text-align: left;
            }

            #formcmmaxweb .avatarmw img {
                float: left;
                margin: 5px 10px 0 0;
                border: 1px solid #ccc;
                padding: 5px;
            }

            .comments-title-maxweb {
                margin: 30px 0;
                font-weight: normal;
            }

            .commentlist {
                margin-top: 10px;
            }

            .comment-author.vcard {
                float: left;
                clear: both;
                z-index: 2;
                position: relative;
                text-align: center;
            }

            .commentlist li {
                overflow: hidden;
                clear: both;
                margin-bottom: 10px;
            }

            .commentlist .children {
                margin-top: 10px;
            }

            .commentBody em {
                color: red;
            }

            .commentBody {
                position: relative;
                margin-left: 35px;
                overflow: hidden;
            }

            .comment-meta.commentmetadata {
                position: relative;
                padding: 0;
                margin: 0 0 5px;
            }

            .noidungcomment {
                font-size: 16px;
                line-height: 24px;
            }

            .cancel-comment-reply a {
                font-size: 14px;
                text-decoration: none;
                color: #a9883f;
            }

            .nocomments {
                border-color: #D98D8D;
                background-color: #FFCECE;
                background-position: left -792px;
                color: #665252;
                padding: 10px;
            }

            .comment-meta.commentmetadata .ngaythang {
                position: absolute;
                top: 0;
                right: 0;
                color: #337ab7;
                opacity: 0.3;
                font-size: 12px;
            }

            .commentBody:hover .comment-meta.commentmetadata .ngaythang {
                opacity: 1;
            }

            .commentBody .reply {
                float: right;
                margin: 0;
                position: absolute;
                bottom: 0;
                right: 0;
            }

            .comment-meta.commentmetadata .fn {
                font-weight: 700;
                color: #333;
                margin: 0;
                text-transform: capitalize;
                font-size: 17px;
                line-height: 22px;
                margin-top: 2px;
            }

            ol.commentlist_mw,
            ol.commentlist_mw ul.children {
                list-style: none;
            }

            ol.commentlist_mw li {
                margin-bottom: 10px;
            }

            ol.commentlist_mw>ul.children {
                margin: 10px 0 15px 35px;
                background: #f1f1f1;
                padding: 10px;
                position: relative;
            }

            ol.commentlist_mw>ul.children:before {
                content: '';
                position: absolute;
                top: -10px;
                left: 14px;
                width: 0;
                height: 0;
                border-bottom: 10px solid #f1f1f1;
                border-left: 10px solid transparent;
                border-right: 10px solid transparent;
            }

            div.error {
                color: red;
                font-size: 12px;
                text-align: left;
                line-height: 16px;
                margin: 3px 0 0;
            }

            .tools_comment a {
                color: #4a90e2;
                font-size: 14px;
                position: relative;
                padding: 0 15px 0 0;
                margin: 0 7px 0 0;
                line-height: 14px;
            }

            .tools_comment a:after {
                content: "";
                width: 4px;
                height: 4px;
                top: 50%;
                margin-top: -2px;
                background: #999;
                position: absolute;
                right: 0;
                border-radius: 50%;
                -moz-border-radius: 50%;
                -webkit-border-radius: 50%;
            }

            .tools_comment a:last-child:after {
                display: none;
            }

            .tools_comment a:last-child {
                padding: 0;
                margin: 0;
            }

            .noidungcomment p {

                margin: 0 0 5px;}#formcmmaxwebinput[type="submit"],
                #formcmmaxweb input[type="button"],
                #formcmmaxweb input[type="reset"],
                #formcmmaxweb article.post-password-required input[type=submit],
                #formcmmaxweb li.bypostauthor cite span {
                    padding: 6px 10px;
                    padding: 0.428571429rem 0.714285714rem;
                    font-size: 11px;
                    font-size: 0.785714286rem;
                    line-height: 1.428571429;
                    font-weight: normal;
                    color: #7c7c7c;
                    background-color: #e6e6e6;
                    background-repeat: repeat-x;
                    background-image: -moz-linear-gradient(top, #f4f4f4, #e6e6e6);
                    background-image: -ms-linear-gradient(top, #f4f4f4, #e6e6e6);
                    background-image: -webkit-linear-gradient(top, #f4f4f4, #e6e6e6);
                    background-image: -o-linear-gradient(top, #f4f4f4, #e6e6e6);
                    background-image: linear-gradient(top, #f4f4f4, #e6e6e6);
                    border: 1px solid #d2d2d2;
                    border-radius: 3px;
                    box-shadow: 0 1px 2px rgba(64, 64, 64, 0.1);
                }

                #formcmmaxweb button,
                #formcmmaxweb input,
                #formcmmaxweb textarea {
                    border: 1px solid #ccc;
                    border-radius: 2px;
                    -moz-border-radius: 2px;
                    -webkit-border-radius: 2px;
                    font-family: inherit;
                    padding: 6px;
                    padding: 0.428571429rem;
                }

                #formcmmaxweb button,
                #formcmmaxweb input[type="submit"],
                #formcmmaxweb input[type="button"],
                #formcmmaxweb input[type="reset"] {
                    cursor: pointer;
                }

                .reply a.comment-reply-link {
                    color: #337ab7;
                    text-decoration: none;
                    font-weight: 400;
                    font-size: 12px;
                }

                .reply a.comment-reply-link:hover {
                    color: #c7a611;
                }

                #formcmmaxweb p,
                #formcmmaxweb input,
                #formcmmaxweb textarea {
                    width: 100%;
                    background: transparent;
                    outline: none;
                    font-size: 14px;
                }

                #formcmmaxweb input,
                #formcmmaxweb textarea {
                    border: 1px solid #d7d7d7;
                    color: #2e2e2e;
                    display: block;
                }

                #formcmmaxweb input:focus,
                #formcmmaxweb textarea:focus {
                    border-color: #337ab7;
                }

                #formcmmaxweb p {
                    margin-bottom: 10px;
                    text-align: center;
                }

                #formcmmaxweb #commentform p {
                    overflow: hidden;
                }

                #formcmmaxweb .name-email p {
                    width: 50%;
                    float: left;
                }

                #formcmmaxweb .name-email p:first-child {
                    padding-right: 5px;
                }

                #formcmmaxweb .name-email p:last-child {
                    padding-left: 5px;
                }

                #formcmmaxweb input#submit {
                    width: auto;
                    font-weight: 400;
                    color: #fff;
                    background: #337ab7;
                    padding: 6px 20px;
                    border: none;
                    float: left;
                    font-size: 16px;
                    outline: none;
                }

                #formcmmaxweb input#submit:hover {
                    background: #c7a611;
                }

                #formcmmaxweb::-webkit-input-placeholder {
                    color: #999999;
                }

                #formcmmaxweb ::-moz-placeholder {
                    /* Firefox 18- */
                    color: #999999;
                }

                #formcmmaxweb ::-moz-placeholder {
                    /* Firefox 19+ */
                    color: #999999;
                }

                #formcmmaxweb ::-ms-input-placeholder {
                    color: #999999;
                }

                /*#comment style*/
                /*bai viet lien quan*/
                .xem-them .tieu-de-xem-them {
                    font-weight: 700;
                    display: block;
                    margin-bottom: 10px;
                    font-size: 19px;
                    color: black;
                }

                .xem-them ul li {
                    margin-bottom: 3px;
                }

                .xem-them ul li a {
                    font-weight: 700;
                    font-size: 16px;
                    color: #2a9e2f;
                }

                .xem-them ul li a:hover {
                    text-decoration: underline;
                }

                .blog-single .entry-meta {
                    text-transform: none;
                    font-size: 14px;
                    letter-spacing: 0;
                    color: gray;
                    border-top: 1px solid #e2e2e2;
                    border-bottom: 1px solid #e2e2e2;
                    padding: 10px;
                    background: #f9f9f9;
                }

                .danh-muc {
                    margin-bottom: 15px;
                }

                .danh-muc span.title,
                .the-tim-kiem span.title {
                    border-radius: 4px;
                    background: #126e32;
                    padding: 4px 10px;
                    color: white;
                    margin-right: 5px;
                }

                .danh-muc a,
                .the-tim-kiem a {
                    line-height: 32px;
                    border-radius: 4px;
                    margin-bottom: 10px;
                    padding: 4px 10px;
                    background: #dedede;
                    color: #464646;
                }

                .danh-muc a:hover,
                .the-tim-kiem a:hover {
                    background: #6dca19;
                    color: white;
                }

                .bai-viet-lien-quan {
                    margin-top: 15px;
                }

                .bai-viet-lien-quan h3 {
                    font-size: 19px;
                    color: black;
                }

                .bai-viet-lien-quan ul {
                    margin-bottom: 0;
                    display: inline-block;
                    width: 100%;
                }

                .bai-viet-lien-quan ul li {
                    list-style: none;
                    width: 25%;
                    color: graytext;
                    float: left;
                    padding-left: 4px;
                    padding-right: 5px;
                }

                .bai-viet-lien-quan ul li .box-image img {
                    height: 120px;
                    border-radius: 4px;
                    object-fit: cover;
                    object-position: center;
                }

                .bai-viet-lien-quan h4 {
                    font-size: 15px;
                    color: black;
                    line-height: 19px;
                    padding-top: 7px;
                    height: 64px;
                    overflow: hidden;
                }

                /*bai viet lien quan*/
                .responsive #top.woocommerce.woocommerce-page .container {
                    max-width: 1100px;
                }

                /* Custom CSS Tablet */
                @media (max-width: 849px) {

                    /*bai viet lien quan*/
                    .bai-viet-lien-quan ul li {
                        width: 50%;
                    }

                    .bai-viet-lien-quan ul li .box-image img {
                        height: 90px;
                    }

                    /*bai viet lien quan*/
                }

                /* Custom CSS Mobile */
                @media (max-width: 549px) {

                    /*bai viet lien quan*/
                    .bai-viet-lien-quan ul li {
                        width: 50%;
                    }

                    .bai-viet-lien-quan ul li .box-image img {
                        height: 90px;
                    }

                    /*bai viet lien quan*/
                }

                .label-new.menu-item>a:after {
                    content: "New";
                }

                .label-hot.menu-item>a:after {
                    content: "Hot";
                }

                .label-sale.menu-item>a:after {
                    content: "Sale";
                }

                .label-popular.menu-item>a:after {
                    content: "Popular";
                }
    </style>
    <style id="flatsome-swatches-css" type="text/css"></style>
    <style id="kirki-inline-styles">
        /* cyrillic-ext */
        @font-face {
            font-family: 'Lora';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/lora/0QI6MX1D_JOuGQbT0gvTJPa787wsuxJMkqt8ndeY9Z6JTg.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */
        @font-face {
            font-family: 'Lora';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/lora/0QI6MX1D_JOuGQbT0gvTJPa787wsuxJFkqt8ndeY9Z6JTg.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Lora';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/lora/0QI6MX1D_JOuGQbT0gvTJPa787wsuxJOkqt8ndeY9Z6JTg.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Lora';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/lora/0QI6MX1D_JOuGQbT0gvTJPa787wsuxJPkqt8ndeY9Z6JTg.woff) format('woff');
            unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Lora';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/lora/0QI6MX1D_JOuGQbT0gvTJPa787wsuxJBkqt8ndeY9Z4.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Quicksand';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/quicksand/6xK-dSZaM9iE8KbpRA_LJ3z8mH9BOJvgkM0o58m-xDwxUD22FNZc.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Quicksand';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/quicksand/6xK-dSZaM9iE8KbpRA_LJ3z8mH9BOJvgkM0o58i-xDwxUD22FNZc.woff) format('woff');
            unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Quicksand';
            font-style: normal;
            font-weight: 500;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/quicksand/6xK-dSZaM9iE8KbpRA_LJ3z8mH9BOJvgkM0o58a-xDwxUD22FA.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* cyrillic-ext */
        @font-face {
            font-family: 'Arsenal';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/arsenal/wXKrE3kQtZQ4pF3D51XcBs4olXcLtA.woff) format('woff');
            unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
        }

        /* cyrillic */
        @font-face {
            font-family: 'Arsenal';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/arsenal/wXKrE3kQtZQ4pF3D51zcBs4olXcLtA.woff) format('woff');
            unicode-range: U+0301, U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Arsenal';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/arsenal/wXKrE3kQtZQ4pF3D51fcBs4olXcLtA.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Arsenal';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/arsenal/wXKrE3kQtZQ4pF3D51bcBs4olXcLtA.woff) format('woff');
            unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Arsenal';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/arsenal/wXKrE3kQtZQ4pF3D51jcBs4olXc.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }

        /* vietnamese */
        @font-face {
            font-family: 'Dancing Script';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/dancing-script/If2cXTr6YS-zF4S-kcSWSVi_sxjsohD9F50Ruu7BMSo3Rep6hNX6pmRMjLo.woff) format('woff');
            unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+0300-0301, U+0303-0304, U+0308-0309, U+0323, U+0329, U+1EA0-1EF9, U+20AB;
        }

        /* latin-ext */
        @font-face {
            font-family: 'Dancing Script';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/dancing-script/If2cXTr6YS-zF4S-kcSWSVi_sxjsohD9F50Ruu7BMSo3ROp6hNX6pmRMjLo.woff) format('woff');
            unicode-range: U+0100-02AF, U+0304, U+0308, U+0329, U+1E00-1E9F, U+1EF2-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }

        /* latin */
        @font-face {
            font-family: 'Dancing Script';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://mowgarden.com/wp-content/fonts/dancing-script/If2cXTr6YS-zF4S-kcSWSVi_sxjsohD9F50Ruu7BMSo3Sup6hNX6pmRM.woff) format('woff');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+0304, U+0308, U+0329, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
    </style>
</head>

<body
    class="home page-template page-template-page-blank page-template-page-blank-php page page-id-3762 theme-flatsome woocommerce-no-js lightbox nav-dropdown-has-shadow mobile-submenu-toggle">
    <!-- Google Tag Manager (noscript) --> <noscript><iframe
            src="https://www.googletagmanager.com/ns.html?id=GTM-MRDFS37C" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript> <!-- End Google Tag Manager (noscript) --> <a
        class="skip-link screen-reader-text" href="#main">Skip to content</a>
    <div id="wrapper">

        @include('layouts.header')

        @yield('headerproduct')

        <main id="main" class="">
            @yield('maincontent')
        </main>

        @include('layouts.footer')
    </div>
    
    <div id="main-menu" class="mobile-sidebar no-scrollbar mfp-hide">
        <div class="sidebar-menu no-scrollbar text-center">
            <ul class="nav nav-sidebar nav-vertical nav-uppercase nav-anim">
                <li class="header-search-form search-form html relative has-icon">
                    <div class="header-search-form-wrapper">
                        <div class="searchform-wrapper ux-search-box relative form-flat is-normal">
                            <form role="search" method="get" class="searchform"
                                action="https://mowgarden.com/">
                                <div class="flex-row relative">
                                    <div class="flex-col flex-grow"> <label class="screen-reader-text"
                                            for="woocommerce-product-search-field-1">Tìm kiếm:</label> <input
                                            type="search" id="woocommerce-product-search-field-1"
                                            class="search-field mb-0" placeholder="Tìm kiếm sản phẩm"
                                            value="" name="s" /> <input type="hidden"
                                            name="post_type" value="product" /> </div>
                                    <div class="flex-col"> <button type="submit" value="Tìm kiếm"
                                            class="ux-search-submit submit-button secondary button icon mb-0"
                                            aria-label="Submit"> <i class="icon-search"></i> </button> </div>
                                </div>
                                <div class="live-search-results text-left z-top"></div>
                            </form>
                        </div>
                    </div>
                </li>
                <li id="menu-item-1840"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1840"><a
                        href="https://mowgarden.com/ban-cay-canh-trong-nha/">Trang chủ</a></li>
                <li id="menu-item-1841"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1841"><a
                        href="https://mowgarden.com/cay-ngoai-troi/">Cây ngoài trời</a></li>
                <li id="menu-item-1842"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1842"><a
                        href="https://mowgarden.com/chau-cay/">Chậu cây</a></li>
                <li id="menu-item-1843"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1843"><a
                        href="#">Phụ kiện</a></li>
                <li id="menu-item-1844"
                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-1844">
                    <a href="https://mowgarden.com/huong-dan-cay-xanh-101/">Hướng dẫn</a>
                    <ul class="sub-menu nav-sidebar-ul children">
                        <li id="menu-item-1845"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1845"><a
                                href="https://mowgarden.com/category/thong-tin-cay-canh/">Thông tin về cây</a></li>
                        <li id="menu-item-1846"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1846"><a
                                href="https://mowgarden.com/category/kien-thuc-va-cach-cham-soc/">Kiến thức &#038;
                                cách chăm sóc</a></li>
                        <li id="menu-item-1847"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1847"><a
                                href="https://mowgarden.com/category/cam-hung-va-y-tuong/">Cảm hứng &#038; ý tưởng</a>
                        </li>
                        <li id="menu-item-1848"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1848"><a
                                href="https://mowgarden.com/category/mow-garden-360/">MOW GARDEN 360°</a></li>
                    </ul>
                </li>
                <li class="account-item has-icon menu-item"> <a href="#"
                        class="nav-top-link nav-top-not-logged-in"> <span class="header-account-title"> Đăng nhập
                        </span> </a> </li>
                <li class="menu-item cart-item has-icon has-child"> <a href="{{route('giohang')}}"
                        title="Giỏ hàng" class="header-cart-link"> <span class="header-cart-title"> <span
                                class="cart-price"><span class="woocommerce-Price-amount amount"><bdi>0<span
                                            class="woocommerce-Price-currencySymbol">&#8363;</span></bdi></span></span>
                        </span> </a>
                    <ul class="children">
                        <li> <!-- Cart Sidebar Popup -->
                            <div id="cart-popup" class="widget_shopping_cart">
                                <div class="cart-popup-inner inner-padding">
                                    <div class="cart-popup-title text-center">
                                        <h4 class="uppercase">Giỏ hàng</h4>
                                        <div class="is-divider"></div>
                                    </div>
                                    <div class="widget_shopping_cart_content">
                                        <p class="woocommerce-mini-cart__empty-message">Chưa có sản phẩm trong giỏ
                                            hàng.</p>
                                    </div>
                                    <div class="cart-sidebar-content relative"></div>
                                    <div class="payment-icons inline-block">
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 64 32">
                                                <path
                                                    d="M10.781 7.688c-0.251-1.283-1.219-1.688-2.344-1.688h-8.376l-0.061 0.405c5.749 1.469 10.469 4.595 12.595 10.501l-1.813-9.219zM13.125 19.688l-0.531-2.781c-1.096-2.907-3.752-5.594-6.752-6.813l4.219 15.939h5.469l8.157-20.032h-5.501l-5.062 13.688zM27.72 26.061l3.248-20.061h-5.187l-3.251 20.061h5.189zM41.875 5.656c-5.125 0-8.717 2.72-8.749 6.624-0.032 2.877 2.563 4.469 4.531 5.439 2.032 0.968 2.688 1.624 2.688 2.499 0 1.344-1.624 1.939-3.093 1.939-2.093 0-3.219-0.251-4.875-1.032l-0.688-0.344-0.719 4.499c1.219 0.563 3.437 1.064 5.781 1.064 5.437 0.032 8.97-2.688 9.032-6.843 0-2.282-1.405-4-4.376-5.439-1.811-0.904-2.904-1.563-2.904-2.499 0-0.843 0.936-1.72 2.968-1.72 1.688-0.029 2.936 0.314 3.875 0.752l0.469 0.248 0.717-4.344c-1.032-0.406-2.656-0.844-4.656-0.844zM55.813 6c-1.251 0-2.189 0.376-2.72 1.688l-7.688 18.374h5.437c0.877-2.467 1.096-3 1.096-3 0.592 0 5.875 0 6.624 0 0 0 0.157 0.688 0.624 3h4.813l-4.187-20.061h-4zM53.405 18.938c0 0 0.437-1.157 2.064-5.594-0.032 0.032 0.437-1.157 0.688-1.907l0.374 1.72c0.968 4.781 1.189 5.781 1.189 5.781-0.813 0-3.283 0-4.315 0z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 64 32">
                                                <path
                                                    d="M35.255 12.078h-2.396c-0.229 0-0.444 0.114-0.572 0.303l-3.306 4.868-1.4-4.678c-0.088-0.292-0.358-0.493-0.663-0.493h-2.355c-0.284 0-0.485 0.28-0.393 0.548l2.638 7.745-2.481 3.501c-0.195 0.275 0.002 0.655 0.339 0.655h2.394c0.227 0 0.439-0.111 0.569-0.297l7.968-11.501c0.191-0.275-0.006-0.652-0.341-0.652zM19.237 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM22.559 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.166-0.241c-0.517-0.749-1.667-1-2.817-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.251-0.155-0.479-0.41-0.479zM8.254 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.262 0.307 0.341 0.761 0.242 1.388zM7.68 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.155 0.479 0.41 0.479h2.378c0.34 0 0.63-0.248 0.683-0.584l0.543-3.444c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.628-1.272zM60.876 7.823l-2.043 12.998c-0.040 0.252 0.155 0.479 0.41 0.479h2.055c0.34 0 0.63-0.248 0.683-0.584l2.015-12.765c0.040-0.252-0.155-0.479-0.41-0.479h-2.299c-0.205 0.001-0.379 0.148-0.41 0.351zM54.744 16.718c-0.23 1.362-1.311 2.276-2.691 2.276-0.691 0-1.245-0.223-1.601-0.644-0.353-0.417-0.485-1.012-0.374-1.674 0.214-1.35 1.313-2.294 2.671-2.294 0.677 0 1.227 0.225 1.589 0.65 0.365 0.428 0.509 1.027 0.404 1.686zM58.066 12.078h-2.384c-0.204 0-0.378 0.148-0.41 0.351l-0.104 0.666-0.167-0.241c-0.516-0.749-1.667-1-2.816-1-2.634 0-4.883 1.996-5.321 4.796-0.228 1.396 0.095 2.731 0.888 3.662 0.727 0.856 1.765 1.212 3.002 1.212 2.123 0 3.3-1.363 3.3-1.363l-0.106 0.662c-0.040 0.252 0.155 0.479 0.41 0.479h2.147c0.341 0 0.63-0.247 0.684-0.584l1.289-8.161c0.040-0.252-0.156-0.479-0.41-0.479zM43.761 12.135c-0.272 1.787-1.636 1.787-2.957 1.787h-0.751l0.527-3.336c0.031-0.202 0.205-0.35 0.41-0.35h0.345c0.899 0 1.747 0 2.185 0.511 0.261 0.307 0.34 0.761 0.241 1.388zM43.187 7.473h-4.979c-0.341 0-0.63 0.248-0.684 0.584l-2.013 12.765c-0.040 0.252 0.156 0.479 0.41 0.479h2.554c0.238 0 0.441-0.173 0.478-0.408l0.572-3.619c0.053-0.337 0.343-0.584 0.683-0.584h1.575c3.279 0 5.172-1.587 5.666-4.732 0.223-1.375 0.009-2.456-0.635-3.212-0.707-0.832-1.962-1.272-3.627-1.272z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 64 32">
                                                <path
                                                    d="M13.043 8.356c-0.46 0-0.873 0.138-1.24 0.413s-0.662 0.681-0.885 1.217c-0.223 0.536-0.334 1.112-0.334 1.727 0 0.568 0.119 0.99 0.358 1.265s0.619 0.413 1.141 0.413c0.508 0 1.096-0.131 1.765-0.393v1.327c-0.693 0.262-1.389 0.393-2.089 0.393-0.884 0-1.572-0.254-2.063-0.763s-0.736-1.229-0.736-2.161c0-0.892 0.181-1.712 0.543-2.462s0.846-1.32 1.452-1.709 1.302-0.584 2.089-0.584c0.435 0 0.822 0.038 1.159 0.115s0.7 0.217 1.086 0.421l-0.616 1.276c-0.369-0.201-0.673-0.333-0.914-0.398s-0.478-0.097-0.715-0.097zM19.524 12.842h-2.47l-0.898 1.776h-1.671l3.999-7.491h1.948l0.767 7.491h-1.551l-0.125-1.776zM19.446 11.515l-0.136-1.786c-0.035-0.445-0.052-0.876-0.052-1.291v-0.184c-0.153 0.408-0.343 0.84-0.569 1.296l-0.982 1.965h1.739zM27.049 12.413c0 0.711-0.257 1.273-0.773 1.686s-1.213 0.62-2.094 0.62c-0.769 0-1.389-0.153-1.859-0.46v-1.398c0.672 0.367 1.295 0.551 1.869 0.551 0.39 0 0.694-0.072 0.914-0.217s0.329-0.343 0.329-0.595c0-0.147-0.024-0.275-0.070-0.385s-0.114-0.214-0.201-0.309c-0.087-0.095-0.303-0.269-0.648-0.52-0.481-0.337-0.818-0.67-1.013-1s-0.293-0.685-0.293-1.066c0-0.439 0.108-0.831 0.324-1.176s0.523-0.614 0.922-0.806 0.857-0.288 1.376-0.288c0.755 0 1.446 0.168 2.073 0.505l-0.569 1.189c-0.543-0.252-1.044-0.378-1.504-0.378-0.289 0-0.525 0.077-0.71 0.23s-0.276 0.355-0.276 0.607c0 0.207 0.058 0.389 0.172 0.543s0.372 0.36 0.773 0.615c0.421 0.272 0.736 0.572 0.945 0.9s0.313 0.712 0.313 1.151zM33.969 14.618h-1.597l0.7-3.22h-2.46l-0.7 3.22h-1.592l1.613-7.46h1.597l-0.632 2.924h2.459l0.632-2.924h1.592l-1.613 7.46zM46.319 9.831c0 0.963-0.172 1.824-0.517 2.585s-0.816 1.334-1.415 1.722c-0.598 0.388-1.288 0.582-2.067 0.582-0.891 0-1.587-0.251-2.086-0.753s-0.749-1.198-0.749-2.090c0-0.902 0.172-1.731 0.517-2.488s0.82-1.338 1.425-1.743c0.605-0.405 1.306-0.607 2.099-0.607 0.888 0 1.575 0.245 2.063 0.735s0.73 1.176 0.73 2.056zM43.395 8.356c-0.421 0-0.808 0.155-1.159 0.467s-0.627 0.739-0.828 1.283-0.3 1.135-0.3 1.771c0 0.5 0.116 0.877 0.348 1.133s0.558 0.383 0.979 0.383 0.805-0.148 1.151-0.444c0.346-0.296 0.617-0.714 0.812-1.255s0.292-1.148 0.292-1.822c0-0.483-0.113-0.856-0.339-1.12-0.227-0.264-0.546-0.396-0.957-0.396zM53.427 14.618h-1.786l-1.859-5.644h-0.031l-0.021 0.163c-0.111 0.735-0.227 1.391-0.344 1.97l-0.757 3.511h-1.436l1.613-7.46h1.864l1.775 5.496h0.021c0.042-0.259 0.109-0.628 0.203-1.107s0.407-1.942 0.94-4.388h1.43l-1.613 7.461zM13.296 20.185c0 0.98-0.177 1.832-0.532 2.556s-0.868 1.274-1.539 1.652c-0.672 0.379-1.464 0.568-2.376 0.568h-2.449l1.678-7.68h2.15c0.977 0 1.733 0.25 2.267 0.751s0.801 1.219 0.801 2.154zM8.925 23.615c0.536 0 1.003-0.133 1.401-0.399s0.71-0.657 0.934-1.174c0.225-0.517 0.337-1.108 0.337-1.773 0-0.54-0.131-0.95-0.394-1.232s-0.64-0.423-1.132-0.423h-0.624l-1.097 5.001h0.575zM18.64 24.96h-4.436l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM20.509 24.96l1.678-7.68h1.661l-1.39 6.335h2.78l-0.294 1.345h-4.436zM26.547 24.96l1.694-7.68h1.656l-1.694 7.68h-1.656zM33.021 23.389c0.282-0.774 0.481-1.27 0.597-1.487l2.346-4.623h1.716l-4.061 7.68h-1.814l-0.689-7.68h1.602l0.277 4.623c0.015 0.157 0.022 0.39 0.022 0.699-0.007 0.361-0.018 0.623-0.033 0.788h0.038zM41.678 24.96h-4.437l1.678-7.68h4.442l-0.293 1.334h-2.78l-0.364 1.686h2.59l-0.299 1.334h-2.59l-0.435 1.98h2.78l-0.293 1.345zM45.849 22.013l-0.646 2.947h-1.656l1.678-7.68h1.949c0.858 0 1.502 0.179 1.933 0.536s0.646 0.881 0.646 1.571c0 0.554-0.15 1.029-0.451 1.426s-0.733 0.692-1.298 0.885l1.417 3.263h-1.803l-1.124-2.947h-0.646zM46.137 20.689h0.424c0.474 0 0.843-0.1 1.108-0.3s0.396-0.504 0.396-0.914c0-0.287-0.086-0.502-0.258-0.646s-0.442-0.216-0.812-0.216h-0.402l-0.456 2.076zM53.712 20.39l2.031-3.11h1.857l-3.355 4.744-0.646 2.936h-1.645l0.646-2.936-1.281-4.744h1.694l0.7 3.11z">
                                                </path>
                                            </svg> </div>
                                        <div class="payment-icon"><svg version="1.1"
                                                xmlns="http://www.w3.org/2000/svg"
                                                xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="0 0 64 32">
                                                <path
                                                    d="M8.498 23.915h-1.588l1.322-5.127h-1.832l0.286-1.099h5.259l-0.287 1.099h-1.837l-1.323 5.127zM13.935 21.526l-0.62 2.389h-1.588l1.608-6.226h1.869c0.822 0 1.44 0.145 1.853 0.435 0.412 0.289 0.62 0.714 0.62 1.273 0 0.449-0.145 0.834-0.432 1.156-0.289 0.322-0.703 0.561-1.245 0.717l1.359 2.645h-1.729l-1.077-2.389h-0.619zM14.21 20.452h0.406c0.454 0 0.809-0.081 1.062-0.243s0.38-0.409 0.38-0.741c0-0.233-0.083-0.407-0.248-0.523s-0.424-0.175-0.778-0.175h-0.385l-0.438 1.682zM22.593 22.433h-2.462l-0.895 1.482h-1.666l3.987-6.252h1.942l0.765 6.252h-1.546l-0.125-1.482zM22.515 21.326l-0.134-1.491c-0.035-0.372-0.052-0.731-0.052-1.077v-0.154c-0.153 0.34-0.342 0.701-0.567 1.081l-0.979 1.64h1.732zM31.663 23.915h-1.78l-1.853-4.71h-0.032l-0.021 0.136c-0.111 0.613-0.226 1.161-0.343 1.643l-0.755 2.93h-1.432l1.608-6.226h1.859l1.77 4.586h0.021c0.042-0.215 0.109-0.524 0.204-0.924s0.406-1.621 0.937-3.662h1.427l-1.609 6.225zM38.412 22.075c0 0.593-0.257 1.062-0.771 1.407s-1.21 0.517-2.088 0.517c-0.768 0-1.386-0.128-1.853-0.383v-1.167c0.669 0.307 1.291 0.46 1.863 0.46 0.389 0 0.693-0.060 0.911-0.181s0.328-0.285 0.328-0.495c0-0.122-0.024-0.229-0.071-0.322s-0.114-0.178-0.2-0.257c-0.088-0.079-0.303-0.224-0.646-0.435-0.479-0.28-0.817-0.559-1.011-0.835-0.195-0.275-0.292-0.572-0.292-0.89 0-0.366 0.108-0.693 0.323-0.982 0.214-0.288 0.522-0.512 0.918-0.673 0.398-0.16 0.854-0.24 1.372-0.24 0.753 0 1.442 0.14 2.067 0.421l-0.567 0.993c-0.541-0.21-1.041-0.316-1.499-0.316-0.289 0-0.525 0.064-0.708 0.192-0.185 0.128-0.276 0.297-0.276 0.506 0 0.173 0.057 0.325 0.172 0.454 0.114 0.129 0.371 0.3 0.771 0.513 0.419 0.227 0.733 0.477 0.942 0.752 0.21 0.273 0.314 0.593 0.314 0.959zM41.266 23.915h-1.588l1.608-6.226h4.238l-0.281 1.082h-2.645l-0.412 1.606h2.463l-0.292 1.077h-2.463l-0.63 2.461zM49.857 23.915h-4.253l1.608-6.226h4.259l-0.281 1.082h-2.666l-0.349 1.367h2.484l-0.286 1.081h-2.484l-0.417 1.606h2.666l-0.28 1.091zM53.857 21.526l-0.62 2.389h-1.588l1.608-6.226h1.869c0.822 0 1.44 0.145 1.853 0.435s0.62 0.714 0.62 1.273c0 0.449-0.145 0.834-0.432 1.156-0.289 0.322-0.703 0.561-1.245 0.717l1.359 2.645h-1.729l-1.077-2.389h-0.619zM54.133 20.452h0.406c0.454 0 0.809-0.081 1.062-0.243s0.38-0.409 0.38-0.741c0-0.233-0.083-0.407-0.248-0.523s-0.424-0.175-0.778-0.175h-0.385l-0.438 1.682zM30.072 8.026c0.796 0 1.397 0.118 1.804 0.355s0.61 0.591 0.61 1.061c0 0.436-0.144 0.796-0.433 1.080-0.289 0.283-0.699 0.472-1.231 0.564v0.026c0.348 0.076 0.625 0.216 0.831 0.421 0.207 0.205 0.31 0.467 0.31 0.787 0 0.666-0.266 1.179-0.797 1.539s-1.267 0.541-2.206 0.541h-2.72l1.611-6.374h2.221zM28.111 13.284h0.938c0.406 0 0.726-0.084 0.957-0.253s0.347-0.403 0.347-0.701c0-0.471-0.317-0.707-0.954-0.707h-0.86l-0.428 1.661zM28.805 10.55h0.776c0.421 0 0.736-0.071 0.946-0.212s0.316-0.344 0.316-0.608c0-0.398-0.296-0.598-0.886-0.598h-0.792l-0.36 1.418zM37.242 12.883h-2.466l-0.897 1.517h-1.669l3.993-6.4h1.945l0.766 6.4h-1.548l-0.125-1.517zM37.163 11.749l-0.135-1.526c-0.035-0.381-0.053-0.748-0.053-1.103v-0.157c-0.153 0.349-0.342 0.718-0.568 1.107l-0.98 1.679h1.736zM46.325 14.4h-1.782l-1.856-4.822h-0.032l-0.021 0.14c-0.111 0.628-0.226 1.188-0.344 1.683l-0.756 3h-1.434l1.611-6.374h1.861l1.773 4.695h0.021c0.042-0.22 0.11-0.536 0.203-0.946s0.406-1.66 0.938-3.749h1.428l-1.611 6.374zM54.1 14.4h-1.763l-1.099-2.581-0.652 0.305-0.568 2.276h-1.59l1.611-6.374h1.596l-0.792 3.061 0.824-0.894 2.132-2.166h1.882l-3.097 3.052 1.517 3.322zM23.040 8.64c0-0.353-0.287-0.64-0.64-0.64h-14.080c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h14.080c0.353 0 0.64-0.287 0.64-0.64v0zM19.2 11.2c0-0.353-0.287-0.64-0.64-0.64h-10.24c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h10.24c0.353 0 0.64-0.287 0.64-0.64v0zM15.36 13.76c0-0.353-0.287-0.64-0.64-0.64h-6.4c-0.353 0-0.64 0.287-0.64 0.64v0c0 0.353 0.287 0.64 0.64 0.64h6.4c0.353 0 0.64-0.287 0.64-0.64v0z">
                                                </path>
                                            </svg> </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div id="arcontactus"></div>
    <script src="https://mowgarden.com/wp-content/plugins/ar-contactus/res/js/maskedinput.min.js?version=2.2.6"></script>
    <script type="text/javascript" id="arcu-main-js">
        var $arcuWidget;
        var zaloWidgetInterval;
        var tawkToInterval;
        var tawkToHideInterval;
        var skypeWidgetInterval;
        var lcpWidgetInterval;
        var closePopupTimeout;
        var lzWidgetInterval;
        var paldeskInterval;
        var arcuOptions;
        var hideCustomerChatInterval;
        var _arCuTimeOut = null;
        var arCuPromptClosed = false;
        var _arCuWelcomeTimeOut = null;
        var arCuMenuOpenedOnce = false;
        var arcuAppleItem = null;

        var arcItems = [];
        window.addEventListener('load', function() {
            $arcuWidget = document.createElement('div');
            var body = document.getElementsByTagName('body')[0];
            $arcuWidget.id = 'arcontactus';

            if (document.getElementById('arcontactus')) {
                document.getElementById('arcontactus').parentElement.removeChild(document.getElementById(
                    'arcontactus'));
            }

            body.appendChild($arcuWidget);

            arCuClosedCookie = arCuGetCookie('arcu-closed');
            $arcuWidget.addEventListener('arcontactus.init', function() {
                $arcuWidget.classList.add('arcuAnimated');
                $arcuWidget.classList.add('flipInY');

                setTimeout(function() {
                    $arcuWidget.classList.remove('flipInY');
                }, 1000);


                if (document.querySelector('#arcu-form-callback form')) {
                    document.querySelector('#arcu-form-callback form').append(contactUs.utils
                        .DOMElementFromHTML(arCUVars._wpnonce));
                }

                if (document.querySelector('#arcu-form-email form')) {
                    document.querySelector('#arcu-form-email form').append(contactUs.utils
                        .DOMElementFromHTML(arCUVars._wpnonce));
                }




                $arcuWidget.addEventListener('arcontactus.successSendFormData', function(event) {});
                $arcuWidget.addEventListener('arcontactus.successSendFormData', function(event) {});
                $arcuWidget.addEventListener('arcontactus.errorSendFormData', function(event) {
                    if (event.detail.data && event.detail.data.message) {
                        alert(event.detail.data.message);
                    }
                });
                $arcuWidget.addEventListener('arcontactus.hideFrom', function() {
                    clearTimeout(closePopupTimeout);
                });
            });
            $arcuWidget.addEventListener('arcontactus.closeMenu', function() {
                arCuCreateCookie('arcumenu-closed', 1, 1);
            });
            var arcItem = {};
            arcItem.id = 'msg-item-1';
            arcItem.class = 'msg-item-facebook-messenger';
            arcItem.title = "Messenger";
            arcItem.icon =
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M224 32C15.9 32-77.5 278 84.6 400.6V480l75.7-42c142.2 39.8 285.4-59.9 285.4-198.7C445.8 124.8 346.5 32 224 32zm23.4 278.1L190 250.5 79.6 311.6l121.1-128.5 57.4 59.6 110.4-61.1-121.1 128.5z"></path></svg>';
            arcItem.includeIconToSlider = true;

            arcItem.href = 'https://m.me/mowgarden';

            arcItem.color = '#567AFF';
            arcItems.push(arcItem);
            var arcItem = {};
            arcItem.id = 'msg-item-7';
            arcItem.class = 'msg-item-phone';
            arcItem.title = "Gọi trực tiếp";
            arcItem.icon =
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg>';
            arcItem.includeIconToSlider = true;

            arcItem.href = 'tel:0838369639';

            arcItem.color = '#3EB891';
            arcItems.push(arcItem);
            var arcItem = {};
            arcItem.id = 'msg-item-11';
            arcItem.class = 'msg-item-zalo';
            arcItem.title = "Quét QR";
            arcItem.icon =
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 460.1 436.6"><path fill="currentColor" class="st0" d="M82.6 380.9c-1.8-.8-3.1-1.7-1-3.5 1.3-1 2.7-1.9 4.1-2.8 13.1-8.5 25.4-17.8 33.5-31.5 6.8-11.4 5.7-18.1-2.8-26.5C69 269.2 48.2 212.5 58.6 145.5 64.5 107.7 81.8 75 107 46.6c15.2-17.2 33.3-31.1 53.1-42.7 1.2-.7 2.9-.9 3.1-2.7-.4-1-1.1-.7-1.7-.7-33.7 0-67.4-.7-101 .2C28.3 1.7.5 26.6.6 62.3c.2 104.3 0 208.6 0 313 0 32.4 24.7 59.5 57 60.7 27.3 1.1 54.6.2 82 .1 2 .1 4 .2 6 .2H290c36 0 72 .2 108 0 33.4 0 60.5-27 60.5-60.3v-.6-58.5c0-1.4.5-2.9-.4-4.4-1.8.1-2.5 1.6-3.5 2.6-19.4 19.5-42.3 35.2-67.4 46.3-61.5 27.1-124.1 29-187.6 7.2-5.5-2-11.5-2.2-17.2-.8-8.4 2.1-16.7 4.6-25 7.1-24.4 7.6-49.3 11-74.8 6zm72.5-168.5c1.7-2.2 2.6-3.5 3.6-4.8 13.1-16.6 26.2-33.2 39.3-49.9 3.8-4.8 7.6-9.7 10-15.5 2.8-6.6-.2-12.8-7-15.2-3-.9-6.2-1.3-9.4-1.1-17.8-.1-35.7-.1-53.5 0-2.5 0-5 .3-7.4.9-5.6 1.4-9 7.1-7.6 12.8 1 3.8 4 6.8 7.8 7.7 2.4.6 4.9.9 7.4.8 10.8.1 21.7 0 32.5.1 1.2 0 2.7-.8 3.6 1-.9 1.2-1.8 2.4-2.7 3.5-15.5 19.6-30.9 39.3-46.4 58.9-3.8 4.9-5.8 10.3-3 16.3s8.5 7.1 14.3 7.5c4.6.3 9.3.1 14 .1 16.2 0 32.3.1 48.5-.1 8.6-.1 13.2-5.3 12.3-13.3-.7-6.3-5-9.6-13-9.7-14.1-.1-28.2 0-43.3 0zm116-52.6c-12.5-10.9-26.3-11.6-39.8-3.6-16.4 9.6-22.4 25.3-20.4 43.5 1.9 17 9.3 30.9 27.1 36.6 11.1 3.6 21.4 2.3 30.5-5.1 2.4-1.9 3.1-1.5 4.8.6 3.3 4.2 9 5.8 14 3.9 5-1.5 8.3-6.1 8.3-11.3.1-20 .2-40 0-60-.1-8-7.6-13.1-15.4-11.5-4.3.9-6.7 3.8-9.1 6.9zm69.3 37.1c-.4 25 20.3 43.9 46.3 41.3 23.9-2.4 39.4-20.3 38.6-45.6-.8-25-19.4-42.1-44.9-41.3-23.9.7-40.8 19.9-40 45.6zm-8.8-19.9c0-15.7.1-31.3 0-47 0-8-5.1-13-12.7-12.9-7.4.1-12.3 5.1-12.4 12.8-.1 4.7 0 9.3 0 14v79.5c0 6.2 3.8 11.6 8.8 12.9 6.9 1.9 14-2.2 15.8-9.1.3-1.2.5-2.4.4-3.7.2-15.5.1-31 .1-46.5z"/></svg>';
            arcItem.includeIconToSlider = true;

            arcItem.href = 'https://mowgarden.com/lien-he/';

            arcItem.color = '#0065F7';
            arcItems.push(arcItem);
            arcuOptions = {
                rootElementId: 'arcontactus',
                credits: false,
                visible: true,
                wordpressPluginVersion: '2.2.6',
                online: null,
                buttonIcon: '<svg viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Canvas" transform="translate(-825 -308)"><g id="Vector"><use xlink:href="#path0_fill0123" transform="translate(825 308)" fill="currentColor"></use></g></g><defs><path fill="currentColor" id="path0_fill0123" d="M 19 4L 17 4L 17 13L 4 13L 4 15C 4 15.55 4.45 16 5 16L 16 16L 20 20L 20 5C 20 4.45 19.55 4 19 4ZM 15 10L 15 1C 15 0.45 14.55 0 14 0L 1 0C 0.45 0 0 0.45 0 1L 0 15L 4 11L 14 11C 14.55 11 15 10.55 15 10Z"></path></defs></svg>',
                layout: 'default',
                drag: false,
                mode: 'regular',
                buttonIconUrl: 'https://mowgarden.com/wp-content/plugins/ar-contactus/res/img/msg.svg',
                showMenuHeader: false,
                menuHeaderText: "How would you like to contact us?",
                menuSubheaderText: "",

                menuHeaderLayout: '',
                menuHeaderIcon: '',

                showHeaderCloseBtn: false,
                headerCloseBtnBgColor: '#008749',
                headerCloseBtnColor: '#ffffff',
                itemsIconType: 'rounded',
                align: 'right',
                reCaptcha: false,
                reCaptchaKey: '',
                countdown: 0,
                theme: '#008749',
                buttonText: "Liên hệ",
                buttonSize: 'huge',
                buttonIconSize: 25,
                menuSize: 'large',
                phonePlaceholder: '',
                callbackSubmitText: '',
                errorMessage: '',
                callProcessText: '',
                callSuccessText: '',
                callbackFormText: '',
                iconsAnimationSpeed: 600,
                iconsAnimationPause: 2000,
                items: arcItems,
                ajaxUrl: 'https://mowgarden.com/wp-admin/admin-ajax.php',
                promptPosition: 'top',
                popupAnimation: 'fadeindown',
                style: '',
                itemsAnimation: 'downtoup',
                backdrop: false,
                forms: {
                    callback: {
                        id: 'callback',
                        header: {
                            content: "Please enter your phone number\nand we call you back soon",
                            layout: "text",
                        },
                        icon: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z"></path></svg>',
                        success: "Thank you.\nWe are call you back soon.",
                        error: "Connection error. Please refresh the page and try again.",
                        action: 'https://mowgarden.com/wp-admin/admin-ajax.php',
                        buttons: [{
                            name: "submit",
                            label: "Waiting for call",
                            type: "submit",
                        }, ],
                        fields: {
                            formId: {
                                name: 'formId',
                                value: 'callback',
                                type: 'hidden'
                            },
                            action: {
                                name: 'action',
                                value: 'arcontactus_request_callback',
                                type: 'hidden'
                            },
                            phone: {
                                name: "phone",
                                enabled: true,
                                required: true,
                                type: "tel",
                                label: "Your phone number",
                                placeholder: "+XXX-XX-XXX-XX-XX",
                                values: [],
                                value: "",
                            },
                        }
                    },
                    email: {
                        id: 'email',
                        header: {
                            content: "Write a email to us!",
                            layout: "text",
                        },
                        icon: '<svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M464 64H48C21.5 64 0 85.5 0 112v288c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h416c8.8 0 16 7.2 16 16v41.4c-21.9 18.5-53.2 44-150.6 121.3-16.9 13.4-50.2 45.7-73.4 45.3-23.2.4-56.6-31.9-73.4-45.3C85.2 197.4 53.9 171.9 32 153.4V112c0-8.8 7.2-16 16-16zm416 320H48c-8.8 0-16-7.2-16-16V195c22.8 18.7 58.8 47.6 130.7 104.7 20.5 16.4 56.7 52.5 93.3 52.3 36.4.3 72.3-35.5 93.3-52.3 71.9-57.1 107.9-86 130.7-104.7v205c0 8.8-7.2 16-16 16z"></path></svg>',
                        success: "Email sent! We will contact you soon.",
                        error: "Error sending email! Please try again!",
                        action: 'https://mowgarden.com/wp-admin/admin-ajax.php',
                        buttons: [{
                            name: "submit",
                            label: "Submit",
                            type: "submit",
                        }, ],
                        fields: {
                            formId: {
                                name: 'formId',
                                value: 'email',
                                type: 'hidden'
                            },
                            action: {
                                name: 'action',
                                value: 'arcontactus_request_email',
                                type: 'hidden'
                            },
                            name: {
                                name: "name",
                                enabled: true,
                                required: false,
                                type: "text",
                                label: "Your name",
                                placeholder: "Enter your name",
                                values: [],
                                value: "",
                            },
                            email: {
                                name: "email",
                                enabled: true,
                                required: true,
                                type: "email",
                                label: "Your email",
                                placeholder: "Enter your email",
                                values: [],
                                value: "",
                            },
                            message: {
                                name: "message",
                                enabled: true,
                                required: true,
                                type: "textarea",
                                label: "Your message",
                                placeholder: "Enter your message",
                                values: [],
                                value: "",
                            },
                            gdpr: {
                                name: "gdpr",
                                enabled: true,
                                required: true,
                                type: "checkbox",
                                label: "I accept GDPR rules",
                                placeholder: "",
                                values: [],
                                value: "1",
                            },
                        }
                    },

                }
            };
            contactUs.init(arcuOptions);
        });
    </script>
    <div class="ux-body-overlay"></div>

    @include('layouts.modal-login')
    
    <script type="text/javascript">
        (function() {
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
            document.body.className = c;
        })();
    </script>
    {{-- <script type="text/template" id="tmpl-variation-template">
        <div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
        <div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
        <div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
    </script> --}}
    <script type="text/template" id="tmpl-unavailable-variation-template">
        <p>Rất tiếc, sản phẩm này hiện không tồn tại. Hãy chọn một phương thức kết hợp khác.</p>
    </script>
    <style id='wp-block-paragraph-inline-css' type='text/css'>
        .is-small-text {
            font-size: .875em
        }

        .is-regular-text {
            font-size: 1em
        }

        .is-large-text {
            font-size: 2.25em
        }

        .is-larger-text {
            font-size: 3em
        }

        .has-drop-cap:not(:focus):first-letter {
            float: left;
            font-size: 8.4em;
            font-style: normal;
            font-weight: 100;
            line-height: .68;
            margin: .05em .1em 0 0;
            text-transform: uppercase
        }

        body.rtl .has-drop-cap:not(:focus):first-letter {
            float: none;
            margin-left: .1em
        }

        p.has-drop-cap.has-background {
            overflow: hidden
        }

        p.has-background {
            padding: 1.25em 2.375em
        }

        :where(p.has-text-color:not(.has-link-color)) a{color:inherit}p.has-text-align-left[style*="writing-mode:vertical-lr"],
        p.has-text-align-right[style*="writing-mode:vertical-rl"] {
            rotate: 180deg
        }
    </style>
    <style id='wp-block-heading-inline-css' type='text/css'>
        h1.has-background,
        h2.has-background,
        h3.has-background,
        h4.has-background,
        h5.has-background,
        h6.has-background {
            padding: 1.25em 2.375em
        }

        h1.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h1.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]),
        h2.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h2.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]),
        h3.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h3.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]),
        h4.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h4.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]),
        h5.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h5.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]),
        h6.has-text-align-left[style*=writing-mode]:where([style*=vertical-lr]),
        h6.has-text-align-right[style*=writing-mode]:where([style*=vertical-rl]) {
            rotate: 180deg
        }
    </style>
    <style id='wp-block-table-inline-css' type='text/css'>
        .wp-block-table {
            overflow-x: auto
        }

        .wp-block-table table {
            border-collapse: collapse;
            width: 100%
        }

        .wp-block-table thead {
            border-bottom: 3px solid
        }

        .wp-block-table tfoot {
            border-top: 3px solid
        }

        .wp-block-table td,
        .wp-block-table th {
            border: 1px solid;
            padding: .5em
        }

        .wp-block-table .has-fixed-layout {
            table-layout: fixed;
            width: 100%
        }

        .wp-block-table .has-fixed-layout td,
        .wp-block-table .has-fixed-layout th {
            word-break: break-word
        }

        .wp-block-table.aligncenter,
        .wp-block-table.alignleft,
        .wp-block-table.alignright {
            display: table;
            width: auto
        }

        .wp-block-table.aligncenter td,
        .wp-block-table.aligncenter th,
        .wp-block-table.alignleft td,
        .wp-block-table.alignleft th,
        .wp-block-table.alignright td,
        .wp-block-table.alignright th {
            word-break: break-word
        }

        .wp-block-table .has-subtle-light-gray-background-color {
            background-color: #f3f4f5
        }

        .wp-block-table .has-subtle-pale-green-background-color {
            background-color: #e9fbe5
        }

        .wp-block-table .has-subtle-pale-blue-background-color {
            background-color: #e7f5fe
        }

        .wp-block-table .has-subtle-pale-pink-background-color {
            background-color: #fcf0ef
        }

        .wp-block-table.is-style-stripes {
            background-color: transparent;
            border-bottom: 1px solid #f0f0f0;
            border-collapse: inherit;
            border-spacing: 0
        }

        .wp-block-table.is-style-stripes tbody tr:nth-child(odd) {
            background-color: #f0f0f0
        }

        .wp-block-table.is-style-stripes.has-subtle-light-gray-background-color tbody tr:nth-child(odd) {
            background-color: #f3f4f5
        }

        .wp-block-table.is-style-stripes.has-subtle-pale-green-background-color tbody tr:nth-child(odd) {
            background-color: #e9fbe5
        }

        .wp-block-table.is-style-stripes.has-subtle-pale-blue-background-color tbody tr:nth-child(odd) {
            background-color: #e7f5fe
        }

        .wp-block-table.is-style-stripes.has-subtle-pale-pink-background-color tbody tr:nth-child(odd) {
            background-color: #fcf0ef
        }

        .wp-block-table.is-style-stripes td,
        .wp-block-table.is-style-stripes th {
            border-color: transparent
        }

        .wp-block-table .has-border-color td,
        .wp-block-table .has-border-color th,
        .wp-block-table .has-border-color tr,
        .wp-block-table .has-border-color>* {
            border-color: inherit
        }

        .wp-block-table table[style*=border-top-color] tr:first-child,
        .wp-block-table table[style*=border-top-color] tr:first-child td,
        .wp-block-table table[style*=border-top-color] tr:first-child th,
        .wp-block-table table[style*=border-top-color]>*,
        .wp-block-table table[style*=border-top-color]>* td,
        .wp-block-table table[style*=border-top-color]>* th {
            border-top-color: inherit
        }

        .wp-block-table table[style*=border-top-color] tr:not(:first-child) {
            border-top-color: currentColor
        }

        .wp-block-table table[style*=border-right-color] td:last-child,
        .wp-block-table table[style*=border-right-color] th,
        .wp-block-table table[style*=border-right-color] tr,
        .wp-block-table table[style*=border-right-color]>* {
            border-right-color: inherit
        }

        .wp-block-table table[style*=border-bottom-color] tr:last-child,
        .wp-block-table table[style*=border-bottom-color] tr:last-child td,
        .wp-block-table table[style*=border-bottom-color] tr:last-child th,
        .wp-block-table table[style*=border-bottom-color]>*,
        .wp-block-table table[style*=border-bottom-color]>* td,
        .wp-block-table table[style*=border-bottom-color]>* th {
            border-bottom-color: inherit
        }

        .wp-block-table table[style*=border-bottom-color] tr:not(:last-child) {
            border-bottom-color: currentColor
        }

        .wp-block-table table[style*=border-left-color] td:first-child,
        .wp-block-table table[style*=border-left-color] th,
        .wp-block-table table[style*=border-left-color] tr,
        .wp-block-table table[style*=border-left-color]>* {
            border-left-color: inherit
        }

        .wp-block-table table[style*=border-style] td,
        .wp-block-table table[style*=border-style] th,
        .wp-block-table table[style*=border-style] tr,
        .wp-block-table table[style*=border-style]>* {
            border-style: inherit
        }

        .wp-block-table table[style*=border-width] td,
        .wp-block-table table[style*=border-width] th,
        .wp-block-table table[style*=border-width] tr,
        .wp-block-table table[style*=border-width]>* {
            border-style: inherit;
            border-width: inherit
        }
    </style>
    <style id='wp-block-list-inline-css' type='text/css'>
        ol,
        ul {
            box-sizing: border-box
        }

        ol.has-background,
        ul.has-background {
            padding: 1.25em 2.375em
        }
    </style>
    <style id='global-styles-inline-css' type='text/css'>
        body {
            --wp--preset--color--black: #000000;
            --wp--preset--color--cyan-bluish-gray: #abb8c3;
            --wp--preset--color--white: #ffffff;
            --wp--preset--color--pale-pink: #f78da7;
            --wp--preset--color--vivid-red: #cf2e2e;
            --wp--preset--color--luminous-vivid-orange: #ff6900;
            --wp--preset--color--luminous-vivid-amber: #fcb900;
            --wp--preset--color--light-green-cyan: #7bdcb5;
            --wp--preset--color--vivid-green-cyan: #00d084;
            --wp--preset--color--pale-cyan-blue: #8ed1fc;
            --wp--preset--color--vivid-cyan-blue: #0693e3;
            --wp--preset--color--vivid-purple: #9b51e0;
            --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
            --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
            --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
            --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
            --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
            --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
            --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
            --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
            --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
            --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
            --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
            --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
            --wp--preset--font-size--small: 13px;
            --wp--preset--font-size--medium: 20px;
            --wp--preset--font-size--large: 36px;
            --wp--preset--font-size--x-large: 42px;
            --wp--preset--spacing--20: 0.44rem;
            --wp--preset--spacing--30: 0.67rem;
            --wp--preset--spacing--40: 1rem;
            --wp--preset--spacing--50: 1.5rem;
            --wp--preset--spacing--60: 2.25rem;
            --wp--preset--spacing--70: 3.38rem;
            --wp--preset--spacing--80: 5.06rem;
            --wp--preset--shadow--natural: 6px 6px 9px rgba(0, 0, 0, 0.2);
            --wp--preset--shadow--deep: 12px 12px 50px rgba(0, 0, 0, 0.4);
            --wp--preset--shadow--sharp: 6px 6px 0px rgba(0, 0, 0, 0.2);
            --wp--preset--shadow--outlined: 6px 6px 0px -3px rgba(255, 255, 255, 1), 6px 6px rgba(0, 0, 0, 1);
            --wp--preset--shadow--crisp: 6px 6px 0px rgba(0, 0, 0, 1);
        }

        :where(.is-layout-flex) {
            gap: 0.5em;
        }

        :where(.is-layout-grid) {
            gap: 0.5em;
        }

        body .is-layout-flow>.alignleft {
            float: left;
            margin-inline-start: 0;
            margin-inline-end: 2em;
        }

        body .is-layout-flow>.alignright {
            float: right;
            margin-inline-start: 2em;
            margin-inline-end: 0;
        }

        body .is-layout-flow>.aligncenter {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained>.alignleft {
            float: left;
            margin-inline-start: 0;
            margin-inline-end: 2em;
        }

        body .is-layout-constrained>.alignright {
            float: right;
            margin-inline-start: 2em;
            margin-inline-end: 0;
        }

        body .is-layout-constrained>.aligncenter {
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained> :where(:not(.alignleft):not(.alignright):not(.alignfull)) {
            max-width: var(--wp--style--global--content-size);
            margin-left: auto !important;
            margin-right: auto !important;
        }

        body .is-layout-constrained>.alignwide {
            max-width: var(--wp--style--global--wide-size);
        }

        body .is-layout-flex {
            display: flex;
        }

        body .is-layout-flex {
            flex-wrap: wrap;
            align-items: center;
        }

        body .is-layout-flex>* {
            margin: 0;
        }

        body .is-layout-grid {
            display: grid;
        }

        body .is-layout-grid>* {
            margin: 0;
        }

        :where(.wp-block-columns.is-layout-flex) {
            gap: 2em;
        }

        :where(.wp-block-columns.is-layout-grid) {
            gap: 2em;
        }

        :where(.wp-block-post-template.is-layout-flex) {
            gap: 1.25em;
        }

        :where(.wp-block-post-template.is-layout-grid) {
            gap: 1.25em;
        }

        .has-black-color {
            color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-color {
            color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-color {
            color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-color {
            color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-color {
            color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-color {
            color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-color {
            color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-color {
            color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-color {
            color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-color {
            color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-color {
            color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-color {
            color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-background-color {
            background-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-background-color {
            background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-background-color {
            background-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-background-color {
            background-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-background-color {
            background-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-background-color {
            background-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-background-color {
            background-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-background-color {
            background-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-background-color {
            background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-background-color {
            background-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-border-color {
            border-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-border-color {
            border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-border-color {
            border-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-border-color {
            border-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-border-color {
            border-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-border-color {
            border-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-border-color {
            border-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-border-color {
            border-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-border-color {
            border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-border-color {
            border-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
            background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
        }

        .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
            background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
        }

        .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-orange-to-vivid-red-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
        }

        .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
            background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
        }

        .has-cool-to-warm-spectrum-gradient-background {
            background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
        }

        .has-blush-light-purple-gradient-background {
            background: var(--wp--preset--gradient--blush-light-purple) !important;
        }

        .has-blush-bordeaux-gradient-background {
            background: var(--wp--preset--gradient--blush-bordeaux) !important;
        }

        .has-luminous-dusk-gradient-background {
            background: var(--wp--preset--gradient--luminous-dusk) !important;
        }

        .has-pale-ocean-gradient-background {
            background: var(--wp--preset--gradient--pale-ocean) !important;
        }

        .has-electric-grass-gradient-background {
            background: var(--wp--preset--gradient--electric-grass) !important;
        }

        .has-midnight-gradient-background {
            background: var(--wp--preset--gradient--midnight) !important;
        }

        .has-small-font-size {
            font-size: var(--wp--preset--font-size--small) !important;
        }

        .has-medium-font-size {
            font-size: var(--wp--preset--font-size--medium) !important;
        }

        .has-large-font-size {
            font-size: var(--wp--preset--font-size--large) !important;
        }

        .has-x-large-font-size {
            font-size: var(--wp--preset--font-size--x-large) !important;
        }
    </style>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/contact-form-7/includes/swv/js/index.js?ver=5.7.7" id="swv-js">
    </script>
    <script type="text/javascript" id="contact-form-7-js-extra">
        /* <![CDATA[ */
        /*swift-is-localization*/
        var wpcf7 = {
            "api": {
                "root": "https:\/\/mowgarden.com\/wp-json\/",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.7.7" id="contact-form-7-js">
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.7.0-wc.7.9.0"
        id="jquery-blockui-js"></script>
    <script type="text/javascript" id="wc-add-to-cart-js-extra">
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "Xem gi\u1ecf h\u00e0ng",
            "cart_url": "https:\/\/mowgarden.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=7.9.0"
        id="wc-add-to-cart-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4-wc.7.9.0"
        id="js-cookie-js"></script>
    <script type="text/javascript" id="woocommerce-js-extra">
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=7.9.0"
        id="woocommerce-js"></script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/dist/vendor/wp-polyfill-inert.min.js?ver=3.1.2"
        id="wp-polyfill-inert-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.14.0"
        id="regenerator-runtime-js"></script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
        id="wp-polyfill-js"></script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/hoverIntent.min.js?ver=1.10.2"
        id="hoverIntent-js"></script>
    <script type="text/javascript" id="flatsome-js-js-extra">
        /* <![CDATA[ */
        /*swift-is-localization*/
        var flatsomeVars = {
            "theme": {
                "version": "3.16.8"
            },
            "ajaxurl": "https:\/\/mowgarden.com\/wp-admin\/admin-ajax.php",
            "rtl": "",
            "sticky_height": "92",
            "assets_url": "https:\/\/mowgarden.com\/wp-content\/themes\/flatsome\/assets\/js\/",
            "lightbox": {
                "close_markup": "<button title=\"%title%\" type=\"button\" class=\"mfp-close\"><svg xmlns=\"http:\/\/www.w3.org\/2000\/svg\" width=\"28\" height=\"28\" viewBox=\"0 0 24 24\" fill=\"none\" stroke=\"currentColor\" stroke-width=\"2\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"feather feather-x\"><line x1=\"18\" y1=\"6\" x2=\"6\" y2=\"18\"><\/line><line x1=\"6\" y1=\"6\" x2=\"18\" y2=\"18\"><\/line><\/svg><\/button>",
                "close_btn_inside": false
            },
            "user": {
                "can_edit_pages": false
            },
            "i18n": {
                "mainMenu": "Main Menu",
                "toggleButton": "Toggle"
            },
            "options": {
                "cookie_notice_version": "1",
                "swatches_layout": false,
                "swatches_box_select_event": false,
                "swatches_box_behavior_selected": false,
                "swatches_box_update_urls": "1",
                "swatches_box_reset": false,
                "swatches_box_reset_extent": false,
                "swatches_box_reset_time": 300,
                "search_result_latency": "0"
            },
            "is_mini_cart_reveal": "1"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/themes/flatsome/assets/js/flatsome.js?ver=c8ede7f4aa030cb285ae3350d627d9fd"
        id="flatsome-js-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/themes/flatsome/inc/integrations/wc-yith-wishlist/wishlist.js?ver=3.10.2"
        id="flatsome-woocommerce-wishlist-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/themes/flatsome/inc/extensions/flatsome-live-search/flatsome-live-search.js?ver=3.16.8"
        id="flatsome-live-search-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/themes/flatsome/assets/js/extensions/flatsome-swatches-frontend.js?ver=3.16.8"
        id="flatsome-swatches-frontend-js"></script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/themes/flatsome/assets/js/woocommerce.js?ver=7cf4045c21263ccd7a48216435319d6f"
        id="flatsome-theme-woocommerce-js-js"></script> <!--[if IE]> <script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/intersection-observer-polyfill@0.1.0/dist/IntersectionObserver.js?ver=0.1.0"
        id="intersection-observer-polyfill-js"></script> <![endif]-->
    <!--[if IE]> <script type="text/javascript"
        src="https://cdn.jsdelivr.net/gh/nuxodin/ie11CustomProperties@4.0.1/ie11CustomProperties.min.js?ver=4.0.1"
        id="css-vars-polyfill-js"></script> <![endif]-->
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.selectBox.min.js?ver=1.2.0"
        id="jquery-selectBox-js"></script>
    <script type="text/javascript"
        src="//mowgarden.com/wp-content/plugins/woocommerce/assets/js/prettyPhoto/jquery.prettyPhoto.min.js?ver=3.1.6"
        id="prettyPhoto-js"></script>
    <script type="text/javascript" id="jquery-yith-wcwl-js-extra">
        /* <![CDATA[ */
        /*swift-is-localization*/
        var yith_wcwl_l10n = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "redirect_to_cart": "no",
            "yith_wcwl_button_position": "shortcode",
            "multi_wishlist": "",
            "hide_add_button": "1",
            "enable_ajax_loading": "",
            "ajax_loader_url": "https:\/\/mowgarden.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader-alt.svg",
            "remove_from_wishlist_after_add_to_cart": "1",
            "is_wishlist_responsive": "1",
            "time_to_close_prettyphoto": "3000",
            "fragments_index_glue": ".",
            "reload_on_found_variation": "1",
            "mobile_media_query": "768",
            "labels": {
                "cookie_disabled": "We are sorry, but this feature is available only if cookies on your browser are enabled.",
                "added_to_cart_message": "<div class=\"woocommerce-notices-wrapper\"><div class=\"woocommerce-message\" role=\"alert\">Product added to cart successfully<\/div><\/div>"
            },
            "actions": {
                "add_to_wishlist_action": "add_to_wishlist",
                "remove_from_wishlist_action": "remove_from_wishlist",
                "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem",
                "load_mobile_action": "load_mobile",
                "delete_item_action": "delete_item",
                "save_title_action": "save_title",
                "save_privacy_action": "save_privacy",
                "load_fragments": "load_fragments"
            },
            "nonce": {
                "add_to_wishlist_nonce": "523ba72503",
                "remove_from_wishlist_nonce": "e5f421f781",
                "reload_wishlist_and_adding_elem_nonce": "a75e68be31",
                "load_mobile_nonce": "bc754a6ced",
                "delete_item_nonce": "f75f37dc05",
                "save_title_nonce": "aaa50551ff",
                "save_privacy_nonce": "8be00c4a1d",
                "load_fragments_nonce": "679b3ef4cc"
            },
            "redirect_after_ask_estimate": "",
            "ask_estimate_redirect_url": "https:\/\/mowgarden.com"
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/yith-woocommerce-wishlist/assets/js/jquery.yith-wcwl.min.js?ver=3.23.0"
        id="jquery-yith-wcwl-js"></script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/underscore.min.js?ver=1.13.4"
        id="underscore-js"></script>
    <script type="text/javascript" id="wp-util-js-extra">
        /* <![CDATA[ */
        /*swift-is-localization*/
        var _wpUtilSettings = {
            "ajax": {
                "url": "\/wp-admin\/admin-ajax.php"
            }
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="https://mowgarden.com/wp-includes/js/wp-util.min.js?ver=6.4.1" id="wp-util-js">
    </script>
    <script type="text/javascript" id="wc-add-to-cart-variation-js-extra">
        /* <![CDATA[ */
        var wc_add_to_cart_variation_params = {
            "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
            "i18n_no_matching_variations_text": "R\u1ea5t ti\u1ebfc, kh\u00f4ng c\u00f3 s\u1ea3n ph\u1ea9m n\u00e0o ph\u00f9 h\u1ee3p v\u1edbi l\u1ef1a ch\u1ecdn c\u1ee7a b\u1ea1n. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c.",
            "i18n_make_a_selection_text": "Ch\u1ecdn c\u00e1c t\u00f9y ch\u1ecdn cho s\u1ea3n ph\u1ea9m tr\u01b0\u1edbc khi cho s\u1ea3n ph\u1ea9m v\u00e0o gi\u1ecf h\u00e0ng c\u1ee7a b\u1ea1n.",
            "i18n_unavailable_text": "R\u1ea5t ti\u1ebfc, s\u1ea3n ph\u1ea9m n\u00e0y hi\u1ec7n kh\u00f4ng t\u1ed3n t\u1ea1i. H\u00e3y ch\u1ecdn m\u1ed9t ph\u01b0\u01a1ng th\u1ee9c k\u1ebft h\u1ee3p kh\u00e1c."
        };
        /* ]]> */
    </script>
    <script type="text/javascript"
        src="https://mowgarden.com/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart-variation.min.js?ver=7.9.0"
        id="wc-add-to-cart-variation-js"></script>
    <script>
        window.sp_lazyload_ga_buffer = [];

        function ga(action, type, event_category, event_action, event_label, event_value) {
            action = action || "";
            type = type || "";
            event_category = event_category || "";
            event_action = event_action || "";
            event_label = event_label || "";
            event_value = event_value || "";
            window.sp_lazyload_ga_buffer.push({
                "action": action,
                "type": type,
                "event_category": event_category,
                "event_action": event_action,
                "event_label": event_label,
                "event_value": event_value
            });
        }
        document.addEventListener("DOMContentLoaded", function(event) {
            ga("send", "pageview");
        });
        (function() {
            function fire() {
                window.removeEventListener("touchstart", fire);
                window.removeEventListener("scroll", fire);
                document.removeEventListener("mousemove", fire);
                window.ga = function(action, type, event_category, event_action, event_label, event_value) {
                    function q(key) {
                        return (window.location.search.replace(new RegExp("^(?:.*[&\?]" + encodeURIComponent(key)
                            .replace(/[\.\+\*]/g, "\$&") + "(?:\=([^&]*))?)?.*$", "i"), "$1"));
                    }
                    var img = new Image();
                    var ec = event_category || "";
                    var ea = event_action || "";
                    var el = event_label || "";
                    var ev = event_value || 0;
                    var event_parameters = "";
                    if (type == "event") {
                        event_parameters = "&ec=" + encodeURIComponent(ec) + "&ea=" + encodeURIComponent(ea) +
                            "&el=" + encodeURIComponent(el) + "&ev=" + encodeURIComponent(ev);
                    }
                    img.src = " https://mowgarden.com?swift-performance-tracking=1&spr=" + parseInt(Math.random() *
                            1000000000) + "&t=" + type + "&dp=" + encodeURIComponent(window.location.pathname) +
                        "&dr=" + encodeURIComponent(document.referrer) + "&cs=" + q(" utm_source") + "&cm=" + q(
                            " utm_medium") + "&cn=" + q(" utm_campaign") + "&ck=" + q(" utm_term") + "&cc=" + q(
                            " utm_content") + "&gclid=" + q(" gclid") + "&dclid=" + q(" dclid") + "&sr=" + window
                        .screen.availWidth + " x" + window.screen.availHeight + "&vp=" + window.innerWidth + " x" +
                        window.innerHeight + "&de=" + document.charset + "&sd=" + screen.colorDepth + "-bits" +
                        "&dt=" + encodeURIComponent(document.title) + event_parameters;
                };
                while (window.sp_lazyload_ga_buffer.length > 0) {
                    var r = window.sp_lazyload_ga_buffer.shift();
                    if (typeof r !== " undefined") {
                        window.ga(r["action"], r["type"], r["event_category"], r["event_action"], r["event_label"], r[
                            "event_value"]);
                    }
                }
            }
            window.addEventListener("load", function() {
                window.addEventListener("touchstart", fire);
                window.addEventListener("scroll", fire);
                document.addEventListener("mousemove", fire);
            });
        })();
    </script> <!--Cached with Swift Performance-->
</body>

</html>
