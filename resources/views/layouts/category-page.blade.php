<section class="section" id="section_1058688093">
    <div class="bg section-bg fill bg-fill bg-loaded">
        <div class="section-bg-overlay absolute fill"></div><button
            class="scroll-for-more z-5 icon absolute bottom h-center" aria-label="Scroll for more"><i
                class="icon-angle-down" style="font-size:42px;"></i></button>
    </div>
    <div class="section-content relative">
        <div class="row align-middle align-center" style="max-width:" id="row-2008724597">
            <div id="col-859550080" class="col medium-8 small-12 large-8">
                <div class="col-inner text-center">
                    <div id="text-1893335258" class="text">
                        <h2 class="uppercase">Cây trồng ở vườn</h2>
                        <p class="thin-font">Giúp cho ngôi nhà của bạn thêm tươi mới và xanh với những sản phẩm cây
                            trong nhà của Cào Cào Garden. Thật tuyệt vời mang đến cho bạn những phút giây thư giãn bên canh
                            nhữngsản phẩm cây xanh chất lượng.</p>
                        <p>
                            <style>
                                #text-1893335258 {
                                    font-size: 1.25rem;
                                    text-align: center;
                                    color: rgb(255, 255, 255);
                                }

                                #text-1893335258>* {
                                    color: rgb(255, 255, 255);
                                }
                            </style>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #section_1058688093 {
            padding-top: 80px;
            padding-bottom: 80px;
            min-height: 40px;
        }

        #section_1058688093 .section-bg-overlay {
            background-color: rgba(0, 0, 0, 0.18);
        }

        #section_1058688093 .section-bg.bg-loaded {
            background-image: url(https://mowgarden.com/wp-content/uploads/2021/07/danh-muc-canh-trong-trong-nha.jpg);
        }

        #section_1058688093 .section-bg {
            background-position: 65% 40%;
        }

        #section_1058688093 .ux-shape-divider--top svg {
            height: 150px;
            --divider-top-width: 100%;
        }

        #section_1058688093 .ux-shape-divider--bottom svg {
            height: 150px;
            --divider-width: 100%;
        }
    </style>
</section>
