<section class="section" id="section_783497120">
    <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
    <div class="section-content relative">
        <div id="text-3375947987" class="text">
            <h2>DANH MỤC SẢN PHẨM</h2>
            <style>
                #text-3375947987 {
                    text-align: center;
                }
            </style>
        </div>
        <div id="gap-100296375" class="gap-element clearfix" style="display:block; height:auto;">
            <style>
                #gap-100296375 {
                    padding-top: 30px;
                }
            </style>
        </div>
        <div class="row row-small align-middle align-center" id="row-1700954491">
            <div id="col-601079787" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-924949894">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-de-trong-trong-nha/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-471838726"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây dễ
                                                    chăm</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-471838726 {
                                            width: 60%;
                                        }

                                        #text-box-471838726 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-924949894 {
                                padding-top: 99.99%;
                            }

                            #banner-924949894 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-canh-de-trong-400x400.jpg);
                            }

                            #banner-924949894 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-924949894 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-1726728275" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-1830869770">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-canh-van-phong/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-1405025062"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây văn
                                                    phòng</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-1405025062 {
                                            width: 60%;
                                        }

                                        #text-box-1405025062 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-1830869770 {
                                padding-top: 99.99%;
                            }

                            #banner-1830869770 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-trong-hanh-lang-loi-di-400x400.jpg);
                            }

                            #banner-1830869770 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-1830869770 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-1372616500" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-1287502275">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-phong-thuy/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-762434120"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây
                                                    phong thủy</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-762434120 {
                                            width: 60%;
                                        }

                                        #text-box-762434120 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-1287502275 {
                                padding-top: 99.99%;
                            }

                            #banner-1287502275 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-kieng-la-trong-nha-400x400.jpg);
                            }

                            #banner-1287502275 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-1287502275 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-388414018" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-1672980196">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-canh-de-ban/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-17273610"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây để
                                                    bàn</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-17273610 {
                                            width: 60%;
                                        }

                                        #text-box-17273610 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-1672980196 {
                                padding-top: 99.99%;
                            }

                            #banner-1672980196 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-canh-de-ban-400x400.jpg);
                            }

                            #banner-1672980196 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-1672980196 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <style>
                #row-1700954491>.col>.col-inner {
                    background-color: rgb(255, 255, 255);
                }
            </style>
        </div>
        <div class="row row-small" id="row-264550451">
            <div id="col-1385369735" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-1809203946">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-thuy-sinh/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-1407210415"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây
                                                    trồng nước</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-1407210415 {
                                            width: 60%;
                                        }

                                        #text-box-1407210415 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-1809203946 {
                                padding-top: 99.99%;
                            }

                            #banner-1809203946 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-canh-thuy-canh-400x400.jpg);
                            }

                            #banner-1809203946 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-1809203946 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-1498328407" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-302450244">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/cay-kieng-la/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-1551654813"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Cây cao
                                                    cấp</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-1551654813 {
                                            width: 60%;
                                        }

                                        #text-box-1551654813 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-302450244 {
                                padding-top: 99.99%;
                            }

                            #banner-302450244 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/cay-canh-cao-cap-trong-nha-400x400.jpg);
                            }

                            #banner-302450244 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-302450244 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-802688908" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-492174457">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/chau-dat-nung/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-1206851756"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Chậu đất
                                                    nung</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-1206851756 {
                                            width: 60%;
                                        }

                                        #text-box-1206851756 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-492174457 {
                                padding-top: 99.99%;
                            }

                            #banner-492174457 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/chau-dat-nung-dep-400x400.jpg);
                            }

                            #banner-492174457 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-492174457 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div id="col-656926554" class="col medium-6 small-6 large-3">
                <div class="col-inner">
                    <div class="is-border"
                        style="border-color:rgb(232, 232, 232);border-width:2px 2px 2px 2px;margin:5px -5px -5px 5px;">
                    </div>
                    <div class="banner has-hover" id="banner-390484809">
                        <div class="banner-inner fill">
                            <div class="banner-bg fill">
                                <div class="bg fill bg-fill bg-loaded"></div>
                            </div>
                            <div class="banner-layers container"> <a class="fill"
                                    href="https://mowgarden.com/chau-xi-mang/">
                                    <div class="fill banner-link"></div>
                                </a>
                                <div id="text-box-406355119"
                                    class="text-box banner-layer x50 md-x50 lg-x50 y95 md-y95 lg-y95 res-text">
                                    <div class="text-box-content text dark">
                                        <div class="text-inner text-center"> <a
                                                class="button primary is-small expand"> <span>Chậu xi
                                                    măng</span> </a> </div>
                                    </div>
                                    <style>
                                        #text-box-406355119 {
                                            width: 60%;
                                        }

                                        #text-box-406355119 .text-box-content {
                                            font-size: 202%;
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                        <style>
                            #banner-390484809 {
                                padding-top: 99.99%;
                            }

                            #banner-390484809 .bg.bg-loaded {
                                background-image: url(https://mowgarden.com/wp-content/uploads/2021/05/chau-cay-trong-nha-dep-380x400.jpg);
                            }

                            #banner-390484809 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #banner-390484809 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <style>
                #row-264550451>.col>.col-inner {
                    background-color: rgb(255, 255, 255);
                }
            </style>
        </div>
    </div>
    <style>
        #section_783497120 {
            padding-top: 50px;
            padding-bottom: 50px;
            background-color: rgb(248, 243, 231);
        }

        #section_783497120 .ux-shape-divider--top svg {
            height: 150px;
            --divider-top-width: 100%;
        }

        #section_783497120 .ux-shape-divider--bottom svg {
            height: 150px;
            --divider-width: 100%;
        }
    </style>
</section>