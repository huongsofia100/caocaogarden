@extends('layouts.layout-comon')

@section('maincontent')
<div id="content" role="main" class="content-area">
    <section class="section" id="section_1994146099">
        <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
        <div class="section-content relative">
            <div class="row row-collapse row-full-width" id="row-1135636684">
                <div id="col-1802192567" class="col small-12 large-12">
                    <div class="col-inner">
                        <div class="slider-wrapper relative" id="slider-1749999889">
                            <div class="slider slider-nav-simple slider-nav-large slider-nav-dark slider-style-normal"
                                data-flickity-options='{ "cellAlign": "left", "imagesLoaded": true, "lazyLoad": 1, "freeScroll": false, "wrapAround": true, "autoPlay": 4000, "pauseAutoPlayOnHover" : false, "prevNextButtons": false, "contain" : true, "adaptiveHeight" : true, "dragThreshold" : 10, "percentPosition": true, "pageDots": true, "rightToLeft": false, "draggable": true, "selectedAttraction": 0.1, "parallax" : 4, "friction": 0.6 }'>
                                <div class="banner has-hover" id="banner-240105624">
                                    <div class="banner-inner fill">
                                        <div class="banner-bg fill">
                                            <div class="bg fill bg-fill bg-loaded"></div>
                                        </div>
                                        <div class="banner-layers container">
                                            <div class="fill banner-link"></div>
                                            <div id="text-box-166489488"
                                                class="text-box banner-layer hide-for-medium x15 md-x15 lg-x15 y50 md-y50 lg-y50 res-text">
                                                <div class="text-box-content text dark">
                                                    <div class="text-inner text-center">
                                                        <div id="text-2975689329" class="text">
                                                            <p class="has-white-color has-text-color"
                                                                style="text-align: left;"><span
                                                                    style="font-size: 140%; color: #224229; font-family: 'times new roman', times, serif;"><strong>Tận
                                                                        hưởng không gian sống
                                                                        xanh</strong></span></p>
                                                            <p class="has-white-color has-text-color"
                                                                style="text-align: justify;"><span
                                                                    style="color: #224229; font-size: 100%;">Bổ
                                                                    sung thêm cây xanh là một cách đơn giản
                                                                    nhất để tạo ra sự thoải mái cho không
                                                                    gian sống của bạn, giúp mang lại hiệu
                                                                    quả công việc và thư giãn mỗi khi trở
                                                                    về</span></p>
                                                            <style>
                                                                #text-2975689329 {
                                                                    font-size: 1.5rem;
                                                                }
                                                            </style>
                                                        </div> <a href="tel:0966889393" target="_self"
                                                            class="button primary is-outline"
                                                            style="border-radius:99px;"> <span>liên
                                                                hệ</span> </a> <a
                                                            href="https://mowgarden.com/ban-cay-canh-trong-nha/"
                                                            target="_self" class="button primary"
                                                            style="border-radius:99px;"> <span>mua
                                                                ngay</span> </a>
                                                    </div>
                                                </div>
                                                <style>
                                                    #text-box-166489488 .text-box-content {
                                                        background-color: rgba(255, 255, 255, 0.709);
                                                        font-size: 100%;
                                                    }

                                                    #text-box-166489488 .text-inner {
                                                        padding: 20px 35px 20px 35px;
                                                    }

                                                    #text-box-166489488 {
                                                        width: 45%;
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        #banner-240105624 {
                                            padding-top: 50%;
                                        }

                                        #banner-240105624 .bg.bg-loaded {
                                            background-image: url(https://mowgarden.com/wp-content/uploads/2022/09/mowgarden-shop-ban-cay-canh.jpg);
                                        }

                                        #banner-240105624 .bg {
                                            background-position: 49% 75%;
                                        }

                                        #banner-240105624 .ux-shape-divider--top svg {
                                            height: 150px;
                                            --divider-top-width: 100%;
                                        }

                                        #banner-240105624 .ux-shape-divider--bottom svg {
                                            height: 150px;
                                            --divider-width: 100%;
                                        }
                                    </style>
                                </div>
                            </div>
                            <div class="loading-spin dark large centered"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #section_1994146099 {
                padding-top: 0px;
                padding-bottom: 0px;
            }

            #section_1994146099 .ux-shape-divider--top svg {
                height: 150px;
                --divider-top-width: 100%;
            }

            #section_1994146099 .ux-shape-divider--bottom svg {
                height: 150px;
                --divider-width: 100%;
            }
        </style>
    </section>

    @include('layouts.product-category')

    <div id="gap-1546182606" class="gap-element clearfix" style="display:block; height:auto;">
        <style>
            #gap-1546182606 {
                padding-top: 50px;
            }
        </style>
    </div>

    @include('layouts.new-product')

    @include('layouts.plant-pot-product')

    <div id="gap-1104394625" class="gap-element clearfix" style="display:block; height:auto;">
        <style>
            #gap-1104394625 {
                padding-top: 10px;
            }
        </style>
    </div>
    
    @include('layouts.office-plant-product')

    @include('layouts.pots')

    <section class="section" id="section_529538225">
        <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
        <div class="section-content relative">
            <div id="gap-870948975" class="gap-element clearfix" style="display:block; height:auto;">
                <style>
                    #gap-870948975 {
                        padding-top: 30px;
                    }
                </style>
            </div>
            <div class="row row-full-width align-center" id="row-1859045414">
                <div id="col-1458409718" class="col small-12 large-12">
                    <div class="col-inner">
                        <div class="banner has-hover hide-for-medium" id="banner-257559589">
                            <div class="banner-inner fill">
                                <div class="banner-bg fill">
                                    <div class="bg fill bg-fill bg-loaded"></div>
                                    <div class="is-border"
                                        style="border-color:rgb(252, 249, 243);border-width:0px 0px 0px 0px;margin:0px 0px 0px -50px;">
                                    </div>
                                </div>
                                <div class="banner-layers container">
                                    <div class="fill banner-link"></div>
                                    <div id="text-box-1934426755"
                                        class="text-box banner-layer hide-for-medium x100 md-x100 lg-x100 y0 md-y50 lg-y50 res-text">
                                        <div class="text-box-content text">
                                            <div class="text-inner text-center">
                                                <div id="text-3418929906" class="text">
                                                    <h2><span
                                                            style="font-size: 45%; color: #224229; font-family: 'book antiqua', palatino, serif;"><strong>Lý
                                                                do chọn MOW Garden?</strong></span></h2>
                                                    <style>
                                                        #text-3418929906 {
                                                            font-size: 1.8rem;
                                                        }
                                                    </style>
                                                </div>
                                                <div id="gap-1016679100" class="gap-element clearfix"
                                                    style="display:block; height:auto;">
                                                    <style>
                                                        #gap-1016679100 {
                                                            padding-top: 18px;
                                                        }
                                                    </style>
                                                </div>
                                                <div class="row" id="row-380110800">
                                                    <div id="col-733619649"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2022/01/soil.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="soil"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/soil.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/soil-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2022/01/soil-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/soil-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: justify;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">tuyển
                                                                                    chọn</span></strong></span>
                                                                    </p>
                                                                    <p style="text-align: left;"><span
                                                                            style="color: #224229; font-size: 100%;">Mọi
                                                                            cây xanh đều phải được chọn lọc
                                                                            kỹ lưỡng</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="col-1692504999"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2021/04/018-watering.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="018 watering"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2021/04/018-watering.png 512w, https://mowgarden.com/wp-content/uploads/2021/04/018-watering-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2021/04/018-watering-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2021/04/018-watering-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: left;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">đa
                                                                                    dạng</span></strong></span>
                                                                    </p>
                                                                    <p style="text-align: left;"><span
                                                                            style="font-size: 100%; color: #224229;">Dễ
                                                                            dàng tìm được sản phẩm mà bạn
                                                                            mong muốn</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="col-357862328"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2022/01/gardening-1.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="gardening 1"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/gardening-1.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-1-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-1-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-1-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: left;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">Đồng
                                                                                    Hành</span></strong></span>
                                                                    </p>
                                                                    <p><span
                                                                            style="font-size: 100%; color: #224229;">Luôn
                                                                            đồng hành và giúp đỡ bạn về mặt
                                                                            kỹ thuật</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row" id="row-2119381216">
                                                    <div id="col-2046145626"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2022/01/gardener.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="gardener"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/gardener.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/gardener-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2022/01/gardener-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/gardener-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: left;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">Đúng
                                                                                    chuẩn</span></strong></span>
                                                                    </p>
                                                                    <p><span
                                                                            style="font-size: 100%; color: #224229;">Sử
                                                                            dụng hình ảnh chụp thực tế giúp
                                                                            dễ hình dung</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="col-1719352443"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2022/01/gardening-2.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="gardening 2"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/gardening-2.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-2-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-2-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/gardening-2-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: left;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">Tin
                                                                                    cậy</span></strong></span>
                                                                    </p>
                                                                    <p><span
                                                                            style="font-size: 100%; color: #224229;">Gửi
                                                                            ảnh thực tế và cụ thể trước khi
                                                                            giao hàng</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="col-600783435"
                                                        class="col medium-4 small-12 large-4">
                                                        <div class="col-inner">
                                                            <div class="icon-box featured-box icon-box-left text-left"
                                                                style="margin:0px 0px 0px 0px;">
                                                                <div class="icon-box-img"
                                                                    style="width: 45px">
                                                                    <div class="icon">
                                                                        <div class="icon-inner"
                                                                            style="color:rgb(34, 66, 41);">
                                                                            <img loading="lazy"
                                                                                decoding="async"
                                                                                width="512"
                                                                                height="512"
                                                                                src="https://mowgarden.com/wp-content/uploads/2021/04/006-turf.png"
                                                                                class="attachment-medium size-medium"
                                                                                alt="006 turf"
                                                                                srcset="https://mowgarden.com/wp-content/uploads/2021/04/006-turf.png 512w, https://mowgarden.com/wp-content/uploads/2021/04/006-turf-125x125.png 125w, https://mowgarden.com/wp-content/uploads/2021/04/006-turf-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2021/04/006-turf-20x20.png 20w"
                                                                                sizes="(max-width: 512px) 100vw, 512px">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon-box-text last-reset">
                                                                    <p class="uppercase"
                                                                        style="text-align: left;"><span
                                                                            style="font-size: 110%;"><strong><span
                                                                                    style="color: #224229;">Cạnh
                                                                                    tranh</span></strong></span>
                                                                    </p>
                                                                    <p><span
                                                                            style="font-size: 100%; color: #224229;">Tối
                                                                            ưu hóa ngân sách nhờ mức giá cực
                                                                            kì cạnh tranh</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <style>
                                            #text-box-1934426755 .text-inner {
                                                padding: 0px 0px 0px 11px;
                                            }

                                            #text-box-1934426755 {
                                                width: 92%;
                                            }

                                            #text-box-1934426755 .text-box-content {
                                                font-size: 100%;
                                            }

                                            @media (min-width:550px) {
                                                #text-box-1934426755 {
                                                    width: 61%;
                                                }
                                            }
                                        </style>
                                    </div>
                                    <div class="img has-hover x100 md-x0 lg-x0 y100 md-y50 lg-y50"
                                        id="image_1637349686">
                                        <div class="img-inner image-cover dark"
                                            style="padding-top:177%;margin:0px 0px 0px -120px;"> <img
                                                loading="lazy" decoding="async" width="1500"
                                                height="1500"
                                                src="https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden.jpg"
                                                class="attachment-original size-original"
                                                alt="vuon cay trong nha mowgarden"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden.jpg 1500w, https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden-800x800.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/08/vuon-cay-trong-nha-mowgarden-20x20.jpg 20w"
                                                sizes="(max-width: 1500px) 100vw, 1500px"> </div>
                                        <style>
                                            #image_1637349686 {
                                                width: 73%;
                                            }

                                            @media (min-width:550px) {
                                                #image_1637349686 {
                                                    width: 32%;
                                                }
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                            <style>
                                #banner-257559589 {
                                    padding-top: 1000px;
                                    background-color: rgb(248, 243, 231);
                                }

                                #banner-257559589 .ux-shape-divider--top svg {
                                    height: 150px;
                                    --divider-top-width: 100%;
                                }

                                #banner-257559589 .ux-shape-divider--bottom svg {
                                    height: 150px;
                                    --divider-width: 100%;
                                }

                                @media (min-width:550px) {
                                    #banner-257559589 {
                                        padding-top: 600px;
                                    }
                                }
                            </style>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            #section_529538225 {
                padding-top: 20px;
                padding-bottom: 20px;
                background-color: rgb(248, 243, 231);
            }

            #section_529538225 .ux-shape-divider--top svg {
                height: 150px;
                --divider-top-width: 100%;
            }

            #section_529538225 .ux-shape-divider--bottom svg {
                height: 150px;
                --divider-width: 100%;
            }
        </style>
    </section>
    <section class="section hide-for-medium" id="section_1388215821">
        <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
        <div class="section-content relative">
            <div class="row" id="row-1921059595">
                <div id="col-1785479453" class="col small-12 large-12">
                    <div class="col-inner text-center">
                        <div id="text-582599194" class="text">
                            <p><span
                                    style="font-size: 140%; color: #224229; font-family: 'book antiqua', palatino, serif;"><strong>Khách
                                        Hàng Nói Về MOW Garden</strong></span></p>
                            <style>
                                #text-582599194 {
                                    font-size: 1.2rem;
                                }
                            </style>
                        </div>
                    </div>
                    <style>
                        #col-1785479453>.col-inner {
                            margin: 20px 0px 0px 0px;
                        }
                    </style>
                </div>
            </div>
            <div class="row align-equal" id="row-1838763917">
                <div id="col-164222833" class="col medium-6 small-12 large-6">
                    <div class="col-inner">
                        <blockquote style="font-size: 100%; font-style: normal;">
                            <p><span style="color: #224229;">Mình mua của shop 2 lần rồi, phải nói là rất
                                    cây rất đẹp luôn ấy. Nhỏ nhỏ,dễ thương, xanh tốt Đóng gói rất là kĩ
                                    càng. Thời gian giao hàng thì rất nhanh. Mình thấy rất yên tâm khi mua
                                    cây ở shop Mọi người nên mua nhé</span></p>
                        </blockquote>
                        <div class="icon-box featured-box circle icon-box-left text-left is-small">
                            <div class="icon-box-img" style="width: 50px">
                                <div class="icon">
                                    <div class="icon-inner"> <img loading="lazy" decoding="async"
                                            width="205" height="205"
                                            src="https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-thale-hoahongsaigon1.jpg"
                                            class="attachment-medium size-medium"
                                            alt="khach hang thale hoahongsaigon1"
                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-thale-hoahongsaigon1.jpg 205w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-thale-hoahongsaigon1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-thale-hoahongsaigon1-20x20.jpg 20w"
                                            sizes="(max-width: 205px) 100vw, 205px"> </div>
                                </div>
                            </div>
                            <div class="icon-box-text last-reset">
                                <p style="margin-bottom: 0px;"><span
                                        style="color: #224229; font-size: 120%;"><strong>Thành
                                            Lễ</strong></span></p>
                                <p><span style="font-size: 110%; color: #224229;">Mẫu ảnh nam,
                                        Freelancer</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="col-975204342" class="col medium-6 small-12 large-6">
                    <div class="col-inner">
                        <blockquote style="font-size: 100%; font-style: normal;">
                            <p><span style="color: #224229;">Mua của shop cây nào về cg đẹp á? Giá lại rẻ
                                    nữa, đóng gói quá chắc chắn, chuyên nghiệp hihi, sẽ lại mua nếu có tiền,
                                    giao hàng siêu nhanh, thích cực</span></p>
                        </blockquote>
                        <div class="icon-box featured-box circle icon-box-left text-left is-small">
                            <div class="icon-box-img" style="width: 50px">
                                <div class="icon">
                                    <div class="icon-inner"> <img loading="lazy" decoding="async"
                                            width="800" height="800"
                                            src="https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-800x800.jpg"
                                            class="attachment-medium size-medium"
                                            alt="khach hang mua hoahongsaigon"
                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-800x800.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/01/khach-hang-mua-hoahongsaigon.jpg 960w"
                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                </div>
                            </div>
                            <div class="icon-box-text last-reset">
                                <p style="margin-bottom: 0px;"><span
                                        style="color: #224229; font-size: 120%;"><strong>Phương
                                            Nga</strong></span></p>
                                <p><span style="color: #224229; font-size: 110%;">Nhân viên văn
                                        phòng</span></p>
                            </div>
                        </div>
                    </div>
                </div>
                <style>
                    #row-1838763917>.col>.col-inner {
                        padding: 40px 40px 40px 40px;
                        background-color: rgb(255, 255, 255);
                    }
                </style>
            </div>
        </div>
        <style>
            #section_1388215821 {
                padding-top: 5px;
                padding-bottom: 5px;
                background-color: rgb(252, 249, 243);
            }

            #section_1388215821 .ux-shape-divider--top svg {
                height: 150px;
                --divider-top-width: 100%;
            }

            #section_1388215821 .ux-shape-divider--bottom svg {
                height: 150px;
                --divider-width: 100%;
            }
        </style>
    </section>
    
    @include('layouts.infor-blog')
    
</div>
@endsection