@extends('layouts.layout-comon')

@section('headerproduct')
    <div class="shop-page-title category-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="is-larger">
                    <nav class="rank-math-breadcrumb breadcrumbs">
                        <p><a href="https://mowgarden.com">Trang chủ</a><span class="separator"> » </span><span
                                class="last">Phụ Kiện Trang Trí</span></p>
                    </nav>
                </div>
                <div class="category-filtering category-filter-row"> <a href="#" data-open="#shop-sidebar"
                        data-pos="left" class="filter-button uppercase plain"> <i class="icon-equalizer"></i> <strong>Chọn
                            danh mục</strong> </a>
                    <div class="inline-block"> </div>
                </div>
            </div>
            <div class="flex-col medium-text-center">
                <p class="woocommerce-result-count hide-for-medium"> Hiển thị 1–48 của 188 kết quả</p>
                <form class="woocommerce-ordering" method="get"> <select name="orderby" class="orderby"
                        aria-label="Đơn hàng của cửa hàng">
                        <option value="popularity">Thứ tự theo mức độ phổ biến</option>
                        <option value="rating">Thứ tự theo điểm đánh giá</option>
                        <option value="date" selected="selected">Mới nhất</option>
                        <option value="price">Thứ tự theo giá: thấp đến cao</option>
                        <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                    </select> <input type="hidden" name="paged" value="1"> </form>
            </div>
        </div>
    </div>
@endsection

@section('maincontent')
    <div class="row category-page-row">
        <div class="col large-12">
            <div class="shop-container">
                <div class="woocommerce-notices-wrapper"></div>
                <div
                    class="products row row-small large-columns-4 medium-columns-4 small-columns-2 has-shadow row-box-shadow-2-hover has-equal-box-heights equalize-box">
                    <div
                        class="product-small col has-hover product type-product post-11733 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-chau-au-pknc035/"
                                            aria-label="Phụ kiện trang trí biệt thự Châu Âu PKNC035"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-biet-thu-chau-au-2-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon" aria-label="Wishlist">
                                                <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11733 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11733"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11733,&quot;parent_product_id&quot;:11733,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11733&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11733" data-product-type="simple"
                                                            data-original-product-id="11733" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11733" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11733" data-product_sku="PKNC035"
                                            aria-label="Thêm “Phụ kiện trang trí biệt thự Châu Âu PKNC035” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11733" href="#quick-view">Xem
                                            nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-chau-au-pknc035/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí biệt thự Châu Âu PKNC035</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>50.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11730 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-vuon-cay-pknc034/"
                                            aria-label="Phụ kiện trang trí biệt thự vườn cây PKNC034"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/33-biet-thu-vuon-cay-2-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11730 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11730"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11730,&quot;parent_product_id&quot;:11730,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11730&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11730" data-product-type="simple"
                                                            data-original-product-id="11730" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11730" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11730" data-product_sku="PKNC034"
                                            aria-label="Thêm “Phụ kiện trang trí biệt thự vườn cây PKNC034” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11730"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-vuon-cay-pknc034/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí biệt thự vườn cây PKNC034</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>30.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11726 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-pknc033/"
                                            aria-label="Phụ kiện trang trí nhà mái rơm PKNC033"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/32-nha-mai-la-3-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11726 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11726"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11726,&quot;parent_product_id&quot;:11726,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11726&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11726" data-product-type="simple"
                                                            data-original-product-id="11726" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11726" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11726" data-product_sku="PKNC033"
                                            aria-label="Thêm “Phụ kiện trang trí nhà mái rơm PKNC033” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11726"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-pknc033/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà mái rơm PKNC033</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>5.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11722 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-lau-dai-da-pknc032/"
                                            aria-label="Phụ kiện trang trí lâu đài đá PKNC032"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/31-lau-dai-da-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11722 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11722"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11722,&quot;parent_product_id&quot;:11722,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11722&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11722" data-product-type="simple"
                                                            data-original-product-id="11722" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11722" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11722" data-product_sku="PKNC032"
                                            aria-label="Thêm “Phụ kiện trang trí lâu đài đá PKNC032” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11722"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-lau-dai-da-pknc032/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí lâu đài đá PKNC032</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>9.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11719 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-toa-thap-nhieu-tang-pknc031/"
                                            aria-label="Phụ kiện trang trí tòa tháp nhiều tầng PKNC031"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang--20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/30-toa-thap-nhieu-tang-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11719 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11719"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11719,&quot;parent_product_id&quot;:11719,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11719&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11719" data-product-type="simple"
                                                            data-original-product-id="11719" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11719" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11719" data-product_sku="PKNC031"
                                            aria-label="Thêm “Phụ kiện trang trí tòa tháp nhiều tầng PKNC031” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11719"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-toa-thap-nhieu-tang-pknc031/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí tòa tháp nhiều tầng PKNC031</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11715 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-ven-bien-pknc030/"
                                            aria-label="Phụ kiện trang trí biệt thự ven biển PKNC030"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-2-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/29-biet-thu-ven-bien-3-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11715 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11715"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11715,&quot;parent_product_id&quot;:11715,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11715&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11715" data-product-type="simple"
                                                            data-original-product-id="11715" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11715" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11715" data-product_sku="PKNC030"
                                            aria-label="Thêm “Phụ kiện trang trí biệt thự ven biển PKNC030” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11715"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-ven-bien-pknc030/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí biệt thự ven biển PKNC030</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>30.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11712 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-van-ly-truong-thanh-pknc029/"
                                            aria-label="Phụ kiện trang trí vạn lý trường thành PKNC029"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/28-van-ly-truong-thanh-2-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11712 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11712"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11712,&quot;parent_product_id&quot;:11712,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11712&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11712" data-product-type="simple"
                                                            data-original-product-id="11712" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11712" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11712" data-product_sku="PKNC029"
                                            aria-label="Thêm “Phụ kiện trang trí vạn lý trường thành PKNC029” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11712"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-van-ly-truong-thanh-pknc029/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí vạn lý trường thành PKNC029</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11709 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-nhieu-mau-pknc028/"
                                            aria-label="Phụ kiện trang trí hàng rào gỗ nhiều màu PKNC028"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-1-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-125x125.jpg 125w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/27-hang-rao-go-30cm-2-20x20.jpg 20w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11709 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11709"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11709,&quot;parent_product_id&quot;:11709,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11709&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11709" data-product-type="simple"
                                                            data-original-product-id="11709" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11709" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11709" data-product_sku="PKNC028"
                                            aria-label="Thêm “Phụ kiện trang trí hàng rào gỗ nhiều màu PKNC028” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11709"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-nhieu-mau-pknc028/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí hàng rào gỗ nhiều màu PKNC028</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>12.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11705 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-cong-ra-vao-pknc027/"
                                            aria-label="Phụ kiện trang trí cổng ra vào PKNC027"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/25-cong-ra-vao-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11705 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11705"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11705,&quot;parent_product_id&quot;:11705,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11705&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11705" data-product-type="simple"
                                                            data-original-product-id="11705" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11705" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11705" data-product_sku="PKNC027"
                                            aria-label="Thêm “Phụ kiện trang trí cổng ra vào PKNC027” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11705"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-cong-ra-vao-pknc027/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí cổng ra vào PKNC027</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>18.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11694 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-lang-ven-bien-pknc026/"
                                            aria-label="Phụ kiện trang trí ngôi làng ven biển PKNC026"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/26-ngoi-lang-ven-bien-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11694 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11694"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11694,&quot;parent_product_id&quot;:11694,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11694&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11694" data-product-type="simple"
                                                            data-original-product-id="11694" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11694" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11694" data-product_sku="PKNC026"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi làng ven biển PKNC026” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11694"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-lang-ven-bien-pknc026/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi làng ven biển PKNC026</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11690 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-cong-ra-vao-pknc025/"
                                            aria-label="Phụ kiện trang trí ngôi cổng ra vào PKNC025"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/24-cong-ra-vao-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11690 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11690"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11690,&quot;parent_product_id&quot;:11690,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11690&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11690" data-product-type="simple"
                                                            data-original-product-id="11690" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11690" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11690" data-product_sku="PKNC025"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi cổng ra vào PKNC025” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11690"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-cong-ra-vao-pknc025/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi cổng ra vào PKNC025</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11676 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mai-rom-pknc024/"
                                            aria-label="Phụ kiện trang trí ngôi nhà mái rơm PKNC024"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-2-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/23-nha-mai-rom-3-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11676 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11676"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11676,&quot;parent_product_id&quot;:11676,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11676&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11676" data-product-type="variable"
                                                            data-original-product-id="11676" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mai-rom-pknc024/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11676" data-product_sku="PKNC024"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí ngôi nhà mái rơm PKNC024”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11676"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mai-rom-pknc024/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà mái rơm PKNC024</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11670 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-giang-sinh-pknc023/"
                                            aria-label="Phụ kiện trang trí ngôi nhà giáng sinh PKNC023"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/22-ngoi-nha-giang-sinh-5-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11670 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11670"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11670,&quot;parent_product_id&quot;:11670,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11670&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11670" data-product-type="simple"
                                                            data-original-product-id="11670" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11670" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11670" data-product_sku="PKNC023"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà giáng sinh PKNC023” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11670"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-giang-sinh-pknc023/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà giáng sinh PKNC023</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11667 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-cay-nam-pknc022/"
                                            aria-label="Phụ kiện trang trí nhà cây nấm PKNC022"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/21-nha-cay-nam-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11667 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11667"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11667,&quot;parent_product_id&quot;:11667,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11667&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11667" data-product-type="simple"
                                                            data-original-product-id="11667" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11667" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11667" data-product_sku="PKNC022"
                                            aria-label="Thêm “Phụ kiện trang trí nhà cây nấm PKNC022” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11667"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-cay-nam-pknc022/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà cây nấm PKNC022</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>40.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11664 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-ong-khoi-pknc021/"
                                            aria-label="Phụ kiện trang trí ngôi nhà ống khói PKNC021"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/20-ngoi-nha-ong-khoi-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11664 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11664"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11664,&quot;parent_product_id&quot;:11664,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11664&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11664" data-product-type="simple"
                                                            data-original-product-id="11664"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11664" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11664" data-product_sku="PKNC021"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà ống khói PKNC021” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11664"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-ong-khoi-pknc021/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà ống khói PKNC021</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11660 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-tre-pknc020/"
                                            aria-label="Phụ kiện trang trí hàng rào tre PKNC020"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/19-hang-rao-tre-truc-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11660 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11660"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11660,&quot;parent_product_id&quot;:11660,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11660&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11660" data-product-type="simple"
                                                            data-original-product-id="11660"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11660" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11660" data-product_sku="PKNC020"
                                            aria-label="Thêm “Phụ kiện trang trí hàng rào tre PKNC020” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11660"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-tre-pknc020/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí hàng rào tre PKNC020</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>8.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11656 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-coc-go-pknc019/"
                                            aria-label="Phụ kiện trang trí hàng rào cọc gỗ PKNC019"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/18-hang-rao-coc-go-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11656 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11656"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11656,&quot;parent_product_id&quot;:11656,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11656&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11656" data-product-type="simple"
                                                            data-original-product-id="11656"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11656" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11656" data-product_sku="PKNC019"
                                            aria-label="Thêm “Phụ kiện trang trí hàng rào cọc gỗ PKNC019” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11656"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-coc-go-pknc019/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí hàng rào cọc gỗ PKNC019</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>8.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11652 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-gach-mai-ngoi-pknc018/"
                                            aria-label="Phụ kiện trang trí biệt thự gạch mái ngói PKNC018"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/17-biet-thu-gach-mai-ngoi-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11652 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11652"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11652,&quot;parent_product_id&quot;:11652,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11652&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11652" data-product-type="simple"
                                                            data-original-product-id="11652"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11652" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11652" data-product_sku="PKNC018"
                                            aria-label="Thêm “Phụ kiện trang trí biệt thự gạch mái ngói PKNC018” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11652"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-biet-thu-gach-mai-ngoi-pknc018/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí biệt thự gạch mái ngói PKNC018</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11649 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-pknc017/"
                                            aria-label="Phụ kiện trang trí hàng rào gỗ PKNC017"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/16-hang-rao-go-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11649 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11649"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11649,&quot;parent_product_id&quot;:11649,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11649&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11649" data-product-type="simple"
                                                            data-original-product-id="11649"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11649" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11649" data-product_sku="PKNC017"
                                            aria-label="Thêm “Phụ kiện trang trí hàng rào gỗ PKNC017” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11649"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-hang-rao-go-pknc017/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí hàng rào gỗ PKNC017</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>9.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11648 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-nam-pknc016/"
                                            aria-label="Phụ kiện trang trí ngôi nhà nấm PKNC016"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11648 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11648"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11648,&quot;parent_product_id&quot;:11648,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11648&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11648" data-product-type="simple"
                                                            data-original-product-id="11648"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11648" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11648" data-product_sku="PKNC016"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà nấm PKNC016” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11648"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-nam-pknc016/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà nấm PKNC016</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>25.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11640 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-nam-pknc015/"
                                            aria-label="Phụ kiện trang trí ngôi nhà nấm PKNC015"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-7-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/15-ngoi-nha-nam-5-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11640 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11640"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11640,&quot;parent_product_id&quot;:11640,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11640&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11640" data-product-type="simple"
                                                            data-original-product-id="11640"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11640" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11640" data-product_sku="PKNC015"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà nấm PKNC015” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11640"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-nam-pknc015/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà nấm PKNC015</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>25.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11637 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc014/"
                                            aria-label="Phụ kiện trang trí ngôi nhà mini PKNC014"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/14-ngoi-nha-mai-la-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11637 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11637"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11637,&quot;parent_product_id&quot;:11637,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11637&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11637" data-product-type="simple"
                                                            data-original-product-id="11637"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11637" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11637" data-product_sku="PKNC014"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà mini PKNC014” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11637"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc014/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà mini PKNC014</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11633 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-coi-xay-gio-pknc013/"
                                            aria-label="Phụ kiện trang trí cối xay gió PKNC013"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/13-coi-xay-gio-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11633 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11633"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11633,&quot;parent_product_id&quot;:11633,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11633&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11633" data-product-type="simple"
                                                            data-original-product-id="11633"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11633" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11633" data-product_sku="PKNC013"
                                            aria-label="Thêm “Phụ kiện trang trí cối xay gió PKNC013” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11633"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-coi-xay-gio-pknc013/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí cối xay gió PKNC013</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>8.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11628 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc012/"
                                            aria-label="Phụ kiện trang trí ngôi nhà mini PKNC012"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/12-ngoi-nha-mini-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11628 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11628"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11628,&quot;parent_product_id&quot;:11628,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11628&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11628" data-product-type="simple"
                                                            data-original-product-id="11628"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11628" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11628" data-product_sku="PKNC012"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà mini PKNC012” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11628"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc012/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà mini PKNC012</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>5.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11625 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-cong-ra-vao-pknc011/"
                                            aria-label="Phụ kiện trang trí cổng ra vào PKNC011"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/11-cong-ra-vao-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11625 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11625"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11625,&quot;parent_product_id&quot;:11625,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11625&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11625" data-product-type="simple"
                                                            data-original-product-id="11625"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11625" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11625" data-product_sku="PKNC011"
                                            aria-label="Thêm “Phụ kiện trang trí cổng ra vào PKNC011” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11625"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-cong-ra-vao-pknc011/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí cổng ra vào PKNC011</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11621 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-chau-au-mini-pknc010/"
                                            aria-label="Phụ kiện trang trí nhà Châu Âu mini PKNC010"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/10-ngoi-nha-chau-au-1-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11621 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11621"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11621,&quot;parent_product_id&quot;:11621,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11621&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11621" data-product-type="simple"
                                                            data-original-product-id="11621"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11621" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11621" data-product_sku="PKNC010"
                                            aria-label="Thêm “Phụ kiện trang trí nhà Châu Âu mini PKNC010” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11621"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-chau-au-mini-pknc010/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà Châu Âu mini PKNC010</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>12.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11618 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc009/"
                                            aria-label="Phụ kiện trang trí ngôi nhà mini PKNC009"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/9-ngoi-nha-mini-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11618 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11618"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11618,&quot;parent_product_id&quot;:11618,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11618&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11618" data-product-type="simple"
                                                            data-original-product-id="11618"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11618" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11618" data-product_sku="PKNC009"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà mini PKNC009” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11618"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc009/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà mini PKNC009</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11616 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc008/"
                                            aria-label="Phụ kiện trang trí ngôi nhà mini PKNC008"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/8-ngoi-nha-nho-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11616 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11616"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11616,&quot;parent_product_id&quot;:11616,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11616&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11616" data-product-type="simple"
                                                            data-original-product-id="11616"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11616" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11616" data-product_sku="PKNC008"
                                            aria-label="Thêm “Phụ kiện trang trí ngôi nhà mini PKNC008” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11616"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngoi-nha-mini-pknc008/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngôi nhà mini PKNC008</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11608 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-ong-khoi-mini-pknc007/"
                                            aria-label="Phụ kiện trang trí nhà ống khói mini PKNC007"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/7-ngoi-nha-ong-khoi-mini-5-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11608 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11608"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11608,&quot;parent_product_id&quot;:11608,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11608&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11608" data-product-type="simple"
                                                            data-original-product-id="11608"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11608" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11608" data-product_sku="PKNC007"
                                            aria-label="Thêm “Phụ kiện trang trí nhà ống khói mini PKNC007” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11608"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-ong-khoi-mini-pknc007/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà ống khói mini PKNC007</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11603 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-go-mini-pknc006/"
                                            aria-label="Phụ kiện trang trí nhà gỗ mini PKNC006"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/6-nho-go-mini-4-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11603 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11603"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11603,&quot;parent_product_id&quot;:11603,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11603&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11603" data-product-type="simple"
                                                            data-original-product-id="11603"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11603" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11603" data-product_sku="PKNC006"
                                            aria-label="Thêm “Phụ kiện trang trí nhà gỗ mini PKNC006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11603"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-go-mini-pknc006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà gỗ mini PKNC006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11600 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-go-mini-pknc005/"
                                            aria-label="Phụ kiện trang trí nhà gỗ mini PKNC005"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-2-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/5-nha-go-mini-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11600 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11600"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11600,&quot;parent_product_id&quot;:11600,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11600&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11600" data-product-type="simple"
                                                            data-original-product-id="11600"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11600" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11600" data-product_sku="PKNC005"
                                            aria-label="Thêm “Phụ kiện trang trí nhà gỗ mini PKNC005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11600"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-go-mini-pknc005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà gỗ mini PKNC005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>18.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11596 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-nho-pknc004/"
                                            aria-label="Phụ kiện trang trí nhà mái rơm nhỏ PKNC004"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/4-nha-mai-rom-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11596 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11596"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11596,&quot;parent_product_id&quot;:11596,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11596&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11596" data-product-type="simple"
                                                            data-original-product-id="11596"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11596" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11596" data-product_sku="PKNC004"
                                            aria-label="Thêm “Phụ kiện trang trí nhà mái rơm nhỏ PKNC004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11596"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-mai-rom-nho-pknc004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà mái rơm nhỏ PKNC004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>12.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11592 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-ong-khoi-nho-pknc003/"
                                            aria-label="Phụ kiện trang trí nhà ống khói nhỏ PKNC003"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/3-ngoi-nha-ong-khoi-nho-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11592 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11592"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11592,&quot;parent_product_id&quot;:11592,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11592&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11592" data-product-type="simple"
                                                            data-original-product-id="11592"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11592" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11592" data-product_sku="PKNC003"
                                            aria-label="Thêm “Phụ kiện trang trí nhà ống khói nhỏ PKNC003” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11592"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-ong-khoi-nho-pknc003/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà ống khói nhỏ PKNC003</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11589 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-nho-pknc002/"
                                            aria-label="Phụ kiện trang trí nhà nhỏ PKNC002"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/2-nha-rom-nho-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11589 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11589"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11589,&quot;parent_product_id&quot;:11589,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11589&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11589" data-product-type="simple"
                                                            data-original-product-id="11589"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11589" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11589" data-product_sku="PKNC002"
                                            aria-label="Thêm “Phụ kiện trang trí nhà nhỏ PKNC002” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11589"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-nho-pknc002/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà nhỏ PKNC002</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11578 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-nha-co-trung-hoa-pknc001/"
                                            aria-label="Phụ kiện trang trí nhà cổ Trung Hoa PKNC001"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/1-nha-co-trung-hoa-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11578 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11578"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11578,&quot;parent_product_id&quot;:11578,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11578&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11578" data-product-type="simple"
                                                            data-original-product-id="11578"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11578" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11578" data-product_sku="PKNC001"
                                            aria-label="Thêm “Phụ kiện trang trí nhà cổ Trung Hoa PKNC001” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11578"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-nha-co-trung-hoa-pknc001/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí nhà cổ Trung Hoa PKNC001</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>12.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11561 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-gau-truc-pktc046/"
                                            aria-label="Phụ kiện trang trí gia đình gấu trúc PKTC046"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/46-gau-truc-dang-yeu-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11561 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11561"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11561,&quot;parent_product_id&quot;:11561,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11561&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11561" data-product-type="variable"
                                                            data-original-product-id="11561"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-gau-truc-pktc046/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11561" data-product_sku="PKTC046"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình gấu trúc PKTC046”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11561"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-gau-truc-pktc046/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình gấu trúc PKTC046</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>8.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11547 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-vit-dan-choi-pktc045/"
                                            aria-label="Phụ kiện trang trí vịt dân chơi PKTC045"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/45-vit-con-dan-choi-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11547 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11547"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11547,&quot;parent_product_id&quot;:11547,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11547&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11547" data-product-type="simple"
                                                            data-original-product-id="11547"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11547" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11547" data-product_sku="PKTC045"
                                            aria-label="Thêm “Phụ kiện trang trí vịt dân chơi PKTC045” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11547"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-vit-dan-choi-pktc045/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí vịt dân chơi PKTC045</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11512 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-ga-pktc044/"
                                            aria-label="Phụ kiện trang trí gia đình gà PKTC044"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/44-gia-dinh-ga-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11512 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11512"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11512,&quot;parent_product_id&quot;:11512,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11512&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11512" data-product-type="variable"
                                                            data-original-product-id="11512"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-ga-pktc044/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11512" data-product_sku="PKTC044"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình gà PKTC044”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11512"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-ga-pktc044/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình gà PKTC044</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>5.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11475 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-soc-nho-pktc043/"
                                            aria-label="Phụ kiện trang trí gia đình sóc nhỏ PKTC043"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/43-gia-dinh-soc-nho-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11475 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11475"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11475,&quot;parent_product_id&quot;:11475,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11475&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11475" data-product-type="variable"
                                                            data-original-product-id="11475"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-soc-nho-pktc043/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11475" data-product_sku="PKTC043"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình sóc nhỏ PKTC043”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11475"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-soc-nho-pktc043/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình sóc nhỏ PKTC043</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>5.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11452 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable has-default-attributes">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-vit-con-di-hoc-pktc042/"
                                            aria-label="Phụ kiện trang trí vịt con đi học PKTC042"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/42-vit-con-di-hoc-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11452 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11452"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11452,&quot;parent_product_id&quot;:11452,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11452&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11452" data-product-type="variable"
                                                            data-original-product-id="11452"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-vit-con-di-hoc-pktc042/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11452" data-product_sku="PKTC042"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí vịt con đi học PKTC042”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11452"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-vit-con-di-hoc-pktc042/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí vịt con đi học PKTC042</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>5.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11443 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-me-con-gau-trang-pktc041/"
                                            aria-label="Phụ kiện trang trí mẹ con gấu trắng PKTC041"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-4-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/41-me-con-gau-trang-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11443 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11443"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11443,&quot;parent_product_id&quot;:11443,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11443&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11443" data-product-type="variable"
                                                            data-original-product-id="11443"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-me-con-gau-trang-pktc041/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11443" data-product_sku="PKTC041"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí mẹ con gấu trắng PKTC041”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11443"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-me-con-gau-trang-pktc041/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí mẹ con gấu trắng PKTC041</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>6.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>9.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11437 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-ngua-ky-lan-pktc040/"
                                            aria-label="Phụ kiện trang trí ngựa kỳ lân PKTC040"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/40-ngua-ky-lan-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11437 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11437"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11437,&quot;parent_product_id&quot;:11437,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11437&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11437" data-product-type="simple"
                                                            data-original-product-id="11437"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11437" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11437" data-product_sku="PKTC040"
                                            aria-label="Thêm “Phụ kiện trang trí ngựa kỳ lân PKTC040” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11437"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-ngua-ky-lan-pktc040/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí ngựa kỳ lân PKTC040</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>18.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11421 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-cong-vien-khung-long-pktc039/"
                                            aria-label="Phụ kiện trang trí công viên khủng long PKTC039"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/39-cong-vien-khung-long-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11421 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11421"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11421,&quot;parent_product_id&quot;:11421,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11421&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11421" data-product-type="variable"
                                                            data-original-product-id="11421"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-cong-vien-khung-long-pktc039/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11421" data-product_sku="PKTC039"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí công viên khủng long PKTC039”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11421"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-cong-vien-khung-long-pktc039/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí công viên khủng long PKTC039</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>18.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11405 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-huu-sao-pktc038/"
                                            aria-label="Phụ kiện trang trí gia đình hưu sao PKTC038"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/38-gia-dinh-huu-sao-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11405 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11405"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11405,&quot;parent_product_id&quot;:11405,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11405&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11405" data-product-type="variable"
                                                            data-original-product-id="11405"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-huu-sao-pktc038/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11405" data-product_sku="PKTC038"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình hưu sao PKTC038”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11405"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-huu-sao-pktc038/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình hưu sao PKTC038</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>10.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11388 status-publish first instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-tho-pktc037/"
                                            aria-label="Phụ kiện trang trí gia đình thỏ PKTC037"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/37-gia-dinh-tho-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11388 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11388"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11388,&quot;parent_product_id&quot;:11388,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11388&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11388" data-product-type="variable"
                                                            data-original-product-id="11388"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-tho-pktc037/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11388" data-product_sku="PKTC037"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình thỏ PKTC037”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11388"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-tho-pktc037/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình thỏ PKTC037</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>16.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11385 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-soc-dang-yeu-pktc036/"
                                            aria-label="Phụ kiện trang trí sóc đáng yêu PKTC036"> <img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/36-soc-con-nhanh-nhen-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11385 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11385"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11385,&quot;parent_product_id&quot;:11385,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11385&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11385" data-product-type="simple"
                                                            data-original-product-id="11385"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11385" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11385" data-product_sku="PKTC036"
                                            aria-label="Thêm “Phụ kiện trang trí sóc đáng yêu PKTC036” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11385"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-soc-dang-yeu-pktc036/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí sóc đáng yêu PKTC036</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11366 status-publish instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-cuu-nho-dang-yeu-pktc035/"
                                            aria-label="Phụ kiện trang trí cừu nhỏ đáng yêu PKTC035"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/35-cuu-nho-dang-yeu-3-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11366 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11366"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11366,&quot;parent_product_id&quot;:11366,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11366&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11366" data-product-type="simple"
                                                            data-original-product-id="11366"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=11366" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="11366" data-product_sku="PKTC035"
                                            aria-label="Thêm “Phụ kiện trang trí cừu nhỏ đáng yêu PKTC035” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11366"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-cuu-nho-dang-yeu-pktc035/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí cừu nhỏ đáng yêu PKTC035</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-11323 status-publish last instock product_cat-phu-kien-trang-tri has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-chuot-hamster-pktc034/"
                                            aria-label="Phụ kiện trang trí gia đình chuột Hamster PKTC034"> <img
                                                width="800" height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-1-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"><img width="800"
                                                height="800"
                                                src="https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2-280x280.jpg 280w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2-768x768.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2-20x20.jpg 20w, https://mowgarden.com/wp-content/uploads/2022/10/34-gia-dinh-chuot-hamster-2-125x125.jpg 125w"
                                                sizes="(max-width: 800px) 100vw, 800px"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-11323 wishlist-fragment on-first-load"
                                                    data-fragment-ref="11323"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:11323,&quot;parent_product_id&quot;:11323,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=11323&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="11323" data-product-type="variable"
                                                            data-original-product-id="11323"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-chuot-hamster-pktc034/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="11323" data-product_sku="PKTC034"
                                            aria-label="Lựa chọn cho “Phụ kiện trang trí gia đình chuột Hamster PKTC034”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="11323"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/phu-kien-trang-tri-gia-dinh-chuot-hamster-pktc034/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Phụ
                                                kiện trang trí gia đình chuột Hamster PKTC034</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>4.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>15.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row -->
                <div class="container">
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers nav-pagination links text-center">
                            <li><span aria-current="page" class="page-number current">1</span></li>
                            <li><a class="page-number" href="https://mowgarden.com/phu-kien-trang-tri/page/2/">2</a>
                            </li>
                            <li><a class="page-number" href="https://mowgarden.com/phu-kien-trang-tri/page/3/">3</a>
                            </li>
                            <li><a class="page-number" href="https://mowgarden.com/phu-kien-trang-tri/page/4/">4</a>
                            </li>
                            <li><a class="next page-number" href="https://mowgarden.com/phu-kien-trang-tri/page/2/"><i
                                        class="icon-angle-right"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div><!-- shop container -->
        </div>
        <div id="shop-sidebar" class="mfp-hide">
            <div class="sidebar-inner">
                <aside id="search-5" class="widget widget_search">
                    <form method="get" class="searchform" action="https://mowgarden.com/" role="search">
                        <div class="flex-row relative">
                            <div class="flex-col flex-grow"> <input type="search" class="search-field mb-0"
                                    name="s" value="" id="s" placeholder="Tìm kiếm sản phẩm"
                                    autocomplete="off"> </div>
                            <div class="flex-col"> <button type="submit"
                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                    aria-label="Submit"> <i class="icon-search"></i> </button> </div>
                        </div>
                        <div class="live-search-results text-left z-top">
                            <div class="autocomplete-suggestions"
                                style="position: absolute; display: none; max-height: 300px; z-index: 9999;"></div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_price_filter-9" class="widget woocommerce widget_price_filter"><span
                        class="widget-title shop-sidebar">Lọc theo giá</span>
                    <div class="is-divider small"></div>
                    <form method="get" action="https://mowgarden.com/phu-kien-trang-tri/">
                        <div class="price_slider_wrapper">
                            <div class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                style="">
                                <div class="ui-slider-range ui-corner-all ui-widget-header"
                                    style="left: 0%; width: 100%;"></div><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default"
                                    style="left: 0%;"></span><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
                            </div>
                            <div class="price_slider_amount" data-step="10"> <label class="screen-reader-text"
                                    for="min_price">Giá thấp nhất</label> <input type="text" id="min_price"
                                    name="min_price" value="3000" data-min="3000" placeholder="Giá thấp nhất"
                                    style="display: none;"> <label class="screen-reader-text" for="max_price">Giá cao
                                    nhất</label> <input type="text" id="max_price" name="max_price"
                                    value="150000" data-max="150000" placeholder="Giá cao nhất"
                                    style="display: none;"> <button type="submit" class="button">Lọc</button>
                                <div class="price_label" style=""> Giá <span class="from">3.000₫</span> —
                                    <span class="to">150.000₫</span> </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_product_categories-13" class="widget woocommerce widget_product_categories"><span
                        class="widget-title shop-sidebar">danh mục</span>
                    <div class="is-divider small"></div>
                    <ul class="product-categories">
                        <li class="cat-item cat-item-109"><a href="https://mowgarden.com/cay-ngoai-troi/">Cây Ngoài
                                Trời</a></li>
                        <li class="cat-item cat-item-93"><a href="https://mowgarden.com/ban-cay-canh-trong-nha/">Cây
                                Trong Nhà</a></li>
                        <li class="cat-item cat-item-121"><a href="https://mowgarden.com/chau-cay/">Chậu Cây Cảnh</a>
                        </li>
                        <li class="cat-item cat-item-395"><a href="https://mowgarden.com/dung-cu-lam-vuon/">Dụng Cụ Làm
                                Vườn</a></li>
                        <li class="cat-item cat-item-393"><a href="https://mowgarden.com/phan-bon/">Phân Bón</a></li>
                        <li class="cat-item cat-item-394 current-cat active"><a
                                href="https://mowgarden.com/phu-kien-trang-tri/">Phụ Kiện Trang Trí</a></li>
                        <li class="cat-item cat-item-15"><a
                                href="https://mowgarden.com/uncategorized/">Uncategorized</a></li>
                        <li class="cat-item cat-item-396"><a href="https://mowgarden.com/dat-trong-cay/">Đất Trồng
                                Cây</a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
@endsection
