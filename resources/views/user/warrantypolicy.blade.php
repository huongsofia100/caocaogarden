@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" class="content-area page-wrapper" role="main">
        <div class="row row-main">
            <div class="large-12 col">
                <div class="col-inner">
                    <p style="font-size:32px"><strong>1. Điều kiện bảo hành – hoàn trả sản phẩm</strong></p>
                    <p>– Quý khách còn giữ xác nhận về việc đã mua hàng tại MOW Garden (hóa đơn mua hàng, biên nhận giao
                        hàng, sao kê của ngân hàng… ).</p>
                    <p>– Sản phẩm không nằm trong danh mục hạn chế đổi trả.</p>
                    <p>– Sản phẩm phải còn nguyên tem, mác, nguyên đai kiện ban đầu (trừ trường hợp sản phẩm bị lỗi hoặc hư
                        hại trong quá trình vận chuyển) và quà tặng kèm (nếu có)<strong>.</strong></p>
                    <p style="font-size:32px"><strong>2. Quy trình thực hiện đổi trả hàng</strong></p>
                    <p>– Quý khách vui lòng kiểm tra kỹ sản phẩm trước khi ký nhận hàng và báo ngay cho nhân viên giao hàng
                        nếu có phát hiện lỗi sản phẩm hoặc liên hệ ngay với bộ phận chăm sóc khách hàng.</p>
                    <p>– Bộ phận Kỹ Thuật sẽ liên hệ ngay với quý khách để xác nhận yêu cầu và lý do đổi trả, thống nhất
                        giao dịch đổi trả cho sản phẩm.</p>
                    <p>– Đối với hàng hoá nhỏ và đơn hàng các tỉnh thành khác: quý khách vui lòng mang tới/vận chuyển tới
                        cửa hàng hoặc kho hàng gần nhất để đổi trả.</p>
                    <p>– Đối với hàng hoá cồng kềnh đã lắp ráp: MOW Garden sẽ bảo hành tận nơi tại TP HCM cho quý khách.</p>
                    <p>– Quý khách hàng vui lòng kiểm tra kỹ sản phẩm một lần nữa trước khi ký nhận đổi trả hàng.</p>
                    <p style="font-size:32px"><strong>3. Quy định đổi trả/ bảo hành</strong></p>
                    <p class="has-medium-font-size"><strong>3.1 – Quy định đổi trả/ bảo hành được áp dụng đối với những hàng
                            hóa và lỗi sau:</strong></p>
                    <p>– Sản phẩm không đúng như thông tin đăng tải (sai công dụng, sai thành phần, …).</p>
                    <p>– Sản phẩm bị bể vỡ/trầy xước/biến dạng trong quá trình vận chuyển trừ trường hợp vận chuyển theo chỉ
                        định của khách hàng.</p>
                    <p>– Sản phẩm nhận không đúng với đơn hàng đã đặt.</p>
                    <p>– Các lỗi sản phẩm do quá trình sản xuất hoặc vận chuyển, lắp ráp như sau:</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; . Trầy tróc, chảy sơn bề mặt.</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; . Móp, méo, cong vênh các chi tiết sản phẩm.</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; . Sứt, mẻ cạnh, góc sản phẩm.</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; . Hở đinh, ốc.</p>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; . Biến dạng sản phẩm (do vận chuyển, do môi trường
                        bảo quản từ phía nhà cung cấp, do quá trình sản xuất…).</p>
                    <p class="has-medium-font-size"><strong>3.2 – Không áp dụng đổi trả đối với những trường hợp
                            sau:</strong></p>
                    <p>– Các sản phẩm trong Chương trình Giảm giá và Khuyến mại hoặc từ Cửa hàng Giảm giá.</p>
                    <p>– Các hư hỏng gây ra trong quá trình sử dụng sau khi mua hàng như sử dụng không cẩn thận, đặt trong
                        môi trường không đảm bảo, bảo quản không đúng hướng dẫn, tải trọng quá mức vệ sinh sản phẩm không
                        đúng cách,…</p>
                    <p>– Các hao mòn thông thường (ví dụ: phai mờ, rỉ sét, lỏng đinh vít hoặc bản lề sau một khoảng thời
                        gian…)</p>
                    <p>– Va chạm do tai nạn khi sử dụng hoặc khi quý khách tự vận chuyển sản phẩm.</p>
                    <p style="font-size:32px"><strong>4. Thời gian giải quyết các đơn hàng đổi trả:</strong></p>
                    <p>– Việc gửi sản phẩm thay thế hoặc hoàn tiền chỉ được bắt đầu sau khi chúng tôi đã hoàn tất việc kiểm
                        tra đánh giá sản phẩm quý khách gửi lại.</p>
                    <p>– Quy trình đánh giá chất lượng sản phẩm từ 3-5 ngày làm việc.</p>
                    <p>– Bộ phận kỹ thuật sẽ liên hệ và thống nhất với khách hàng về quyết định sửa chữa hoặc đổi trả, thống
                        nhất lịch giao hàng với quý khách hàng.</p>
                    <p style="font-size:32px"><strong>5. Phí vận chuyển đối sản phẩm đổi trả/bảo hành</strong></p>
                    <p>– MOW Garden bảo hành miễn phí đối với các sản phẩm cồng kềnh trong thời hạn bảo hành.</p>
                    <p>– Quý khách thanh toán phí vận chuyển đối với các đơn hàng đổi trả theo biểu phí tại mục II.3</p>
                </div>
            </div>
        </div>
    </div>
@endsection
