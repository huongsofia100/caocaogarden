@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" class="content-area page-wrapper" role="main">
        <div class="row row-main">
            <div class="large-12 col">
                <div class="col-inner">
                    <p style="font-size:32px"><strong>1. Phạm vi giao hàng</strong></p>
                    <p>Cào Cào Garden phục vụ giao hàng cho khách hàng tại các địa điểm như sau:</p>
                    <p class="has-medium-font-size"><strong>1.1 – Khu vực Tp  Huế</strong></p>
                    <p>Đơn hàng sẽ được giao đến địa điểm khách hàng yêu cầu, đối với hàng hoá cần phải lắp đặt, bộ phận
                        giao nhận sẽ lắp ráp hoàn thiện cho khách hàng. Ngoại trừ các trường hợp hạn chế như khu vực văn
                        phòng, chung cư cao tầng có quy định hạn chế ra vào, các khu cao tầng không có khu vực gửi xe, khách
                        hàng vui lòng gọi hotline để được hỗ trợ 0783 655 566.</p>
                    <p>– Trường hợp quý khách không sử dụng dịch vụ lắp đặt đã bao gồm trong phí giao hàng, Cào Cào Garden sẽ
                        không chịu trách nhiệm dịch vụ lắp đặt về sau.</p>
                    <p>Quý khách vui lòng trả tất cả các loại phí phát sinh theo quy định của Ban Quản lý nơi minh sinh sống
                        liên quan đến việc giao hàng, sử dụng thang máy, thi công…</p>
                    <p class="has-medium-font-size"><strong>1.2 – Các tỉnh thành khác trong nước</strong></p>
                    <p>– Cào Cào Garden chỉ giao hàng đến những tỉnh thành khác những sản phẩm theo quy định vì lí do tính chất
                        dễ vỡ của sản phẩm trong lúc vận chuyển (vd: chậu gốm sứ, xi măng,…)</p>
                    <p>– Có 2 hình thức giao hàng:</p>
                    <ul>
                        <li>Hình thức 1: Cào Cào Garden sẽ giao hàng bằng dịch vụ Viettel Post tận nơi cho khách hàng theo địa
                            chỉ mà khách hàng cung cấp.</li>
                        <li>Hình thức 2: Đơn hàng sẽ được giao tới kho bãi các đơn vị vận chuyển trung gian theo chỉ định
                            của khách hàng trong khu vực nội thành thành phố Hô Chí Minh như bến xe, nhà xe chuyên chở, ga
                            xe lửa, ga hàng không, các đơn vị chuyển phát,… Cào Cào Garden không chịu trách nhiệm vận chuyển đến
                            tận nơi cho quý khách hàng trong trường hợp này, khách hàng liên hệ với đơn vị vận chuyển trung
                            gian để nhận hàng.</li>
                    </ul>
                    <p style="font-size:32px"><strong>2. Thời gian giao hàng</strong></p>
                    <p>– Thời gian xử lí giao hàng vào 2 khung giờ: <strong><em>9h -12h</em></strong> và
                        <strong><em>14h-17h</em></strong> từ thứ 2 đến thứ 7 (trừ ngày lễ, tết). Trường hợp giao hàng ngoài
                        giờ hành chính sẽ có phụ phí.</p>
                    <p>– Đơn hàng sẽ được xử lý và giao hàng trong vòng 3 ngày từ ngày xác nhận đơn hàngà đối với sản phẩm
                        có sẵn và 7-14 ngày đối với sản phẩm đặt hàng.</p>
                    <p>– Quý khách vui lòng sắp xếp thời gian hoặc sắp xếp người nhận hàng theo lịch giao hàng đã thống
                        nhất.</p>
                    <p>– Trường hợp quý khách có thay đổi về thời gian giao hàng, hủy đơn hàng hoặc có bất cứ thay đổi nào
                        về đơn hàng, quý khách vui lòng liên hệ đến đường dây nóng của Cào Cào Garden trước 24h đối với đơn hàng
                        tại tp. Hồ Chí Minh và 48h đối với đơn hàng tại các tỉnh khác. Trường hợp hủy đơn hàng muộn, Quý
                        khách vui lỏng thanh toán phí giao hàng theo đơn hàng của quý khách.</p>
                    <p style="font-size:32px"><strong>3. Phí giao hàng</strong></p>
                    <p class="has-medium-font-size"><strong>3.1 – Khu vực TP.Huế</strong></p>
                    <p><em>– Đối với hàng hoá có kích thước nhỏ và trọng lượng nhẹ:</em>&nbsp;(kích thước cạnh dài nhất
                        &lt;40cm và dưới 10kg), Phí giao hàng sẽ được tính theo phí dịch vụ Grab, lalamove, Ahamove áp dụng
                        mức giá hiện hành không bao gồm mã khuyến mãi.</p>
                    <p>– Trường hợp giao hàng ngoài giờ hành chính, quý khách vui lòng thanh toán thêm 30% phí giao hàng
                        theo quy định và tối thiểu là 50.000 đ</p>
                    <p><em>Lưu ý: Cào Cào Garden là đơn vị quyết định phương thức vận chuyển cuối cùng để đảm bảo an toàn cho
                            hàng hoá cách tốt nhất.</em></p>
                    <p class="has-medium-font-size"><strong>3.2 – Các tỉnh thành khác trong nước</strong></p>
                    <p>– Phí giao hàng đến đơn vị vận chuyển trung gian bằng Phí giao hàng hoá cồng kềnh tại khu vực TP.Huế</p>
                    <p>– Phí giao từ đơn vị trung gian đến địa chỉ của quý khách hàng, Cào Cào Garden sẽ báo cho quý khách phí
                        giao hàng bằng dịch vụ giao hàng Viettel Post hoặc đơn vị khác sau khi nhận được đơn đặt hàng từ quý
                        khách.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
