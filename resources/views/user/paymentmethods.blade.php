@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" class="content-area page-wrapper" role="main">
        <div class="row row-main">
            <div class="large-12 col">
                <div class="col-inner">
                    <h3 class="wp-block-heading">Các hình thức thanh toán</h3>
                    <p>– Thanh toán trực tiếp tiền mặt.</p>
                    <p>– Thanh toán qua cổng thanh toán trực tiếp trên website mowgarden.com</p>
                    <p>– Thanh toán khi nhận được hàng (COD) chỉ áp dụng cho khách hàng tại TP.Huế</p>
                    <p>– Thanh toán bằng hình thức chuyển khoản</p>
                    <h3 class="wp-block-heading"><strong>Thông tin chi tiết về số tài khoản</strong></h3>
                    <p><strong>Ngân hàng TMCP Ngoại Thương Việt Nam</strong><br>Chi nhánh TP.Huế<br><strong>Chủ tài
                            khoản:</strong>&nbsp;Ngô Thị Hường<br><strong>Số Tài Khoản:</strong>&nbsp;1017730869
                    </p>
                    
                    <h3 class="wp-block-heading">Mọi thông tin chi tiết, quý khách hàng vui lòng liên hệ:</h3>
                    <p><strong><u>Lưu ý:</u></strong></p>
                    <p>Nội dung chuyển khoản ghi rõ HỌ TÊN hoặc TÊN ĐƠN VỊ + MÃ ĐƠN HÀNG. Sau khi chuyển khoản, chúng tôi sẽ
                        liên hệ xác nhận và tiến hành giao hàng. Nếu sau thời gian thỏa thuận mà chúng tôi không giao hàng
                        hoặc không phản hồi lại, quý khách có thể gửi khiếu nại trực tiếp về công ty và yêu cầu bồi thường
                        nếu chứng minh được sự chậm trễ làm ảnh hưởng đến kinh doanh của quý khách.</p>
                    <p><em>Đối với khách hàng có nhu cầu mua số lượng lớn để kinh doanh hoặc buôn sỉ vui lòng liên hệ trực
                            tiếp với chúng tôi để có chính sách giá cả hợp lý. Và việc thanh toán sẽ được thực hiện theo hợp
                            đồng.</em></p>
                    <p><em>Chúng tôi cam kết kinh doanh minh bạch, hợp pháp, bán hàng chất lượng.</em></p>
                </div>
            </div>
        </div>
    </div>
@endsection
