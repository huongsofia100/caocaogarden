@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" class="content-area page-wrapper" role="main">
        <div class="row row-main">
            <div class="large-12 col">
                <div class="col-inner">
                    <section class="section" id="section_571893357">
                        <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
                        <div class="section-content relative">
                            <div class="row row-large align-middle align-center" id="row-1689268517">
                                <div id="col-429529382" class="col medium-4 small-12 large-4">
                                    <div class="col-inner">
                                        <div id="text-1792322353" class="text">
                                            <p style="text-align: center;"><a
                                                    href="https://mowgarden.com/category/kien-thuc-va-cach-cham-soc/"><strong>KIẾN
                                                        THỨC &amp; CÁCH CHĂM SÓC</strong></a></p>
                                            <style>
                                                #text-1792322353 {
                                                    font-size: 1.25rem;
                                                    line-height: 1;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                                <div id="col-1751507603" class="col medium-4 small-12 large-4">
                                    <div class="col-inner">
                                        <div id="text-433389738" class="text">
                                            <p style="text-align: center;"><a
                                                    href="https://mowgarden.com/category/cam-hung-va-y-tuong/"><strong>CẢM
                                                        HỨNG &amp; Ý TƯỞNG</strong></a></p>
                                            <style>
                                                #text-433389738 {
                                                    font-size: 1.25rem;
                                                    line-height: 1;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                                <div id="col-1546035262" class="col medium-4 small-12 large-4">
                                    <div class="col-inner">
                                        <div id="text-3190250508" class="text">
                                            <p style="text-align: center;"><a
                                                    href="https://mowgarden.com/category/mow-garden-360/"><strong>MOW GARDEN
                                                        360°</strong></a></p>
                                            <style>
                                                #text-3190250508 {
                                                    font-size: 1.25rem;
                                                    line-height: 1;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    #row-1689268517>.col>.col-inner {
                                        padding: 0px 0px -52px 0px;
                                    }
                                </style>
                            </div>
                            <div
                                class="row large-columns-2 medium-columns- small-columns-1 row-small row-full-width has-shadow row-box-shadow-1">
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/top-10-cay-phong-thuy-hop-menh-kim/" class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img loading="lazy"
                                                            decoding="async" width="800" height="600"
                                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-800x600.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay phong thuy hop menh kim"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-phong-thuy-hop-menh-kim.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Phong Thủy
                                                        </p>
                                                        <h5 class="post-title is-larger">Top 10 cây phong thủy hợp mệnh Kim
                                                            giúp kích hoạt vượng khí và mang lại may mắn.</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/trong-cay-phong-thuy-trong-nha/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img loading="lazy"
                                                            decoding="async" width="800" height="599"
                                                            src="https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-800x599.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="huong dan trong cay phong thuy trong nha"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-800x599.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-1068x800.jpg 1068w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha-768x575.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/04/huong-dan-trong-cay-phong-thuy-trong-nha.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Phong Thủy
                                                        </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn lựa chọn cây phong thủy
                                                            trong nhà để tăng vượng khí</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-cach-cham-soc-cay-hanh-phuc/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img loading="lazy"
                                                            decoding="async" width="800" height="534"
                                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-800x534.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay hanh phuc la cay gi"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-800x534.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-la-cay-gi.jpg 1000w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc Thông tin về cây </p>
                                                        <h5 class="post-title is-larger">Cây hạnh phúc là cây gì? Hướng dẫn
                                                            cách chăm sóc cây hạnh phúc trong nhà</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cung-cap-cay-xanh-van-phong-cho-cong-ty-thiet-ke-cua-nhat/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="600"
                                                            src="https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-800x600.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay canh van phong 7"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-1536x1152.jpg 1536w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-2048x1536.jpg 2048w, https://mowgarden.com/wp-content/uploads/2023/03/cay-canh-van-phong-7-20x15.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">Cung cấp cây xanh văn phòng cho
                                                            công ty thiết kế Gifu của Nhật</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/bang-gia-ban-cay-monstera/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="600"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-600x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="bảng giá bán cây monstera"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/11/bang-gia-ban-cay-monstera-510x340.jpg 510w"
                                                            sizes="(max-width: 600px) 100vw, 600px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">Bảng giá bán cây Monstera mới nhất
                                                            hôm nay 2021</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/dat-trong-sen-da/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="600"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-600x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="đất trồng sen đá"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/dat-trong-sen-da-510x340.jpg 510w"
                                                            sizes="(max-width: 600px) 100vw, 600px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách trộn đất trồng sen
                                                            đá đơn giản</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/sen-da-kim-cuong/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="600"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-600x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="các loại sen đá kim cương"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/cac-loai-sen-da-kim-cuong-510x340.jpg 510w"
                                                            sizes="(max-width: 600px) 100vw, 600px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Sự thật thú vị về cây Sen Đá Kim
                                                            Cương</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cac-loai-sen-da/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="600"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-600x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="tên các loại sen đá"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/ten-cac-loai-sen-da-pho-thong-va-quy-hiem-510x340.jpg 510w"
                                                            sizes="(max-width: 600px) 100vw, 600px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Thư viện Các Loại Sen Đá phổ thông
                                                            và quý hiếm</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-trang-tri-sen-da-dep/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/10/cach-trang-tri-sen-da-dep-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach trang tri sen da dep"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/10/cach-trang-tri-sen-da-dep-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/10/cach-trang-tri-sen-da-dep.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/10/cach-trang-tri-sen-da-dep-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/10/cach-trang-tri-sen-da-dep-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Cảm hứng
                                                            và ý tưởng </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách trang trí sen đá
                                                            đẹp và độc đáo có thể bạn chưa biết</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/huong-dan-cach-xu-ly-sen-da-khi-moi-mua-ve/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="600"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-800x600.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cách xử lý sen đá mới mua về"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-1536x1152.jpg 1536w, https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve-20x15.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/09/cach-xu-ly-sen-da-moi-mua-ve.jpg 1600w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hưỡng dẫn cách xử lý sen đá khi
                                                            mới mua về</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-monstera-la-cay-gi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="600"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3-800x600.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="monstera deliciosa 3"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3-20x15.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/06/monstera-deliciosa-3.jpg 1279w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Cây trầu bà Nam Mỹ (Monstera) là
                                                            cây gì? Các loại cây Monstera</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-trong-sen-da/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="600"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da-800x600.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cách trồng sen đá"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da-20x15.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/05/cach-trong-sen-da.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách trồng sen đá trong
                                                            chậu từ A đến Z</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/nhung-loai-cay-trong-trong-nha-dep/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="453"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep-800x453.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="nhung loai cay trong trong nha dep"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep-800x453.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep-1200x680.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep-768x435.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep-20x11.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/05/nhung-loai-cay-trong-trong-nha-dep.jpg 1378w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Top 50 Loại Cây Trồng Trong Nhà
                                                            Đẹp</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/6-cach-tuoi-nuoc-cho-cay-khi-dang-di-du-lich/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="501"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/tuoi-nuoc-tu-dong-800x501.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="tuoi nuoc tu dong"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/tuoi-nuoc-tu-dong-800x501.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/tuoi-nuoc-tu-dong-768x481.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/tuoi-nuoc-tu-dong-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/tuoi-nuoc-tu-dong.jpg 1000w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Cảm hứng
                                                            và ý tưởng </p>
                                                        <h5 class="post-title is-larger">6 Cách Tưới Nước Tự Động Cho Cây
                                                            Để Yên Tâm Đi Du Lịch</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-trong-cay-ngu-gia-bi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-ngu-gia-bi-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach cham soc cay ngu gia bi 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-ngu-gia-bi-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-ngu-gia-bi-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-ngu-gia-bi-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-ngu-gia-bi-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng Dẫn Cách Trồng Cây Ngũ Gia
                                                            Bì</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-ngu-gia-bi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cong-dung-cay-ngu-gia-bi-1-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cong dung cay ngu gia bi 1 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cong-dung-cay-ngu-gia-bi-1-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cong-dung-cay-ngu-gia-bi-1-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cong-dung-cay-ngu-gia-bi-1-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cong-dung-cay-ngu-gia-bi-1-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Đặc điểm, ý nghĩa và tác dụng của
                                                            cây ngũ gia bì</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cach-cham-soc-cay-bach-ma-hoang-tu/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2022/01/cach-cham-soc-cay-bach-ma-hoang-tu-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach cham soc cay bach ma hoang tu"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/cach-cham-soc-cay-bach-ma-hoang-tu-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/01/cach-cham-soc-cay-bach-ma-hoang-tu.jpg 1200w, https://mowgarden.com/wp-content/uploads/2022/01/cach-cham-soc-cay-bach-ma-hoang-tu-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/01/cach-cham-soc-cay-bach-ma-hoang-tu-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Cách Chăm Sóc Cây Bạch Mã Hoàng Tử
                                                        </h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-bach-ma-hoang-tu/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2022/01/cay-bach-ma-hoang-tu-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay bach ma hoang tu"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/cay-bach-ma-hoang-tu-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2022/01/cay-bach-ma-hoang-tu.jpg 1200w, https://mowgarden.com/wp-content/uploads/2022/01/cay-bach-ma-hoang-tu-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2022/01/cay-bach-ma-hoang-tu-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Đặc Điểm &amp; Ý Nghĩa Cây Bạch Mã
                                                            Hoàng Tử</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-duoi-cong/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-duoi-cong-10-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay duoi cong 10 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-duoi-cong-10-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-duoi-cong-10-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-duoi-cong-10-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-duoi-cong-10-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây Kiến thức và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Cây Đuôi Công: Đặc Điểm, Ý Nghĩa
                                                            &amp; Cách Chăm Sóc Cây Đuôi Công</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-bang-singapore/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-bang-singapore-5-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay bang singapore 5"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-bang-singapore-5-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-bang-singapore-5.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-bang-singapore-5-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-bang-singapore-5-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Cây Bàng Singapore: Đặc điểm, ý
                                                            nghĩa phong thủy và cách chăm sóc đầy đủ</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cach-trong-va-cham-soc-cay-thuong-xuan/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-va-cham-soc-cay-thuong-xuan-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach trong va cham soc cay thuong xuan"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-va-cham-soc-cay-thuong-xuan-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-va-cham-soc-cay-thuong-xuan.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-va-cham-soc-cay-thuong-xuan-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-va-cham-soc-cay-thuong-xuan-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Cách Trồng &amp; Chăm Sóc Cây
                                                            Thường Xuân Toàn Tập</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-thuong-xuan/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/CAY-THUONG-XUAN-LA-GI-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="CAY THUONG XUAN LA GI 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/CAY-THUONG-XUAN-LA-GI-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/CAY-THUONG-XUAN-LA-GI-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/CAY-THUONG-XUAN-LA-GI-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/CAY-THUONG-XUAN-LA-GI-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây Kiến thức và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Đặc Điểm Và Ý Nghĩa Cây Thường
                                                            Xuân</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/mua-cay-canh-mini-o-dau/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-o-dau-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="mua cay canh mini o dau 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-o-dau-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-o-dau-1-768x511.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-o-dau-1-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-o-dau-1.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">Cửa hàng bán cây cảnh mini chất
                                                            lượng &amp; uy tín MOW Garden</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-nhan-giong-sen-da/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="534"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/huong-dan-cach-nhan-giong-sen-da-1-800x534.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="huong dan cach nhan giong sen da 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/huong-dan-cach-nhan-giong-sen-da-1-800x534.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/huong-dan-cach-nhan-giong-sen-da-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/huong-dan-cach-nhan-giong-sen-da-1-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/huong-dan-cach-nhan-giong-sen-da-1.jpg 1024w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Top 4 Cách Nhân Giống Sen Đá Hiệu
                                                            Quả</h5>
                                                        <div class="is-divider"></div>
                                                        <p class="from_the_blog_comments uppercase is-xsmall"> 1 Comment
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cay-de-ban-lam-viec-hop-menh-kim/" class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Top 10 Cây Để Bàn Làm Việc Hợp
                                                            Mệnh Kim</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/ban-cay-canh-de-ban-lam-viec-tphcm/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-de-ban-tphcm-2-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="mua cay canh mini de ban tphcm 2"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-de-ban-tphcm-2-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-de-ban-tphcm-2.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-de-ban-tphcm-2-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/mua-cay-canh-mini-de-ban-tphcm-2-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">MOW Garden – Shop Bán Cây Cảnh Để
                                                            Bàn tại Tp Hồ Chí Minh</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cac-loai-cay-luoi-ho/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cac-loai-cay-luoi-ho-thong-thuong-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="các loại cây lưỡi hổ"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cac-loai-cay-luoi-ho-thong-thuong-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cac-loai-cay-luoi-ho-thong-thuong-768x511.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cac-loai-cay-luoi-ho-thong-thuong-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/cac-loai-cay-luoi-ho-thong-thuong.jpg 1000w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Danh Sách Các Loại Cây Lưỡi Hổ
                                                            Thông Thường</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-xanh-trong-phong-khach/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-de-trong-phong-khach-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay de trong phong khach 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-de-trong-phong-khach-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-de-trong-phong-khach-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-de-trong-phong-khach-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-de-trong-phong-khach-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">22 Loại Cây Xanh Trồng Trong Phòng
                                                            Khách Giúp Rước Tài Lộc Vào Nhà</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-trong-trong-phong-ngu/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="453"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/top-10-cay-nen-trong-trong-phong-ngu-1-800x453.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="top 10 cây đặt trong phòng ngủ"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/top-10-cay-nen-trong-trong-phong-ngu-1-800x453.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/top-10-cay-nen-trong-trong-phong-ngu-1-768x435.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/top-10-cay-nen-trong-trong-phong-ngu-1-20x11.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/top-10-cay-nen-trong-trong-phong-ngu-1.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Top 10 Cây Trồng Trong Phòng Ngủ
                                                            Giúp Cải Thiện Giấc Ngủ</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cach-cham-soc-cay-canh-trong-nha/" class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="501"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-trong-nha-800x501.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cách chăm sóc cây trong nhà"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-trong-nha-800x501.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-trong-nha-768x481.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-trong-nha-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-cay-trong-nha.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng Dẫn Cách Chăm Sóc Cây Cảnh
                                                            Trong Nhà</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/top-12-cay-canh-van-phong-de-song/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="455"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song-800x455.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay canh van phong de song"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song-800x455.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song-1200x683.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song-768x437.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song-20x11.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/cay-canh-van-phong-de-song.jpg 1400w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Top 12 Cây Cảnh Văn Phòng Dễ Sống
                                                            và Phổ Biến Nhất</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/tieu-canh-terrarium-la-gi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="450"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/terrarium-la-gi-800x450.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="terrarium la gi"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/terrarium-la-gi-800x450.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/terrarium-la-gi-768x432.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/terrarium-la-gi-20x11.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/terrarium-la-gi.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Tiểu cảnh Terrarium là gì? Nghệ
                                                            thuật trồng cây cảnh trong lọ thủy tinh</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-cham-soc-sen-da/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-sen-da-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach cham soc sen da"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-sen-da-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-sen-da.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-sen-da-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-cham-soc-sen-da-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách chăm sóc sen đá
                                                            luôn đẹp và khỏe mạnh</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cac-loai-cay-loc-khong-khi-trong-nha/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-loc-khong-khi-trong-nha-1-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay loc khong khi trong nha 1"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-loc-khong-khi-trong-nha-1-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-loc-khong-khi-trong-nha-1.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-loc-khong-khi-trong-nha-1-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-loc-khong-khi-trong-nha-1-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Top 20 loại cây lọc không khí
                                                            trong nhà</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/phan-tan-cham-la-gi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="602"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham-602x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="phan bon tham cham"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham-602x400.jpg 602w, https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham-800x531.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham-768x510.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham-510x339.jpg 510w, https://mowgarden.com/wp-content/uploads/2021/04/phan-bon-tham-cham.jpg 1200w"
                                                            sizes="(max-width: 602px) 100vw, 602px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Phân tan chậm là gì? Có nên sử
                                                            dụng cho cây trồng không?</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-u-phan-huu-co-tai-nha/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="531"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-u-phan-huu-co-tai-nha-800x531.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach u phan huu co tai nha"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-u-phan-huu-co-tai-nha-800x531.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-u-phan-huu-co-tai-nha-768x510.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-u-phan-huu-co-tai-nha-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/cach-u-phan-huu-co-tai-nha.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn: Cách ủ phân hữu cơ tại
                                                            nhà từ A đến Z</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cach-nhan-giong-cay-kim-tien/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="600"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien-600x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cách nhân giống cây kim tiền"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien-600x400.jpg 600w, https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-nhan-giong-cay-kim-tien-510x340.jpg 510w"
                                                            sizes="(max-width: 600px) 100vw, 600px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">2 cách nhân giống cây kim tiền đơn
                                                            giản dễ thực hiện</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/cach-trong-va-cham-soc-cay-huong-thao/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-cay-huong-thao-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cach trong cay huong thao"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-cay-huong-thao-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-cay-huong-thao.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-cay-huong-thao-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cach-trong-cay-huong-thao-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách trồng và chăm sóc
                                                            cây hương thảo sống bền và khỏe đẹp</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/da-perlite/" class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/da-perlite-la-gi-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="da perlite la gi"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/da-perlite-la-gi-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/da-perlite-la-gi.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/da-perlite-la-gi-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/da-perlite-la-gi-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Đá Perlite là gì? Hướng dẫn sử
                                                            dụng đá Perlite hiệu quả</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cac-loai-hoa-hong-de-trong/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="601"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na-800x601.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="top cac giong hoa hong de trong viet na"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na-800x601.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na-1065x800.jpg 1065w, https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na-768x577.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na-20x15.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/top-cac-giong-hoa-hong-de-trong-viet-na.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Thông tin
                                                            về cây </p>
                                                        <h5 class="post-title is-larger">Top 20 các loại hoa hồng dễ trồng,
                                                            đẹp rực rỡ dành cho người mới</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/peat-moss-la-gi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/peat-moss-la-gi-2.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="peat moss la gi 2"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/peat-moss-la-gi-2.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/peat-moss-la-gi-2-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/peat-moss-la-gi-2-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Rêu than bùn Peat Moss là gì? Công
                                                            dụng và ý nghĩa</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/cay-huong-thao-bi-den-la/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="534"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-huong-thao-bi-den-la-800x534.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay huong thao bi den la"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-huong-thao-bi-den-la-800x534.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-huong-thao-bi-den-la-768x513.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-huong-thao-bi-den-la-20x13.jpg 20w, https://mowgarden.com/wp-content/uploads/2021/04/cay-huong-thao-bi-den-la.jpg 1200w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Kiến thức
                                                            và cách chăm sóc </p>
                                                        <h5 class="post-title is-larger">Hướng dẫn cách phòng, trị cây
                                                            hương thảo bị đen lá</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/hoa-kim-tien-co-y-nghia-gi/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/04/cay-kim-tien-co-hoa-khong-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="cay kim tien co hoa khong"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/04/cay-kim-tien-co-hoa-khong-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/cay-kim-tien-co-hoa-khong.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/cay-kim-tien-co-hoa-khong-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/cay-kim-tien-co-hoa-khong-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> Phong
                                                            Thủy Thông tin về cây </p>
                                                        <h5 class="post-title is-larger">Hoa kim tiền trông như thế nào?
                                                            Cây kim tiền ra hoa có ý nghĩa gì?</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a
                                            href="https://mowgarden.com/bang-gia-cay-bang-singapore-2021/" class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="800"
                                                            height="533"
                                                            src="https://mowgarden.com/wp-content/uploads/2021/03/15-vuon-uom-cay-bang-singapore-gia-re-800x533.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="vườn ươm cây bàng singapore giá rẻ"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2021/03/15-vuon-uom-cay-bang-singapore-gia-re-800x533.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/03/15-vuon-uom-cay-bang-singapore-gia-re.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/03/15-vuon-uom-cay-bang-singapore-gia-re-768x512.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/03/15-vuon-uom-cay-bang-singapore-gia-re-20x13.jpg 20w"
                                                            sizes="(max-width: 800px) 100vw, 800px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">Báo Giá Cây Bàng Singapore Đủ Loại
                                                            2021</h5>
                                                        <div class="is-divider"></div>
                                                        <p class="from_the_blog_comments uppercase is-xsmall"> 1 Comment
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                                <div class="col post-item">
                                    <div class="col-inner"> <a href="https://mowgarden.com/gia-sen-da-bao-nhieu/"
                                            class="plain">
                                            <div class="box box-normal box-text-bottom box-blog-post has-hover">
                                                <div class="box-image">
                                                    <div class="image-cover" style="padding-top:250px;"> <img
                                                            loading="lazy" decoding="async" width="533"
                                                            height="400"
                                                            src="https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-533x400.jpg"
                                                            class="attachment-medium size-medium wp-post-image"
                                                            alt="giá sen đá"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-533x400.jpg 533w, https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu-510x383.jpg 510w, https://mowgarden.com/wp-content/uploads/2020/10/gia-sen-da-bao-nhieu.jpg 1200w"
                                                            sizes="(max-width: 533px) 100vw, 533px"> </div>
                                                </div>
                                                <div class="box-text text-center">
                                                    <div class="box-text-inner blog-post-inner">
                                                        <p class="cat-label tag-label is-xxsmall op-7 uppercase"> MOW
                                                            Garden 360° </p>
                                                        <h5 class="post-title is-larger">Giá sen đá bao nhiêu 1 cây? Nguồn
                                                            hàng sen đá giá rẻ ở đâu?</h5>
                                                        <div class="is-divider"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a> </div>
                                </div>
                            </div>
                        </div>
                        <style>
                            #section_571893357 {
                                padding-top: 30px;
                                padding-bottom: 30px;
                            }

                            #section_571893357 .ux-shape-divider--top svg {
                                height: 150px;
                                --divider-top-width: 100%;
                            }

                            #section_571893357 .ux-shape-divider--bottom svg {
                                height: 150px;
                                --divider-width: 100%;
                            }
                        </style>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
