@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" role="main" class="content-area">
        <section class="section dark" id="section_927622080">
            <div class="bg section-bg fill bg-fill bg-loaded">
                <div class="section-bg-overlay absolute fill"></div>
            </div>
            <div class="section-content relative">
                <div class="row" id="row-720806595">
                    <div id="col-473854894" class="col small-12 large-12">
                        <div class="col-inner text-center">
                            <div id="text-1831414774" class="text">
                                <p style="font-size: 200%;"><strong>Liên hệ với Cào Cào Garden</strong></p>
                                <style>
                                    #text-1831414774 {
                                        font-size: 1.5rem;
                                        line-height: 1;
                                    }
                                </style>
                            </div>
                        </div>
                        <style>
                            #col-473854894>.col-inner {
                                padding: 20px 0px 0px 0px;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <style>
                #section_927622080 {
                    padding-top: 140px;
                    padding-bottom: 140px;
                    background-color: rgb(40, 40, 40);
                }

                #section_927622080 .section-bg-overlay {
                    background-color: rgba(0, 0, 0, 0.3);
                }

                #section_927622080 .section-bg.bg-loaded {
                    background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/lien-he-mowgarden.jpg);
                }

                #section_927622080 .section-bg {
                    background-position: 45% 0%;
                }

                #section_927622080 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_927622080 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
        <section class="section" id="section_186784127">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row" id="row-509843538">
                    <div id="col-1376567193" class="col small-12 large-12">
                        <div class="col-inner">
                            <div class="row align-equal" id="row-2035999207">
                                {{-- <div id="col-129645817" class="col medium-6 small-12 large-6">
                                    <div class="col-inner text-center">
                                        <div class="icon-box featured-box icon-box-center text-center">
                                            <div class="icon-box-img" style="width: 60px">
                                                <div class="icon">
                                                    <div class="icon-inner"> <img loading="lazy" decoding="async"
                                                            width="512" height="512"
                                                            src="https://mowgarden.com/wp-content/uploads/2022/01/call.png"
                                                            class="attachment-medium size-medium" alt="call"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/call.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/call-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/call-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/call-125x125.png 125w"
                                                            sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                </div>
                                            </div>
                                            <div class="icon-box-text last-reset">
                                                <p><span style="font-size: 150%;"><strong>Thông tin liên hệ</strong></span>
                                                </p>
                                                <ul class="list-info">
                                                    <li style="text-align: left;"><strong>Địa chỉ vườn: </strong>18 Kiệt 116 Nguyễn Lộ
                                                        Trạch, tổ 14, Thành phố Huế, Thừa Thiên Huế</li>
                                                    <li style="text-align: left;"><strong>Hotline:&nbsp;</strong>0783 655 566</li>
                                                    <li style="text-align: left;">
                                                        <strong>Email:&nbsp;</strong>caocaogarden@gmail.com</li>
                                                    <li style="text-align: left;"><strong>Mở cửa: </strong>8:00 – 19:30</li>
                                                </ul>
                                                <div id="gap-449993108" class="gap-element clearfix"
                                                    style="display:block; height:auto;">
                                                    <style>
                                                        #gap-449993108 {
                                                            padding-top: 20px;
                                                        }
                                                    </style>
                                                </div> <a rel="noopener noreferrer" href="" target="_blank"
                                                    class="button primary"> <span>0783 655 566</span> </a>
                                            </div>
                                        </div>
                                        <div class="icon-box featured-box icon-box-center text-center">
                                            <div class="icon-box-img" style="width: 60px">
                                                <div class="icon">
                                                    <div class="icon-inner"> <img loading="lazy" decoding="async"
                                                            width="512" height="512"
                                                            src="https://mowgarden.com/wp-content/uploads/2022/01/call.png"
                                                            class="attachment-medium size-medium" alt="call"
                                                            srcset="https://mowgarden.com/wp-content/uploads/2022/01/call.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/call-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/call-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/call-125x125.png 125w"
                                                            sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                </div>
                                            </div>
                                            <div class="icon-box-text last-reset">
                                                <div id="gap-1536775265" class="gap-element clearfix"
                                                    style="display:block; height:auto;">
                                                    <style>
                                                        #gap-1536775265 {
                                                            padding-top: 20px;
                                                        }
                                                    </style>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> --}}

                                <div id="col-968252483" class="col small-12 large-12">
                                    <div class="col-inner text-center" style="background-color:rgb(255,255,255);">
                                        <p><span style="font-family: 'book antiqua', palatino, serif; font-size: 200%;"><strong>Để
                                                    lại lời nhắn</strong></span>
                                        <p></p>
                                        <div class="is-divider divider clearfix"></div>
                                        <div class="row align-center" id="row-1607714082">
                                            <div id="col-250314886" class="col medium-11 small-12 large-9">
                                                <div class="col-inner text-left">
                                                    <div class="wpcf7 js" id="wpcf7-f11-p1471-o1" lang="en-US"
                                                        dir="ltr">
                                                        <div class="screen-reader-response">
                                                            <p role="status" aria-live="polite" aria-atomic="true">
                                                            </p>
                                                            <ul></ul>
                                                        </div>
                                                        <form action="/lien-he/#wpcf7-f11-p1471-o1" method="post"
                                                            class="wpcf7-form init" aria-label="Contact form"
                                                            novalidate="novalidate" data-status="init">
                                                            <div style="display: none;"> <input type="hidden"
                                                                    name="_wpcf7" value="11"> <input type="hidden"
                                                                    name="_wpcf7_version" value="5.7.7"> <input
                                                                    type="hidden" name="_wpcf7_locale" value="en_US">
                                                                <input type="hidden" name="_wpcf7_unit_tag"
                                                                    value="wpcf7-f11-p1471-o1"> <input type="hidden"
                                                                    name="_wpcf7_container_post" value="1471">
                                                                <input type="hidden" name="_wpcf7_posted_data_hash"
                                                                    value="">
                                                            </div>
                                                            <div class="form-flat">
                                                                <p><span class="wpcf7-form-control-wrap"
                                                                        data-name="your-name"><input size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                            aria-required="true" aria-invalid="false"
                                                                            placeholder="Tên" value="" type="text"
                                                                            name="your-name"></span>
                                                                </p>
                                                                <p><span class="wpcf7-form-control-wrap"
                                                                        data-name="your-email"><input size="40"
                                                                            class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                            aria-required="true" aria-invalid="false"
                                                                            placeholder="Email" value=""
                                                                            type="email" name="your-email"></span>
                                                                </p>
                                                                <p><span class="wpcf7-form-control-wrap"
                                                                        data-name="your-message">
                                                                        <textarea cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"
                                                                            placeholder="Tin nhắn" name="your-message"></textarea>
                                                                    </span> </p>
                                                                <p><input
                                                                        class="wpcf7-form-control has-spinner wpcf7-submit button"
                                                                        type="submit" value="Gửi"><span
                                                                        class="wpcf7-spinner"></span> </p>
                                                            </div>
                                                            <div class="wpcf7-response-output" aria-hidden="true">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                                <div id="col-577251816" class="col small-12 large-12">
                                    <div class="col-inner text-left">
                                        <p>
                                        <div class="mapouter">
                                            <div class="gmap_canvas"><iframe
                                                    src="https://maps.google.com/maps?q=c%C3%A0o%20c%C3%A0o%20garden&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                                                    frameborder="0" scrolling="no"
                                                    style="width: 970px; height: 490px;"></iframe>
                                                <style>
                                                    .mapouter {
                                                        position: relative;
                                                        height: 490px;
                                                        width: 970px !important;
                                                        background: #fff;
                                                    }

                                                    .maprouter a {
                                                        color: #fff !important;
                                                        position: absolute !important;
                                                        top: 0 !important;
                                                        z-index: 0 !important;
                                                    }
                                                </style><a href="https://blooketjoin.org/blooket-login/">blooket login</a>
                                                <style>
                                                    .gmap_canvas {
                                                        overflow: hidden;
                                                        height: 490px;
                                                        width: 970px
                                                    }

                                                    .gmap_canvas iframe {
                                                        position: relative;
                                                        z-index: 2
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                        </p>
                                    </div>
                                </div>
                                <style>
                                    #row-2035999207>.col>.col-inner {
                                        padding: 40px 40px 40px 40px;
                                        background-color: rgb(255, 255, 255);
                                    }
                                </style>
                            </div>
                        </div>
                        <style>
                            #col-1376567193>.col-inner {
                                margin: -100px 0px 0px 0px;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <style>
                #section_186784127 {
                    padding-top: 30px;
                    padding-bottom: 30px;
                    background-color: rgb(244, 244, 244);
                }

                #section_186784127 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_186784127 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
    </div>
@endsection
