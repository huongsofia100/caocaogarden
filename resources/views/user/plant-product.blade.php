@extends('layouts.layout-comon')

@section('headerproduct')
@include('layouts.category-page')
<div class="shop-page-title category-page-title page-title">
    <div class="page-title-inner flex-row medium-flex-wrap container">
        <div class="flex-col flex-grow medium-text-center">
            <div class="is-larger">
                <nav class="rank-math-breadcrumb breadcrumbs">
                    <p><a href="https://mowgarden.com">Trang chủ</a><span class="separator"> » </span><span
                            class="last">Cây Ở Vườn</span></p>
                </nav>
            </div>
            <div class="category-filtering category-filter-row"> <a href="#" data-open="#shop-sidebar"
                    data-pos="left" class="filter-button uppercase plain"> <i class="fa-solid fa-list"></i> <strong>Chọn
                        danh mục</strong> </a>
                <div class="inline-block"> </div>
            </div>
        </div>
        <div class="flex-col medium-text-center">
            <p class="woocommerce-result-count hide-for-medium"> Hiển thị 1–48 của 730 kết quả</p>
            <form class="woocommerce-ordering" method="get"> <select name="orderby" class="orderby"
                    aria-label="Đơn hàng của cửa hàng">
                    <option value="popularity">Thứ tự theo mức độ phổ biến</option>
                    <option value="rating">Thứ tự theo điểm đánh giá</option>
                    <option value="date" selected="selected">Mới nhất</option>
                    <option value="price">Thứ tự theo giá: thấp đến cao</option>
                    <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                </select> <input type="hidden" name="paged" value="1"> </form>
        </div>
    </div>
</div>
@endsection

@section('maincontent')
    <div class="row category-page-row">
        <div class="col large-12">
            <div class="shop-container">
                <div class="woocommerce-notices-wrapper"></div>
                <div
                    class="products row row-small large-columns-4 medium-columns-4 small-columns-2 has-shadow row-box-shadow-2-hover has-equal-box-heights equalize-box">
                    <div
                        class="product-small col has-hover product type-product post-14478 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-phat-tai-bo-5-cay-thiet-moc-lan-cptk001/"
                                            aria-label="Cây phát tài bộ 5 - Cây thiết mộc lan CPTK001"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/cay-phat-tai-bo-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/cay-phat-tai-bo-mowgarden-02-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon" aria-label="Wishlist">
                                                <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14478 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14478"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14478,&quot;parent_product_id&quot;:14478,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14478&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14478" data-product-type="simple"
                                                            data-original-product-id="14478" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14478" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14478" data-product_sku="CPTK001"
                                            aria-label="Thêm “Cây phát tài bộ 5 - Cây thiết mộc lan CPTK001” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14478" href="#quick-view">Xem
                                            nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-phat-tai-bo-5-cay-thiet-moc-lan-cptk001/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây phát
                                                tài bộ 5 – Cây thiết mộc lan CPTK001</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>750.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14421 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                            aria-label="Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/cay-kim-ngan-3-than-chau-trang-mowgarden-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14421 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14421"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14421,&quot;parent_product_id&quot;:14421,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14421&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14421" data-product-type="simple"
                                                            data-original-product-id="14421" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14421" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14421" data-product_sku="LONI040"
                                            aria-label="Thêm “Cây kim ngân ba thân để bàn chậu sứ gấu BearBrick LONI040” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14421"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-ba-than-de-ban-chau-su-gau-bearbrick-loni040/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây kim
                                                ngân ba thân để bàn chậu sứ gấu BearBrick LONI040</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>280.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14419 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                            aria-label="Cây trầu bà đế vương xanh chậu mặt cool 'Imperial Green' chậu sứ PHIG006">
                                            <img width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/cay-trau-ba-de-vuong-xanh-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14419 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14419"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14419,&quot;parent_product_id&quot;:14419,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14419&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14419" data-product-type="simple"
                                                            data-original-product-id="14419" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14419" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14419" data-product_sku="PHIG006"
                                            aria-label="Thêm “Cây trầu bà đế vương xanh chậu mặt cool 'Imperial Green' chậu sứ PHIG006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14419"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-chau-mat-cool-imperial-green-chau-su-phig006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà đế vương xanh chậu mặt cool ‘Imperial Green’ chậu sứ PHIG006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14417 status-publish last instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-mini product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                            aria-label="Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002">
                                            <img width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/luoi-ho-nga-voi-mowgarden-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14417 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14417"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14417,&quot;parent_product_id&quot;:14417,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14417&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14417" data-product-type="simple"
                                                            data-original-product-id="14417" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14417" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14417" data-product_sku="sans001-1"
                                            aria-label="Thêm “Cây Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14417"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-nga-voi-vuong-mien-chau-su-de-ban-sansevireria-cylindrica-sans002/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                Lưỡi Hổ Vương Miện chậu sứ để bàn Sansevireria Cylindrica SANS002</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14415 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                            aria-label="Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/co-lan-chi-de-ban-mowgaden-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14415 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14415"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14415,&quot;parent_product_id&quot;:14415,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14415&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14415" data-product-type="simple"
                                                            data-original-product-id="14415" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14415" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14415" data-product_sku=""
                                            aria-label="Thêm “Cây cỏ lan chi để bàn chậu sứ mặt cười SPI004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14415"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-co-lan-chi-de-ban-chau-su-mat-cuoi-spi004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây cỏ
                                                lan chi để bàn chậu sứ mặt cười SPI004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14413 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-canh-de-ban product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1">
                                <div class="callout badge badge-square">
                                    <div class="badge-inner secondary on-sale"><span class="onsale">-9%</span></div>
                                </div>
                            </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                            aria-label="Cây trầu bà đế vương đỏ để bàn 'Red Rojo' chậu sứ PHIR008"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/07/trau-ba-de-vuong-do-de-ban-mowgarden-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14413 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14413"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14413,&quot;parent_product_id&quot;:14413,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14413&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14413" data-product-type="simple"
                                                            data-original-product-id="14413" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14413" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14413" data-product_sku="PHIR008"
                                            aria-label="Thêm “Cây trầu bà đế vương đỏ để bàn 'Red Rojo' chậu sứ PHIR008” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14413"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-de-vuong-do-de-ban-red-rojo-chau-su/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà đế vương đỏ để bàn ‘Red Rojo’ chậu sứ PHIR008</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><del
                                                aria-hidden="true"><span
                                                    class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></del>
                                            <ins><span class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></ins></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14341 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                            aria-label="Cây lưỡi hổ Bantel Sensation chậu ươm STBS001"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-luoi-ho-bengar-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14341 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14341"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14341,&quot;parent_product_id&quot;:14341,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14341&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14341" data-product-type="simple"
                                                            data-original-product-id="14341" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14341" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14341" data-product_sku="STBS001"
                                            aria-label="Thêm “Cây lưỡi hổ Bantel Sensation chậu ươm STBS001” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14341"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-luoi-ho-bantel-sensation-chau-uom-stbs001/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                lưỡi hổ Bantel Sensation chậu ươm STBS001</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14334 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-lon-chau-da-mai-rade033/"
                                            aria-label="Cây hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hanh-phuc-2-tang-chau-da-mai-tron-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hanh-phuc-2-tang-chau-da-mai-tron-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14334 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14334"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14334,&quot;parent_product_id&quot;:14334,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14334&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14334" data-product-type="simple"
                                                            data-original-product-id="14334" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14334" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14334" data-product_sku="RADE033"
                                            aria-label="Thêm “Cây hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14334"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-lon-chau-da-mai-rade033/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hạnh phúc để sàn 2 tầng lớn chậu đá mài RADE033</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13997 status-publish first instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-cot-chau-xi-mang-tru-vuong-ctbc007/"
                                            aria-label="Cây trầu bà cột chậu xi măng trụ vuông CTBC007"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-trau-ba-cot-chau-xi-mang-van-soc-ngang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-trau-ba-cot-chau-xi-mang-van-soc-ngang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13997 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13997"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13997,&quot;parent_product_id&quot;:13997,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13997&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13997" data-product-type="simple"
                                                            data-original-product-id="13997" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13997" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13997" data-product_sku="CTBC007"
                                            aria-label="Thêm “Cây trầu bà cột chậu xi măng trụ vuông CTBC007” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13997"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-cot-chau-xi-mang-tru-vuong-ctbc007/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà cột chậu xi măng trụ vuông CTBC007</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.100.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13989 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                            aria-label="Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-hong-mon-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13989 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13989"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13989,&quot;parent_product_id&quot;:13989,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13989&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13989" data-product-type="simple"
                                                            data-original-product-id="13989" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13989" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13989" data-product_sku="ANTH010"
                                            aria-label="Thêm “Cây hồng môn cỡ nhỏ chậu sứ trắng ANTH010” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13989"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-hong-mon-co-nho-chau-su-trang-anth010/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hồng môn cỡ nhỏ chậu sứ trắng ANTH010</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13986 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                            aria-label="Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/cay-ngu-gia-bi-cam-thach-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13986 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13986"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13986,&quot;parent_product_id&quot;:13986,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13986&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13986" data-product-type="simple"
                                                            data-original-product-id="13986" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13986" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13986" data-product_sku="SCHE020"
                                            aria-label="Thêm “Cây ngũ gia bì cẩm thạch nhỏ chậu ươm SCHE020” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13986"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-ngu-gia-bi-cam-thach-nho-chau-uom-sche020/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây ngũ
                                                gia bì cẩm thạch nhỏ chậu ươm SCHE020</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13905 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1">
                                <div class="callout badge badge-square">
                                    <div class="badge-inner secondary on-sale"><span class="onsale">-6%</span></div>
                                </div>
                            </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                            aria-label="Cây kim ngân một thân để bàn chậu sứ LONI039"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-mot-than-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13905 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13905"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13905,&quot;parent_product_id&quot;:13905,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13905&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13905" data-product-type="simple"
                                                            data-original-product-id="13905" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13905" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13905" data-product_sku="LONI039"
                                            aria-label="Thêm “Cây kim ngân một thân để bàn chậu sứ LONI039” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13905"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-mot-than-de-ban-chau-su-loni039/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây kim
                                                ngân một thân để bàn chậu sứ LONI039</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><del
                                                aria-hidden="true"><span
                                                    class="woocommerce-Price-amount amount"><bdi>480.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></del>
                                            <ins><span class="woocommerce-Price-amount amount"><bdi>450.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></ins></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13901 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                            aria-label="Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1a-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-co-la-xe-de-ban-chau-su-trang-1-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13901 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13901"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13901,&quot;parent_product_id&quot;:13901,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13901&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13901" data-product-type="simple"
                                                            data-original-product-id="13901" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13901" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13901" data-product_sku="LIVI005"
                                            aria-label="Thêm “Cây cọ lá xẻ mini để bàn chậu sứ hoa văn LIVI005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13901"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-co-la-xe-mini-de-ban-chau-su-hoa-van-livi005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây cọ
                                                lá xẻ mini để bàn chậu sứ hoa văn LIVI005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13860 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-chau-su-hoa-van-rade032/"
                                            aria-label="Cây hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-van-phong-2-tang-chau-su-hoa-tiet-mowgarden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-van-phong-2-tang-chau-su-hoa-tiet-mowgarden-01-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13860 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13860"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13860,&quot;parent_product_id&quot;:13860,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13860&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13860" data-product-type="simple"
                                                            data-original-product-id="13860" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13860" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13860" data-product_sku="RADE032"
                                            aria-label="Thêm “Cây hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13860"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-hanh-phuc-de-san-2-tang-chau-su-hoa-van-rade032/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hạnh phúc để sàn 2 tầng chậu sứ hoa văn RADE032</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>900.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13835 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1">
                                <div class="callout badge badge-square">
                                    <div class="badge-inner secondary on-sale"><span class="onsale">-8%</span></div>
                                </div>
                            </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                            aria-label="Cây hạnh phúc để sàn chậu sứ RADE031"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-trang-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hanh-phuc-de-san-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13835 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13835"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13835,&quot;parent_product_id&quot;:13835,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13835&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13835" data-product-type="simple"
                                                            data-original-product-id="13835"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13835" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13835" data-product_sku="RADE031"
                                            aria-label="Thêm “Cây hạnh phúc để sàn chậu sứ RADE031” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13835"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-hanh-phuc-de-san-chau-su-rade031/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hạnh phúc để sàn chậu sứ RADE031</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><del
                                                aria-hidden="true"><span
                                                    class="woocommerce-Price-amount amount"><bdi>600.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></del>
                                            <ins><span class="woocommerce-Price-amount amount"><bdi>550.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></ins></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13812 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-de-ban-that-binh-tieu-canh-chau-su-loni038/"
                                            aria-label="Cây kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-that-binh-lon-de-san-chau-su-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-de-ban-that-binh-chau-su-mowgarden-02-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13812 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13812"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13812,&quot;parent_product_id&quot;:13812,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13812&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13812" data-product-type="simple"
                                                            data-original-product-id="13812"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13812" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13812" data-product_sku="LONI038"
                                            aria-label="Thêm “Cây kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13812"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-de-ban-that-binh-tieu-canh-chau-su-loni038/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                kim ngân để bàn thắt bính tiểu cảnh chậu sứ LONI038</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>380.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13807 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-luoi-ho-xanh-mini-black-gold-chau-su-shbg004-sao-chep/"
                                            aria-label="Cây lưỡi hổ xanh để bàn mini 'Black Gold' chậu sứ SHBG005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/luoi-ho-xanh-mini-mowgarden-01-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/luoi-ho-xanh-mini-mowgarden-02-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13807 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13807"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13807,&quot;parent_product_id&quot;:13807,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13807&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13807" data-product-type="simple"
                                                            data-original-product-id="13807"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13807" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13807" data-product_sku="SHBG005"
                                            aria-label="Thêm “Cây lưỡi hổ xanh để bàn mini 'Black Gold' chậu sứ SHBG005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13807"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-luoi-ho-xanh-mini-black-gold-chau-su-shbg004-sao-chep/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                lưỡi hổ xanh để bàn mini ‘Black Gold’ chậu sứ SHBG005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13802 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-lan-y-chau-co-lon-de-ban-chau-su-trang-peac005/"
                                            aria-label="Cây Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-lan-y-de-ban-chau-su-mowgarden-01-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-lan-y-de-ban-chau-su-mowgarden-02-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13802 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13802"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13802,&quot;parent_product_id&quot;:13802,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13802&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13802" data-product-type="simple"
                                                            data-original-product-id="13802"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13802" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13802" data-product_sku="PEAC005"
                                            aria-label="Thêm “Cây Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13802"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-lan-y-chau-co-lon-de-ban-chau-su-trang-peac005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                Lan ý chậu cỡ lớn để bàn chậu sứ trắng PEAC005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13797 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-canh-nhiet-doi product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-phu-quy-chau-su-tho-cam-de-ban-agla104/"
                                            aria-label="Cây phú quý chậu sứ thổ cẩm để bàn AGLA104"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phu-quy-chau-su-tho-cam-mow-garden-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phu-quy-chau-su-tho-cam-mow-garden-02-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13797 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13797"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13797,&quot;parent_product_id&quot;:13797,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13797&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13797" data-product-type="simple"
                                                            data-original-product-id="13797"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13797" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13797" data-product_sku="AGLA104"
                                            aria-label="Thêm “Cây phú quý chậu sứ thổ cẩm để bàn AGLA104” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13797"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-phu-quy-chau-su-tho-cam-de-ban-agla104/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                phú quý chậu sứ thổ cẩm để bàn AGLA104</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13789 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-imperial-green-chau-su-phig005/"
                                            aria-label="Cây trầu bà đế vương xanh 'Imperial Green' chậu sứ PHIG005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/12-cay-trau-ba-de-vuong-xanh-de-ban-chau-su-trang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13789 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13789"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13789,&quot;parent_product_id&quot;:13789,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13789&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13789" data-product-type="simple"
                                                            data-original-product-id="13789"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13789" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13789" data-product_sku="PHIG005"
                                            aria-label="Thêm “Cây trầu bà đế vương xanh 'Imperial Green' chậu sứ PHIG005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13789"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-de-vuong-xanh-imperial-green-chau-su-phig005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà đế vương xanh ‘Imperial Green’ chậu sứ PHIG005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13784 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-ngoc-ngan-de-ban-chau-su-agsn010/"
                                            aria-label="Cây ngọc ngân để bàn chậu sứ AGSN010"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/10-cay-ngoc-ngan-nho-de-ban-chau-su--800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/10-cay-ngoc-ngan-nho-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13784 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13784"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13784,&quot;parent_product_id&quot;:13784,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13784&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13784" data-product-type="simple"
                                                            data-original-product-id="13784"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13784" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13784" data-product_sku="AGSN010"
                                            aria-label="Thêm “Cây ngọc ngân để bàn chậu sứ AGSN010” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13784"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-ngoc-ngan-de-ban-chau-su-agsn010/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                ngọc ngân để bàn chậu sứ AGSN010</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13781 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-3-than-de-ban-chau-marle-loni037/"
                                            aria-label="Cây kim ngân 3 thân để bàn chậu marle LONI037"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/9-cay-kim-ngan-3-than-nho-de-ban-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/9-cay-kim-ngan-3-than-nho-de-ban-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13781 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13781"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13781,&quot;parent_product_id&quot;:13781,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13781&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13781" data-product-type="simple"
                                                            data-original-product-id="13781"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13781" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13781" data-product_sku="LONI037"
                                            aria-label="Thêm “Cây kim ngân 3 thân để bàn chậu marle LONI037” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13781"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-3-than-de-ban-chau-marle-loni037/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                kim ngân 3 thân để bàn chậu marle LONI037</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>140.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13777 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-ban-cong product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                            aria-label="Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/11-cay-bang-dai-loan-cam-thach-chau-su-trang-3-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13777 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13777"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13777,&quot;parent_product_id&quot;:13777,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13777&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13777" data-product-type="simple"
                                                            data-original-product-id="13777"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13777" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13777" data-product_sku="BUBU007"
                                            aria-label="Thêm “Cây bàng Đài Loan cẩm thạch chậu sứ BUBU007” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13777"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-bang-dai-loan-cam-thach-chau-su-bubu007/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                bàng Đài Loan cẩm thạch chậu sứ BUBU007</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.200.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13773 status-publish last instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-mot-than-co-thu-de-ban-chau-su-rade030/"
                                            aria-label="Cây hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/8-cay-hanh-phuc-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/8-cay-hanh-phuc-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13773 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13773"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13773,&quot;parent_product_id&quot;:13773,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13773&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13773" data-product-type="simple"
                                                            data-original-product-id="13773"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13773" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13773" data-product_sku="RADE030"
                                            aria-label="Thêm “Cây hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13773"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-hanh-phuc-mot-than-co-thu-de-ban-chau-su-rade030/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hạnh phúc một thân cổ thụ để bàn chậu sứ RADE030</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13768 status-publish first instock product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-bang-singapore-mini-de-ban-chau-su-lyra048/"
                                            aria-label="Cây bàng Singapore mini để bàn chậu sứ LYRA048"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/7-cay-bang-singapore-nho-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/7-cay-bang-singapore-nho-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13768 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13768"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13768,&quot;parent_product_id&quot;:13768,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13768&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13768" data-product-type="simple"
                                                            data-original-product-id="13768"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13768" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13768" data-product_sku="LYRA048"
                                            aria-label="Thêm “Cây bàng Singapore mini để bàn chậu sứ LYRA048” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13768"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-bang-singapore-mini-de-ban-chau-su-lyra048/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                bàng Singapore mini để bàn chậu sứ LYRA048</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13763 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-canh-de-ban product_cat-cay-phong-thuy product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hanh-phuc-mini-de-ban-chau-su-rade029/"
                                            aria-label="Cây hạnh phúc mini để bàn chậu sứ RADE029"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/5-cay-hanh-phuc-nho-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/5-cay-hanh-phuc-nho-de-ban-chau-su-3-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13763 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13763"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13763,&quot;parent_product_id&quot;:13763,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13763&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13763" data-product-type="simple"
                                                            data-original-product-id="13763"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13763" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13763" data-product_sku="RADE029"
                                            aria-label="Thêm “Cây hạnh phúc mini để bàn chậu sứ RADE029” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13763"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-hanh-phuc-mini-de-ban-chau-su-rade029/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hạnh phúc mini để bàn chậu sứ RADE029</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>220.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13760 status-publish instock product_cat-ban-cay-canh-trong-nha product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-tien-mini-de-ban-chau-su-zami033/"
                                            aria-label="Cây kim tiền mini để bàn chậu sứ ZAMI033"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/4-cay-kim-tien-mini-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/4-cay-kim-tien-mini-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13760 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13760"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13760,&quot;parent_product_id&quot;:13760,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13760&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13760" data-product-type="simple"
                                                            data-original-product-id="13760"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13760" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13760" data-product_sku=""
                                            aria-label="Thêm “Cây kim tiền mini để bàn chậu sứ ZAMI033” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13760"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-kim-tien-mini-de-ban-chau-su-zami033/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                kim tiền mini để bàn chậu sứ ZAMI033</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13757 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-van-loc-son-red-anjamani-de-ban-chau-gom-hoa-agra005/"
                                            aria-label="Cây vạn lộc son ‘Red Anjamani’ để bàn chậu gốm hoa AGRA005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/3-cay-van-loc-son-chau-gom-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/3-cay-van-loc-son-chau-gom-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13757 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13757"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13757,&quot;parent_product_id&quot;:13757,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13757&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13757" data-product-type="simple"
                                                            data-original-product-id="13757"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13757" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13757" data-product_sku="AGRA005"
                                            aria-label="Thêm “Cây vạn lộc son ‘Red Anjamani’ để bàn chậu gốm hoa AGRA005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13757"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-van-loc-son-red-anjamani-de-ban-chau-gom-hoa-agra005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                vạn lộc son ‘Red Anjamani’ để bàn chậu gốm hoa AGRA005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13754 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-truong-sinh-xanh-de-ban-chau-su-peob006/"
                                            aria-label="Cây trường sinh xanh để bàn chậu sứ PEOB006"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/2-cay-truong-sinh-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/2-cay-truong-sinh-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13754 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13754"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13754,&quot;parent_product_id&quot;:13754,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13754&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13754" data-product-type="simple"
                                                            data-original-product-id="13754"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13754" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13754" data-product_sku="PEOB006"
                                            aria-label="Thêm “Cây trường sinh xanh để bàn chậu sứ PEOB006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13754"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-truong-sinh-xanh-de-ban-chau-su-peob006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trường sinh xanh để bàn chậu sứ PEOB006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>140.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13750 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-canh-nhiet-doi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-vang-chau-su-hoa-tiet-phig006/"
                                            aria-label="Cây trầu bà đế vương vàng chậu sứ họa tiết PHGO006"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/1-cay-trau-ba-de-vuong-vang-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/1-cay-trau-ba-de-vuong-vang-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13750 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13750"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13750,&quot;parent_product_id&quot;:13750,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13750&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13750" data-product-type="simple"
                                                            data-original-product-id="13750"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13750" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13750" data-product_sku="PHGO006"
                                            aria-label="Thêm “Cây trầu bà đế vương vàng chậu sứ họa tiết PHGO006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13750"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-de-vuong-vang-chau-su-hoa-tiet-phig006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà đế vương vàng chậu sứ họa tiết PHGO006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13740 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-van-loc-son-red-anjamani-de-ban-chau-su-agra004/"
                                            aria-label="Cây vạn lộc son ‘Red Anjamani’ để bàn chậu sứ AGRA004"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-van-loc-son-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-van-loc-son-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13740 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13740"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13740,&quot;parent_product_id&quot;:13740,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13740&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13740" data-product-type="simple"
                                                            data-original-product-id="13740"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13740" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13740" data-product_sku="AGRA004"
                                            aria-label="Thêm “Cây vạn lộc son ‘Red Anjamani’ để bàn chậu sứ AGRA004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13740"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-van-loc-son-red-anjamani-de-ban-chau-su-agra004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                vạn lộc son ‘Red Anjamani’ để bàn chậu sứ AGRA004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>220.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13737 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-no-de-ban-chau-su-trang-loni036/"
                                            aria-label="Cây kim ngân nơ để bàn chậu sứ trắng LONI036"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-no-de-ban-chau-su-trang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-no-de-ban-chau-su-trang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13737 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13737"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13737,&quot;parent_product_id&quot;:13737,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13737&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13737" data-product-type="simple"
                                                            data-original-product-id="13737"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13737" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13737" data-product_sku="LONI036"
                                            aria-label="Thêm “Cây kim ngân nơ để bàn chậu sứ trắng LONI036” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13737"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-no-de-ban-chau-su-trang-loni036/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                kim ngân nơ để bàn chậu sứ trắng LONI036</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>150.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13715 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-luoi-ho-thai-de-ban-futura-superba-chau-su-safs005/"
                                            aria-label="Cây lưỡi hổ Thái để bàn 'Futura superba' chậu sứ SAFS005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-luoi-ho-thai-lun-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-luoi-ho-thai-lun-de-ban-chau-su-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13715 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13715"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13715,&quot;parent_product_id&quot;:13715,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13715&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13715" data-product-type="simple"
                                                            data-original-product-id="13715"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13715" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13715" data-product_sku="SAFS005"
                                            aria-label="Thêm “Cây lưỡi hổ Thái để bàn 'Futura superba' chậu sứ SAFS005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13715"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-luoi-ho-thai-de-ban-futura-superba-chau-su-safs005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                lưỡi hổ Thái để bàn ‘Futura superba’ chậu sứ SAFS005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>360.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13711 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-thanh-xuan-chau-su-hoa-tiet-tbtx010/"
                                            aria-label="Cây trầu bà thanh xuân chậu sứ họa tiết TBTX010"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-trau-ba-thanh-xuan-chau-su-xuat-khau-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-trau-ba-thanh-xuan-chau-su-xuat-khau-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13711 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13711"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13711,&quot;parent_product_id&quot;:13711,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13711&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13711" data-product-type="simple"
                                                            data-original-product-id="13711"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13711" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13711" data-product_sku="TBTX009-1"
                                            aria-label="Thêm “Cây trầu bà thanh xuân chậu sứ họa tiết TBTX010” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13711"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-thanh-xuan-chau-su-hoa-tiet-tbtx010/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà thanh xuân chậu sứ họa tiết TBTX010</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>950.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13596 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-de-ban-chau-su-tho-cam-sche019/"
                                            aria-label="Cây ngũ gia bì để bàn chậu sứ thổ cẩm SCHE019"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-ngu-gia-bi-chau-su-hoa-tiet-trang-tri-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13596 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13596"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13596,&quot;parent_product_id&quot;:13596,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13596&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13596" data-product-type="simple"
                                                            data-original-product-id="13596"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13596" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13596" data-product_sku="SCHE019"
                                            aria-label="Thêm “Cây ngũ gia bì để bàn chậu sứ thổ cẩm SCHE019” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13596"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-ngu-gia-bi-de-ban-chau-su-tho-cam-sche019/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                ngũ gia bì để bàn chậu sứ thổ cẩm SCHE019</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>220.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13592 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-phat-tai-nui-2-tang-dang-thap-chau-da-mai-cptn014/"
                                            aria-label="Cây phát tài núi 2 tầng dáng thấp chậu đá mài CPTN014"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phat-tai-nui-chau-da-mai-wax-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phat-tai-nui-chau-da-mai-wax-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13592 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13592"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13592,&quot;parent_product_id&quot;:13592,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13592&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13592" data-product-type="simple"
                                                            data-original-product-id="13592"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13592" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13592" data-product_sku="CPTN014"
                                            aria-label="Thêm “Cây phát tài núi 2 tầng dáng thấp chậu đá mài CPTN014” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13592"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-phat-tai-nui-2-tang-dang-thap-chau-da-mai-cptn014/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                phát tài núi 2 tầng dáng thấp chậu đá mài CPTN014</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>1.650.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13579 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-canh-van-phong product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-tung-bong-lai-bonsai-tieu-canh-chau-su-tubo004/"
                                            aria-label="Cây tùng bồng lai bonsai tiểu cảnh chậu sứ TUBO004"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-tung-bong-lai-bonsai-tieu-canh-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-tung-bong-lai-bonsai-tieu-canh-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13579 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13579"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13579,&quot;parent_product_id&quot;:13579,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13579&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13579" data-product-type="simple"
                                                            data-original-product-id="13579"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13579" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13579" data-product_sku="TUBO004"
                                            aria-label="Thêm “Cây tùng bồng lai bonsai tiểu cảnh chậu sứ TUBO004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13579"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-tung-bong-lai-bonsai-tieu-canh-chau-su-tubo004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                tùng bồng lai bonsai tiểu cảnh chậu sứ TUBO004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>2.200.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13576 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-can-it-anh-sang product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-trau-ba-de-vuong-do-red-rojo-chau-su-phir007/"
                                            aria-label="Cây trầu bà đế vương đỏ 'Red Rojo' chậu sứ PHIR007"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-trau-ba-de-vuong-do-chau-su-hoa-tiet-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-trau-ba-de-vuong-do-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13576 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13576"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13576,&quot;parent_product_id&quot;:13576,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13576&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13576" data-product-type="simple"
                                                            data-original-product-id="13576"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13576" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13576" data-product_sku="PHIR007"
                                            aria-label="Thêm “Cây trầu bà đế vương đỏ 'Red Rojo' chậu sứ PHIR007” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13576"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-trau-ba-de-vuong-do-red-rojo-chau-su-phir007/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trầu bà đế vương đỏ ‘Red Rojo’ chậu sứ PHIR007</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>550.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13571 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-kim-ngan-that-binh-tieu-canh-chau-dat-nung-loni035/"
                                            aria-label="Cây kim ngân thắt bính tiểu cảnh chậu đất nung LONI035"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-binh-chau-dat-nung-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-kim-ngan-binh-chau-dat-nung-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13571 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13571"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13571,&quot;parent_product_id&quot;:13571,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13571&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13571" data-product-type="simple"
                                                            data-original-product-id="13571"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13571" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13571" data-product_sku="LONI035"
                                            aria-label="Thêm “Cây kim ngân thắt bính tiểu cảnh chậu đất nung LONI035” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13571"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-kim-ngan-that-binh-tieu-canh-chau-dat-nung-loni035/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                kim ngân thắt bính tiểu cảnh chậu đất nung LONI035</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>420.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13569 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-canh-treo-trong-nha product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-thuong-xuan-cam-thach-chau-nhua-hede002/"
                                            aria-label="Cây thường xuân cẩm thạch chậu nhựa HEDE002"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-thuong-xuan-cam-thach-chau-uom-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13569 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13569"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13569,&quot;parent_product_id&quot;:13569,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13569&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13569" data-product-type="simple"
                                                            data-original-product-id="13569"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13569" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13569" data-product_sku="HEDE002"
                                            aria-label="Thêm “Cây thường xuân cẩm thạch chậu nhựa HEDE002” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13569"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-thuong-xuan-cam-thach-chau-nhua-hede002/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                thường xuân cẩm thạch chậu nhựa HEDE002</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13557 status-publish first instock product_cat-cay-kieng-la product_cat-cay-de-trong-trong-nha product_cat-cay-canh-nhiet-doi product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-hong-hac-than-cam-billietiae-chau-banh-phbi005/"
                                            aria-label="Cây hồng hạc thân cam &quot;Billietiae' chậu banh PHBI005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-hong-hac-chan-cam-chau-trai-banh-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13557 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13557"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13557,&quot;parent_product_id&quot;:13557,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13557&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13557" data-product-type="simple"
                                                            data-original-product-id="13557"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13557" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13557" data-product_sku="PHBI005"
                                            aria-label="Thêm “Cây hồng hạc thân cam &quot;Billietiae' chậu banh PHBI005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13557"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-hong-hac-than-cam-billietiae-chau-banh-phbi005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                hồng hạc thân cam “Billietiae’ chậu banh PHBI005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>550.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13550 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-bang-singapore-2-than-chau-da-mai-lyra047/"
                                            aria-label="Cây bàng Singapore 2 thân chậu đá mài LYRA047"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-bang-singapore-chau-da-mai-tru-tron-1a-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-bang-singapore-chau-da-mai-tru-tron-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13550 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13550"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13550,&quot;parent_product_id&quot;:13550,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13550&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13550" data-product-type="simple"
                                                            data-original-product-id="13550"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13550" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13550" data-product_sku="LYRA047"
                                            aria-label="Thêm “Cây bàng Singapore 2 thân chậu đá mài LYRA047” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13550"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-bang-singapore-2-than-chau-da-mai-lyra047/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                bàng Singapore 2 thân chậu đá mài LYRA047</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>680.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13541 status-publish instock product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-phong-thuy product_cat-cay-trang-tri-nha-cua product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-bang-singapore-nhieu-than-chau-su-trang-lyra046/"
                                            aria-label="Cây bàng Singapore nhiều thân chậu sứ trắng LYRA046"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-bang-singapore-co-lon-chau-su-trang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-bang-singapore-co-lon-chau-su-trang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13541 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13541"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13541,&quot;parent_product_id&quot;:13541,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13541&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13541" data-product-type="simple"
                                                            data-original-product-id="13541"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13541" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13541" data-product_sku="LYRA046"
                                            aria-label="Thêm “Cây bàng Singapore nhiều thân chậu sứ trắng LYRA046” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13541"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-bang-singapore-nhieu-than-chau-su-trang-lyra046/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                bàng Singapore nhiều thân chậu sứ trắng LYRA046</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>2.250.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13528 status-publish last instock product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-da-bup-do-cam-thach-1-than-lon-chau-su-bdct006/"
                                            aria-label="Cây đa búp đỏ cẩm thạch 1 thân lớn chậu sứ BDCT006"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-da-bup-do-cam-thach-chau-su-tho-cam-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13528 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13528"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13528,&quot;parent_product_id&quot;:13528,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13528&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13528" data-product-type="simple"
                                                            data-original-product-id="13528"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13528" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13528" data-product_sku="BDCT006"
                                            aria-label="Thêm “Cây đa búp đỏ cẩm thạch 1 thân lớn chậu sứ BDCT006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13528"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-da-bup-do-cam-thach-1-than-lon-chau-su-bdct006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây đa
                                                búp đỏ cẩm thạch 1 thân lớn chậu sứ BDCT006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>570.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13518 status-publish first instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-cao-va-lon-trong-nha product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha product_cat-cay-trong-phong-khach product_cat-cay-truoc-cua-va-hanh-lang has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-ngu-gia-bi-de-ban-chau-dat-nung-sche018/"
                                            aria-label="Cây ngũ gia bì để bàn chậu đất nung SCHE018"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-ngu-gia-bi-de-ban-chau-dat-nung-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13518 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13518"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13518,&quot;parent_product_id&quot;:13518,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13518&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13518" data-product-type="simple"
                                                            data-original-product-id="13518"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13518" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13518" data-product_sku="SCHE018"
                                            aria-label="Thêm “Cây ngũ gia bì để bàn chậu đất nung SCHE018” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13518"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-ngu-gia-bi-de-ban-chau-dat-nung-sche018/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                ngũ gia bì để bàn chậu đất nung SCHE018</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>150.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13513 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-kieng-la product_cat-cay-phong-thuy product_cat-cay-trong-trong-nha-bep-va-phong-tam product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-truc-mat-troi-compacta-chau-su-hoa-tiet-tho-drco004/"
                                            aria-label="Cây trúc mặt trời 'Compacta' chậu sứ họa tiết thổ DRCO004"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-phat-tai-bup-de-ban-chau-su-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13513 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13513"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13513,&quot;parent_product_id&quot;:13513,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13513&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13513" data-product-type="simple"
                                                            data-original-product-id="13513"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13513" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13513" data-product_sku="DRCO004"
                                            aria-label="Thêm “Cây trúc mặt trời 'Compacta' chậu sứ họa tiết thổ DRCO004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13513"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-truc-mat-troi-compacta-chau-su-hoa-tiet-tho-drco004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trúc mặt trời ‘Compacta’ chậu sứ họa tiết thổ DRCO004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>200.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13510 status-publish instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-kieng-la product_cat-cay-canh-nhiet-doi product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-mon-vay-rong-dragon-scale-chau-su-trang-abds004/"
                                            aria-label="Cây môn vảy rồng 'Dragon Scale' chậu sứ trắng ABDS004"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-mon-vay-rong-chau-trung-vo-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-rong-xanh-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13510 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13510"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13510,&quot;parent_product_id&quot;:13510,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13510&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13510" data-product-type="simple"
                                                            data-original-product-id="13510"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13510" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13510" data-product_sku="ABDS004"
                                            aria-label="Thêm “Cây môn vảy rồng 'Dragon Scale' chậu sứ trắng ABDS004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13510"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-mon-vay-rong-dragon-scale-chau-su-trang-abds004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                môn vảy rồng ‘Dragon Scale’ chậu sứ trắng ABDS004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13508 status-publish last instock product_cat-cay-canh-de-ban product_cat-cay-can-it-anh-sang product_cat-cay-canh-van-phong product_cat-cay-de-trong-trong-nha product_cat-cay-loc-khong-khi product_cat-cay-phong-thuy product_cat-ban-cay-canh-trong-nha has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/cay-truong-sinh-de-ban-chau-dat-nung-marble-peob005/"
                                            aria-label="Cây trường sinh để bàn chậu đất nung Marble PEOB005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/cay-truong-sinh-chau-dat-nung-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13508 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13508"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13508,&quot;parent_product_id&quot;:13508,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13508&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13508" data-product-type="simple"
                                                            data-original-product-id="13508"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=13508" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="13508" data-product_sku="PEOB005"
                                            aria-label="Thêm “Cây trường sinh để bàn chậu đất nung Marble PEOB005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13508"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 95.8906px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 41.2031px;"><a
                                                href="https://mowgarden.com/cay-truong-sinh-de-ban-chau-dat-nung-marble-peob005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Cây
                                                trường sinh để bàn chậu đất nung Marble PEOB005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>200.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row -->
                <div class="container">
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers nav-pagination links text-center">
                            <li><span aria-current="page" class="page-number current">1</span></li>
                            <li><a class="page-number" href="https://mowgarden.com/ban-cay-canh-trong-nha/page/2/">2</a>
                            </li>
                            <li><a class="page-number" href="https://mowgarden.com/ban-cay-canh-trong-nha/page/3/">3</a>
                            </li>
                            <li><a class="page-number" href="https://mowgarden.com/ban-cay-canh-trong-nha/page/4/">4</a>
                            </li>
                            <li><span class="page-number dots">…</span></li>
                            <li><a class="page-number"
                                    href="https://mowgarden.com/ban-cay-canh-trong-nha/page/14/">14</a></li>
                            <li><a class="page-number"
                                    href="https://mowgarden.com/ban-cay-canh-trong-nha/page/15/">15</a></li>
                            <li><a class="page-number"
                                    href="https://mowgarden.com/ban-cay-canh-trong-nha/page/16/">16</a></li>
                            <li><a class="next page-number"
                                    href="https://mowgarden.com/ban-cay-canh-trong-nha/page/2/"><i
                                        class="icon-angle-right"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div><!-- shop container -->
        </div>
        <div id="shop-sidebar" class="mfp-hide">
            <div class="sidebar-inner">
                <aside id="search-5" class="widget widget_search">
                    <form method="get" class="searchform" action="https://mowgarden.com/" role="search">
                        <div class="flex-row relative">
                            <div class="flex-col flex-grow"> <input type="search" class="search-field mb-0"
                                    name="s" value="" id="s" placeholder="Tìm kiếm sản phẩm"
                                    autocomplete="off"> </div>
                            <div class="flex-col"> <button type="submit"
                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                    aria-label="Submit"> <i class="icon-search"></i> </button> </div>
                        </div>
                        <div class="live-search-results text-left z-top">
                            <div class="autocomplete-suggestions"
                                style="position: absolute; display: none; max-height: 300px; z-index: 9999;"></div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_price_filter-9" class="widget woocommerce widget_price_filter"><span
                        class="widget-title shop-sidebar">Lọc theo giá</span>
                    <div class="is-divider small"></div>
                    <form method="get" action="https://mowgarden.com/ban-cay-canh-trong-nha/">
                        <div class="price_slider_wrapper">
                            <div class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                style="">
                                <div class="ui-slider-range ui-corner-all ui-widget-header"
                                    style="left: 0%; width: 100%;"></div><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default"
                                    style="left: 0%;"></span><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
                            </div>
                            <div class="price_slider_amount" data-step="10"> <label class="screen-reader-text"
                                    for="min_price">Giá thấp nhất</label> <input type="text" id="min_price"
                                    name="min_price" value="0" data-min="0" placeholder="Giá thấp nhất"
                                    style="display: none;"> <label class="screen-reader-text" for="max_price">Giá cao
                                    nhất</label> <input type="text" id="max_price" name="max_price"
                                    value="7500000" data-max="7500000" placeholder="Giá cao nhất"
                                    style="display: none;"> <button type="submit" class="button">Lọc</button>
                                <div class="price_label" style=""> Giá <span class="from">0₫</span> — <span
                                        class="to">7.500.000₫</span> </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_product_categories-13" class="widget woocommerce widget_product_categories"><span
                        class="widget-title shop-sidebar">danh mục</span>
                    <div class="is-divider small"></div>
                    <ul class="product-categories">
                        <li class="cat-item cat-item-109"><a href="https://mowgarden.com/cay-ngoai-troi/">Cây Ngoài
                                Trời</a></li>
                        <li class="cat-item cat-item-93 current-cat cat-parent active has-child" aria-expanded="false">
                            <a href="https://mowgarden.com/ban-cay-canh-trong-nha/">Cây Trong Nhà</a><button
                                class="toggle" aria-label="Toggle"><i class="icon-angle-down"></i></button>
                            <ul class="children">
                                <li class="cat-item cat-item-330"><a
                                        href="https://mowgarden.com/cay-can-it-anh-sang/">Cây Cần Ít Ánh Sáng</a></li>
                                <li class="cat-item cat-item-94"><a href="https://mowgarden.com/cay-canh-mini/">Cây Cảnh
                                        Mini</a></li>
                                <li class="cat-item cat-item-105"><a
                                        href="https://mowgarden.com/cay-canh-van-phong/">Cây Cảnh Văn Phòng</a></li>
                                <li class="cat-item cat-item-104"><a href="https://mowgarden.com/cay-canh-de-ban/">Cây
                                        Cảnh Để Bàn</a></li>
                                <li class="cat-item cat-item-92"><a
                                        href="https://mowgarden.com/cay-cao-va-lon-trong-nha/">Cây Cao &amp; Lớn Trong
                                        Nhà</a></li>
                                <li class="cat-item cat-item-100"><a
                                        href="https://mowgarden.com/cay-de-trong-trong-nha/">Cây Dễ Trồng Trong Nhà</a>
                                </li>
                                <li class="cat-item cat-item-97"><a href="https://mowgarden.com/cay-kieng-la/">Cây Kiểng
                                        lá</a></li>
                                <li class="cat-item cat-item-99"><a href="https://mowgarden.com/cay-loc-khong-khi/">Cây
                                        Lọc Không Khí</a></li>
                                <li class="cat-item cat-item-95"><a href="https://mowgarden.com/cay-canh-nhiet-doi/">Cây
                                        Nhiệt Đới</a></li>
                                <li class="cat-item cat-item-98"><a href="https://mowgarden.com/cay-phong-thuy/">Cây
                                        Phong Thủy</a></li>
                                <li class="cat-item cat-item-103"><a href="https://mowgarden.com/cay-thuy-sinh/">Cây
                                        Thủy Sinh</a></li>
                                <li class="cat-item cat-item-102"><a
                                        href="https://mowgarden.com/cay-trang-tri-nha-cua/">Cây Trang Trí Nhà Cửa</a></li>
                                <li class="cat-item cat-item-96"><a
                                        href="https://mowgarden.com/cay-canh-treo-trong-nha/">Cây Treo Trong Nhà</a></li>
                                <li class="cat-item cat-item-332"><a
                                        href="https://mowgarden.com/cay-trong-ban-cong/">Cây Trồng Ban Công</a></li>
                                <li class="cat-item cat-item-331"><a
                                        href="https://mowgarden.com/cay-trong-trong-nha-bep-va-phong-tam/">Cây Trong Bếp
                                        &amp; Nhà Tắm</a></li>
                                <li class="cat-item cat-item-106"><a
                                        href="https://mowgarden.com/cay-trong-phong-khach/">Cây Trong Phòng Khách</a></li>
                                <li class="cat-item cat-item-108"><a
                                        href="https://mowgarden.com/cay-truoc-cua-va-hanh-lang/">Cây Trước Cửa &amp; Hành
                                        Lang</a></li>
                            </ul>
                        </li>
                        <li class="cat-item cat-item-121"><a href="https://mowgarden.com/chau-cay/">Chậu Cây Cảnh</a>
                        </li>
                        <li class="cat-item cat-item-395"><a href="https://mowgarden.com/dung-cu-lam-vuon/">Dụng Cụ Làm
                                Vườn</a></li>
                        <li class="cat-item cat-item-393"><a href="https://mowgarden.com/phan-bon/">Phân Bón</a></li>
                        <li class="cat-item cat-item-394"><a href="https://mowgarden.com/phu-kien-trang-tri/">Phụ Kiện
                                Trang Trí</a></li>
                        <li class="cat-item cat-item-15"><a
                                href="https://mowgarden.com/uncategorized/">Uncategorized</a></li>
                        <li class="cat-item cat-item-396"><a href="https://mowgarden.com/dat-trong-cay/">Đất Trồng
                                Cây</a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
@endsection
