@extends('layouts.layout-comon')

@section('headerproduct')
    <div class="shop-page-title category-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <div class="is-larger">
                    <nav class="rank-math-breadcrumb breadcrumbs">
                        <p><a href="https://mowgarden.com">Trang chủ</a><span class="separator"> » </span><span
                                class="last">Chậu Cây Cảnh</span></p>
                    </nav>
                </div>
                <div class="category-filtering category-filter-row"> <a href="#" data-open="#shop-sidebar"
                        data-pos="left" class="filter-button uppercase plain"> <i class="fa-solid fa-list"></i> <strong>Chọn
                            danh mục</strong> </a>
                    <div class="inline-block"> </div>
                </div>
            </div>
            <div class="flex-col medium-text-center">
                <p class="woocommerce-result-count hide-for-medium"> Hiển thị 1–48 của 222 kết quả</p>
                <form class="woocommerce-ordering" method="get"> <select name="orderby" class="orderby"
                        aria-label="Đơn hàng của cửa hàng">
                        <option value="popularity">Thứ tự theo mức độ phổ biến</option>
                        <option value="rating">Thứ tự theo điểm đánh giá</option>
                        <option value="date" selected="selected">Mới nhất</option>
                        <option value="price">Thứ tự theo giá: thấp đến cao</option>
                        <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                    </select> <input type="hidden" name="paged" value="1"> </form>
            </div>
        </div>
    </div>
@endsection

@section('maincontent')
    <div class="row category-page-row">
        <div class="col large-12">
            <div class="shop-container">
                <div class="woocommerce-notices-wrapper"></div>
                <div
                    class="products row row-small large-columns-4 medium-columns-4 small-columns-2 has-shadow row-box-shadow-2-hover has-equal-box-heights equalize-box">
                    <div
                        class="product-small col has-hover product type-product post-14289 status-publish first instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                            aria-label="Chậu đá mài Granito dáng trụ vót màu trắng XMDM018"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-da-mai-vot-day-3-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-remy-5-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon" aria-label="Wishlist">
                                                <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14289 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14289"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14289,&quot;parent_product_id&quot;:14289,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14289&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14289" data-product-type="variable"
                                                            data-original-product-id="14289" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="14289" data-product_sku="XMDM018"
                                            aria-label="Lựa chọn cho “Chậu đá mài Granito dáng trụ vót màu trắng XMDM018”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14289" href="#quick-view">Xem
                                            nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-da-mai-granito-dang-tru-vot-mau-trang-xmdm018/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu đá
                                                mài Granito dáng trụ vót màu trắng XMDM018</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>420.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-14000 status-publish instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-tru-vuong-van-soc-ngang-mau-den-xmdm017/"
                                            aria-label="Chậu xi măng hình trụ vuông vân sọc ngang màu đen XMDM017"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-xi-mang-hinh-tru-son-den-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-xi-mang-hinh-tru-son-den-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-14000 wishlist-fragment on-first-load"
                                                    data-fragment-ref="14000"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:14000,&quot;parent_product_id&quot;:14000,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=14000&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="14000" data-product-type="simple"
                                                            data-original-product-id="14000" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=14000" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="14000" data-product_sku="XMDM017"
                                            aria-label="Thêm “Chậu xi măng hình trụ vuông vân sọc ngang màu đen XMDM017” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="14000"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-hinh-tru-vuong-van-soc-ngang-mau-den-xmdm017/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu xi
                                                măng hình trụ vuông vân sọc ngang màu đen XMDM017</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>500.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13971 status-publish instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hinh-tru-hoa-tiet-geometric-gosu059/"
                                            aria-label="Chậu gốm sứ hình trụ họa tiết Geometric GOSU059"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-tru-hoa-tiet-geo-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-tru-hoa-tiet-geo-4-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13971 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13971"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13971,&quot;parent_product_id&quot;:13971,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13971&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13971" data-product-type="variable"
                                                            data-original-product-id="13971" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hinh-tru-hoa-tiet-geometric-gosu059/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13971" data-product_sku="GOSU059"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ hình trụ họa tiết Geometric GOSU059”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small tooltipstered"><strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13971"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hinh-tru-hoa-tiet-geometric-gosu059/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ hình trụ họa tiết Geometric GOSU059</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13963 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-la-monstera-co-dia-gosu057/"
                                            aria-label="Chậu gốm sứ họa tiết lá Monstera có dĩa GOSU057"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-trang-hoa-tiet-monstera-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-trang-hoa-tiet-monstera-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13963 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13963"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13963,&quot;parent_product_id&quot;:13963,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13963&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13963" data-product-type="variable"
                                                            data-original-product-id="13963" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-la-monstera-co-dia-gosu057/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13963" data-product_sku="GOSU057"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết lá Monstera có dĩa GOSU057”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13963"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-la-monstera-co-dia-gosu057/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết lá Monstera có dĩa GOSU057</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13953 status-publish first instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-hoa-mau-trang-co-dia-gosu056/"
                                            aria-label="Chậu gốm sứ họa tiết hoa màu trắng có dĩa GOSU056"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-trang-trang-tri-hoa-tiet-4-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-trang-trang-tri-hoa-tiet-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13953 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13953"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13953,&quot;parent_product_id&quot;:13953,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13953&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13953" data-product-type="variable"
                                                            data-original-product-id="13953" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-hoa-mau-trang-co-dia-gosu056/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13953" data-product_sku="GOSU056"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết hoa màu trắng có dĩa GOSU056”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13953"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-hoa-mau-trang-co-dia-gosu056/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết hoa màu trắng có dĩa GOSU056</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13945 status-publish instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-vien-hoa-cuc-co-dia-mau-trang-gosu055/"
                                            aria-label="Chậu gốm sứ viền hoa cúc có dĩa màu trắng GOSU055"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-gom-su-hoa-cuc-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-gom-su-hoa-cuc-3-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13945 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13945"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13945,&quot;parent_product_id&quot;:13945,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13945&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13945" data-product-type="variable"
                                                            data-original-product-id="13945" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-vien-hoa-cuc-co-dia-mau-trang-gosu055/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13945" data-product_sku="GOSU055"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ viền hoa cúc có dĩa màu trắng GOSU055”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13945"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-vien-hoa-cuc-co-dia-mau-trang-gosu055/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ viền hoa cúc có dĩa màu trắng GOSU055</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>20.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13927 status-publish instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hinh-tru-trung-mau-trang-gosu054/"
                                            aria-label="Chậu gốm sứ hình trụ trứng màu trắng GOSU054"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-tru-trung-mau-trang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-tru-trung-mau-trang-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13927 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13927"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13927,&quot;parent_product_id&quot;:13927,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13927&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13927" data-product-type="variable"
                                                            data-original-product-id="13927" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hinh-tru-trung-mau-trang-gosu054/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13927" data-product_sku="GOSU054"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ hình trụ trứng màu trắng GOSU054”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13927"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hinh-tru-trung-mau-trang-gosu054/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ hình trụ trứng màu trắng GOSU054</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13919 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hinh-khoi-van-gon-song-mau-trang-gosu053/"
                                            aria-label="Chậu gốm sứ hình khối vân gợn sóng màu trắng GOSU053"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-khoi-vuong-van-gon-song-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-su-hinh-khoi-vuong-van-gon-song-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13919 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13919"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13919,&quot;parent_product_id&quot;:13919,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13919&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13919" data-product-type="variable"
                                                            data-original-product-id="13919" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hinh-khoi-van-gon-song-mau-trang-gosu053/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13919" data-product_sku="GOSU053"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ hình khối vân gợn sóng màu trắng GOSU053”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13919"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hinh-khoi-van-gon-song-mau-trang-gosu053/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ hình khối vân gợn sóng màu trắng GOSU053</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>150.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13910 status-publish first instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-tru-tron-hoa-tiet-ke-soc-mau-trang-gosu052/"
                                            aria-label="Chậu gốm sứ trụ tròn họa tiết kẻ sọc màu trắng GOSU052"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-gom-su-tru-tron-trang-hoa-tiet-ke-soc-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/04/chau-gom-su-tru-tron-trang-hoa-tiet-ke-soc-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13910 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13910"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13910,&quot;parent_product_id&quot;:13910,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13910&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13910" data-product-type="variable"
                                                            data-original-product-id="13910" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-tru-tron-hoa-tiet-ke-soc-mau-trang-gosu052/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13910" data-product_sku="GOSU052"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ trụ tròn họa tiết kẻ sọc màu trắng GOSU052”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13910"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-tru-tron-hoa-tiet-ke-soc-mau-trang-gosu052/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ trụ tròn họa tiết kẻ sọc màu trắng GOSU052</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>230.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13886 status-publish instock product_cat-chau-xi-mang product_cat-kieu-hinh-bau has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                            aria-label="Chậu đá mài Granito cao cấp dáng Remy màu trắng XMDM015"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-granito-dang-remy-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-remy-5-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13886 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13886"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13886,&quot;parent_product_id&quot;:13886,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13886&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13886" data-product-type="variable"
                                                            data-original-product-id="13886" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13886" data-product_sku="XMDM015"
                                            aria-label="Lựa chọn cho “Chậu đá mài Granito cao cấp dáng Remy màu trắng XMDM015”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13886"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-da-mai-granito-dang-remy-mau-trang-xmdm015/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu đá
                                                mài Granito cao cấp dáng Remy màu trắng XMDM015</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>360.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>560.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13559 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-wax-xam-hoa-tiet-hoa-danu126/"
                                            aria-label="Chậu đất nung wax xám họa tiết hoa DANU126"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-dat-nung-son-wax-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-dat-nung-son-wax-3-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13559 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13559"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13559,&quot;parent_product_id&quot;:13559,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13559&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13559" data-product-type="variable"
                                                            data-original-product-id="13559" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-dat-nung-wax-xam-hoa-tiet-hoa-danu126/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13559" data-product_sku="DANU126"
                                            aria-label="Lựa chọn cho “Chậu đất nung wax xám họa tiết hoa DANU126”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13559"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-wax-xam-hoa-tiet-hoa-danu126/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung wax xám họa tiết hoa DANU126</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13423 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-tam-giac-mau-trang-gosu051/"
                                            aria-label="Chậu gốm sứ họa tiết tam giác màu trắng GOSU051"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-tam-giac-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-tam-giac-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13423 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13423"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13423,&quot;parent_product_id&quot;:13423,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13423&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13423" data-product-type="variable"
                                                            data-original-product-id="13423" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-tam-giac-mau-trang-gosu051/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13423" data-product_sku="GOSU051"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết tam giác màu trắng GOSU051”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13423"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-tam-giac-mau-trang-gosu051/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết tam giác màu trắng GOSU051</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>230.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13413 status-publish first instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-o-vuong-mau-trang-gosu050/"
                                            aria-label="Chậu gốm sứ họa tiết ô vuông màu trắng GOSU050"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-o-vuong-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-o-vuong-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13413 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13413"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13413,&quot;parent_product_id&quot;:13413,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13413&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13413" data-product-type="variable"
                                                            data-original-product-id="13413" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-o-vuong-mau-trang-gosu050/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13413" data-product_sku="GOSU050"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết ô vuông màu trắng GOSU050”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13413"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-o-vuong-mau-trang-gosu050/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết ô vuông màu trắng GOSU050</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>230.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13405 status-publish instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-dan-may-mau-den-gosu049/"
                                            aria-label="Chậu gốm sứ họa tiết đan mây màu đen GOSU049"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-dan-may-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-dan-may-3-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13405 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13405"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13405,&quot;parent_product_id&quot;:13405,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13405&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13405" data-product-type="variable"
                                                            data-original-product-id="13405" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-dan-may-mau-den-gosu049/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13405" data-product_sku="GOSU049"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết đan mây màu đen GOSU049”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13405"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-dan-may-mau-den-gosu049/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết đan mây màu đen GOSU049</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>250.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13397 status-publish instock product_cat-chau-gom-su product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-mau-den-gosu048/"
                                            aria-label="Chậu gốm sứ họa tiết thổ cẩm màu đen GOSU048"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-tho-cam-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-gom-su-hoa-tiet-tho-cam-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13397 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13397"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13397,&quot;parent_product_id&quot;:13397,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13397&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13397" data-product-type="variable"
                                                            data-original-product-id="13397"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-mau-den-gosu048/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13397" data-product_sku="GOSU048"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết thổ cẩm màu đen GOSU048”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13397"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-mau-den-gosu048/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết thổ cẩm màu đen GOSU048</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>250.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-13361 status-publish last instock product_cat-chau-xi-mang has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                            aria-label="Chậu xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-van-quan-roi-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2023/03/chau-xi-mang-da-mai-van-quan-roi-4-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-13361 wishlist-fragment on-first-load"
                                                    data-fragment-ref="13361"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:13361,&quot;parent_product_id&quot;:13361,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=13361&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="13361" data-product-type="variable"
                                                            data-original-product-id="13361"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="13361" data-product_sku="XMDM014"
                                            aria-label="Lựa chọn cho “Chậu xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="13361"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-nhe-hinh-tru-vat-day-van-quan-roi-xmdm014/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng nhẹ hình trụ vát đáy vân quấn rối XMDM014</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>220.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>350.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12489 status-publish first instock product_cat-chau-xi-mang product_cat-chau-cay-kieu-tru-dung has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-ve-zigzac-xmdm013/"
                                            aria-label="Chậu xi măng đá mài trụ tròn vẽ zigzac XMDM013"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/12/chau-xi-mang-da-mai-ve-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12489 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12489"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12489,&quot;parent_product_id&quot;:12489,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12489&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12489" data-product-type="simple"
                                                            data-original-product-id="12489"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12489" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12489" data-product_sku="XMDM013"
                                            aria-label="Thêm “Chậu xi măng đá mài trụ tròn vẽ zigzac XMDM013” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12489"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-ve-zigzac-xmdm013/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài trụ tròn vẽ zigzac XMDM013</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-1608 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay-kieu-tru-dung has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                            aria-label="Chậu xi măng đá mài trụ tròn dáng thấp XMDM012"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2021/04/chau-xi-mang-da-mai-tru-tron-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2021/04/chau-xi-mang-da-mai-tron-ong-thap-5-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-1608 wishlist-fragment on-first-load"
                                                    data-fragment-ref="1608"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:1608,&quot;parent_product_id&quot;:1608,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=1608&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="1608" data-product-type="variable"
                                                            data-original-product-id="1608" data-title="Add to wishlist"
                                                            rel="nofollow"> <i class="yith-wcwl-icon fa fa-heart-o"></i>
                                                            <span>Add to wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="1608" data-product_sku="XMDM012"
                                            aria-label="Lựa chọn cho “Chậu xi măng đá mài trụ tròn dáng thấp XMDM012”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="1608"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-tru-tron-dang-thap-xmdm012/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài trụ tròn dáng thấp XMDM012</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>80.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>150.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12453 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-decor-32x52cm-xmdm011/"
                                            aria-label="Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM011"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/12/chau-xi-mang-dai-mai-son-hoa-tiet-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12453 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12453"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12453,&quot;parent_product_id&quot;:12453,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12453&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12453" data-product-type="simple"
                                                            data-original-product-id="12453"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12453" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12453" data-product_sku="XMDM011"
                                            aria-label="Thêm “Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM011” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12453"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-decor-32x52cm-xmdm011/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng hình giọt nước sơn decor 32x52cm XMDM011</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>440.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12438 status-publish last instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-trang-tri-32x52cm-xmdm010/"
                                            aria-label="Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM010"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-son-mau-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12438 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12438"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12438,&quot;parent_product_id&quot;:12438,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12438&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12438" data-product-type="simple"
                                                            data-original-product-id="12438"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12438" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12438" data-product_sku="XMDM010"
                                            aria-label="Thêm “Chậu xi măng hình giọt nước sơn decor 32x52cm XMDM010” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12438"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-son-trang-tri-32x52cm-xmdm010/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng hình giọt nước sơn decor 32x52cm XMDM010</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>480.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12434 status-publish first instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-32x52cm-xmdm009/"
                                            aria-label="Chậu xi măng hình giọt nước 32x52cm XMDM009"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-mau-den-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12434 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12434"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12434,&quot;parent_product_id&quot;:12434,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12434&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12434" data-product-type="simple"
                                                            data-original-product-id="12434"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12434" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12434" data-product_sku="XMDM009"
                                            aria-label="Thêm “Chậu xi măng hình giọt nước 32x52cm XMDM009” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12434"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-hinh-giot-nuoc-32x52cm-xmdm009/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng hình giọt nước 32x52cm XMDM009</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12431 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-hinh-tru-son-hoa-tiet-40x40cm-xmdm008/"
                                            aria-label="Chậu xi măng hình trụ sơn họa tiết 40x40cm XMDM008"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-son-mau-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12431 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12431"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12431,&quot;parent_product_id&quot;:12431,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12431&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12431" data-product-type="simple"
                                                            data-original-product-id="12431"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12431" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12431" data-product_sku="40x40cm"
                                            aria-label="Thêm “Chậu xi măng hình trụ sơn họa tiết 40x40cm XMDM008” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12431"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-hinh-tru-son-hoa-tiet-40x40cm-xmdm008/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng hình trụ sơn họa tiết 40x40cm XMDM008</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>520.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12428 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-dau-dan-hoa-tiet-29x39cm-xmdm007/"
                                            aria-label="Chậu xi măng đá mài đầu đạn họa tiết 29x39cm XMDM007"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-hinh-dau-dan-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12428 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12428"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12428,&quot;parent_product_id&quot;:12428,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12428&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12428" data-product-type="simple"
                                                            data-original-product-id="12428"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12428" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12428" data-product_sku="XMDM007"
                                            aria-label="Thêm “Chậu xi măng đá mài đầu đạn họa tiết 29x39cm XMDM007” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12428"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-dau-dan-hoa-tiet-29x39cm-xmdm007/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài đầu đạn họa tiết 29x39cm XMDM007</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>320.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12426 status-publish last instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-dau-dan-29x39cm-xmdm006/"
                                            aria-label="Chậu xi măng đá mài đầu đạn 29x39cm XMDM006"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-dau-dan-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12426 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12426"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12426,&quot;parent_product_id&quot;:12426,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12426&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12426" data-product-type="simple"
                                                            data-original-product-id="12426"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12426" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12426" data-product_sku="XMDM006"
                                            aria-label="Thêm “Chậu xi măng đá mài đầu đạn 29x39cm XMDM006” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12426"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-dau-dan-29x39cm-xmdm006/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài đầu đạn 29x39cm XMDM006</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>240.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12421 status-publish first instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-remy-hoa-tiet-zigzac-37x35cm-xmdm005/"
                                            aria-label="Chậu xi măng đá mài Remy họa tiết zigzac 37x35cm XMDM005"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-hoa-tiet-zig-zac-1-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12421 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12421"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12421,&quot;parent_product_id&quot;:12421,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12421&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12421" data-product-type="simple"
                                                            data-original-product-id="12421"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12421" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12421" data-product_sku="XMDM005"
                                            aria-label="Thêm “Chậu xi măng đá mài Remy họa tiết zigzac 37x35cm XMDM005” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12421"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-remy-hoa-tiet-zigzac-37x35cm-xmdm005/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài Remy họa tiết zigzac 37x35cm XMDM005</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>420.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12405 status-publish instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-de-ban-mau-vang-vien-do-12x12cm-gosu047/"
                                            aria-label="Chậu gốm sứ để bàn màu vàng viền đỏ 12x12cm GOSU047"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-son-nhu-vang-2-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12405 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12405"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12405,&quot;parent_product_id&quot;:12405,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12405&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12405" data-product-type="simple"
                                                            data-original-product-id="12405"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12405" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12405" data-product_sku="GOSU047"
                                            aria-label="Thêm “Chậu gốm sứ để bàn màu vàng viền đỏ 12x12cm GOSU047” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12405"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-de-ban-mau-vang-vien-do-12x12cm-gosu047/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ để bàn màu vàng viền đỏ 12x12cm GOSU047</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>90.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12403 status-publish instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-de-banin-chu-tai-loc-12x12cm-gosu046/"
                                            aria-label="Chậu gốm sứ để bàn in chữ &quot;Tài Lộc&quot; 12x12cm GOSU046">
                                            <img width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-son-nhu-vang-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12403 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12403"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12403,&quot;parent_product_id&quot;:12403,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12403&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12403" data-product-type="simple"
                                                            data-original-product-id="12403"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12403" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12403" data-product_sku="GOSU046"
                                            aria-label="Thêm “Chậu gốm sứ để bàn in chữ &quot;Tài Lộc&quot; 12x12cm GOSU046” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12403"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-de-banin-chu-tai-loc-12x12cm-gosu046/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ để bàn in chữ “Tài Lộc” 12x12cm GOSU046</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>90.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12371 status-publish last instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-remy-37x35cm-xmdm004/"
                                            aria-label="Chậu xi măng đá mài Remy 37x35cm XMDM004"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-remy-thap-37x35-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12371 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12371"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12371,&quot;parent_product_id&quot;:12371,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12371&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12371" data-product-type="simple"
                                                            data-original-product-id="12371"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12371" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12371" data-product_sku="XMDM004"
                                            aria-label="Thêm “Chậu xi măng đá mài Remy 37x35cm XMDM004” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12371"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-remy-37x35cm-xmdm004/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài Remy 37x35cm XMDM004</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12358 status-publish first instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-tru-tron-40x40cm-xmdm003/"
                                            aria-label="Chậu xi măng đá mài hình trụ tròn 40x40cm XMDM003"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-hinh-tru-tron-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-hinh-tru-tron-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12358 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12358"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12358,&quot;parent_product_id&quot;:12358,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12358&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12358" data-product-type="simple"
                                                            data-original-product-id="12358"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12358" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12358" data-product_sku="XMDM003"
                                            aria-label="Thêm “Chậu xi măng đá mài hình trụ tròn 40x40cm XMDM003” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12358"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-tru-tron-40x40cm-xmdm003/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài hình trụ tròn 40x40cm XMDM003</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>280.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12326 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-lu-34x42cm-xmdm002/"
                                            aria-label="Chậu xi măng đá mài hình lu 34x42cm XMDM002"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-hinh-lu-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12326 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12326"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12326,&quot;parent_product_id&quot;:12326,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12326&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12326" data-product-type="simple"
                                                            data-original-product-id="12326"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12326" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12326" data-product_sku="XMDM002"
                                            aria-label="Thêm “Chậu xi măng đá mài hình lu 34x42cm XMDM002” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12326"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-lu-34x42cm-xmdm002/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài hình lu 34x42cm XMDM002</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>340.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12305 status-publish instock product_cat-chau-xi-mang product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable has-default-attributes">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-giot-nuoc-20x35cm-xmdm001/"
                                            aria-label="Chậu xi măng đá mài hình giọt nước 20x35cm XMDM001"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-nho-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-xi-mang-da-mai-giot-nuoc-nho-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12305 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12305"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12305,&quot;parent_product_id&quot;:12305,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12305&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12305" data-product-type="variable"
                                                            data-original-product-id="12305"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-giot-nuoc-20x35cm-xmdm001/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="12305" data-product_sku="XMDM001"
                                            aria-label="Lựa chọn cho “Chậu xi măng đá mài hình giọt nước 20x35cm XMDM001”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12305"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-xi-mang-da-mai-hinh-giot-nuoc-20x35cm-xmdm001/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                xi măng đá mài hình giọt nước 20x35cm XMDM001</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12290 status-publish last instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-tron-bau-hoa-tiet-hoa-cuc-danu124/"
                                            aria-label="Chậu đất nung tròn bầu họa tiết hoa cúc DANU124"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-dang-tron-bau-hoa-cuc-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-dang-tron-bau-hoa-cuc-4-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12290 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12290"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12290,&quot;parent_product_id&quot;:12290,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12290&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12290" data-product-type="variable"
                                                            data-original-product-id="12290"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-dat-nung-tron-bau-hoa-tiet-hoa-cuc-danu124/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="12290" data-product_sku="DANU124"
                                            aria-label="Lựa chọn cho “Chậu đất nung tròn bầu họa tiết hoa cúc DANU124”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12290"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-tron-bau-hoa-tiet-hoa-cuc-danu124/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung tròn bầu họa tiết hoa cúc DANU124</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>180.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12288 status-publish first instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-tru-tron-vat-day-18x18cm-danu123/"
                                            aria-label="Chậu đất nung trụ tròn vát đáy 18x18cm DANU123"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-tru-tron-vat-day-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12288 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12288"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12288,&quot;parent_product_id&quot;:12288,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12288&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12288" data-product-type="simple"
                                                            data-original-product-id="12288"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12288" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12288" data-product_sku="DANU123"
                                            aria-label="Thêm “Chậu đất nung trụ tròn vát đáy 18x18cm DANU123” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12288"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-tru-tron-vat-day-18x18cm-danu123/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung trụ tròn vát đáy 18x18cm DANU123</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>45.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12284 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-vat-day-son-nham-17x15cm-danu122/"
                                            aria-label="Chậu đất nung vát đáy sơn nhám 17x15cm DANU122"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-den-son-nham-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12284 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12284"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12284,&quot;parent_product_id&quot;:12284,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12284&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12284" data-product-type="simple"
                                                            data-original-product-id="12284"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12284" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12284" data-product_sku="DANU122"
                                            aria-label="Thêm “Chậu đất nung vát đáy sơn nhám 17x15cm DANU122” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12284"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-vat-day-son-nham-17x15cm-danu122/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung vát đáy sơn nhám 17x15cm DANU122</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>30.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12278 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-wax-hoa-cuc-18x30-25x34cm-danu121/"
                                            aria-label="Chậu đất nung wax hoa cúc 18x30-25x34cm DANU121"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-hoa-tiet-hoa-cuc-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-hoa-tiet-hoa-cuc-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12278 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12278"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12278,&quot;parent_product_id&quot;:12278,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12278&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12278" data-product-type="variable"
                                                            data-original-product-id="12278"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-dat-nung-wax-hoa-cuc-18x30-25x34cm-danu121/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="12278" data-product_sku="DANU121"
                                            aria-label="Lựa chọn cho “Chậu đất nung wax hoa cúc 18x30-25x34cm DANU121”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12278"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-wax-hoa-cuc-18x30-25x34cm-danu121/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung wax hoa cúc 18×30-25x34cm DANU121</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>155.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>245.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12275 status-publish last instock product_cat-chau-dat-nung product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail sale shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1">
                                <div class="callout badge badge-square">
                                    <div class="badge-inner secondary on-sale"><span class="onsale">-10%</span></div>
                                </div>
                            </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-wax-mau-xanh-20x17cm-danu120/"
                                            aria-label="Chậu đất nung wax màu xanh 20x17cm DANU120"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-wax-xanh-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-wax-xanh-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12275 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12275"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12275,&quot;parent_product_id&quot;:12275,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12275&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12275" data-product-type="simple"
                                                            data-original-product-id="12275"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12275" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12275" data-product_sku="DANU120"
                                            aria-label="Thêm “Chậu đất nung wax màu xanh 20x17cm DANU120” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12275"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-wax-mau-xanh-20x17cm-danu120/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung wax màu xanh 20x17cm DANU120</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><del
                                                aria-hidden="true"><span
                                                    class="woocommerce-Price-amount amount"><bdi>100.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></del>
                                            <ins><span class="woocommerce-Price-amount amount"><bdi>90.000<span
                                                            class="woocommerce-Price-currencySymbol">₫</span></bdi></span></ins></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12262 status-publish first instock product_cat-chau-dat-nung product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-son-nham-cao-cap-co-dia-20x20cm-danu119/"
                                            aria-label="Chậu đất nung sơn nhám cao cấp có dĩa 20x20cm DANU119"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-son-cao-cap-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-son-cao-cap-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12262 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12262"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12262,&quot;parent_product_id&quot;:12262,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12262&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12262" data-product-type="simple"
                                                            data-original-product-id="12262"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12262" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12262" data-product_sku="DANU119"
                                            aria-label="Thêm “Chậu đất nung sơn nhám cao cấp có dĩa 20x20cm DANU119” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12262"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-son-nham-cao-cap-co-dia-20x20cm-danu119/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung sơn nhám cao cấp có dĩa 20x20cm DANU119</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12251 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail sale shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1">
                                <div class="callout badge badge-square">
                                    <div class="badge-inner secondary on-sale"><span class="onsale">-20%</span></div>
                                </div>
                            </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-hoa-tiet-vintage-25-17-14cm-danu118/"
                                            aria-label="Chậu đất nung họa tiết Vintage 25x25-18x18-14x14cm DANU118"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-vintage-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-vintage-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12251 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12251"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12251,&quot;parent_product_id&quot;:12251,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12251&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12251" data-product-type="variable"
                                                            data-original-product-id="12251"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-dat-nung-hoa-tiet-vintage-25-17-14cm-danu118/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="12251" data-product_sku="DANU118"
                                            aria-label="Lựa chọn cho “Chậu đất nung họa tiết Vintage 25x25-18x18-14x14cm DANU118”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12251"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-hoa-tiet-vintage-25-17-14cm-danu118/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung họa tiết Vintage 25×25-18×18-14x14cm DANU118</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>45.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>160.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12248 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-tru-tron-van-da-giac-15x14cm-danu117/"
                                            aria-label="Chậu đất nung trụ tròn vân đa giác 15x14cm DANU117"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-tru-tron-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-tru-tron-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12248 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12248"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12248,&quot;parent_product_id&quot;:12248,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12248&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12248" data-product-type="simple"
                                                            data-original-product-id="12248"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12248" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12248" data-product_sku="DANU117"
                                            aria-label="Thêm “Chậu đất nung trụ tròn vân đa giác 15x14cm DANU117” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12248"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-tru-tron-van-da-giac-15x14cm-danu117/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung trụ tròn vân đa giác 15x14cm DANU117</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>45.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12245 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-hoa-cuc-20x18cm-gosu045/"
                                            aria-label="Chậu gốm sứ họa tiết hoa cúc 20x18cm GOSU045"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-hoa-tiet-hoa-1-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-hoa-tiet-hoa-2-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12245 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12245"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12245,&quot;parent_product_id&quot;:12245,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12245&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12245" data-product-type="simple"
                                                            data-original-product-id="12245"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12245" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12245" data-product_sku="GOSU045"
                                            aria-label="Thêm “Chậu gốm sứ họa tiết hoa cúc 20x18cm GOSU045” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12245"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-hoa-cuc-20x18cm-gosu045/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết hoa cúc 20x18cm GOSU045</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12242 status-publish first instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-van-tho-cam-son-mau-17x17cm-danu116/"
                                            aria-label="Chậu đất nung vân thổ cẩm sơn màu 17x17cm DANU116"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-son-trang-van-tho-cam-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-son-xanh-tho-cam-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12242 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12242"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12242,&quot;parent_product_id&quot;:12242,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12242&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12242" data-product-type="simple"
                                                            data-original-product-id="12242"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12242" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12242" data-product_sku="DANU116"
                                            aria-label="Thêm “Chậu đất nung vân thổ cẩm sơn màu 17x17cm DANU116” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12242"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-van-tho-cam-son-mau-17x17cm-danu116/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung vân thổ cẩm sơn màu 17x17cm DANU116</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12237 status-publish instock product_cat-chau-dat-nung product_cat-chau-cay has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-dat-nung-tru-tron-gan-ke-16x14cm-danu115/"
                                            aria-label="Chậu đất nung trụ tròn gân kẻ 16x14cm DANU115"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-dat-nung-tru-tron-gan-ke-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12237 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12237"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12237,&quot;parent_product_id&quot;:12237,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12237&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12237" data-product-type="simple"
                                                            data-original-product-id="12237"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12237" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12237" data-product_sku="DANU115"
                                            aria-label="Thêm “Chậu đất nung trụ tròn gân kẻ 16x14cm DANU115” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12237"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-dat-nung-tru-tron-gan-ke-16x14cm-danu115/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                đất nung trụ tròn gân kẻ 16x14cm DANU115</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12234 status-publish instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hinh-da-giac-10x10cm-gosu044/"
                                            aria-label="Chậu gốm sứ hình đa giác 10x10cm GOSU044"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-hinh-da-giac-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12234 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12234"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12234,&quot;parent_product_id&quot;:12234,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12234&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12234" data-product-type="simple"
                                                            data-original-product-id="12234"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12234" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12234" data-product_sku="GOSU044"
                                            aria-label="Thêm “Chậu gốm sứ hình đa giác 10x10cm GOSU044” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12234"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hinh-da-giac-10x10cm-gosu044/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ hình đa giác 10x10cm GOSU044</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>50.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12223 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-variable">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-16x14-20x18cm-gosu043/"
                                            aria-label="Chậu gốm sứ họa tiết thổ cẩm 16x14-20x18cm GOSU043"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-mau-xanh-2-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"><img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-mau-xanh-4-800x960.jpg"
                                                class="show-on-hover absolute fill hide-for-small back-imageshow-on-hover absolute fill hide-for-small back-image hover-zoom"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12223 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12223"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12223,&quot;parent_product_id&quot;:12223,&quot;product_type&quot;:&quot;variable&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12223&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12223" data-product-type="variable"
                                                            data-original-product-id="12223"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-16x14-20x18cm-gosu043/"
                                            data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_variable add_to_cart_button"
                                            data-product_id="12223" data-product_sku="GOSU043"
                                            aria-label="Lựa chọn cho “Chậu gốm sứ họa tiết thổ cẩm 16x14-20x18cm GOSU043”"
                                            aria-describedby="This product has multiple variants. The options may be chosen on the product page"
                                            rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Lựa chọn các tùy chọn">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12223"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 136.219px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-16x14-20x18cm-gosu043/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết thổ cẩm 16×14-20x18cm GOSU043</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 43.1875px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>60.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span> –
                                            <span class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12220 status-publish first instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-20x18cm-gosu042/"
                                            aria-label="Chậu gốm sứ họa tiết thổ cẩm 20x18cm GOSU042"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-hoa-tiet-tho-cam-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12220 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12220"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12220,&quot;parent_product_id&quot;:12220,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12220&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12220" data-product-type="simple"
                                                            data-original-product-id="12220"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12220" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12220" data-product_sku="GOSU042"
                                            aria-label="Thêm “Chậu gốm sứ họa tiết thổ cẩm 20x18cm GOSU042” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12220"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-su-hoa-tiet-tho-cam-20x18cm-gosu042/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm sứ họa tiết thổ cẩm 20x18cm GOSU042</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12218 status-publish instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-gan-soc-doc-20x18cm-gosu041/"
                                            aria-label="Chậu gốm gân sọc dọc 20x18cm GOSU041"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-gan-soc-mau-trang-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12218 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12218"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12218,&quot;parent_product_id&quot;:12218,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12218&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12218" data-product-type="simple"
                                                            data-original-product-id="12218"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12218" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12218" data-product_sku="GOSU041"
                                            aria-label="Thêm “Chậu gốm gân sọc dọc 20x18cm GOSU041” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12218"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-gan-soc-doc-20x18cm-gosu041/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm gân sọc dọc 20x18cm GOSU041</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12214 status-publish instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-hoa-tiet-giot-nuoc-20x18cm-gosu040/"
                                            aria-label="Chậu gốm họa tiết giọt nước 20x18cm GOSU040"> <img
                                                width="800" height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-van-giot-nuoc-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12214 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12214"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12214,&quot;parent_product_id&quot;:12214,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12214&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12214" data-product-type="simple"
                                                            data-original-product-id="12214"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12214" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12214" data-product_sku="GOSU040"
                                            aria-label="Thêm “Chậu gốm họa tiết giọt nước 20x18cm GOSU040” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12214"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-hoa-tiet-giot-nuoc-20x18cm-gosu040/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm họa tiết giọt nước 20x18cm GOSU040</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        class="product-small col has-hover product type-product post-12212 status-publish last instock product_cat-chau-gom-su product_cat-chau-cay product_cat-chau-mini-de-ban has-post-thumbnail shipping-taxable purchasable product-type-simple">
                        <div class="col-inner">
                            <div class="badge-container absolute left top z-1"> </div>
                            <div class="product-small box">
                                <div class="box-image">
                                    <div class="image-zoom_in"> <a
                                            href="https://mowgarden.com/chau-gom-hoa-tiet-tam-giac-20x18cm-gosu039/"
                                            aria-label="Chậu gốm họa tiết tam giác 20x18cm GOSU039"> <img width="800"
                                                height="960"
                                                src="https://mowgarden.com/wp-content/uploads/2022/11/chau-gom-su-mau-trang-800x960.jpg"
                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                alt="" decoding="async" loading="lazy"> </a> </div>
                                    <div class="image-tools is-small top right show-on-hover">
                                        <div class="wishlist-icon"> <button
                                                class="wishlist-button button is-outline circle icon"
                                                aria-label="Wishlist"> <i class="icon-heart"></i> </button>
                                            <div class="wishlist-popup dark">
                                                <div class="yith-wcwl-add-to-wishlist add-to-wishlist-12212 wishlist-fragment on-first-load"
                                                    data-fragment-ref="12212"
                                                    data-fragment-options="{&quot;base_url&quot;:&quot;&quot;,&quot;in_default_wishlist&quot;:false,&quot;is_single&quot;:false,&quot;show_exists&quot;:false,&quot;product_id&quot;:12212,&quot;parent_product_id&quot;:12212,&quot;product_type&quot;:&quot;simple&quot;,&quot;show_view&quot;:false,&quot;browse_wishlist_text&quot;:&quot;Browse wishlist&quot;,&quot;already_in_wishslist_text&quot;:&quot;The product is already in your wishlist!&quot;,&quot;product_added_text&quot;:&quot;Product added!&quot;,&quot;heading_icon&quot;:&quot;fa-heart-o&quot;,&quot;available_multi_wishlist&quot;:false,&quot;disable_wishlist&quot;:false,&quot;show_count&quot;:false,&quot;ajax_loading&quot;:false,&quot;loop_position&quot;:&quot;after_add_to_cart&quot;,&quot;item&quot;:&quot;add_to_wishlist&quot;}">
                                                    <!-- ADD TO WISHLIST -->
                                                    <div class="yith-wcwl-add-button"> <a
                                                            href="?add_to_wishlist=12212&amp;_wpnonce=23224bc69e"
                                                            class="add_to_wishlist single_add_to_wishlist"
                                                            data-product-id="12212" data-product-type="simple"
                                                            data-original-product-id="12212"
                                                            data-title="Add to wishlist" rel="nofollow"> <i
                                                                class="yith-wcwl-icon fa fa-heart-o"></i> <span>Add to
                                                                wishlist</span> </a> </div> <!-- COUNT TEXT -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="image-tools is-small hide-for-small bottom left show-on-hover"> </div>
                                    <div
                                        class="image-tools grid-tools text-center hide-for-small bottom hover-slide-in show-on-hover">
                                        <a href="?add-to-cart=12212" data-quantity="1"
                                            class="add-to-cart-grid no-padding is-transparent product_type_simple add_to_cart_button ajax_add_to_cart"
                                            data-product_id="12212" data-product_sku="GOSU039"
                                            aria-label="Thêm “Chậu gốm họa tiết tam giác 20x18cm GOSU039” vào giỏ hàng"
                                            aria-describedby="" rel="nofollow">
                                            <div class="cart-icon tooltip is-small" title="Thêm vào giỏ hàng">
                                                <strong>+</strong></div>
                                        </a> <a class="quick-view quick-view-added" data-prod="12212"
                                            href="#quick-view">Xem nhanh</a> </div>
                                </div>
                                <div class="box-text box-text-products" style="height: 114.625px;">
                                    <div class="title-wrapper">
                                        <p class="name product-title woocommerce-loop-product__title"
                                            style="height: 59.9219px;"><a
                                                href="https://mowgarden.com/chau-gom-hoa-tiet-tam-giac-20x18cm-gosu039/"
                                                class="woocommerce-LoopProduct-link woocommerce-loop-product__link">Chậu
                                                gốm họa tiết tam giác 20x18cm GOSU039</a></p>
                                    </div>
                                    <div class="price-wrapper" style="height: 21.5938px;"> <span class="price"><span
                                                class="woocommerce-Price-amount amount"><bdi>120.000<span
                                                        class="woocommerce-Price-currencySymbol">₫</span></bdi></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row -->
                <div class="container">
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers nav-pagination links text-center">
                            <li><span aria-current="page" class="page-number current">1</span></li>
                            <li><a class="page-number" href="https://mowgarden.com/chau-cay/page/2/">2</a></li>
                            <li><a class="page-number" href="https://mowgarden.com/chau-cay/page/3/">3</a></li>
                            <li><a class="page-number" href="https://mowgarden.com/chau-cay/page/4/">4</a></li>
                            <li><a class="page-number" href="https://mowgarden.com/chau-cay/page/5/">5</a></li>
                            <li><a class="next page-number" href="https://mowgarden.com/chau-cay/page/2/"><i
                                        class="icon-angle-right"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div><!-- shop container -->
        </div>
        <div id="shop-sidebar" class="mfp-hide">
            <div class="sidebar-inner">
                <aside id="search-5" class="widget widget_search">
                    <form method="get" class="searchform" action="https://mowgarden.com/" role="search">
                        <div class="flex-row relative">
                            <div class="flex-col flex-grow"> <input type="search" class="search-field mb-0"
                                    name="s" value="" id="s" placeholder="Tìm kiếm sản phẩm"
                                    autocomplete="off"> </div>
                            <div class="flex-col"> <button type="submit"
                                    class="ux-search-submit submit-button secondary button icon mb-0"
                                    aria-label="Submit"> <i class="icon-search"></i> </button> </div>
                        </div>
                        <div class="live-search-results text-left z-top">
                            <div class="autocomplete-suggestions"
                                style="position: absolute; display: none; max-height: 300px; z-index: 9999;"></div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_price_filter-9" class="widget woocommerce widget_price_filter"><span
                        class="widget-title shop-sidebar">Lọc theo giá</span>
                    <div class="is-divider small"></div>
                    <form method="get" action="https://mowgarden.com/chau-cay/">
                        <div class="price_slider_wrapper">
                            <div class="price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                style="">
                                <div class="ui-slider-range ui-corner-all ui-widget-header"
                                    style="left: 0%; width: 100%;"></div><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default"
                                    style="left: 0%;"></span><span tabindex="0"
                                    class="ui-slider-handle ui-corner-all ui-state-default" style="left: 100%;"></span>
                            </div>
                            <div class="price_slider_amount" data-step="10"> <label class="screen-reader-text"
                                    for="min_price">Giá thấp nhất</label> <input type="text" id="min_price"
                                    name="min_price" value="10000" data-min="10000" placeholder="Giá thấp nhất"
                                    style="display: none;"> <label class="screen-reader-text" for="max_price">Giá cao
                                    nhất</label> <input type="text" id="max_price" name="max_price"
                                    value="850000" data-max="850000" placeholder="Giá cao nhất"
                                    style="display: none;"> <button type="submit" class="button">Lọc</button>
                                <div class="price_label" style=""> Giá <span class="from">10.000₫</span> —
                                    <span class="to">850.000₫</span> </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </form>
                </aside>
                <aside id="woocommerce_product_categories-13" class="widget woocommerce widget_product_categories"><span
                        class="widget-title shop-sidebar">danh mục</span>
                    <div class="is-divider small"></div>
                    <ul class="product-categories">
                        <li class="cat-item cat-item-109"><a href="https://mowgarden.com/cay-ngoai-troi/">Cây Ngoài
                                Trời</a></li>
                        <li class="cat-item cat-item-93"><a href="https://mowgarden.com/ban-cay-canh-trong-nha/">Cây
                                Trong Nhà</a></li>
                        <li class="cat-item cat-item-121 current-cat cat-parent active has-child" aria-expanded="false">
                            <a href="https://mowgarden.com/chau-cay/">Chậu Cây Cảnh</a><button class="toggle"
                                aria-label="Toggle"><i class="icon-angle-down"></i></button>
                            <ul class="children">
                                <li class="cat-item cat-item-137"><a href="https://mowgarden.com/chau-cao/">Chậu Cao</a>
                                </li>
                                <li class="cat-item cat-item-134"><a
                                        href="https://mowgarden.com/chau-cay-treo-tuong/">Chậu Cây Treo Tường</a></li>
                                <li class="cat-item cat-item-136"><a href="https://mowgarden.com/chau-co-lon/">Chậu Cỡ
                                        Lớn</a></li>
                                <li class="cat-item cat-item-135"><a href="https://mowgarden.com/chau-co-trung/">Chậu Cỡ
                                        Trung</a></li>
                                <li class="cat-item cat-item-130"><a href="https://mowgarden.com/chau-composite/">Chậu
                                        Composite</a></li>
                                <li class="cat-item cat-item-132"><a href="https://mowgarden.com/chau-go/">Chậu Gỗ</a>
                                </li>
                                <li class="cat-item cat-item-760"><a href="https://mowgarden.com/chau-gom-su/">Chậu Gốm
                                        Sứ</a></li>
                                <li class="cat-item cat-item-122"><a
                                        href="https://mowgarden.com/chau-cay-hinh-chu-nhat/">Chậu Hình Chữ Nhật</a></li>
                                <li class="cat-item cat-item-133"><a href="https://mowgarden.com/chau-mini-de-ban/">Chậu
                                        Mini Để Bàn</a></li>
                                <li class="cat-item cat-item-128"><a href="https://mowgarden.com/chau-nhua/">Chậu
                                        Nhựa</a></li>
                                <li class="cat-item cat-item-129"><a href="https://mowgarden.com/chau-xi-mang/">Chậu Xi
                                        Măng</a></li>
                                <li class="cat-item cat-item-131"><a href="https://mowgarden.com/chau-dat-nung/">Chậu
                                        Đất Nung</a></li>
                                <li class="cat-item cat-item-125"><a href="https://mowgarden.com/kieu-chau-treo/">Kiểu
                                        Chậu Treo</a></li>
                                <li class="cat-item cat-item-127"><a href="https://mowgarden.com/kieu-chau-vuong/">Kiểu
                                        Chậu Vuông</a></li>
                                <li class="cat-item cat-item-124"><a href="https://mowgarden.com/kieu-hinh-bau/">Kiểu
                                        Hình Bầu</a></li>
                                <li class="cat-item cat-item-126"><a
                                        href="https://mowgarden.com/kieu-hinh-bau-tron/">Kiểu Hình Bầu Tròn</a></li>
                                <li class="cat-item cat-item-123"><a
                                        href="https://mowgarden.com/chau-cay-kieu-tru-dung/">Kiểu Trụ Đứng</a></li>
                            </ul>
                        </li>
                        <li class="cat-item cat-item-395"><a href="https://mowgarden.com/dung-cu-lam-vuon/">Dụng Cụ Làm
                                Vườn</a></li>
                        <li class="cat-item cat-item-393"><a href="https://mowgarden.com/phan-bon/">Phân Bón</a></li>
                        <li class="cat-item cat-item-394"><a href="https://mowgarden.com/phu-kien-trang-tri/">Phụ Kiện
                                Trang Trí</a></li>
                        <li class="cat-item cat-item-15"><a
                                href="https://mowgarden.com/uncategorized/">Uncategorized</a></li>
                        <li class="cat-item cat-item-396"><a href="https://mowgarden.com/dat-trong-cay/">Đất Trồng
                                Cây</a></li>
                    </ul>
                </aside>
            </div>
        </div>
    </div>
@endsection
