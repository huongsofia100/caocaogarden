@extends('layouts.layout-comon')

@section('maincontent')
    <div id="content" role="main" class="content-area">
        <section class="section dark" id="section_1092186381">
            <div class="bg section-bg fill bg-fill bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row align-center" id="row-751855758">
                    <div id="col-1787916955" class="col medium-7 small-12 large-8">
                        <div class="col-inner">
                            <div id="gap-649875221" class="gap-element clearfix" style="display:block; height:auto;">
                                <style>
                                    #gap-649875221 {
                                        padding-top: 132px;
                                    }
                                </style>
                            </div>
                            <div id="text-3068461258" class="text">
                                <h2><span style="font-size: 250%; color: #214738">Giới thiệu</span></h2>
                                <style>
                                    #text-3068461258 {
                                        text-align: center;
                                        color: #f6cfb2;
                                    }

                                    #text-3068461258>* {
                                        color: #f6cfb2;
                                    }
                                </style>
                            </div>
                            <div class="text-center">
                                <div class="is-divider divider clearfix"
                                    style="max-width:100px;height:5px;background-color:rgba(34, 66, 41, 0.85);"></div>
                            </div>
                            <div id="text-332697700" class="text">
                                <p><em><span style="font-size: 100%; color: #214738;">Bringing Life to People and People to
                                            Life</span></em></p>
                                <style>
                                    #text-332697700 {
                                        font-size: 1.5rem;
                                        line-height: 1.25;
                                        text-align: center;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                #section_1092186381 {
                    padding-top: 0px;
                    padding-bottom: 0px;
                    min-height: 350px;
                    background-color: rgb(255, 255, 255);
                }

                #section_1092186381 .section-bg.bg-loaded {
                    background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/anh-nen-trang-gioi-thieu-2.jpg);
                }

                #section_1092186381 .section-bg {
                    background-position: 48% 70%;
                }

                #section_1092186381 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_1092186381 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }

                @media (min-width:550px) {
                    #section_1092186381 {
                        min-height: 380px;
                    }
                }
            </style>
        </section>
        <section class="section" id="section_1293065026">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row row-large align-middle" style="max-width:1300px" id="row-151410377">
                    <div id="col-962525273" class="col medium-10 small-12 large-7">
                        <div class="col-inner">
                            <div id="text-300354747" class="text">
                                <p><span style="font-size: 220%;"><strong><span
                                                style="font-family: 'book antiqua', palatino, serif;">Món quà đến từ thiên
                                                nhiên</span></strong></span></p>
                                <style>
                                    #text-300354747 {
                                        font-size: 1rem;
                                        line-height: 1.25;
                                    }
                                </style>
                            </div>
                            <div class="is-divider divider clearfix"
                                style="max-width:100px;height:4px;background-color:rgba(34, 66, 41, 0.85);"></div>
                            <p style="text-align: justify">Mỗi năm cây xanh có thể hấp thụ trung bình được khoảng 21kg khí
                                CO2, nó tương đương với 22 tấn khí carbonic trong suốt cuộc đời của cây. Điều đó nói lên tầm
                                quan trọng của cây xanh đối với môi trường sống của con người. Toàn bộ lượng khí ô nhiễn
                                (các oxit nitơ, amoniac, SO2 và ôzôn) được cây xanh chuyển hóa thành oxi giúp cho con người
                                được thở.</p>
                            <p style="text-align: justify">Việc kết nối với thường xuyên với thiên nhiên sẽ mang lại nhiều
                                lợi ích lâu dài về mặt tinh thần, sức khỏe và cân bằng. Với đời sống hiện đại thì việc liên
                                kết với thiên nhiên ngày càng ít ỏi. Thiên nhiên có phải là một thứ xa xỉ hay không? Tại sao
                                chúng ta không tạo ra những mảng xanh thư thái ngay trong chính nơi chúng thường sống.&nbsp;
                            </p>
                            <p style="text-align: justify"><strong>Cào Cào Garden</strong> tin rằng một khi bạn đã đi tìm
                                kiếm
                                mảng xanh cho không gian sống cho mình thì bạn đang cố gắng tạo ra kết nối với chính bản
                                thân mình. Và bạn cũng là một người có ý thức bảo vệ môi trường và giúp mọi người hành động
                                vì mẹ Thiên Nhiên.</p>
                            <p style="text-align: justify;">Tôn chỉ giúp <strong>Cào Cào Garden</strong> hoạt động chính là
                                tinh
                                thần trách nhiên với thiên nhiên cũng như mang lại những sản phẩm xanh và tự nhiên tới cộng
                                đồng bằng một dịch vụ chất lượng. Chúng tôi hy vọng rằng mình có thể lan tỏa mạnh mẽ tinh
                                thần liên kết và bảo vệ Mẹ Thiên Nhiên nhiều hơn. Từ đó, cuộc sống loài người sẽ trở nên tốt
                                đẹp hơn từ những nỗ lực bảo vệ này.&nbsp;</p>
                        </div>
                        <style>
                            #col-962525273>.col-inner {
                                padding: 0px 0px 0px 0;
                            }

                            @media (min-width:550px) {
                                #col-962525273>.col-inner {
                                    padding: 0px 0px 0px 30px;
                                }
                            }
                        </style>
                    </div>
                    <div id="col-87428143" class="col medium-12 small-12 large-5">
                        <div class="col-inner">
                            <div class="banner has-hover" id="banner-2129110129">
                                <div class="banner-inner fill">
                                    <div class="banner-bg fill">
                                        <div class="bg fill bg-fill bg-loaded"></div>
                                    </div>
                                    <div class="banner-layers container">
                                        <div class="fill banner-link"></div>
                                    </div>
                                </div>
                                <style>
                                    #banner-2129110129 {
                                        padding-top: 300px;
                                        background-color: rgb(255, 255, 255);
                                    }

                                    #banner-2129110129 .bg.bg-loaded {
                                        background-image: url(https://mowgarden.com/wp-content/uploads/2021/06/trong-cay-trong-nha.jpg);
                                    }

                                    #banner-2129110129 .bg {
                                        background-position: 8% 39%;
                                    }

                                    #banner-2129110129 .ux-shape-divider--top svg {
                                        height: 150px;
                                        --divider-top-width: 100%;
                                    }

                                    #banner-2129110129 .ux-shape-divider--bottom svg {
                                        height: 150px;
                                        --divider-width: 100%;
                                    }

                                    @media (min-width:550px) {
                                        #banner-2129110129 {
                                            padding-top: 600px;
                                        }
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                #section_1293065026 {
                    padding-top: 50px;
                    padding-bottom: 50px;
                }

                #section_1293065026 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_1293065026 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }

                @media (min-width:550px) {
                    #section_1293065026 {
                        padding-top: 45px;
                        padding-bottom: 45px;
                    }
                }
            </style>
        </section>
        <section class="section" id="section_153800731">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row row-large row-full-width" id="row-417723495">
                    <div id="col-1468173030" class="col medium-4 small-12 large-4">
                        <div class="col-inner text-center">
                            <div class="icon-box featured-box icon-box-top text-left">
                                <div class="icon-box-img" style="width: 100px">
                                    <div class="icon">
                                        <div class="icon-inner"> <img loading="lazy" decoding="async" width="512"
                                                height="512"
                                                src="https://mowgarden.com/wp-content/uploads/2022/01/fence.png"
                                                class="attachment-medium size-medium" alt="fence"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/fence.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/fence-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/fence-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/fence-125x125.png 125w"
                                                sizes="(max-width: 512px) 100vw, 512px"> </div>
                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <div id="text-2982957736" class="text">
                                        <p style="font-size: 160%;"><strong>Tạo kết nối</strong></p>
                                        <p style="text-align: justify;">Mối liên kết giữa con người và thiên nhiên là một
                                            điều kì diệu của tạo hóa. Khi chúng ta hòa hợp với thiên nhiên thì cuộc sống của
                                            trở nên tốt đẹp hơn. Và chúng tôi luôn mong mỏi sẽ có thể tạo ra thật nhiều kết
                                            nối này hơn nữa.</p>
                                        <style>
                                            #text-2982957736 {
                                                text-align: center;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="col-69886099" class="col medium-4 small-12 large-4">
                        <div class="col-inner text-center">
                            <div class="icon-box featured-box icon-box-top text-left">
                                <div class="icon-box-img" style="width: 100px">
                                    <div class="icon">
                                        <div class="icon-inner"> <img loading="lazy" decoding="async" width="512"
                                                height="512"
                                                src="https://mowgarden.com/wp-content/uploads/2022/01/gloves.png"
                                                class="attachment-medium size-medium" alt="gloves"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/gloves.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/gloves-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/gloves-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/gloves-125x125.png 125w"
                                                sizes="(max-width: 512px) 100vw, 512px"> </div>
                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <div id="text-2149696114" class="text">
                                        <p style="font-size: 160%;"><strong>Sự chân thành</strong></p>
                                        <p style="text-align: justify;">Và sau khi sự kết nối giữa con người và thiên nhiên
                                            được hình thành thì những tác động tích cực từ hoạt động này sẽ bắt đầu diễn ra.
                                            Tâm hồn được tác động mạnh mẽ và chúng tôi muốn chia sẽ điều này với sự chân
                                            thành.</p>
                                        <style>
                                            #text-2149696114 {
                                                text-align: center;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="col-2025786971" class="col medium-4 small-12 large-4">
                        <div class="col-inner text-center">
                            <div class="icon-box featured-box icon-box-top text-left">
                                <div class="icon-box-img" style="width: 100px">
                                    <div class="icon">
                                        <div class="icon-inner"> <img loading="lazy" decoding="async" width="512"
                                                height="512"
                                                src="https://mowgarden.com/wp-content/uploads/2022/01/sprout.png"
                                                class="attachment-medium size-medium" alt="sprout"
                                                srcset="https://mowgarden.com/wp-content/uploads/2022/01/sprout.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/sprout-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/sprout-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/sprout-125x125.png 125w"
                                                sizes="(max-width: 512px) 100vw, 512px"> </div>
                                    </div>
                                </div>
                                <div class="icon-box-text last-reset">
                                    <div id="text-273795213" class="text">
                                        <p style="font-size: 160%;"><strong>Cùng đồng hành</strong></p>
                                        <p style="text-align: justify;"><strong>Cào Cào Garden</strong> không bao giờ muốn
                                            dừng
                                            lại trên chặng hành trình này. Chúng tôi luôn chủ động thay đổi để không ngừng
                                            cải thiện chất lượng dịch vụ. Ngoài ra, chúng tôi cũng luôn đồng hành cùng khách
                                            hàng để tạo ra một không gian sống thật sự xứng đáng.</p>
                                        <style>
                                            #text-273795213 {
                                                text-align: center;
                                            }
                                        </style>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="gap-606200376" class="gap-element clearfix" style="display:block; height:auto;">
                    <style>
                        #gap-606200376 {
                            padding-top: 50px;
                        }
                    </style>
                </div>
            </div>
            <style>
                #section_153800731 {
                    padding-top: 10px;
                    padding-bottom: 10px;
                }

                #section_153800731 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_153800731 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
        <section class="section" id="section_133795945">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row row-collapse row-full-width" id="row-627271132">
                    <div id="col-1041323184" class="col small-12 large-12">
                        <div class="col-inner">
                            <div class="banner has-hover has-parallax" id="banner-341287862">
                                <div class="banner-inner fill">
                                    <div class="banner-bg fill parallax-active" data-parallax="-8"
                                        data-parallax-container=".banner" data-parallax-background=""
                                        style="height: 802px; transform: translate3d(0px, -609.76px, 0px); backface-visibility: hidden;">
                                        <div class="bg fill bg-fill bg-loaded"></div>
                                        <div class="overlay"></div>
                                    </div>
                                    <div class="banner-layers container">
                                        <div class="fill banner-link"></div>
                                    </div>
                                </div>
                                <style>
                                    #banner-341287862 {
                                        padding-top: 40%;
                                    }

                                    #banner-341287862 .bg.bg-loaded {
                                        background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/gioi-thieu-vuon-kieng-la-mowgarden.jpg);
                                    }

                                    #banner-341287862 .overlay {
                                        background-color: rgba(253, 240, 230, 0.093);
                                    }

                                    #banner-341287862 .bg {
                                        background-position: 44% 100%;
                                    }

                                    #banner-341287862 .ux-shape-divider--top svg {
                                        height: 150px;
                                        --divider-top-width: 100%;
                                    }

                                    #banner-341287862 .ux-shape-divider--bottom svg {
                                        height: 150px;
                                        --divider-width: 100%;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                #section_133795945 {
                    padding-top: 0px;
                    padding-bottom: 0px;
                    min-height: 400px;
                }

                #section_133795945 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_133795945 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }

                @media (min-width:550px) {
                    #section_133795945 {
                        min-height: 400px;
                    }
                }
            </style>
        </section>
        <section class="section" id="section_344339029">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row row-collapse align-right" id="row-331797434">
                    <div id="col-761038966" class="col medium-6 small-12 large-6" data-animate="fadeInLeft"
                        data-animated="true">
                        <div class="col-inner">
                            <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_1099392213">
                                <div class="img-inner image-cover dark" style="padding-top:120%;"> <img loading="lazy"
                                        decoding="async" width="1200" height="900"
                                        src="https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh.jpg"
                                        class="attachment-original size-original" alt="duong xi quynh"
                                        srcset="https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh.jpg 1200w, https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh-800x600.jpg 800w, https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh-1067x800.jpg 1067w, https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh-768x576.jpg 768w, https://mowgarden.com/wp-content/uploads/2021/04/duong-xi-quynh-20x15.jpg 20w"
                                        sizes="(max-width: 1200px) 100vw, 1200px"> </div>
                                <style>
                                    #image_1099392213 {
                                        width: 100%;
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                    <div id="col-803624236" class="col medium-5 small-10 large-5" data-animate="fadeInRight"
                        data-animated="true">
                        <div class="col-inner text-left" style="background-color:rgb(253, 240, 230);">
                            <h3><span style="font-size: 150%;">“Trong mỗi bước đi cùng với thiên nhiên, chúng ta nhận được
                                    nhiều hơn những gì ta tìm kiếm”.</span></h3>
                            <p><span style="font-size: 120%;"> – John Muir.</span></p>
                        </div>
                        <style>
                            #col-803624236>.col-inner {
                                padding: 60px 50px 40px 50px;
                                margin: 115px 0px 0px -85px;
                            }
                        </style>
                    </div>
                    <style>
                        #row-331797434>.col>.col-inner {
                            background-color: rgb(255, 255, 255);
                        }
                    </style>
                </div>
            </div>
            <style>
                #section_344339029 {
                    padding-top: 50px;
                    padding-bottom: 50px;
                }

                #section_344339029 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_344339029 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
        <section class="section" id="section_1273570513">
            <div class="bg section-bg fill bg-fill bg-loaded">
                <div class="section-bg-overlay absolute fill"></div>
            </div>
            <div class="section-content relative">
                <div class="row row-small align-center" id="row-1008618642">
                    <div id="col-257917516" class="col medium-10 small-12 large-10">
                        <div class="col-inner">
                            <div id="text-1148141832" class="text">
                                <p style="text-align: center;"><span
                                        style="font-family: 'book antiqua', palatino, serif; font-size: 200%;"><strong>Câu
                                            chuyện của Cào Cào Garden</strong></span></p>
                                <style>
                                    #text-1148141832 {
                                        line-height: 1.25;
                                    }
                                </style>
                            </div>
                            <div style="text-align: justify;">Con người chúng ta vốn dĩ luôn mưu cầu hạnh phúc. Chúng ta
                                đều biết, bản thân con người hạnh phúc hay không là ở tâm. Nếu tâm bất động, hoặc dễ hiểu
                                hơn là tâm đã đủ vui vẻ thì mọi người sẽ dễ dàng chạm đến niềm vui sướng, niềm hạnh phúc
                                đích thực. Và đi tìm hạnh phúc đích thực là sứ mệnh cũng như là lý do vì sao mình lại
                                <strong>Cào Cào Garden</strong>.&nbsp;
                            </div>
                            <div>&nbsp;</div>
                            <div style="text-align: justify;">Triết lý của <strong>Cào Cào Garden</strong> không chỉ dừng
                                lại
                                qua những sản phẩm cây xanh, những thiết kế mô hình không gian xanh mang tính thẩm mỹ, nghệ
                                thuật cao đến khách hàng mà thật lòng mong muốn khách hàng dành thời gian hiểu hơn về việc
                                trồng cây, hiểu hơn về thiên nhiên, yêu thương và trân trọng những gì do chính mình gieo
                                trồng nên.</div>
                            <div>&nbsp;</div>
                            <div style="text-align: justify;">Vì những trải nghiệm ấy sẽ hướng khách hàng đến ‘sự thiền’.
                                Mà ‘thiền’ là cách tốt nhất dẫn mọi người đến sự cân bằng trong cuộc sống cũng như trong tâm
                                thức của chính mình. Từ đó, chúng ta sẽ có cơ hội để chạm tới trái tim cơ bản thuần khiết
                                giữa con người và thiên nhiên.&nbsp;</div>
                            <div>&nbsp;</div>
                            <div>MOW Garden giúp bạn tạo ra những kết nối với thực vật và tự nhiên, từ đó giúp bạn nuôi
                                dưỡng một cuộc sống tốt đẹp hơn.</div>
                        </div>
                        <style>
                            #col-257917516>.col-inner {
                                padding: 0px 100px 0px 100px;
                                margin: 0px 100px 0px 0100pxpx;
                            }
                        </style>
                    </div>
                    <style>
                        #row-1008618642>.col>.col-inner {
                            padding: 30px 0px 0px 0px;
                        }
                    </style>
                </div>
            </div>
            <style>
                #section_1273570513 {
                    padding-top: 60px;
                    padding-bottom: 60px;
                }

                #section_1273570513 .section-bg-overlay {
                    background-color: rgba(253, 240, 230, 0.413);
                }

                #section_1273570513 .section-bg.bg-loaded {
                    background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/gioi-thieu-mowgarden-4.jpg);
                }

                #section_1273570513 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_1273570513 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
        <section class="section" id="section_77175599">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded"> </div>
            <div class="section-content relative">
                <div id="gap-1891726663" class="gap-element clearfix" style="display:block; height:auto;">
                    <style>
                        #gap-1891726663 {
                            padding-top: 50px;
                        }
                    </style>
                </div>
                <div class="row" style="max-width:1300px" id="row-1549761597">
                    <div id="col-1362851407" class="col medium-11 small-12 large-9">
                        <div class="col-inner">
                            <div class="banner has-hover" id="banner-2069091259">
                                <div class="banner-inner fill">
                                    <div class="banner-bg fill">
                                        <div class="bg fill bg-fill bg-loaded"></div>
                                    </div>
                                    <div class="banner-layers container">
                                        <div class="fill banner-link"></div>
                                    </div>
                                </div>
                                <style>
                                    #banner-2069091259 {
                                        padding-top: 350px;
                                        background-color: rgb(255, 255, 255);
                                    }

                                    #banner-2069091259 .bg.bg-loaded {
                                        background-image: url(https://mowgarden.com/wp-content/uploads/2022/01/gioi-thieu-mowgarden-2-1200x800.jpg);
                                    }

                                    #banner-2069091259 .bg {
                                        background-position: 52% 65%;
                                    }

                                    #banner-2069091259 .ux-shape-divider--top svg {
                                        height: 150px;
                                        --divider-top-width: 100%;
                                    }

                                    #banner-2069091259 .ux-shape-divider--bottom svg {
                                        height: 150px;
                                        --divider-width: 100%;
                                    }

                                    @media (min-width:550px) {
                                        #banner-2069091259 {
                                            padding-top: 550px;
                                        }
                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row align-right" style="max-width:1300px" id="row-550508206">
                    <div id="col-456459910" class="col medium-8 small-12 large-5">
                        <div class="col-inner dark" style="background-color:rgba(34, 66, 41, 0.9);">
                            <div id="text-4152633289" class="text">
                                <h2>Hãy trân trọng thiên nhiên nơi bạn sống</h2>
                                <style>
                                    #text-4152633289 {
                                        font-size: 1.5rem;
                                    }

                                    @media (min-width:550px) {
                                        #text-4152633289 {
                                            font-size: 1.25rem;
                                        }
                                    }
                                </style>
                            </div>
                            <p>Những món quà của thiên nhiên thường khó có thể đánh giá bằng tiền bạc. Cũng giống như không
                                khí trong lành thường được dùng thoải mái cho đến khi trở nên khan hiếm.</p>
                        </div>
                        <style>
                            #col-456459910>.col-inner {
                                padding: 30px 20px 30px 20px;
                                margin: -50px 0px 0px 0px;
                            }

                            @media (min-width:550px) {
                                #col-456459910>.col-inner {
                                    padding: 50px 50px 50px 50px;
                                    margin: -250px 0px 0px 0px;
                                }
                            }
                        </style>
                    </div>
                </div>
                <div id="gap-2085423333" class="gap-element clearfix" style="display:block; height:auto;">
                    <style>
                        #gap-2085423333 {
                            padding-top: 50px;
                        }
                    </style>
                </div>
            </div>
            <style>
                #section_77175599 {
                    padding-top: 0px;
                    padding-bottom: 0px;
                    margin-bottom: 0px;
                    min-height: 0px;
                }

                #section_77175599 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_77175599 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
        <section class="section" id="section_1914397436">
            <div class="bg section-bg fill bg-fill bg-loaded"> </div>
            <div class="section-content relative">
                <div class="row" id="row-44109532">
                    <div id="col-1471397395" class="col small-12 large-12">
                        <div class="col-inner dark">
                            <div id="text-591065420" class="text">
                                <p style="color: #214738;"><span
                                        style="font-family: 'book antiqua', palatino, serif; font-size: 250%;"><strong>Thông
                                            tin liên hệ</strong></span></p>
                                <style>
                                    #text-591065420 {
                                        font-size: 1rem;
                                        line-height: 0.75;
                                        text-align: center;
                                    }
                                </style>
                            </div>
                            <div class="text-center">
                                <div class="is-divider divider clearfix"
                                    style="max-width:100px;height:4px;background-color:rgba(34, 66, 41, 0.85);"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="row-1628929745">
                    <div id="col-525953926" class="col small-12 large-12" style="height: 570px">
                        <div class="col-inner box-shadow-3">
                            <div class="row row-collapse" id="row-966846135">
                                <div id="col-796191619" class="col medium-12 small-12 large-6">
                                    <div class="col-inner">
                                        <div class="row row-small" id="row-1091623524">
                                            <div id="col-2005028117" class="col medium-6 small-12 large-6">
                                                <div class="col-inner text-center">
                                                    <div class="icon-box featured-box icon-box-top text-left">
                                                        <div class="icon-box-img" style="width: 60px">
                                                            <div class="icon">
                                                                <div class="icon-inner"> <img loading="lazy"
                                                                        decoding="async" width="512" height="512"
                                                                        src="https://mowgarden.com/wp-content/uploads/2022/01/locally-sourced.png"
                                                                        class="attachment-medium size-medium"
                                                                        alt="locally sourced"
                                                                        srcset="https://mowgarden.com/wp-content/uploads/2022/01/locally-sourced.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/locally-sourced-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/locally-sourced-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/locally-sourced-125x125.png 125w"
                                                                        sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="icon-box-text last-reset">
                                                            <div id="text-3618966949" class="text">
                                                                <p><strong>Địa chỉ</strong></p>
                                                                <style>
                                                                    #text-3618966949 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="text-935795365" class="text">
                                                        <p>18 Kiệt 116 Nguyễn Lộ Trạch, tổ 14, <br>Thành phố Huế, Thừa Thiên
                                                            Huế
                                                        </p>
                                                        <style>
                                                            #text-935795365 {
                                                                text-align: center;
                                                            }
                                                        </style>
                                                    </div>
                                                    <div id="gap-51434481" class="gap-element clearfix"
                                                        style="display:block; height:auto;">
                                                        <style>
                                                            #gap-51434481 {
                                                                padding-top: 30px;
                                                            }
                                                        </style>
                                                    </div>
                                                    <div class="icon-box featured-box icon-box-top text-left">
                                                        <div class="icon-box-img" style="width: 60px">
                                                            <div class="icon">
                                                                <div class="icon-inner"> <img loading="lazy"
                                                                        decoding="async" width="512" height="512"
                                                                        src="https://mowgarden.com/wp-content/uploads/2022/01/social-media.png"
                                                                        class="attachment-medium size-medium"
                                                                        alt="social media"
                                                                        srcset="https://mowgarden.com/wp-content/uploads/2022/01/social-media.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/social-media-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/social-media-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/social-media-125x125.png 125w"
                                                                        sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="icon-box-text last-reset">
                                                            <div id="text-664663566" class="text">
                                                                <p><strong>Mạng xã hội</strong></p>
                                                                <style>
                                                                    #text-664663566 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <div id="text-2780584842" class="text">
                                                                <p>Faceboook<br>Youtube<br>Instagram</p>
                                                                <style>
                                                                    #text-2780584842 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #col-2005028117>.col-inner {
                                                        padding: 60px 0px 50px 0px;
                                                    }

                                                    @media (min-width:550px) {
                                                        #col-2005028117>.col-inner {
                                                            padding: 60px 0px 50px 50px;
                                                        }
                                                    }

                                                    @media (min-width:850px) {
                                                        #col-2005028117>.col-inner {
                                                            padding: 60px 0px 0px 50px;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                            <div id="col-182131954" class="col medium-6 small-12 large-6">
                                                <div class="col-inner text-center">
                                                    <div class="icon-box featured-box icon-box-top text-left">
                                                        <div class="icon-box-img" style="width: 60px">
                                                            <div class="icon">
                                                                <div class="icon-inner"> <img loading="lazy"
                                                                        decoding="async" width="512" height="512"
                                                                        src="https://mowgarden.com/wp-content/uploads/2022/01/gmail.png"
                                                                        class="attachment-medium size-medium"
                                                                        alt="gmail"
                                                                        srcset="https://mowgarden.com/wp-content/uploads/2022/01/gmail.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/gmail-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/gmail-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/gmail-125x125.png 125w"
                                                                        sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="icon-box-text last-reset">
                                                            <div id="text-1937325388" class="text">
                                                                <p><strong>Email</strong></p>
                                                                <style>
                                                                    #text-1937325388 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="text-1600246997" class="text">
                                                        <p>caocaogarden@gmail.com</p>
                                                        <style>
                                                            #text-1600246997 {
                                                                text-align: center;
                                                            }
                                                        </style>
                                                    </div>
                                                    <div id="gap-1839793348" class="gap-element clearfix"
                                                        style="display:block; height:auto;">
                                                        <style>
                                                            #gap-1839793348 {
                                                                padding-top: 30px;
                                                            }
                                                        </style>
                                                    </div>
                                                    <div class="icon-box featured-box icon-box-top text-left">
                                                        <div class="icon-box-img" style="width: 60px">
                                                            <div class="icon">
                                                                <div class="icon-inner"> <img loading="lazy"
                                                                        decoding="async" width="512" height="512"
                                                                        src="https://mowgarden.com/wp-content/uploads/2022/01/call.png"
                                                                        class="attachment-medium size-medium"
                                                                        alt="call"
                                                                        srcset="https://mowgarden.com/wp-content/uploads/2022/01/call.png 512w, https://mowgarden.com/wp-content/uploads/2022/01/call-280x280.png 280w, https://mowgarden.com/wp-content/uploads/2022/01/call-20x20.png 20w, https://mowgarden.com/wp-content/uploads/2022/01/call-125x125.png 125w"
                                                                        sizes="(max-width: 512px) 100vw, 512px"> </div>
                                                            </div>
                                                        </div>
                                                        <div class="icon-box-text last-reset">
                                                            <div id="text-2653503509" class="text">
                                                                <p><strong>Hotline</strong></p>
                                                                <style>
                                                                    #text-2653503509 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                            <div id="text-3255881536" class="text">
                                                                <p>0783 655 566</p>
                                                                <style>
                                                                    #text-3255881536 {
                                                                        text-align: center;
                                                                    }
                                                                </style>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <style>
                                                    #col-182131954>.col-inner {
                                                        padding: 0px 0px 60px 0px;
                                                    }

                                                    @media (min-width:550px) {
                                                        #col-182131954>.col-inner {
                                                            padding: 60px 50px 0px 0px;
                                                        }
                                                    }
                                                </style>
                                            </div>
                                        </div>
                                    </div>
                                    <style>
                                        #col-796191619>.col-inner {
                                            border-radius: 62px;
                                        }
                                    </style>
                                </div>
                                <div id="col-1104746325" class="col medium-12 small-12 large-6">
                                    <div class="col-inner">
                                        <div id="text-2351314291" class="text">
                                            <p><iframe loading="lazy" style="border: 0;"
                                                    src="https://maps.google.com/maps?q=c%C3%A0o%20c%C3%A0o%20garden&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                                                    width="600" height="550"
                                                    allowfullscreen="allowfullscreen"></iframe></p>
                                            {{-- <div class="mapouter">
                                                <div class="gmap_canvas"><iframe
                                                        src="https://maps.google.com/maps?q=c%C3%A0o%20c%C3%A0o%20garden&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                                                        frameborder="0" scrolling="no"
                                                        style="width: 410px; height: 470px;"></iframe>
                                                    <style>
                                                        .mapouter {
                                                            position: relative;
                                                            height: 470px;
                                                            width: 410px;
                                                            background: #fff;
                                                        }

                                                        .maprouter a {
                                                            color: #fff !important;
                                                            position: absolute !important;
                                                            top: 0 !important;
                                                            z-index: 0 !important;
                                                        }
                                                    </style><a href="https://blooketjoin.org/">blooket</a>
                                                    <style>
                                                        .gmap_canvas {
                                                            overflow: hidden;
                                                            height: 470px;
                                                            width: 410px
                                                        }

                                                        .gmap_canvas iframe {
                                                            position: relative;
                                                            z-index: 2
                                                        }
                                                    </style>
                                                </div>
                                            </div> --}}
                                            <style>
                                                #text-2351314291 {
                                                    text-align: center;
                                                }
                                            </style>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <style>
                        #row-1628929745>.col>.col-inner {
                            background-color: rgba(255, 255, 255, 0.7);
                        }
                    </style>
                </div>
            </div>
            <style>
                #section_1914397436 {
                    padding-top: 100px;
                    padding-bottom: 100px;
                }

                #section_1914397436 .section-bg.bg-loaded {
                    background-image: 1463;
                }

                #section_1914397436 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_1914397436 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
    </div>
@endsection
