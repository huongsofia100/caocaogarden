@extends('layouts.layout-comon')

@section('maincontent')
    <div class="checkout-page-title page-title">
        <div class="page-title-inner flex-row medium-flex-wrap container">
            <div class="flex-col flex-grow medium-text-center">
                <nav
                    class="breadcrumbs flex-row flex-row-center heading-font checkout-breadcrumbs text-center strong h2 uppercase">
                    <a href="{{route('giohang')}}" class="current">
                        Giỏ hàng </a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="https://mowgarden.com/checkout/" class="hide-for-small">
                        Thanh toán </a>
                    <span class="divider hide-for-small"><i class="icon-angle-right"></i></span>
                    <a href="#" class="no-click hide-for-small">
                        Hoàn thành </a>
                </nav>
            </div>
        </div>
    </div>
    
    <div class="cart-container container page-wrapper page-checkout">
        <div class="woocommerce">
            <div class="text-center pt pb">
                <div class="woocommerce-notices-wrapper"></div>
                <div class="woocommerce-info message-wrapper">
                    <div class="message-container container medium-text-center">
                        Chưa có sản phẩm nào trong giỏ hàng. </div>
                </div>
                <p class="return-to-shop">
                    <a class="button primary wc-backward" href="https://mowgarden.com/shop/">
                        Quay trở lại cửa hàng </a>
                </p>
            </div>
        </div>



        <section class="section" id="section_506147823">
            <div class="bg section-bg fill bg-fill bg-loaded bg-loaded">
            </div>
            <div class="section-content relative">
                <div class="row row-full-width align-center" id="row-81703350">
                    <div id="col-804035717" class="col small-12 large-12">
                        <div class="col-inner text-center">
                            <a href="https://mowgarden.com/checkout/" target="_self" class="button primary"
                                style="border-radius:99px;">
                                <span>thanh toán</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>


            <style>
                #section_506147823 {
                    padding-top: 30px;
                    padding-bottom: 30px;
                }

                #section_506147823 .ux-shape-divider--top svg {
                    height: 150px;
                    --divider-top-width: 100%;
                }

                #section_506147823 .ux-shape-divider--bottom svg {
                    height: 150px;
                    --divider-width: 100%;
                }
            </style>
        </section>
    </div>
@endsection
