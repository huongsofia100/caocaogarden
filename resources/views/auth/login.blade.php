@extends('layouts.app')


@section('css')

    @vite(['resources/sass/app.scss', 'resources/js/app.js'])

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
@endsection

@section('title', 'Login')

@section('content')

    <div class="img js-fullheight">
        <div class="container my-5">
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-6 text-center mb-3">
                    <h2 class="text-center heading-section">LOGIN</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-7 col-lg-4">
                    <div class="login-wrap ">
                        <h3 class="text-center mb-3">Have An Account?</h3>
                        <form method="POST" action="{{ route('login') }}" class="login-form">
                            @csrf
                            <div class="form-group mb-3">
                                <label class="label" for="email">{{ __('Email Address') }}</label>
                                <input id="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror" placeholder="Email"
                                    value="{{ old('email') }}" autocomplete="email" name="email" autofocus>
                                <div class="icon1"><i class="bx bxs-user"></i></div>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label class="label" for="password">{{ __('Password') }}</label>
                                <input id="password" type="password"
                                    class="form-control
                            @error('password') is-invalid @enderror"
                                    placeholder="Password" name="password" autocomplete="current-password">
                                <div class="icon2"><i class='bx bxs-lock-alt'></i></div>
                                <i class="fa-regular fa-eye" onclick="changeTypePassword()"></i>
                                <i class="fa-regular fa-eye-slash" onclick="changeTypePassword()"></i>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="remember-password">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>
                                <label class="checkbox-wrap checkbox-primary">{{ __('Remember Password') }}
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <div class="forgot-password">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot password?') }}
                                    </a>
                                @endif
                            </div>
                            <div class="d-grid my-3 gap-3">
                                <button type="submit" class="login form-control submit px-3">{{ __('Login') }}</button>
                            </div>
                        </form>
                        <p class="text-center">OR</p>
                        <div class="d-grid my-3 gap-3">
                            <a href="#" class="lg-google form-control btn submit px-3 py-2"
                                type="button">
                                <img class="google" src="https://i.imgur.com/bI58wkm.png" alt="Google">
                                Login with Google
                            </a>
                        </div>
                        <div class="w-full text-center p-t-55">
                            <span class="txt2">
                                Not a member?
                            </span>
                            <a href="register" class="txt2 bo1">
                                Register now
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function changeTypePassword() {
            document.getElementById('password').type = document.getElementById('password').type == 'text' ? 'password' :
                'text';
        }
    </script>
@endsection