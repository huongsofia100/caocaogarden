@extends('layouts.app')

@section('css')
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/register.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
@endsection

@section('title', 'Register')

@section('content')
<div class="img js-fullheight">
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-3">
                <h2 class="text-center heading-section">REGISTER</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-4">
                <div class="register-wrap ">
                    <h3 class="text-center mb-3">Create Your Account</h3>
                    <form method="POST" action="{{ route('register') }}" class="register-form">
                        @csrf
                        <div class="form-group mb-3">
                            <label class="label" for="name">{{ __('Name') }}</label>
                            <input id="name" type="text"
                                class="form-control  @error('name') is-invalid @enderror" name="name"
                                value="{{ old('name') }}" placeholder="Your Name" required autocomplete="name"
                                autofocus>
                            <div class="icon-1"><span class="bx bxs-user"></span></div>
                        </div>
                        <div class="form-group mb-3">
                            <label class="label" for="email">{{ __('Email Address') }}</label>
                            <input id="email" type="text"
                                class="form-control  @error('email') is-invalid @enderror" placeholder="Your Email"
                                name="email" value="{{ old('email') }}" required autocomplete="email">
                            <div class="icon-2"><span class='bx bxs-envelope'></span></div>
                        </div>
                        <div class="form-group mb-3">
                            <label class="label" for="password">{{ __('Password') }}</label>
                            <div class="icon3"><span class="bx bxs-lock-alt"></span></div>
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" placeholder="Your Password"
                                name="password" required autocomplete="new-password">
                            <i class="fa-regular fa-eye" onclick="changeTypePassword1()"></i>
                            <i class="fa-regular fa-eye-slash" onclick="changeTypePassword1()"></i>
                        </div>
                        <div class="form-group mb-3">
                            <label class="label" for="password-confirm">{{ __('Re-type Password') }}</label>
                            <div class="icon4"><span class="bx bxs-lock-alt"></span></div>
                            <input id="password-confirm" type="password" class="form-control"
                                placeholder="Re-type Your Password" name="password_confirmation" required
                                autocomplete="new-password">
                            <i class="eye fa-regular fa-eye" onclick="changeTypePassword2()"></i>
                            <i class="eyeslash fa-regular fa-eye-slash" onclick="changeTypePassword2()"></i>
                        </div>
                        <label class="control control--checkbox mb-1">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                {{ old('remember') ? 'checked' : '' }}>
                            <span class="caption">Creating an account means
                                you're okay with our <a class="text" href="#">Terms and Conditions</a> and our
                                <a class="text" href="#">Privacy Policy</a>.</span>
                        </label>
                        <div class="d-grid my-3 gap-3">
                            <button type="submit" class="form-control submit px-3 py-2">{{ __('Register') }}</button>
                        </div>
                    </form>
                    <p class="text-center">OR</p>
                    <div class="d-grid my-3 gap-3">
                        <a href="#" class="lg-google form-control btn px-3 py-2" type="button">
                            <img class="google" src="https://i.imgur.com/bI58wkm.png" alt="Google">
                            Register with Google
                        </a>
                    </div>
                    <p class="text-1 text-center">I'm already a member! <a class="login" data-toggle="tab"
                            href="login">Log In</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function changeTypePassword1() {
        document.getElementById('password').type = document.getElementById('password').type == 'text' ? 'password' :
            'text';
    }
    function changeTypePassword2() {
        document.getElementById('password-confirm').type = document.getElementById('password-confirm').type == 'text' ?
            'password' : 'text';
    }
</script>
@endsection