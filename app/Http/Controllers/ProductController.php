<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function indexplant()
    {
        return view('user.plant-product');
    }

    public function indexpots()
    {
        return view('user.pots-product');
    }

    public function indexaccessory()
    {
        return view('user.accessory-product');
    }

    
}
