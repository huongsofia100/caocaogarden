<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function gioithieu()
    {
        return view('user.introduce');
    }

    public function lienhe()
    {
        return view('user.contact');
    }

    public function chinhsachbaomat()
    {
        return view('user.securitypolicy');
    }

    public function chinhsachbaohanh()
    {
        return view('user.warrantypolicy');
    }

    public function phuongthucthanhtoan()
    {
        return view('user.paymentmethods');
    }

    public function phuongthucvanchuyen()
    {
        return view('user.shippingmethod');
    }

    public function huongdan()
    {
        return view('user.instruction');
    }
}
